unit UsersFORM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, DBAccess, MyAccess, MemDS, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxDBData, cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, cxCurrencyEdit, cxTextEdit,
  cxNavigator, cxDBNavigator, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, UcxGridDatasets, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinscxPCPainter;
type
  TUsersFRM = class(TForm)
    tblUsr: TMyTable;
    dsUsr: TMyDataSource;
    tvUsr: TcxGridDBTableView;
    lvlUsr: TcxGridLevel;
    gridUsr: TcxGrid;
    tvUsridusr: TcxGridDBColumn;
    tvUsrfullname: TcxGridDBColumn;
    tvUsranualgoal: TcxGridDBColumn;
    cxDBNavigator1: TcxDBNavigator;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure tblUsrBeforePost(DataSet: TDataSet);
    procedure tblUsrDeleteError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
  private
    mngr_tvUsr: TcxGridDataSetsManager;
  public
    { Public declarations }
  end;

var
  UsersFRM: TUsersFRM;

implementation

uses Main;

{$R *.dfm}

procedure TUsersFRM.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try tblUsr.Post; except end;
    mngr_tvUsr.Free;
end;

procedure TUsersFRM.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var errorFound: boolean;
begin
    errorFound:=false;

    if mngr_tvUsr.isEditing then begin
        MessageDlg('����������� � �������� ��� ������� ��� ���� ������ ��� ��� ����� ����.', mtError, [mbOK], 0);
        ErrorFound:=true;
    end;

    if not errorFound then begin
        tblUsr.First;
        while not tblUsr.Eof do begin
            if Trim(tblUsr.FieldByName('fullname').AsString)='' then begin
                MessageDlg('������� ������� �������� ����� �������������.'+#13+#10+'��� ��� ������ 0.2beta ��� ���� ���� ��� �����������.'+#13+#10+''+#13+#10+'����� ������������� ��������.', mtError, [mbOK], 0);
                ErrorFound:=true;
                break;
            end;
            tblUsr.Next;
        end;
    end;

    CanClose:=not errorFound;
end;

procedure TUsersFRM.FormCreate(Sender: TObject);
begin
    tblUsr.Open;
    mngr_tvUsr:=TcxGridDataSetsManager.Create(tvUsr);
end;

procedure TUsersFRM.tblUsrBeforePost(DataSet: TDataSet);
begin
    if Trim(DataSet.FieldByName('fullname').AsString)='' then begin
        MessageDlg('��� ������������ ���� �������', mtError, [mbOK], 0);
        Abort;
    end

    else if DataSet.FieldByName('anualgoal').AsFloat=0 then begin
        MessageDlg('������ �� ������� ������ ��������', mtError, [mbOK], 0);
        Abort;
    end;
end;

procedure TUsersFRM.tblUsrDeleteError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
    MessageDlg('��� ����������� �������� ��������  ��� ��� ����� �������� ������������� ����������', mtError, [mbOK], 0);
    Abort;
end;

end.
