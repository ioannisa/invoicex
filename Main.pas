unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, DBAccess, MyAccess, MyEmbConnection, DAScript, MyScript, MemDS,
  StdCtrls, Grids, DBGrids, MyDacVCL, MyCall, MyClasses, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, cxContainer,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxDBEdit, cxLabel, Menus, cxButtons,
  UsersFORM, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxPC, ExtCtrls,
  ExtActns, dxSkinsForm, cxGridBandedTableView, cxGridDBBandedTableView,
  cxCurrencyEdit, InvoiceFORM, dxSkinsdxStatusBarPainter, dxStatusBar, DADump,
  MyDump, MyBackup, iniFiles, ImgList, cxGridExportLink,  cxExport, dxPSGlbl,
  dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxSkinsdxBarPainter, dxPSCore, dxPScxCommon, dxPScxGridLnk, wait,
  cxHyperLinkEdit, jpeg, functionalities, cxImageComboBox, cxNavigator,
  cxDBNavigator, dxBar, cxGridChartView, cxGridDBChartView, cxGroupBox,
  cxRadioGroup, cxCheckComboBox, ImporterFORM, cxProgressBar, ZipForge,
  SHELLAPI, GIFImg, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, ChangeLogFORM, cxImage, dxGDIPlusClasses, WinInet,
  IdBaseComponent, VideoWindowFORM, cxHint, OleCtrls, SHDocVw, cxCalendar,
  cxCheckBox, cxBarEditItem, cxVariants, UcxGridDataSets, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinscxPCPainter, cxPCdxBarPopupMenu,
  dxPScxGridLayoutViewLnk, dxSkinsdxRibbonPainter, dxScreenTip, dxCustomHint,
  cxLocalization;

type
  TMainFRM = class(TForm)
    EmbeddedConnection: TMyEmbConnection;
    MyScript1: TMyScript;
    tblUsr: TMyTable;
    Button1: TButton;
    dsUsr: TMyDataSource;
    Button2: TButton;
    tblUsr2: TMyTable;
    dsUsr2: TMyDataSource;
    qrCusInv: TMyQuery;
    dsCusInv: TMyDataSource;
    dxSkinController1: TdxSkinController;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    popupManagement: TPopupMenu;
    transferMI: TMenuItem;
    qr: TMyQuery;
    dlgBackup: TSaveDialog;
    dlgRestore: TOpenDialog;
    MyDump1: TMyDump;
    N1: TMenuItem;
    editMI: TMenuItem;
    delMI: TMenuItem;
    newMI: TMenuItem;
    dxStatusBar1: TdxStatusBar;
    ImageList1: TImageList;
    popupExport: TPopupMenu;
    excelMI: TMenuItem;
    xmlMI: TMenuItem;
    htmlMI: TMenuItem;
    csvMI: TMenuItem;
    dlgSaveExportedFile: TSaveDialog;
    printer: TdxComponentPrinter;
    printerLink1: TdxGridReportLink;
    scrolltimer: TTimer;
    tblStore: TMyTable;
    dsStore: TMyDataSource;
    panelCredits: TPanel;
    Panel3: TPanel;
    Image1: TImage;
    Image2: TImage;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel7: TcxLabel;
    cxLabel6: TcxLabel;
    cxLabel8: TcxLabel;
    cxLabel9: TcxLabel;
    cxLabel10: TcxLabel;
    cxLabel11: TcxLabel;
    lblMySQLEmbedded: TcxLabel;
    lblMarquee: TcxLabel;
    cxLabel13: TcxLabel;
    tblStoreidstore: TLargeIntField;
    tblStoredescr: TWideStringField;
    tblStoreafm: TWideStringField;
    tblStoreidExpenseType: TLargeIntField;
    tblStorecorrectAFM: TIntegerField;
    tblExpenseType: TMyTable;
    dsExpenseType: TMyDataSource;
    cxStyle2: TcxStyle;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    N3: TdxBarButton;
    N2: TdxBarSubItem;
    Skin1: TdxBarButton;
    Black1: TdxBarButton;
    Blue1: TdxBarButton;
    Caramel1: TdxBarButton;
    Coffee1: TdxBarButton;
    Darkroom1: TdxBarButton;
    DarkSide1: TdxBarButton;
    Foggy1: TdxBarButton;
    GlassOceans1: TdxBarButton;
    iMaginary1: TdxBarButton;
    Lilian1: TdxBarButton;
    LiquidSky1: TdxBarButton;
    LondonLiquidSky1: TdxBarButton;
    McSkin1: TdxBarButton;
    MoneyTwins1: TdxBarButton;
    Office2007Black1: TdxBarButton;
    Office2007Blue1: TdxBarButton;
    Office2007Green1: TdxBarButton;
    Office2007Pink1: TdxBarButton;
    Office2007Silver1: TdxBarButton;
    Pumpkin1: TdxBarButton;
    Seven1: TdxBarButton;
    Sharp1: TdxBarButton;
    Silver1: TdxBarButton;
    Springtime1: TdxBarButton;
    Stardust1: TdxBarButton;
    Summer20081: TdxBarButton;
    Valentine1: TdxBarButton;
    Xmas2008Blue1: TdxBarButton;
    N8: TdxBarSubItem;
    Backup1: TdxBarButton;
    Restore1: TdxBarButton;
    N6: TdxBarButton;
    N4: TdxBarSubItem;
    N7: TdxBarButton;
    Panel5: TPanel;
    Panel1: TPanel;
    cxLabel1: TcxLabel;
    btnAddUsr: TcxButton;
    cbboxUsr: TcxDBLookupComboBox;
    cxLabel2: TcxLabel;
    btnBackup: TcxButton;
    btnRestore: TcxButton;
    btnClearDB: TcxButton;
    mainPageControl: TcxPageControl;
    tsInvoices: TcxTabSheet;
    gridInput: TcxGrid;
    bvInput: TcxGridDBBandedTableView;
    bvInputinvNum: TcxGridDBBandedColumn;
    bvInputinvDate: TcxGridDBBandedColumn;
    bvInputyearDate: TcxGridDBBandedColumn;
    bvInputmonthDate: TcxGridDBBandedColumn;
    bvInputdayDate: TcxGridDBBandedColumn;
    bvInputweekOfDate: TcxGridDBBandedColumn;
    bvInputweekdayDate: TcxGridDBBandedColumn;
    bvInputamount: TcxGridDBBandedColumn;
    bvInputstoreName: TcxGridDBBandedColumn;
    bvInputstoreAfm: TcxGridDBBandedColumn;
    bvInputexpenseDescr: TcxGridDBBandedColumn;
    bvInputexpenseMasterDescr: TcxGridDBBandedColumn;
    lvlInput: TcxGridLevel;
    Panel2: TPanel;
    btnNewInvoice: TcxButton;
    btnPrint: TcxButton;
    cxTabSheet2: TcxTabSheet;
    Panel4: TPanel;
    cxLabel14: TcxLabel;
    cxLabel16: TcxLabel;
    cxLabel17: TcxLabel;
    gridCMP: TcxGrid;
    tvCMP: TcxGridDBTableView;
    tvCMPidstore: TcxGridDBColumn;
    tvCMPdescr: TcxGridDBColumn;
    tvCMPafm: TcxGridDBColumn;
    tvCMPcorrectAFM: TcxGridDBColumn;
    tvCMPidExpenseType: TcxGridDBColumn;
    tvCMPColumn1: TcxGridDBColumn;
    tvCMPColumn2: TcxGridDBColumn;
    lvlCMP: TcxGridLevel;
    cxDBNavigator1: TcxDBNavigator;
    dxBarSubItem1: TdxBarSubItem;
    About1: TdxBarButton;
    qrCorrect: TMyQuery;
    qrCusInvidusr: TLargeIntField;
    qrCusInvinvNum: TWideStringField;
    qrCusInvinvDate: TDateField;
    qrCusInvyearDate: TLargeIntField;
    qrCusInvmonthDate: TLargeIntField;
    qrCusInvdayDate: TLargeIntField;
    qrCusInvweekOfDate: TLargeIntField;
    qrCusInvweekDayDate: TWideStringField;
    qrCusInvamount: TFloatField;
    qrCusInvreason: TWideMemoField;
    qrCusInvidstore: TLargeIntField;
    qrCusInvstoreName: TWideStringField;
    qrCusInvstoreAfm: TWideStringField;
    qrCusInvidExpenseType: TLargeIntField;
    qrCusInvexpenseDescr: TWideStringField;
    qrCusInvexpenseMasterDescr: TWideStringField;
    lblGroup: TcxLabel;
    rgUsrStats: TcxRadioGroup;
    bvInputFullname: TcxGridDBBandedColumn;
    qrCusInvfullname: TWideStringField;
    lvlAnual: TcxGridLevel;
    tvAnual: TcxGridDBTableView;
    qrAnual: TMyQuery;
    dsAnual: TMyDataSource;
    tvAnualfullname: TcxGridDBColumn;
    tvAnualqty: TcxGridDBColumn;
    tvAnualtotal: TcxGridDBColumn;
    tvAnualsetYear: TcxGridDBColumn;
    qrAnualidUsr: TLargeIntField;
    qrAnualfullname: TWideStringField;
    qrAnualqty: TLargeIntField;
    qrAnualtotal: TFloatField;
    qrAnuala: TFloatField;
    qrAnualb: TFloatField;
    qrAnualdesired: TFloatField;
    qrAnualfinal: TFloatField;
    tvAnuala: TcxGridDBColumn;
    tvAnualb: TcxGridDBColumn;
    tvAnualdesired: TcxGridDBColumn;
    tvAnualfinal: TcxGridDBColumn;
    qrAnualanualGoal: TFloatField;
    tvAnualanualGoal: TcxGridDBColumn;
    qrAnualsetYear: TLargeIntField;
    lblChartInfo: TcxLabel;
    lvlCh1: TcxGridLevel;
    ch1: TcxGridDBChartView;
    ch1Series1: TcxGridDBChartSeries;
    ch1DataGroup1: TcxGridDBChartDataGroup;
    ch1DataGroup7: TcxGridDBChartDataGroup;
    lvlCh2: TcxGridLevel;
    ch2: TcxGridDBChartView;
    ch2DataGroup1: TcxGridDBChartDataGroup;
    ch2DataGroup2: TcxGridDBChartDataGroup;
    ch2DataGroup3: TcxGridDBChartDataGroup;
    ch2Series1: TcxGridDBChartSeries;
    ch2Series2: TcxGridDBChartSeries;
    ch1Series2: TcxGridDBChartSeries;
    N5: TMenuItem;
    resetGridItemsMI: TMenuItem;
    dxBarSubItem2: TdxBarSubItem;
    importerMI: TdxBarButton;
    expExcelMI: TdxBarButton;
    dxBarSeparator2: TdxBarSeparator;
    dxBarSeparator3: TdxBarSeparator;
    expCSVMI: TdxBarButton;
    expXMLMI: TdxBarButton;
    expHTMLMI: TdxBarButton;
    exportOfficialMI: TdxBarButton;
    dxBarSeparator1: TdxBarSeparator;
    qr2: TMyQuery;
    dxStatusBar1Container4: TdxStatusBarContainerControl;
    progBar: TcxProgressBar;
    zip: TZipForge;
    cxLabel12: TcxLabel;
    Image3: TImage;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    Image4: TImage;
    Image5: TImage;
    Image6: TImage;
    btnChanges: TdxBarButton;
    btnExport: TcxButton;
    ilExport: TImageList;
    imgDirections: TcxImage;
    dxBarSubItem3: TdxBarSubItem;
    video1MI: TdxBarButton;
    video2MI: TdxBarButton;
    video3MI: TdxBarButton;
    video4MI: TdxBarButton;
    grid1: TMenuItem;
    gridByItemMI: TMenuItem;
    gridByItemCategoryMI: TMenuItem;
    gridByItemMonthMI: TMenuItem;
    gridByItemWeekMI: TMenuItem;
    gridByItemWeekDayMI: TMenuItem;
    N9: TMenuItem;
    gridByStoreMI: TMenuItem;
    N11: TMenuItem;
    resetGridItems2MI: TMenuItem;
    cbboxGroup: TcxComboBox;
    memo_grid_by_itemCategory: TMemo;
    memo_grid_by_item: TMemo;
    memo_grid_by_month: TMemo;
    memo_grid_by_week: TMemo;
    memo_grid_by_store: TMemo;
    memo_grid_by_weekday: TMemo;
    cxHintStyleController1: TcxHintStyleController;
    qrCusInvweekdaynum: TLargeIntField;
    btnDonate: TdxBarButton;
    lblAnualInfo: TcxLabel;
    cbboxYearStats: TcxRadioGroup;
    pdfMI: TMenuItem;
    expPDFMI: TdxBarButton;
    lblMySQLVersion: TcxLabel;
    popupCMP: TPopupMenu;
    miApplyExpenseType: TMenuItem;
    cxTabSheet1: TcxTabSheet;
    Panel6: TPanel;
    cxLabel3: TcxLabel;
    cxLabel15: TcxLabel;
    cxLabel18: TcxLabel;
    cxLabel19: TcxLabel;
    cxLabel20: TcxLabel;
    cxLabel22: TcxLabel;
    cxLabel23: TcxLabel;
    Panel7: TPanel;
    Panel8: TPanel;
    cxDBNavigator2: TcxDBNavigator;
    gridExpenseMasterType: TcxGrid;
    tvExpenseMasterType: TcxGridDBTableView;
    tvExpenseMasterTypeidexpenseMasterType: TcxGridDBColumn;
    tvExpenseMasterTypedescr: TcxGridDBColumn;
    lvlExpenseMasterType: TcxGridLevel;
    tblExpenseMasterType: TMyTable;
    dsExpenseMasterType: TMyDataSource;
    Panel9: TPanel;
    cxDBNavigator3: TcxDBNavigator;
    gridExpenseType: TcxGrid;
    tvExpenseType: TcxGridDBTableView;
    tvExpenseTypeidexpenseType: TcxGridDBColumn;
    tvExpenseTypedescr: TcxGridDBColumn;
    tvExpenseTypeidExpenseMasterType: TcxGridDBColumn;
    tvExpenseTypeincluded: TcxGridDBColumn;
    lvlExpenseType: TcxGridLevel;
    tblExpenseDetailType: TMyTable;
    dsExpenseDetailType: TMyDataSource;
    rgIncluded: TcxRadioGroup;
    cxLocalizer1: TcxLocalizer;
    BluePrint1: TdxBarButton;
    DevExpressDarkStyle1: TdxBarButton;
    DevExpressStyle1: TdxBarButton;
    HighContrast1: TdxBarButton;
    SevenClassic1: TdxBarButton;
    SharpPlus1: TdxBarButton;
    TheAsphaltWorld1: TdxBarButton;
    UserSkin1: TdxBarButton;
    VS20101: TdxBarButton;
    WhitePrint1: TdxBarButton;
    qrCusInvidusrinva: TLargeIntField;
    Button3: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure btnAddUsrClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cbboxUsrPropertiesChange(Sender: TObject);
    procedure btnNewInvoiceClick(Sender: TObject);
    procedure bvInputCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure popupManagementPopup(Sender: TObject);
    procedure subMenuItmClick(Sender: TObject);
    procedure btnBackupClick(Sender: TObject);
    procedure btnRestoreClick(Sender: TObject);
    procedure btnClearDBClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure editMIClick(Sender: TObject);
    procedure delMIClick(Sender: TObject);
    procedure excelMIClick(Sender: TObject);
    procedure csvMIClick(Sender: TObject);
    procedure xmlMIClick(Sender: TObject);
    procedure htmlMIClick(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormResize(Sender: TObject);
    procedure scrolltimerTimer(Sender: TObject);
    procedure tblStoreCalcFields(DataSet: TDataSet);
    procedure mainPageControlChange(Sender: TObject);
    procedure Panel3Click(Sender: TObject);
    procedure About1Click(Sender: TObject);
    procedure Skin1Click(Sender: TObject);
    procedure Black1Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure cbboxYearStatsPropertiesChange(Sender: TObject);
    procedure qrAnualCalcFields(DataSet: TDataSet);
    procedure tblStoreBeforePost(DataSet: TDataSet);
    procedure tblStoreAfterPost(DataSet: TDataSet);
    procedure tblStoreAfterCancel(DataSet: TDataSet);
    procedure resetGridItemsMIClick(Sender: TObject);
    procedure exportOfficialMIClick(Sender: TObject);
    procedure importerMIClick(Sender: TObject);
    procedure tblStoreBeforeEdit(DataSet: TDataSet);
    procedure tblStoreDeleteError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure lblMySQLEmbeddedClick(Sender: TObject);
    procedure dxStatusBar1Panels2Click(Sender: TObject);
    procedure dxStatusBar1Panels0Click(Sender: TObject);
    procedure Image1Click(Sender: TObject);

    procedure btnChangesClick(Sender: TObject);
    procedure tblStorePostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure video1MIClick(Sender: TObject);
    procedure video2MIClick(Sender: TObject);
    procedure video3MIClick(Sender: TObject);
    procedure video4MIClick(Sender: TObject);
    procedure gridByItemMIClick(Sender: TObject);
    procedure gridByItemCategoryMIClick(Sender: TObject);
    procedure gridByItemMonthMIClick(Sender: TObject);
    procedure gridByItemWeekMIClick(Sender: TObject);
    procedure gridByItemWeekDayMIClick(Sender: TObject);
    procedure bvInputDataControllerGroupingChanged(Sender: TObject);
    procedure gridByStoreMIClick(Sender: TObject);
    procedure gridInputActiveTabChanged(Sender: TcxCustomGrid;
      ALevel: TcxGridLevel);
    procedure cbboxGroupPropertiesChange(Sender: TObject);
    procedure btnDonateClick(Sender: TObject);
    procedure rgUsrStatsPropertiesChange(Sender: TObject);
    procedure pdfMIClick(Sender: TObject);
    procedure bvInputinvDateGetFilterValues(Sender: TcxCustomGridTableItem;
      AValueList: TcxDataFilterValueList);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure EmbeddedConnectionAfterConnect(Sender: TObject);
    procedure miApplyExpenseTypeClick(Sender: TObject);
    procedure popupCMPPopup(Sender: TObject);
    procedure tvExpenseMasterTypeCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure tvExpenseTypeCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure tblExpenseMasterTypeDeleteError(DataSet: TDataSet;
      E: EDatabaseError; var Action: TDataAction);
    procedure tblExpenseDetailTypeDeleteError(DataSet: TDataSet;
      E: EDatabaseError; var Action: TDataAction);
    procedure tblExpenseMasterTypeBeforeDelete(DataSet: TDataSet);
    procedure tblExpenseDetailTypeBeforeDelete(DataSet: TDataSet);
    procedure tblExpenseMasterTypeBeforeEdit(DataSet: TDataSet);
    procedure tblExpenseDetailTypeBeforeEdit(DataSet: TDataSet);
    procedure tblExpenseMasterTypeAfterPost(DataSet: TDataSet);
    procedure tblExpenseDetailTypeAfterInsert(DataSet: TDataSet);
    procedure tblExpenseMasterTypePostError(DataSet: TDataSet;
      E: EDatabaseError; var Action: TDataAction);
    procedure tblExpenseDetailTypePostError(DataSet: TDataSet;
      E: EDatabaseError; var Action: TDataAction);
    procedure btnExportClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    first: boolean;
    prvStoreExpense: integer;
    autoGrouping: boolean;

    mngr_tvCMP: TcxGridDataSetsManager;
    mngr_tvExpenseMasterType: TcxGridDataSetsManager;
    mngr_tvExpenseDetailType: TcxGridDataSetsManager;

    procedure editInvoice;
    procedure setCellMerge(setup: boolean);
    procedure StoreRestoreGridToRegistry(view: TcxGridTableView; store: boolean; savenameOrEmptyStringForDefault: String);
    procedure StoreRestoreGridToFile(view: TcxGridTableView; store: boolean; filename: TFileName);
    procedure fullRefresh(includingUsers: boolean);
    procedure clearAllSkinMasks;
    procedure clearAllSkinMasks2;

    procedure URL_OnDownloadProgress(Sender: TDownLoadURL;
         Progress, ProgressMax: Cardinal;
         StatusCode: TURLDownloadStatus;
         StatusText: String; var Cancel: Boolean);
    procedure DoDownload(fromURL, toFile, params: String);
    procedure checkSubFile(filename: String);
    procedure setupGlobalParam;

    procedure dbupdate1;
    procedure dbupdate2;
  public
    procedure execute(what: String);
    procedure setupImportedCompanies;
  end;

var
  MainFRM: TMainFRM;
  globalParam: String;

implementation

{$R *.dfm}
{$R localization_greek.res}

function getVersion : string;
{ ---------------------------------------------------------
   Extracts the FileVersion element of the VERSIONINFO
   structure that Delphi maintains as part of a project's
   options.

   Results are returned as a standard string.  Failure
   is reported as "".

   Note that this implementation was derived from similar
   code used by Delphi to validate ComCtl32.dll.  For
   details, see COMCTRLS.PAS, line 3541.
  -------------------------------------------------------- }
const
   NOVIDATA = '';

var
  dwInfoSize,           // Size of VERSIONINFO structure
  dwVerSize,            // Size of Version Info Data
  dwWnd: DWORD;         // Handle for the size call.
  FI: PVSFixedFileInfo; // Delphi structure; see WINDOWS.PAS
  ptrVerBuf: Pointer;   // pointer to a version buffer
  strFileName,          // Name of the file to check
  strVersion : string;  // Holds parsed version number
begin

   strFileName := paramStr( 0 );
   dwInfoSize :=
      getFileVersionInfoSize( pChar( strFileName ), dwWnd);

   if ( dwInfoSize = 0 ) then
      result := NOVIDATA
   else
   begin

      getMem( ptrVerBuf, dwInfoSize );
      try

         if getFileVersionInfo( pChar( strFileName ),
            dwWnd, dwInfoSize, ptrVerBuf ) then

            if verQueryValue( ptrVerBuf, '\',
                              pointer(FI), dwVerSize ) then

            strVersion :=
               format( '%d.%d.%d',
                       [ hiWord( FI.dwFileVersionMS ),
                         loWord( FI.dwFileVersionMS ),
                         hiWord( FI.dwFileVersionLS ) ] );

            {strVersion :=
               format( '%d.%d.%d.%d',
                       [ hiWord( FI.dwFileVersionMS ),
                         loWord( FI.dwFileVersionMS ),
                         hiWord( FI.dwFileVersionLS ),
                         loWord( FI.dwFileVersionLS ) ] );}

      finally
        freeMem( ptrVerBuf );
      end;
    end;
  Result := strVersion;
end;

function GetComputerNetName: string;
var
  buffer: array[0..255] of char;
  size: dword;
begin
  size := 256;
  if GetComputerName(buffer, size) then
    Result := buffer
  else
    Result := ''
end;

//----------------------------------------------------------------------

procedure TMainFRM.About1Click(Sender: TObject);
begin
    panelCredits.Left:=round(width/2)-round(panelCredits.Width/2);
    scrolltimer.Enabled:=true;
    panelCredits.Visible:=true;
end;

procedure TMainFRM.Black1Click(Sender: TObject);
var ini: TIniFile;
    skinName: String;
begin
    skinName:=TdxBarButton(Sender).Caption;
    dxSkinController1.SkinName:=skinName;
    dxSkinController1.NativeStyle:=false;

    clearAllSkinMasks;
    TdxBarButton(Sender).Style:=cxStyle1;

    try
        ini:=TIniFile.Create(ExtractFilePath(Application.Exename)+'\local.ini');
        ini.WriteString('SKIN', 'SELECTED', skinName);
        ini.Free;
    except
    end;
end;

procedure TMainFRM.btnAddUsrClick(Sender: TObject);
var screen: TUsersFRM;
begin
    screen:=TUsersFRM.Create(Self);
    try
        screen.ShowModal;
    finally
        screen.Release;
        tblUsr.Refresh;
        tblUsr2.Refresh;
    end;
end;

procedure TMainFRM.btnBackupClick(Sender: TObject);
begin
    dlgBackup.FileName:='InvoiceX_'+FormatDateTime('yyyy-mm-dd_hhnnss', now)+'.sql';
    if dlgBackup.Execute then begin
        try

            MyDump1.BackupToFile(dlgBackup.FileName);

            if MessageDlg('�� ��������� ��������� ������������� ��������!'+#13+#10+''+#13+#10+'������ �� ���������� ��� ������ ���� ������������ �� ������ ���;', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
                ShellExecute(Handle,
                      'OPEN',
                      PChar('explorer.exe'),
                      PChar('/select, "' + dlgBackup.FileName + '"'),
                      nil,
                      SW_NORMAL) ;
        except
            on e:Exception do begin
                MessageDlg('������ ���� �� backup �� ������ ���������:'+#13+#10+''+#13+#10+e.Message, mtError, [mbOK], 0);
            end;
        end;
    end;
end;

procedure TMainFRM.btnChangesClick(Sender: TObject);
var screen: TChangeLogFRM;
begin
    screen:=TChangeLogFRM.Create(Self);
    try
        screen.ShowModal;
    finally
        screen.Free;
    end;
end;

procedure TMainFRM.btnClearDBClick(Sender: TObject);
begin
    if MessageDlg('� ������� ���� �� �������� ������� �� ���� ��������� ���.'+#13+#10+'���������� ��������;', mtWarning, [mbYes, mbNo], 0)=mrNo then exit;
    if MessageDlg('������������� ��� 2� ���� ��� ���������� �� ��������� ��� ����� ��������� ���;'+#13+#10+'��� �� �������� ��� ����� ������� �� ������!'+#13+#10+''+#13+#10+'���������� ��������;', mtWarning, [mbYes, mbNo], 0)=mrNo then exit;

    MyScript1.Execute;
    //MyDump1.RestoreFromFile(ExtractFilePath(Application.Exename)+'\data\invoicex_0_empty_database.sql');
    DeleteFile(ExtractFilePath(Application.Exename)+'\local.ini');
    dbupdate1;
    dbupdate2;

    MessageDlg('����� ���������� ��� ����� ���������.', mtInformation, [mbOK], 0);

    fullRefresh(true);
end;

procedure TMainFRM.btnDonateClick(Sender: TObject);
var ifResult: TModalResult;
    ini: TIniFile;
    base, serverFile, clientFile, nameOfFile: String;
begin
    MessageDlg('�������� �� ���������� ��� ���� �� ����� ���� PAYPAL.'+#13+#10+''+#13+#10+'������������ ��� ��� ����������!',  mtInformation, [mbOK], 0);

    try // check internet availability
        ini:=TIniFile.Create(ExtractFilePath(Application.Exename)+'\server.ini');
        base:=ini.ReadString('server', 'dir', 'http://www.cybernate.gr');
        ini.Free;

        execute(base+'/software/invoicex/donate.html');
    except
        MessageDlg('�� �������� �� ����� ������ ������� ��� internet.', mtWarning, [mbOK], 0);
    end;
end;

procedure TMainFRM.btnExportClick(Sender: TObject);
var P: TPoint;
begin
    P:=Point(0,0);
    P:=btnExport.ClientToScreen(P);

    btnExport.DropDownMenu.Popup(P.X, P.Y+btnExport.Height);
end;

procedure TMainFRM.btnNewInvoiceClick(Sender: TObject);
var screen: TInvoiceFRM;
begin
    if VarIsNull(cbboxUsr.EditValue) then begin
        MessageDlg('������������ ����������� ��� ��������.', mtError, [mbOK], 0);
        exit;
    end;

    screen:=TInvoiceFRM.Create(Self, cbboxUsr.EditValue);
    try
        screen.ShowModal;
    finally
        screen.Release;
        refreshSmart(bvInput);
        refreshSmart(tvAnual);
        mngr_tvCMP.refreshSmart;
    end;
end;

procedure TMainFRM.btnPrintClick(Sender: TObject);
var tmp1: String;
begin
    case rgUsrStats.ItemIndex of
        1: tmp1:='';
        0: tmp1:='���������: '+cbboxUsr.Text+#13#10;
    end;

    case cbboxYearStats.ItemIndex of
        0: tmp1:=tmp1+'';
        1: tmp1:=tmp1+' ��� �� ���� '+FormatDateTime('yyyy', now);
        2: tmp1:=tmp1+' ��� �� ���� '+IntToStr(StrToInt(FormatDateTime('yyyy', now))-1)
    end;

    tmp1:='������ ���������� '#13#10+tmp1;

    printerLink1.ReportTitle.Text:=tmp1;
    printer.Preview(true, printerLink1);
end;

procedure TMainFRM.btnRestoreClick(Sender: TObject);
begin
    if MessageDlg('������������� ��� ����� backup �� ������ ��� ������� ��� ����� ����� ��� ������ �� backup ��� ����!'+#13+#10+'���������� ��������;', mtWarning, [mbYes, mbNo], 0)=mrNo then exit;
    if dlgRestore.Execute then begin
        try
            MyDump1.RestoreFromFile(dlgRestore.FileName);
            dbupdate1;
            dbupdate2;
            MessageDlg('����� ������������ ��� ����� ���������.', mtInformation, [mbOK], 0);
        except
            on e:Exception do begin
                MessageDlg('������ ���� ��� ������������ �� ������ ���������:'+#13+#10+''+#13+#10+e.Message, mtError, [mbOK], 0);
            end;
        end;

        fullRefresh(true);

        mngr_tvCMP.gotoGridTop;
        gotoGridTop(bvInput);
    end;
end;

procedure TMainFRM.Button1Click(Sender: TObject);
  procedure CreateDatabaseStruct;
  begin
    EmbeddedConnection.Open;
    MyScript1.Execute;
//    EmbeddedConnection.Database := 'invoices';
    tblUsr.Open;
  end;
begin
    EmbeddedConnection.Open;

    try
    tblUsr.Open;
    except
        on E: EMyError do begin
        if E.ErrorCode = ER_NO_SUCH_TABLE then begin
          CreateDatabaseStruct;
          tblUsr.Open;
        end
        else
          raise;
      end;
    end;
end;

procedure TMainFRM.Button2Click(Sender: TObject);
var myQR: TMyQuery;
begin
    Button1Click(nil);
   //EmbeddedConnection.Connect;
end;

procedure TMainFRM.Button3Click(Sender: TObject);
var ini: TIniFile;
begin
    ini:=TIniFile.Create('ini.ini');
    //StrToInt('text');
end;

procedure TMainFRM.bvInputCellDblClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
    editInvoice;
end;

procedure TMainFRM.bvInputDataControllerGroupingChanged(Sender: TObject);
begin
    bvInput.ViewData.Collapse(true);

    if not autoGrouping then
        cbboxGroup.ItemIndex:=-1;

    autoGrouping:=false;
end;

procedure TMainFRM.bvInputinvDateGetFilterValues(Sender: TcxCustomGridTableItem;
  AValueList: TcxDataFilterValueList);
var
  AItemIndex: Integer;
begin
    AItemIndex := AValueList.FindItemByKind(fviSpecial, foTomorrow);
    if AItemIndex <> -1 then
        AValueList.Delete(AItemIndex);

    AItemIndex := AValueList.FindItemByKind(fviSpecial, foNext7Days);
    if AItemIndex <> -1 then
        AValueList.Delete(AItemIndex);

    AItemIndex := AValueList.FindItemByKind(fviSpecial, foNext14Days);
    if AItemIndex <> -1 then
        AValueList.Delete(AItemIndex);

    AItemIndex := AValueList.FindItemByKind(fviSpecial, foNext30Days);
    if AItemIndex <> -1 then
        AValueList.Delete(AItemIndex);

    AItemIndex := AValueList.FindItemByKind(fviSpecial, foNextWeek);
    if AItemIndex <> -1 then
        AValueList.Delete(AItemIndex);

    AItemIndex := AValueList.FindItemByKind(fviSpecial, foNextTwoWeeks);
    if AItemIndex <> -1 then
        AValueList.Delete(AItemIndex);

    AItemIndex := AValueList.FindItemByKind(fviSpecial, foNextMonth);
    if AItemIndex <> -1 then
        AValueList.Delete(AItemIndex);

    AItemIndex := AValueList.FindItemByKind(fviSpecial, foNextYear);
    if AItemIndex <> -1 then
        AValueList.Delete(AItemIndex);
end;

procedure TMainFRM.cbboxGroupPropertiesChange(Sender: TObject);
begin
    case cbboxGroup.ItemIndex of
       -1: autoGrouping:=false;
        0: resetGridItemsMIClick(nil);
        1: gridByItemMIClick(nil);
        2: gridByItemCategoryMIClick(nil);
        3: gridByStoreMIClick(nil);
        4: gridByItemMonthMIClick(nil);
        5: gridByItemWeekMIClick(nil);
        6: gridByItemWeekDayMIClick(nil);
    end;
end;

procedure TMainFRM.cbboxUsrPropertiesChange(Sender: TObject);
var extraSQL1, extraSQL2, extraSQL3: String;
begin
    with qrCusInv do begin
        DisableControls;
        Close;

        if rgUsrStats.ItemIndex=0 then
            extraSQL1:=' and u.idusr='+VarToStr(cbboxUsr.EditValue)+' ';

        case cbboxYearStats.ItemIndex of
            1: extraSQL2:=' and year(u.invDate)='+FormatDateTime('yyyy', now);
            2: extraSQL2:=' and year(u.invDate)='+IntToStr(StrToInt(FormatDateTime('yyyy', now))-1);
        end;

        case rgIncluded.ItemIndex of
            0: extraSQL3:='';
            1: extraSQL3:=' and v.included is true';
            2: extraSQL3:=' and v.included is false';
        end;

        SQL.Text:=
            'SELECT idusrinv, u.idusr, usr.fullname, invNum, '#13#10+
            ' invDate, year(invDate) yearDate, month(invDate) monthDate, day(invDate) dayDate, week(invDate) weekOfDate, '#13#10+

            ' case weekday(invdate) '#13#10+
            ' when 0 then ''��'' '#13#10+
            ' when 1 then ''��'' '#13#10+
            ' when 2 then ''��'' '#13#10+
            ' when 3 then ''��'' '#13#10+
            ' when 4 then ''��'' '#13#10+
            ' when 5 then ''��'' '#13#10+
            ' when 6 then ''��'' '#13#10+
            ' end weekDayDate, '#13#10+

            ' weekday(invdate) weekdaynum, '#13#10+

            ' amount, reason, '#13#10+
            ' u.idstore, s.descr storeName, s.afm storeAfm, '#13#10+
            ' u.idExpenseType, '#13#10+
            ' v.descrDetail expenseDescr, v.descrMaster expenseMasterDescr '#13#10+

            ' FROM usrinv u, store s, v_expensetype v, usr '#13#10+
            ' where '#13#10+
            ' u.idStore=s.idStore '#13#10+
            ' and u.idUsr=usr.idUSr '#13#10+
            ' and v.idDetail=u.idExpenseType '+extraSQL1+extraSQL2+extraSQL3;

        try
            Open;
        except
            dbupdate2;
            Open;
        end;

        EnableControls;
    end;

    with qrAnual do begin
        DisableControls;

        Close;

        SQL.Text:=
            'select u.idUsr, usr.fullname, count(*) qty, sum(amount) total, cast(year(invDate) as unsigned) setYear, usr.anualGoal '#13#10+
            ' from usrinv u, usr, expenseType v '#13#10+
            ' where u.idUsr=usr.idUsr '#13#10+
            extraSQL1+extraSQL2+extraSQL3+
            ' and u.idexpensetype=v.idexpensetype '#13#10+
            ' group by u.idUsr, year(invDate) '#13#10+
            ' order by year(invDate) desc, usr.fullname';
        Open;

        EnableControls;
    end;

    gotoGridTop(bvInput);
end;

procedure TMainFRM.cbboxYearStatsPropertiesChange(Sender: TObject);
begin
    bvInputyearDate.Visible:=(cbboxYearStats.ItemIndex=0);
    if not VarIsNull(cbboxUsr.EditValue) then
        cbboxUsrPropertiesChange(Sender);
end;

procedure TMainFRM.dbupdate1;
var myQR: TMyQuery;
begin
    myQR:=TMyQuery.Create(Self);
    try
        myQR.Connection:=EmbeddedConnection;

        myQR.SQL.Text:='select descr from expensetype where descr like ''�����%''';
        myQR.Active:=true;

        if myQR.RecordCount>0 then begin
            waitForm.setMessage('���������� ����� ���������');
            Application.ProcessMessages;

            myQR.SQL.Text:='update expensemastertype set descr=''����� �����'' where idexpensemastertype=9'; myQR.execute;
            myQR.SQL.Text:='update expensetype set descr=''������, �������, ������� ���'' where idexpensetype=11'; myQR.execute;
            myQR.SQL.Text:='update expensetype set descr=''���������, ��������'' where idexpensetype=15'; myQR.execute;
            myQR.SQL.Text:='update expensetype set descr=''��������� (Spa, �����, ��������������)'' where idexpensetype=26'; myQR.execute;
            myQR.SQL.Text:='update expensetype set descr=''���������� ���������� (���������, ��������������)'' where idexpensetype=53'; myQR.execute;
            myQR.SQL.Text:='update expensetype set descr=''���������, ����������, ����������'' where idexpensetype=55'; myQR.execute;
            myQR.SQL.Text:='update expensetype set descr=''���������, ���������, ���� ������'' where idexpensetype=56'; myQR.execute;

            myQR.SQL.Text:='ALTER TABLE usr ADD COLUMN afm VARCHAR(9) DEFAULT NULL AFTER anualgoal, ADD UNIQUE INDEX Index_3(afm)';
            myQR.Execute;

            myQR.SQL.Text:='ALTER TABLE expensetype ADD COLUMN included BOOLEAN NOT NULL DEFAULT 1 AFTER idExpenseMasterType, ADD INDEX Index_3(included)';
            myQR.Execute;

            myQR.SQL.Text:='ALTER TABLE usrinv ADD COLUMN included BOOLEAN NOT NULL DEFAULT 1 AFTER idExpenseType, ADD INDEX Index_5(included)';
            myQR.Execute;
        end;
    finally
        myQR.Free;
    end;
end;

procedure TMainFRM.dbupdate2;
var myQR: TMyQuery;
begin
    myQR:=TMyQuery.Create(Self);
    try
        myQR.Connection:=EmbeddedConnection;

        myQR.SQL.Text:='select descr from expensetype where idexpensetype=33 and included is false';
        myQR.Active:=true;

        if myqr.RecordCount=0 then begin
            myQR.SQL.Text:='update expenseType set included=false where idexpensetype in (33,34,36)';
            myQR.Execute;


            myQR.SQL.Text:='DROP VIEW IF EXISTS `v_expensetype`';
            myQR.Execute;

            myQR.SQL.Text:=
                'CREATE VIEW `invoices`.`v_expensetype` AS '#13#10+
                'select `et`.`idexpenseType` AS `idDetail`,`et`.`descr` AS `descrDetail`,`emt`.`idexpenseMasterType` AS `idMaster`,`emt`.`descr` AS `descrMaster`, '#13#10+
                '(case trim(`et`.`descr`) when '''' then `emt`.`descr` else concat(`emt`.`descr`,'' - '',`et`.`descr`) end) AS `fullDescr`, '#13#10+
                '`et`.`included` '#13#10+
                'from (`expensetype` `et` join `expensemastertype` `emt`) '#13#10+
                'where (`et`.`idExpenseMasterType` = `emt`.`idexpenseMasterType`)';
            myQR.Execute;
        end;
    finally
        myQR.Free;
    end;
end;

procedure TMainFRM.delMIClick(Sender: TObject);
var selectedCount, i: integer;
    msg: String;
begin
    selectedCount:=bvInput.Controller.SelectedRecordCount;
    if selectedCount=0 then begin
        MessageDlg('��� ����� �������� ����� �������� ���� ��������.', mtError, [mbYes, mbNo], 0);
        exit;
    end else if selectedCount=1 then
        msg:='������ �� ���������� ��� ���������� ��������;'
    else
        msg:='������ �� ���������� ��� ����������� '+IntToStr(selectedCount)+' ����������;';


    if MessageDlg(msg, mtInformation, [mbYes, mbNo], 0)=mrNo then exit;

    for I := 0 to selectedCount - 1 do begin
        qrCusInv.RecNo:=bvInput.Controller.SelectedRecords[i].RecordIndex+1;
        qr.SQL.Text:='delete from usrinv where idusrinv='+qrCusInv.FieldByName('idusrinv').AsString;
        qr.Execute;
    end;

    bvInput.Controller.ClearSelection;

    refreshSmart(bvInput);
    refreshSmart(tvAnual);
end;

procedure TMainFRM.editInvoice;
var screen: TInvoiceFRM;
begin
    if VarIsNull(cbboxUsr.EditValue) then begin
        MessageDlg('������������ ����������� ��� ��������.', mtError, [mbOK], 0);
        exit;
    end;

    screen:=TInvoiceFRM.Create(Self, cbboxUsr.EditValue, qrCusInv.FieldByName('idusrinv').AsInteger);
    try
        screen.ShowModal;
    finally
        screen.Release;
        refreshSmart(bvInput);
        refreshSmart(tvAnual);
    end;
end;

procedure TMainFRM.editMIClick(Sender: TObject);
begin
    editInvoice;
end;

procedure TMainFRM.EmbeddedConnectionAfterConnect(Sender: TObject);
begin
    lblMySQLVersion.Caption:=EmbeddedConnection.ServerVersion;
end;

procedure TMainFRM.excelMIClick(Sender: TObject);
var aFileName: TFileName;
begin
    dlgSaveExportedFile.Title:='������� ���������� �� Excel';
    dlgSaveExportedFile.Filter:='������ Excel 97-2003|*.xls';
    dlgSaveExportedFile.DefaultExt:='xls';

    if Trim(dlgSaveExportedFile.FileName)<>'' then begin
        if ExtractFileExt(dlgSaveExportedFile.FileName)<>'.'+dlgSaveExportedFile.DefaultExt then begin
            if Trim(ExtractFileExt(dlgSaveExportedFile.FileName))<>'' then begin
                dlgSaveExportedFile.FileName:=ExtractFileName(dlgSaveExportedFile.FileName);
                dlgSaveExportedFile.FileName:=copy(dlgSaveExportedFile.FileName, 1, length(dlgSaveExportedFile.FileName)-length(ExtractFileExt(dlgSaveExportedFile.FileName)));
            end;
        end;
    end;


    if dlgSaveExportedFile.Execute then begin
        aFileName:=dlgSaveExportedFile.FileName;

        if FileExists(aFileName) then begin
            if MessageDlg('�������, ������� ��� ������ �� ���� �� �����.'+#13+#10+'������ �� �� ��������������� �� �� �������� ��� �� �������;', mtWarning, [mbYes, mbNo], 0)=mrNo then exit;
        end;

        setCellMerge(false);
        ExportGridToExcel(aFileName, gridInput, false, true, false, 'xls');
        setCellMerge(true);

        if MessageDlg('� ������� ��� ������� ���  �� EXCEL ���� ��������!'+#13+#10+''+#13+#10+'������ �� ���������� ��� ������ ���� ����� ��������� �� ������ ���;', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
            ShellExecute(Handle,
                  'OPEN',
                  PChar('explorer.exe'),
                  PChar('/select, "' + aFileName + '"'),
                  nil,
                  SW_NORMAL) ;
    end;
end;

procedure TMainFRM.csvMIClick(Sender: TObject);
var aFileName: TFileName;
begin
    dlgSaveExportedFile.Title:='������� ���������� �� CSV/Text File';
    dlgSaveExportedFile.Filter:='������ CSV|*.csv|������ ��������|*.txt';
    dlgSaveExportedFile.DefaultExt:='csv';

    if Trim(dlgSaveExportedFile.FileName)<>'' then begin
        if ExtractFileExt(dlgSaveExportedFile.FileName)<>'.'+dlgSaveExportedFile.DefaultExt then begin
            if Trim(ExtractFileExt(dlgSaveExportedFile.FileName))<>'' then begin
                dlgSaveExportedFile.FileName:=ExtractFileName(dlgSaveExportedFile.FileName);
                dlgSaveExportedFile.FileName:=copy(dlgSaveExportedFile.FileName, 1, length(dlgSaveExportedFile.FileName)-length(ExtractFileExt(dlgSaveExportedFile.FileName)));
            end;
        end;
    end;

    if dlgSaveExportedFile.Execute then begin
        aFileName:=dlgSaveExportedFile.FileName;

        if FileExists(aFileName) then begin
            if MessageDlg('�������, ������� ��� ������ �� ���� �� �����.'+#13+#10+'������ �� �� ��������������� �� �� �������� ��� �� �������;', mtWarning, [mbYes, mbNo], 0)=mrNo then exit;
        end;

        bvInput.StoreToIniFile('tmp', true);
        //StoreRestoreGridToRegistry(bvInput, false);
        StoreRestoreGridToFile(bvInput, false, ExtractFilePath(Application.Exename)+'\grid.ini');

        setCellMerge(false);
        bvInput.OptionsView.Footer:=false;
        bvInput.OptionsView.BandHeaders:=false;
        bvInput.Bands[2].Visible:=false;
        ExportGridToText(aFileName, gridInput, false, true, #9, '', '', StringReplace(ExtractFileExt(aFileName), '.', '', [rfReplaceAll]));
        bvInput.OptionsView.BandHeaders:=true;
        bvInput.OptionsView.Footer:=true;
        setCellMerge(true);

        bvInput.RestoreFromIniFile('tmp', true, true);

        if MessageDlg('� ������� ��� ������� ��� �� CSV ���� ��������!'+#13+#10+''+#13+#10+'������ �� ���������� ��� ������ ���� ����� ��������� �� ������ ���;', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
            ShellExecute(Handle,
                  'OPEN',
                  PChar('explorer.exe'),
                  PChar('/select, "' + aFileName + '"'),
                  nil,
                  SW_NORMAL) ;
    end;
end;

procedure TMainFRM.execute(what: String);
begin
    ShellExecute(Application.Handle,
             PChar('open'),
             PChar(what),
             PChar(0),
             nil,
             SW_NORMAL);

end;

procedure TMainFRM.exportOfficialMIClick(Sender: TObject);
begin
    MessageDlg('�� �������� �������� ��� �� ��������� ����������� ��� ����� ����� �����.'+#13+#10+'�� ������ ����������� ������ ��� ��������� ����� ������� ������� ����������.', mtInformation, [mbOK], 0);
end;

procedure TMainFRM.FormActivate(Sender: TObject);
var ini: TIniFile;
    tmp: integer;

    buffer: PWideChar;
    size: Cardinal;
    tmpStr: String;
begin
    BringWindowToTop(Application.Handle);
    Invalidate;

    if not first then exit;
    first:=false;

    ini:=TIniFile.Create(ExtractFilePath(Application.Exename)+'\local.ini');
    try
        tmpStr:=ini.ReadString('SKIN', 'SELECTED', 'Black');
    finally
        ini.Free;
    end;

    if Trim(tmpStr)<>'NO SKIN' then begin
        try
            dxSkinController1.SkinName:=tmpStr;
        except
            dxSkinController1.SkinName:='Black';
        end;
        dxSkinController1.NativeStyle:=false;
    end else
        dxSkinController1.NativeStyle:=true;

    Self.Enabled:=false;

    clearAllSkinMasks2;
    Application.ProcessMessages;

    autoGrouping:=false;

    lblMarquee.DoubleBuffered:=true;

    bvInputyearDate.Visible:=false;
    bvInputFullname.Visible:=false;

    //setupGlobalParam;
    globalParam:='';

    waitForm.setMessage('Online ������� ��������� �������...');
    waitForm.Show;

    Application.ProcessMessages;

    try
        checkSubFile('Loader.exe');
        checkSubFile('libmysqld.dll');
    except
        progBar.Visible:=false;
        //MessageDlg('�������� ��� �������� ��� ��� internet � �� ���������� �� ���� ����� ��������� � Cybernate Server.'+#13+#10+'� �������� ��� ���� �� ���� �� ������� �� ��� ������ ������ ���������� ��� �� ��������� �� ���� ��� �������� ���.', mtWarning, [mbOK], 0);
    end;

    dxStatusBar1.Panels[4].Width:=1;
    dxStatusBar1.Panels[4].Visible:=false;

    //StoreRestoreGridToRegistry(bvInput, true);
    StoreRestoreGridToFile(bvInput, true, ExtractFilePath(Application.Exename)+'\grid.ini');

    ini:=TIniFile.Create(ExtractFilePath(Application.Exename)+'\local.ini');
    try
        tmp:=ini.ReadInteger('TMP', 'LAST_USER', -1);
    finally
        ini.Free;
    end;
    if tmp<>-1 then begin
        cbboxUsr.EditValue:=tmp;
    end;

    try
        waitForm.setMessage('������ ����� ���������...');
        Application.ProcessMessages;

        Button2Click(Self);

        tblUsr.Active:=true;
        tblUsr2.Active:=true;
        tblStore.Active:=true;
        tblExpenseType.Active:=true;

        dbupdate1;
        dbupdate2;
    finally
        waitForm.Hide;
    end;

    qrCorrect.SQL.Text:='select idusr from usr where trim(fullname)=''''';
    qrCorrect.Active:=true;

    mngr_tvCMP.gotoGridTop;
    gotoGridTop(bvInput);

    tblExpenseMasterType.Open;
    tblExpenseDetailType.Open;

    self.Enabled:=true;

    if qrCorrect.RecordCount>0 then begin
        MessageDlg('������� ������� �������� ����� �������������.'+#13+#10+'��� ��� ������ 0.2beta ��� ���� ���� ��� �����������.'+#13+#10+''+#13+#10+'�� ���������� ���� ����� ������������ ��������� ��� �� ������ ��� ���������� ���', mtError, [mbOK], 0);
        btnAddUsrClick(Self);
    end;

    Invalidate;

    //dxStatusBar1.Panels[5].Text:='������� ��� �����������';
    //Application.ProcessMessages;
end;

procedure TMainFRM.FormClose(Sender: TObject; var Action: TCloseAction);
var ini: TIniFile;
begin
    ini:=TIniFile.Create(ExtractFilePath(Application.Exename)+'\local.ini');
    try
        if not VarIsNull(cbboxUsr.EditValue) then
            ini.WriteInteger('TMP', 'LAST_USER', cbboxUsr.EditValue)
        else
            ini.WriteInteger('TMP', 'LAST_USER', -1);
    finally
        ini.Free;
    end;

    mngr_tvCMP.Free;
    mngr_tvExpenseMasterType.Free;
    mngr_tvExpenseDetailType.Free;
end;

procedure TMainFRM.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose:=(MessageDlg('���������� ����� ��� ��� ��������;', mtConfirmation, [mbYes, mbNo], 0)=mrYes);
end;

procedure TMainFRM.FormCreate(Sender: TObject);
var ini: TIniFile;
    tmpStr: String;
begin
    first:=true;

    cxLocalizer1.Active:=True;
    cxLocalizer1.Locale:=1032;

    DateSeparator:='/';
    ShortDateFormat:='dd/mm/yyyy';
    CurrencyString:='�';
    CurrencyFormat := 4;    // 4 means after with a space

    mngr_tvCMP:=TcxGridDataSetsManager.Create(tvCMP);
    mngr_tvExpenseMasterType:=TcxGridDataSetsManager.Create(tvExpenseMasterType);
    mngr_tvExpenseDetailType:=TcxGridDataSetsManager.Create(tvExpenseType);
end;

procedure TMainFRM.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if key=VK_ESCAPE then begin
        panelCredits.Visible:=false;
        scrolltimer.Enabled:=false;

        if imgDirections.Visible then begin
           self.Enabled:=true;
           imgDirections.Visible:=false;
           tblStore.Filtered:=false;

           mngr_tvCMP.gotoGridTop;
        end;
    end;
end;

procedure TMainFRM.FormResize(Sender: TObject);
begin
    panelCredits.Left:=round(width/2)-round(panelCredits.Width/2);
end;

procedure TMainFRM.fullRefresh(includingUsers: boolean);
var savePlace: TBookmark;
    usedBookmark: boolean;
begin
    if includingUsers then begin
        tblUsr.Close; tblUsr.Open;
        tblUsr2.Close; tblUsr2.Open;
    end;

    mngr_tvCMP.refreshSmart;
    refreshSmart(bvInput);
    refreshSmart(tvAnual);
    refreshSmart(tvExpenseMasterType);
    refreshSmart(tvExpenseType);
end;

procedure TMainFRM.htmlMIClick(Sender: TObject);
var aFileName: TFileName;
begin
    dlgSaveExportedFile.Title:='������� ���������� �� HTML';
    dlgSaveExportedFile.Filter:='������ HTML|*.html|������ HTM|*.htm';
    dlgSaveExportedFile.DefaultExt:='html';

    if Trim(dlgSaveExportedFile.FileName)<>'' then begin
        if ExtractFileExt(dlgSaveExportedFile.FileName)<>'.'+dlgSaveExportedFile.DefaultExt then begin
            if Trim(ExtractFileExt(dlgSaveExportedFile.FileName))<>'' then begin
                dlgSaveExportedFile.FileName:=ExtractFileName(dlgSaveExportedFile.FileName);
                dlgSaveExportedFile.FileName:=copy(dlgSaveExportedFile.FileName, 1, length(dlgSaveExportedFile.FileName)-length(ExtractFileExt(dlgSaveExportedFile.FileName)));
            end;
        end;
    end;

    if dlgSaveExportedFile.Execute then begin
        aFileName:=dlgSaveExportedFile.FileName;

        if FileExists(aFileName) then begin
            if MessageDlg('�������, ������� ��� ������ �� ���� �� �����.'+#13+#10+'������ �� �� ��������������� �� �� �������� ��� �� �������;', mtWarning, [mbYes, mbNo], 0)=mrNo then exit;
        end;

        setCellMerge(false);
        ExportGridToHTML(aFileName, gridInput, false, true, 'html');
        setCellMerge(true);

        if MessageDlg('� ������� ��� ������� ��� �� HTML ���� ��������!'+#13+#10+''+#13+#10+'������ �� ���������� ��� ������ ���� ����� ��������� �� ������ ���;', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
            ShellExecute(Handle,
                  'OPEN',
                  PChar('explorer.exe'),
                  PChar('/select, "' + aFileName + '"'),
                  nil,
                  SW_NORMAL) ;
    end;
end;

procedure TMainFRM.Image1Click(Sender: TObject);
begin
    execute('http://www.cybernate.gr/');
end;

procedure TMainFRM.importerMIClick(Sender: TObject);
var screen: TImporterFRM;
begin
    if VarIsNull(cbboxUsr.EditValue) then begin
        MessageDlg('������������ ����������� ��� ��������.', mtError, [mbOK], 0);
        exit;
    end;

    screen:=TImporterFRM.Create(Self);
    try
        screen.ShowModal;
    finally
        screen.Free;
        fullRefresh(false);

        gotoGridTop(bvInput);

        if importedNewCmp then begin
            importedNewCmp:=false;
            Application.ProcessMessages;
            setupImportedCompanies;
        end;

        mngr_tvCMP.gotoGridTop;
    end;
end;

procedure TMainFRM.lblMySQLEmbeddedClick(Sender: TObject);
begin
    execute('http://www.mysql.com/oem/');
end;

procedure TMainFRM.mainPageControlChange(Sender: TObject);
begin
    if mainPageControl.ActivePage=tsInvoices then begin
        if not VarIsNull(cbboxUsr.EditValue) then begin
            refreshSmart(bvInput);
            refreshSmart(tvAnual);
        end;
    end;
end;

procedure TMainFRM.miApplyExpenseTypeClick(Sender: TObject);
var myQR: TMyQuery;
begin
    if MessageDlg('������ ����� ������ ��� ���� ��� ���������� ��� ����� ���������� '+#13+#10+'�� ���� �� ��� �� ���� ��� ���� ������ ����;'+#13+#10+''+#13+#10+'��� ���������� ������ �� ������� �������� ����� ����������:'+#13+#10+'�.�. ��� ��������� ������� ���������� ������ �� ����� '+#13+#10+'1) �������� �� ���� ������ �������� �����������'+#13+#10+'2) �������� �� ���� ������ ������ ������� ���������'+#13+#10+''+#13+#10+''+#13+#10+'���������� �� ����������� ���� ��� ���������� ��� ����������� ����� �� ���� ��� ���� ������;', mtWarning, [mbYes, mbNo], 0)=mrNo then exit;

    if VarIsNull(tblStore.FieldByName('idExpenseType').AsVariant) then begin
        MessageDlg('� ���������� ���� ���������� ��� IMPORT ��������� ��� ��� ��� ���� ������������� ����� �� ������� ����� ������', mtError, [mbOK], 0);
        exit;
    end;


    myQR:=TMyQuery.Create(Self);
    try
        myQR.Connection:=EmbeddedConnection;
        myQR.SQL.Text:=
            'update usrinv set idExpenseType='+tblStore.FieldByName('idExpenseType').AsString+
            ' where idStore='+tblStore.FieldByName('idStore').AsString;
        myQR.Execute;
    finally
        myQR.Free;
        refreshSmart(bvInput);
    end;
end;

procedure TMainFRM.N3Click(Sender: TObject);
begin
    Close;
end;

procedure TMainFRM.gridByItemMIClick(Sender: TObject);
var F: TextFile;
begin
    AssignFile(F, ExtractFilePath(Application.Exename)+'\grid_by_item.ini');
    rewrite(F);
    write(F, memo_grid_by_item.Lines.Text);
    CloseFile(F);

    autoGrouping:=true;
    cbboxGroup.ItemIndex:=1;

    StoreRestoreGridToFile(bvInput, false, ExtractFilePath(Application.Exename)+'\grid_by_item.ini');
    autoGrouping:=false;

    DeleteFile(ExtractFilePath(Application.Exename)+'\grid_by_item.ini');
    bvInputFullname.Visible:=(rgUsrStats.ItemIndex=1);
    bvInputyearDate.Visible:=(cbboxYearStats.ItemIndex=0);

    bvInput.ViewData.Collapse(true);
end;

procedure TMainFRM.gridByItemCategoryMIClick(Sender: TObject);
var F: TextFile;
begin
    AssignFile(F, ExtractFilePath(Application.Exename)+'\grid_by_itemCategory.ini');
    rewrite(F);
    write(F, memo_grid_by_itemCategory.Lines.Text);
    CloseFile(F);

    autoGrouping:=true;
    cbboxGroup.ItemIndex:=2;

    StoreRestoreGridToFile(bvInput, false, ExtractFilePath(Application.Exename)+'\grid_by_itemCategory.ini');
    autoGrouping:=false;

    DeleteFile(ExtractFilePath(Application.Exename)+'\grid_by_itemCategory.ini');
    bvInputFullname.Visible:=(rgUsrStats.ItemIndex=1);
    bvInputyearDate.Visible:=(cbboxYearStats.ItemIndex=0);

    bvInput.ViewData.Collapse(true);
end;

procedure TMainFRM.gridByStoreMIClick(Sender: TObject);
var F: TextFile;
begin
    AssignFile(F, ExtractFilePath(Application.Exename)+'\grid_by_store.ini');
    rewrite(F);
    write(F, memo_grid_by_store.Lines.Text);
    CloseFile(F);

    autoGrouping:=true;
    cbboxGroup.ItemIndex:=3;

    StoreRestoreGridToFile(bvInput, false, ExtractFilePath(Application.Exename)+'\grid_by_store.ini');
    autoGrouping:=false;

    DeleteFile(ExtractFilePath(Application.Exename)+'\grid_by_store.ini');
    bvInputFullname.Visible:=(rgUsrStats.ItemIndex=1);
    bvInputyearDate.Visible:=(cbboxYearStats.ItemIndex=0);

    bvInput.ViewData.Collapse(true);
end;

procedure TMainFRM.gridByItemMonthMIClick(Sender: TObject);
var F: TextFile;
begin
    AssignFile(F, ExtractFilePath(Application.Exename)+'\grid_by_month.ini');
    rewrite(F);
    write(F, memo_grid_by_month.Lines.Text);
    CloseFile(F);

    autoGrouping:=true;
    cbboxGroup.ItemIndex:=4;

    StoreRestoreGridToFile(bvInput, false, ExtractFilePath(Application.Exename)+'\grid_by_month.ini');
    autoGrouping:=false;

    DeleteFile(ExtractFilePath(Application.Exename)+'\grid_by_month.ini');
    bvInputFullname.Visible:=(rgUsrStats.ItemIndex=1);
    bvInputyearDate.Visible:=(cbboxYearStats.ItemIndex=0);

    bvInput.ViewData.Collapse(true);
end;

procedure TMainFRM.gridByItemWeekMIClick(Sender: TObject);
var F: TextFile;
begin
    AssignFile(F, ExtractFilePath(Application.Exename)+'\grid_by_week.ini');
    rewrite(F);
    write(F, memo_grid_by_week.Lines.Text);
    CloseFile(F);

    autoGrouping:=true;
    cbboxGroup.ItemIndex:=5;

    StoreRestoreGridToFile(bvInput, false, ExtractFilePath(Application.Exename)+'\grid_by_week.ini');
    autoGrouping:=false;

    DeleteFile(ExtractFilePath(Application.Exename)+'\grid_by_week.ini');
    bvInputFullname.Visible:=(rgUsrStats.ItemIndex=1);
    bvInputyearDate.Visible:=(cbboxYearStats.ItemIndex=0);

    bvInput.ViewData.Collapse(true);
end;

procedure TMainFRM.gridByItemWeekDayMIClick(Sender: TObject);
var F: TextFile;
begin
    AssignFile(F, ExtractFilePath(Application.Exename)+'\grid_by_weekday.ini');
    rewrite(F);
    write(F, memo_grid_by_weekday.Lines.Text);
    CloseFile(F);

    autoGrouping:=true;
    cbboxGroup.ItemIndex:=6;

    StoreRestoreGridToFile(bvInput, false, ExtractFilePath(Application.Exename)+'\grid_by_weekday.ini');
    autoGrouping:=false;

    DeleteFile(ExtractFilePath(Application.Exename)+'\grid_by_weekday.ini');
    bvInputFullname.Visible:=(rgUsrStats.ItemIndex=1);
    bvInputyearDate.Visible:=(cbboxYearStats.ItemIndex=0);

    bvInput.ViewData.Collapse(true);
end;

procedure TMainFRM.gridInputActiveTabChanged(Sender: TcxCustomGrid;
  ALevel: TcxGridLevel);
begin
//    btnNewInvoice.Visible:=(ALevel.Tag=1);
//    btnExport.Visible:=(ALevel.Tag=1);
    lblGroup.Visible:=(ALevel.Tag=1);
    cbboxGroup.Visible:=(ALevel.Tag=1);
    lblChartInfo.Visible:=(ALevel.Tag>=3);
    lblAnualInfo.Visible:=(ALevel.Tag=2);
end;

procedure TMainFRM.Panel3Click(Sender: TObject);
begin
    panelCredits.Visible:=false;
    scrolltimer.Enabled:=false;
end;

procedure TMainFRM.pdfMIClick(Sender: TObject);
var
  ASettings: TdxPSPDFReportExportOptions;
  tmp1: String;
  aFileName: TFileName;
begin
    gridInput.ActiveLevel:=lvlInput;

    dlgSaveExportedFile.Title:='������� ���������� �� PDF';
    dlgSaveExportedFile.Filter:='������ PDF|*.pdf';
    dlgSaveExportedFile.DefaultExt:='pdf';

    if Trim(dlgSaveExportedFile.FileName)<>'' then begin
        if ExtractFileExt(dlgSaveExportedFile.FileName)<>'.'+dlgSaveExportedFile.DefaultExt then begin
            if Trim(ExtractFileExt(dlgSaveExportedFile.FileName))<>'' then begin
                dlgSaveExportedFile.FileName:=ExtractFileName(dlgSaveExportedFile.FileName);
                dlgSaveExportedFile.FileName:=copy(dlgSaveExportedFile.FileName, 1, length(dlgSaveExportedFile.FileName)-length(ExtractFileExt(dlgSaveExportedFile.FileName)));
            end;
        end;
    end;

    if dlgSaveExportedFile.Execute then begin
        aFileName:=dlgSaveExportedFile.FileName;

        if FileExists(aFileName) then begin
            if MessageDlg('�������, ������� ��� ������ �� ���� �� �����.'+#13+#10+'������ �� �� ��������������� �� �� �������� ��� �� �������;', mtWarning, [mbYes, mbNo], 0)=mrNo then exit;
        end;

        setCellMerge(false);

        ASettings := TdxPSPDFReportExportOptions.Create;
        try
            ASettings.CompressStreams := True;

            case rgUsrStats.ItemIndex of
                1: tmp1:='';
                0: tmp1:='���������: '+cbboxUsr.Text+#13#10;
            end;

            case cbboxYearStats.ItemIndex of
                0: tmp1:=tmp1+'';
                1: tmp1:=tmp1+' ��� �� ���� '+FormatDateTime('yyyy', now);
                2: tmp1:=tmp1+' ��� �� ���� '+IntToStr(StrToInt(FormatDateTime('yyyy', now))-1)
            end;
            tmp1:='������ ���������� '#13#10+tmp1;
            printerLink1.ReportTitle.Text:=tmp1;

            printerLink1.ExportToPdf(aFileName, False, ASettings);

        finally
            ASettings.Free;
        end;
        setCellMerge(true);


        if MessageDlg('� ������� ��� ������� ��� �� PDF ���� ��������!'+#13+#10+''+#13+#10+'������ �� ���������� ��� ������ ���� ����� ��������� �� ������ ���;', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
            ShellExecute(Handle,
                  'OPEN',
                  PChar('explorer.exe'),
                  PChar('/select, "' + aFileName + '"'),
                  nil,
                  SW_NORMAL) ;
    end;
end;

procedure TMainFRM.popupCMPPopup(Sender: TObject);
begin
    miApplyExpenseType.Enabled:=(tblStore.RecordCount>0);
end;

procedure TMainFRM.popupManagementPopup(Sender: TObject);
var subItem: TMenuItem;
    counter: integer;
begin
    transferMI.Clear;
    counter:=0;

    if ((tblUsr.RecordCount>1) and (qrCusInv.RecordCount>0)) then begin
        tblUsr2.IndexFieldNames:='fullname';
        tblUsr2.First;
        while not tblUsr2.Eof do begin
            if tblUsr2.FieldByName('idusr').AsInteger<>qrCusInv.FieldByName('idusr').AsInteger then begin
                subItem := TMenuItem.Create(transferMI);
                subItem.Caption:=tblUsr2.FieldByName('fullname').AsString;
                subItem.Tag:=tblUsr2.FieldByName('idusr').AsInteger;
                subItem.OnClick:=subMenuItmClick;
                transferMI.Add(subItem);
                inc(counter);
            end;

            tblUsr2.Next;
        end;
        tblUsr2.IndexFieldNames:='';
    end;

    newMI.Enabled:=(not VarIsNull(cbboxUsr.EditValue));
    editMI.Enabled:=(qrCusInv.RecordCount>0);
    delMI.Enabled:=(qrCusInv.RecordCount>0);

    transferMI.Enabled:=((counter>0) and (qrCusInv.RecordCount>0));
end;

procedure TMainFRM.qrAnualCalcFields(DataSet: TDataSet);
var first12: double;
begin
    with DataSet do begin
        if FieldByName('anualGoal').AsFloat<=6000 then begin
            FieldByName('a').AsFloat:=0;
            FieldByName('b').AsFloat:=0;
            FieldByName('desired').AsFloat:=0;
            FieldByName('final').AsFloat:=0;
        end else begin
            first12:=FieldByName('anualGoal').AsFloat;
            if first12>=12000 then
                first12:=12000;
            FieldByName('a').AsFloat:=(first12*1.1)-first12;

            if FieldByName('anualGoal').AsFloat>12000 then begin
                FieldByName('b').AsFloat:=((FieldByName('anualGoal').AsFloat-first12)*1.3)-(FieldByName('anualGoal').AsFloat-first12);
            end else
                FieldByName('b').AsFloat:=0;

            if FieldByName('a').AsFloat+FieldByName('b').AsFloat <= 15000 then
                FieldByName('desired').AsFloat:=FieldByName('a').AsFloat+FieldByName('b').AsFloat
            else
                FieldByName('desired').AsFloat:=15000;

            FieldByName('final').AsFloat:=((FieldByName('total').AsFloat-FieldByName('desired').AsFloat)*1.1)-(FieldByName('total').AsFloat-FieldByName('desired').AsFloat);
        end;
    end;
end;

procedure TMainFRM.resetGridItemsMIClick(Sender: TObject);
begin
    autoGrouping:=true;

    if Sender<>nil then begin
        if MessageDlg('���� �� ���������� ��� ������� ���� ������ ���� ���� ��� �� ��������� �� groups.'+#13+#10+''+#13+#10+'���������� ��������� ��� ��������� ��� grid ���� ������ ���� �����;', mtConfirmation, [mbYes, mbNo], 0)=mrYes then begin
            //StoreRestoreGridToRegistry(bvInput, false);
            StoreRestoreGridToFile(bvInput, false, ExtractFilePath(Application.Exename)+'\grid.ini');
            bvInputFullname.Visible:=(rgUsrStats.ItemIndex=1);
            bvInputyearDate.Visible:=(cbboxYearStats.ItemIndex=0);

            cbboxGroup.ItemIndex:=0;
        end;
    end else begin
        //StoreRestoreGridToRegistry(bvInput, false);
        StoreRestoreGridToFile(bvInput, false, ExtractFilePath(Application.Exename)+'\grid.ini');
        bvInputFullname.Visible:=(rgUsrStats.ItemIndex=1);
        bvInputyearDate.Visible:=(cbboxYearStats.ItemIndex=0);
    end;
end;

procedure TMainFRM.rgUsrStatsPropertiesChange(Sender: TObject);
begin
    bvInputFullname.Visible:=(rgUsrStats.ItemIndex=1);
    if not VarIsNull(cbboxUsr.EditValue) then
        cbboxUsrPropertiesChange(Sender);
end;

procedure TMainFRM.scrolltimerTimer(Sender: TObject);
var txt : string;
begin
    txt:= lblMarquee.Caption;
    lblMarquee.Caption:= Copy(txt, 2, length(txt)-1) + Copy(txt,1,1); // right to left
 // lblMarquee.Caption:= Copy(txt,length(txt)-1,1) + Copy(txt, 1, length(txt)-1); // left to right
    lblMarquee.Invalidate;
end;

procedure TMainFRM.setCellMerge(setup: boolean);
begin
    bvInputFullname.Options.CellMerging:=setup;
    bvInputinvDate.Options.CellMerging:=setup;
    bvInputweekdayDate.Options.CellMerging:=setup;
    bvInputdayDate.Options.CellMerging:=setup;
    bvInputweekOfDate.Options.CellMerging:=setup;
    bvInputmonthDate.Options.CellMerging:=setup;
    bvInputyearDate.Options.CellMerging:=setup;
end;

procedure TMainFRM.setupGlobalParam;
var appVer, compName, usrName, winVer, nowDate: String;

  SerialNum : DWord;
  a, b : dword;
  Buffer  : array [0..255] of char;
  Num:string;
begin
    appVer:=getVersion;
    compName:=getComputerNetName;
    usrName:=GetUserName;
    winVer:=StringReplace(GetWindowsVersion, ' ', '+', [rfReplaceAll]);
    nowDate:=formatDateTime('yyyy-mm-dd_hhnn', now);

    GetVolumeInformation(PWideChar(ExtractFileDrive(Application.ExeName)+'\'), nil,0,@SerialNum, a, b, nil, 0);
    Num:=IntToHex(HiWord(SerialNum),4)+IntToHex(LoWord(SerialNum),4);

    globalParam:='?serial='+Num+'&compName='+compName+'&usrName='+usrName+'&winVer='+winVer;
end;

procedure TMainFRM.setupImportedCompanies;
begin
    mainPageControl.ActivePage:=cxTabSheet2;
    imgDirections.Visible:=true;
    self.Enabled:=false;
    tblStore.Filtered:=true;
//    MessageDlg('�������� ���������� �� ������ ��� ���������.'+#13+#10+'������ [��] ���� ����� ��������� ��� �������', mtWarning, [mbOK], 0);
//    imgDirections.Visible:=false;
    //MessageDlg('�� ������������ ��� ��� ������� ��� ������� ��� ��� ����� �������� �� �������'+#13+#10+'��'' ������� ���� ������.'+#13+#10+''+#13+#10+'�� ����������� ��� tab ������������ �������� �� ������ ���� ������.'+#13+#10+''+#13+#10+'��� ������������ ��� ��� ����� �������� �� ������ ���� ���� ���� �� ����������'+#13+#10+'�� ������ ���� ��� ���� ������.', mtConfirmation, [mbOK], 0);
end;

procedure TMainFRM.Skin1Click(Sender: TObject);
var ini: TIniFile;
    skinName: String;
begin
    dxSkinController1.NativeStyle:=true;

    clearAllSkinMasks;
    Skin1.Style:=cxStyle1;

    try
        ini:=TIniFile.Create(ExtractFilePath(Application.Exename)+'\local.ini');
        ini.WriteString('SKIN', 'SELECTED', 'NO SKIN');
        ini.Free;
    except
    end;
end;

procedure TMainFRM.StoreRestoreGridToFile(view: TcxGridTableView;
  store: boolean; filename: TFileName);
begin
    if store then
        view.StoreToIniFile(filename, True, [], view.Name)
    else
        view.RestoreFromIniFile(filename, False, False, [], view.Name);
end;

procedure TMainFRM.StoreRestoreGridToRegistry(view: TcxGridTableView; store: boolean; savenameOrEmptyStringForDefault: String);
var
  AStoreKey, ASaveViewName: string;
  I: Integer;
  AOptions: TcxGridStorageOptions;
begin
  //The path to the layout
  AStoreKey := 'Software\Cybernate\InvoiceX';
  //The name to refer to the stored settings
  if Trim(savenameOrEmptyStringForDefault)='' then
    ASaveViewName := view.Name
  else
    ASaveViewName:=savenameOrEmptyStringForDefault;
  AOptions := [];
  //Create items in the tvTarget View
//  for I := 0 to tvSource.ItemCount - 1 do
//    tvTarget.CreateItem;

  //Save settings of the tvSource View
  if store then
      view.StoreToRegistry(AStoreKey, True, AOptions, ASaveViewName)
  else
      //Apply settings to the tvTarget View
      view.RestoreFromRegistry(AStoreKey, False, False, AOptions, ASaveViewName);

  //Make certain that the tvTarget View displays the same data
  //as the tvSource View
//  tvTarget.DataController.DataSource :=
//    tvSource.DataController.DataSource;
end;

procedure TMainFRM.subMenuItmClick(Sender: TObject);

var selectedCount, i: integer;
    msg: String;
begin
    selectedCount:=bvInput.Controller.SelectedRecordCount;
    if selectedCount=0 then begin
        MessageDlg('��� ����� �������� ����� �������� ���� ��������.', mtError, [mbYes, mbNo], 0);
        exit;
    end else if selectedCount=1 then
        msg:='��������� �������� ���� ���������'#13#10#13#10+cbboxUsr.Text+' -> '+TMenuItem(Sender).Caption+#13#10#13#10'������������� ��� �������� ����;'
    else
        msg:='��������� �������� '+IntToStr(selectedCount)+' ����������'#13#10#13#10+cbboxUsr.Text+' -> '+TMenuItem(Sender).Caption+#13#10#13#10'������������� ��� �������� ����;';

    if MessageDlg(msg, mtWarning, [mbYes, mbNo], 0)=mrYes then begin
        for i := 0 to selectedCount - 1 do begin
            qrCusInv.RecNo:=bvInput.Controller.SelectedRecords[i].RecordIndex+1;
            qr.SQL.Text:='update usrinv set idusr='+IntToStr(TMenuItem(Sender).Tag)+' where idusrinv='+IntToStr(qrCusInv.FieldByName('idusrinv').AsInteger);
            qr.Execute;
        end;

        bvInput.Controller.ClearSelection;

        refreshSmart(bvInput);
        refreshSmart(tvAnual);
    end;
end;

procedure TMainFRM.tblExpenseDetailTypeAfterInsert(DataSet: TDataSet);
begin
    with DataSet do begin
        FieldByName('included').AsBoolean:=true;
    end;
end;

procedure TMainFRM.tblExpenseDetailTypeBeforeDelete(DataSet: TDataSet);
begin
    with DataSet do begin
        if FieldByName('idExpenseType').AsInteger<=68 then begin
            MessageDlg('��� ����������� �������� ������������� ��� ���� ������� ��� �� �������', mtError, [mbOk], 0);
            Abort;
        end;
    end;
end;

procedure TMainFRM.tblExpenseDetailTypeBeforeEdit(DataSet: TDataSet);
begin
    with DataSet do begin
        if FieldByName('idExpenseType').AsInteger<=68 then begin
            MessageDlg('��� ����������� �������� ������������� ��� ���� ������� ��� �� �������', mtError, [mbOk], 0);
            Abort;
        end;
    end;
end;

procedure TMainFRM.tblExpenseDetailTypeDeleteError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
    MessageDlg('�������� ���������� ��� ������� �� ���� ��� ������������ ������.'+#13+#10+'��� ����������� � �������� ����� ��� ����������.', mtError, [mbOK], 0);
    Abort;
end;

procedure TMainFRM.tblExpenseDetailTypePostError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
    MessageDlg('������� ��� ������������ ��� ���� �� ������ ��������� �� ���� ��� ���������', mtError, [mbOk], 0);
end;

procedure TMainFRM.tblExpenseMasterTypeAfterPost(DataSet: TDataSet);
begin
    refreshSmart(bvInput);
    refreshSmart(tvCMP);
end;

procedure TMainFRM.tblExpenseMasterTypeBeforeDelete(DataSet: TDataSet);
begin
    with DataSet do begin
        if FieldByName('idExpenseMasterType').AsInteger<=18 then begin
            MessageDlg('��� ����������� �������� ������� ���������� ��� ���� ������� ��� �� �������', mtError, [mbOk], 0);
            Abort;
        end;
    end;
end;

procedure TMainFRM.tblExpenseMasterTypeBeforeEdit(DataSet: TDataSet);
begin
    with DataSet do begin
        if FieldByName('idExpenseMasterType').AsInteger<=18 then begin
            MessageDlg('��� ����������� �������� ������� ���������� ��� ���� ������� ��� �� �������', mtError, [mbOk], 0);
            Abort;
        end;
    end;
end;

procedure TMainFRM.tblExpenseMasterTypeDeleteError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
    MessageDlg('�������� ������������� ��� ���������� ��� ���� �� ������ ��������� ������.'+#13+#10+'��� ����������� � �������� ����� ��� ����������.', mtError, [mbOK], 0);
    Abort;
end;

procedure TMainFRM.tblExpenseMasterTypePostError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
    MessageDlg('������� ��� ������ ��������� �� ���� ��� ���������', mtError, [mbOk], 0);
end;

procedure TMainFRM.tblStoreAfterCancel(DataSet: TDataSet);
begin
    prvStoreExpense:=0;
end;

procedure TMainFRM.tblStoreAfterPost(DataSet: TDataSet);
begin
    if prvStoreExpense=-1 then begin
        qr2.SQL.Text:=
            'update usrinv set idExpenseType='+DataSet.FieldByName('idExpenseType').AsString+
            ' where idStore='+DataSet.FieldByName('idStore').AsString;
        qr2.Execute;

        refreshSmart(bvInput);
    end;
    prvStoreExpense:=0;
end;

procedure TMainFRM.tblStoreBeforeEdit(DataSet: TDataSet);
begin
    with DataSet do begin
        if VarIsNull(FieldByName('idExpenseType').AsVariant) then
            prvStoreExpense:=-1
        else
            prvStoreExpense:=FieldByName('idExpenseType').AsInteger;
    end;
end;

procedure TMainFRM.tblStoreBeforePost(DataSet: TDataSet);
begin
    with DataSet do begin
        if Trim(FieldByName('descr').AsString)='' then begin
            MessageDlg('��� ������� ��������', mtError, [mbOK], 0);
            Abort;
        end else
        if length(Trim(FieldByName('afm').AsString))<>9 then begin
            MessageDlg('������ ��� 9 ����������� ����������', mtWarning, [mbOK], 0);
            Abort;
        end else
        if VarIsNull(FieldByName('idExpenseType').AsVariant) then begin
            MessageDlg('����� ��'' ������� ���� ������ �� ���� ��� ����������', mtWarning, [mbOK], 0);
            Abort;
        end else
        if not checkAFM(Trim(FieldByName('afm').AsString)) then begin
            if MessageDlg('������� �� ��� ��� �������� ����������� �� ����� �����!'#13#10'���������� ���������� ���'' ��� ����;', mtWarning, [mbYes, mbNo], 0)=mrNo then
                Abort;
        end;
    end;
end;

procedure TMainFRM.tblStoreCalcFields(DataSet: TDataSet);
begin
    with DataSet do begin
        if checkAFM(FieldByName('afm').AsString) then
            FieldByName('correctAFM').AsInteger:=1
        else
            FieldByName('correctAFM').AsInteger:=0;
    end;
end;

procedure TMainFRM.tblStoreDeleteError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
    MessageDlg('��� ����������� �������� ����������� ��� ��� ����� �������� ������������� ����������', mtError, [mbOK], 0);
    Abort;
end;

procedure TMainFRM.tblStorePostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
    MessageDlg('������� ���������� �� �� ���� ���', mtError, [mbOK], 0);
    Abort;
end;

procedure TMainFRM.tvExpenseMasterTypeCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  AColumn: TcxCustomGridTableItem;
begin
    AColumn := (Sender as TcxGridDBTableView).GetColumnByFieldName('idexpenseMasterType');
    if AViewInfo.RecordViewInfo.GridRecord.Values[AColumn.Index]<=18 then
        ACanvas.Font.Color := clRed;
end;

procedure TMainFRM.tvExpenseTypeCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var
  AColumn: TcxCustomGridTableItem;
begin
    AColumn := (Sender as TcxGridDBTableView).GetColumnByFieldName('idexpenseType');
    if AViewInfo.RecordViewInfo.GridRecord.Values[AColumn.Index]<=68 then
        ACanvas.Font.Color := clRed;
end;

procedure TMainFRM.xmlMIClick(Sender: TObject);
var aFileName: TFileName;
begin
    dlgSaveExportedFile.Title:='������� ���������� �� XML';
    dlgSaveExportedFile.Filter:='������ xml|*.xml';
    dlgSaveExportedFile.DefaultExt:='xml';

    if Trim(dlgSaveExportedFile.FileName)<>'' then begin
        if ExtractFileExt(dlgSaveExportedFile.FileName)<>'.'+dlgSaveExportedFile.DefaultExt then begin
            if Trim(ExtractFileExt(dlgSaveExportedFile.FileName))<>'' then begin
                dlgSaveExportedFile.FileName:=ExtractFileName(dlgSaveExportedFile.FileName);
                dlgSaveExportedFile.FileName:=copy(dlgSaveExportedFile.FileName, 1, length(dlgSaveExportedFile.FileName)-length(ExtractFileExt(dlgSaveExportedFile.FileName)));
            end;
        end;
    end;

    if dlgSaveExportedFile.Execute then begin
        aFileName:=dlgSaveExportedFile.FileName;

        if FileExists(aFileName) then begin
            if MessageDlg('�������, ������� ��� ������ �� ���� �� �����.'+#13+#10+'������ �� �� ��������������� �� �� �������� ��� �� �������;', mtWarning, [mbYes, mbNo], 0)=mrNo then exit;
        end;

        setCellMerge(false);
        ExportGridToXML(aFileName, gridInput, false, true, 'xml');
        setCellMerge(true);

        if MessageDlg('� ������� ��� ������� ��� �� XML ���� ��������!'+#13+#10+''+#13+#10+'������ �� ���������� ��� ������ ���� ����� ��������� �� ������ ���;', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
            ShellExecute(Handle,
                  'OPEN',
                  PChar('explorer.exe'),
                  PChar('/select, "' + aFileName + '"'),
                  nil,
                  SW_NORMAL) ;
    end;
end;

procedure TMainFRM.URL_OnDownloadProgress(Sender: TDownLoadURL;
         Progress, ProgressMax: Cardinal;
         StatusCode: TURLDownloadStatus;
         StatusText: String; var Cancel: Boolean);
begin
   ProgBar.Properties.Max:=ProgressMax;
   ProgBar.Position:= Progress;
   Application.ProcessMessages;
end;

procedure TMainFRM.video1MIClick(Sender: TObject);
var screen: TVideoWindowFRM;
    ini: TIniFile;
    base: String;
begin
    ini:=TIniFile.Create(ExtractFilePath(Application.Exename)+'\server.ini');
    base:=ini.ReadString('server', 'dir', 'http://www.cybernate.gr');
    ini.Free;

    screen:=TVideoWindowFRM.Create(Self, base+'/software/invoicex/video/INVOICEX-BETA_controller.swf', TdxBarButton(Sender).Caption, 1100 ,667);
    try
        screen.ShowModal;
    finally
        screen.Free;
    end;
end;

procedure TMainFRM.video2MIClick(Sender: TObject);
var screen: TVideoWindowFRM;
    ini: TIniFile;
    base: String;
begin
    ini:=TIniFile.Create(ExtractFilePath(Application.Exename)+'\server.ini');
    base:=ini.ReadString('server', 'dir', 'http://www.cybernate.gr');
    ini.Free;

    screen:=TVideoWindowFRM.Create(Self, base+'/software/invoicex/video2/import/IMPORT_controller.swf', TdxBarButton(Sender).Caption, 1000 ,580);
    try
        screen.ShowModal;
    finally
        screen.Free;
    end;
end;

procedure TMainFRM.video3MIClick(Sender: TObject);
var screen: TVideoWindowFRM;
    ini: TIniFile;
    base: String;
begin
    ini:=TIniFile.Create(ExtractFilePath(Application.Exename)+'\server.ini');
    base:=ini.ReadString('server', 'dir', 'http://www.cybernate.gr');
    ini.Free;

    screen:=TVideoWindowFRM.Create(Self, base+'/software/invoicex/video2/restoregrid/RESTORE-GRID_controller.swf', TdxBarButton(Sender).Caption, 1000 ,580);
    try
        screen.ShowModal;
    finally
        screen.Free;
    end;
end;

procedure TMainFRM.video4MIClick(Sender: TObject);
var screen: TVideoWindowFRM;
    ini: TIniFile;
    base: String;
begin
    ini:=TIniFile.Create(ExtractFilePath(Application.Exename)+'\server.ini');
    base:=ini.ReadString('server', 'dir', 'http://www.cybernate.gr');
    ini.Free;

    screen:=TVideoWindowFRM.Create(Self, base+'/software/invoicex/video2/massworks/MassWorks_controller.swf', TdxBarButton(Sender).Caption, 1000 ,580);
    try
        screen.ShowModal;
    finally
        screen.Free;
    end;
end;

procedure TMainFRM.DoDownload(fromURL, toFile, params: String);
begin
   with TDownloadURL.Create(self) do
   try
     URL:=fromURL+params;
     WinInet.DeleteUrlCacheEntry(PWideChar(URL));

     FileName := toFile;
     OnDownloadProgress := URL_OnDownloadProgress;

     ExecuteTarget(nil) ;
   finally
     Free;
   end;
end;

procedure TMainFRM.dxStatusBar1Panels0Click(Sender: TObject);
begin
    execute('http://www.cybernate.gr/');
end;

procedure TMainFRM.dxStatusBar1Panels2Click(Sender: TObject);
begin
    execute('http://www.mysql.com/oem/');
end;

function GetCheckSum(FileName: string): DWORD;
var
  F: file of DWORD;
  P: Pointer;
  Fsize: DWORD;
  Buffer: array [0..500] of DWORD;
begin
  FileMode := 0;
  AssignFile(F, FileName);
  Reset(F);
  Seek(F, FileSize(F) div 2);
  Fsize := FileSize(F) - 1 - FilePos(F);
  if Fsize > 500 then Fsize := 500;
  BlockRead(F, Buffer, Fsize);
  Close(F);
  P := @Buffer;
  asm
     xor eax, eax
     xor ecx, ecx
     mov edi , p
     @again:
       add eax, [edi + 4*ecx]
       inc ecx
       cmp ecx, fsize
     jl @again
     mov @result, eax
   end;
end;

procedure TMainFRM.checkSubFile(filename: String);
var ini: TIniFile;
    base, pingLocation, ServerFile, clientFile: String;
    checksum_server, checksum_local: String;
    chLocal: Cardinal;

    F: TextFile;

    nameOfFile: String;
begin
    dxStatusBar1.Panels[4].Width:=300;
    dxStatusBar1.Panels[4].Visible:=true;

    progBar.Visible:=true;
    progBar.Properties.Text:='Checking '+filename;
    Application.ProcessMessages;

    ini:=TIniFile.Create(ExtractFilePath(Application.Exename)+'\server.ini');
    base:=ini.ReadString('server', 'dir', 'http://www.cybernate.gr');
    ini.Free;

    pingLocation:=StringReplace(base, 'http://', '', [rfReplaceAll]);

    serverFile:=base+'/software/invoicex/'+ StringReplace(filename, '\', '/', [rfReplaceAll]);
    clientFile:=ExtractFilePath(Application.Exename)+filename;
    nameOfFile:=ExtractFileName(clientFile);

    DoDownload(PChar(ServerFile+'.txt'), PChar(clientFile+'.txt'), globalParam);

    AssignFile(F, clientFile+'.txt');
    reset(F);
    Read(F, checksum_server);
    CloseFile(F);

//    chLocal:=getCheckSum(clientFile);
    checksum_local:=md5(clientFile);

    if checksum_server<>checksum_local then begin
        progBar.Properties.Text:='Downloading '+filename+'...';
        Application.ProcessMessages;
        DoDownload(PChar(ServerFile+'.zip'), PChar(clientFile+'.zip'), globalParam);

        progBar.Properties.Text:='�������� ����������...';
        Application.ProcessMessages;

        with zip do begin
            // The name of the ZIP file to unzip
            FileName := nameOfFile+'.zip';
            // Open an existing archive
            OpenArchive(fmOpenRead);
            // Set base (default) directory for all archive operations
            BaseDir := ExtractFilePath(Application.ExeName);
            // Extract all files from the archive to C:\Temp folder
            ExtractFiles('*.*');// (nameOfFile);
            CloseArchive();
            DeleteFile(clientFile+'.zip');
        end;
    end;
    ProgBar.Visible:=false;
    DeleteFile(clientFile+'.txt');

    dxStatusBar1.Panels[4].Width:=1;
    dxStatusBar1.Panels[4].Visible:=false;
    Application.ProcessMessages;
end;

procedure TMainFRM.clearAllSkinMasks;
begin
    Skin1.Style:=nil;
    Black1.Style:=nil;
    Blue1.Style:=nil;
    BluePrint1.Style:=nil;
    Caramel1.Style:=nil;
    Coffee1.Style:=nil;
    Darkroom1.Style:=nil;
    DarkSide1.Style:=nil;
    DevExpressDarkStyle1.Style:=nil;
    DevExpressStyle1.Style:=nil;
    Foggy1.Style:=nil;
    GlassOceans1.Style:=nil;
    HighContrast1.Style:=nil;
    iMaginary1.Style:=nil;
    Lilian1.Style:=nil;
    LiquidSky1.Style:=nil;
    LondonLiquidSky1.Style:=nil;
    McSkin1.Style:=nil;
    MoneyTwins1.Style:=nil;
    Office2007Black1.Style:=nil;
    Office2007Blue1.Style:=nil;
    Office2007Green1.Style:=nil;
    Office2007Pink1.Style:=nil;
    Office2007Silver1.Style:=nil;
    dxBarButton1.Style:=nil;
    dxBarButton2.Style:=nil;
    dxBarButton3.Style:=nil;
    Pumpkin1.Style:=nil;
    Seven1.Style:=nil;
    SevenClassic1.Style:=nil;
    Sharp1.Style:=nil;
    SharpPlus1.Style:=nil;
    Silver1.Style:=nil;
    Springtime1.Style:=nil;
    Stardust1.Style:=nil;
    Summer20081.Style:=nil;
    TheAsphaltWorld1.Style:=nil;
    UserSkin1.Style:=nil;
    Valentine1.Style:=nil;
    VS20101.Style:=nil;
    WhitePrint1.Style:=nil;
    Xmas2008Blue1.Style:=nil;
end;

procedure TMainFRM.clearAllSkinMasks2;
begin
    clearAllSkinMasks;

    if dxSkinController1.NativeStyle then Skin1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=Black1.Caption then Black1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=Blue1.Caption then Blue1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=BluePrint1.Caption then BluePrint1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=Caramel1.Caption then Caramel1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=Coffee1.Caption then Coffee1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=Darkroom1.Caption then Darkroom1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=DarkSide1.Caption then DarkSide1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=DevExpressDarkStyle1.Caption then DevExpressDarkStyle1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=DevExpressStyle1.Caption then DevExpressStyle1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=Foggy1.Caption then Foggy1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=GlassOceans1.Caption then GlassOceans1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=HighContrast1.Caption then HighContrast1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=iMaginary1.Caption then iMaginary1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=Lilian1.Caption then Lilian1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=LiquidSky1.Caption then LiquidSky1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=LondonLiquidSky1.Caption then LondonLiquidSky1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=McSkin1.Caption then McSkin1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=MoneyTwins1.Caption then MoneyTwins1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=Office2007Black1.Caption then Office2007Black1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=Office2007Blue1.Caption then Office2007Blue1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=Office2007Green1.Caption then Office2007Green1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=Office2007Pink1.Caption then Office2007Pink1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=Office2007Silver1.Caption then Office2007Silver1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=dxBarButton1.Caption then dxBarButton1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=dxBarButton2.Caption then dxBarButton2.Style:=cxStyle1 else
    if dxSkinController1.SkinName=dxBarButton3.Caption then dxBarButton3.Style:=cxStyle1 else
    if dxSkinController1.SkinName=Pumpkin1.Caption then Pumpkin1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=Seven1.Caption then Seven1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=SevenClassic1.Caption then SevenClassic1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=Sharp1.Caption then Sharp1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=SharpPlus1.Caption then SharpPlus1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=Silver1.Caption then Silver1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=Springtime1.Caption then Springtime1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=Stardust1.Caption then Stardust1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=Summer20081.Caption then Summer20081.Style:=cxStyle1 else
    if dxSkinController1.SkinName=TheAsphaltWorld1.Caption then TheAsphaltWorld1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=UserSkin1.Caption then UserSkin1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=Valentine1.Caption then Valentine1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=VS20101.Caption then VS20101.Style:=cxStyle1 else
    if dxSkinController1.SkinName=WhitePrint1.Caption then WhitePrint1.Style:=cxStyle1 else
    if dxSkinController1.SkinName=Xmas2008Blue1.Caption then Xmas2008Blue1.Style:=cxStyle1;
end;

end.
