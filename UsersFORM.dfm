object UsersFRM: TUsersFRM
  Left = 559
  Top = 347
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #935#949#953#961#953#963#964#941#962'...'
  ClientHeight = 294
  ClientWidth = 492
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object gridUsr: TcxGrid
    Left = 0
    Top = 25
    Width = 492
    Height = 269
    Align = alClient
    TabOrder = 0
    object tvUsr: TcxGridDBTableView
      DataController.DataSource = dsUsr
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = ',0'
          Kind = skCount
          Column = tvUsridusr
        end
        item
          Format = ',0'
          Kind = skCount
          Column = tvUsrfullname
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.FocusCellOnTab = True
      OptionsBehavior.FocusFirstCellOnNewRecord = True
      OptionsBehavior.GoToNextCellOnEnter = True
      OptionsBehavior.ImmediateEditor = False
      OptionsBehavior.IncSearch = True
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object tvUsridusr: TcxGridDBColumn
        Caption = #913'/'#913
        DataBinding.FieldName = 'idusr'
        PropertiesClassName = 'TcxTextEditProperties'
        Visible = False
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Options.Focusing = False
      end
      object tvUsrfullname: TcxGridDBColumn
        Caption = #908#957#959#956#945' '#935#949#953#961#953#963#964#942
        DataBinding.FieldName = 'fullname'
        HeaderAlignmentHorz = taCenter
      end
      object tvUsranualgoal: TcxGridDBColumn
        Caption = #917#964#942#963#953#959' '#917#953#963#972#948#951#956#945
        DataBinding.FieldName = 'anualgoal'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.DisplayFormat = ',0.00 '#8364';-,0.00 '#8364
        HeaderAlignmentHorz = taCenter
        Width = 130
      end
    end
    object lvlUsr: TcxGridLevel
      GridView = tvUsr
    end
  end
  object cxDBNavigator1: TcxDBNavigator
    Left = 0
    Top = 0
    Width = 490
    Height = 25
    Buttons.Images = MainFRM.ImageList1
    Buttons.First.Visible = False
    Buttons.PriorPage.Visible = False
    Buttons.Prior.Visible = False
    Buttons.Next.Visible = False
    Buttons.NextPage.Visible = False
    Buttons.Last.Visible = False
    Buttons.Insert.Hint = #917#953#963#945#947#969#947#942' '#957#941#959#965' '#967#949#953#961#953#963#964#942
    Buttons.Insert.ImageIndex = 50
    Buttons.Append.Visible = False
    Buttons.Delete.Hint = #916#953#945#947#961#945#966#942' '#967#949#953#961#953#963#964#942
    Buttons.Delete.ImageIndex = 49
    Buttons.Edit.Hint = #924#949#964#945#946#959#955#942' '#967#949#953#961#953#963#964#942
    Buttons.Edit.ImageIndex = 64
    Buttons.Edit.Visible = True
    Buttons.Post.Hint = #913#960#959#952#942#954#949#965#963#951' '#960#961#959#963#952#942#954#951#962'/'#956#949#964#945#946#959#955#974#957
    Buttons.Post.ImageIndex = 0
    Buttons.Cancel.Hint = #913#954#973#961#969#963#951' '#960#961#959#963#952#942#954#951#962'/'#956#949#964#945#946#959#955#942#962
    Buttons.Cancel.ImageIndex = 39
    Buttons.Refresh.Visible = False
    Buttons.SaveBookmark.Visible = False
    Buttons.GotoBookmark.Visible = False
    Buttons.Filter.Visible = False
    DataSource = dsUsr
    InfoPanel.Font.Charset = DEFAULT_CHARSET
    InfoPanel.Font.Color = clDefault
    InfoPanel.Font.Height = -11
    InfoPanel.Font.Name = 'Tahoma'
    InfoPanel.Font.Style = []
    InfoPanel.ParentFont = False
    Align = alTop
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
  end
  object tblUsr: TMyTable
    TableName = 'usr'
    Connection = MainFRM.EmbeddedConnection
    BeforePost = tblUsrBeforePost
    OnDeleteError = tblUsrDeleteError
    Left = 248
    Top = 56
  end
  object dsUsr: TMyDataSource
    DataSet = tblUsr
    Left = 288
    Top = 56
  end
end
