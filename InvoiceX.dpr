program InvoiceX;

uses
  EMemLeaks,
  EResLeaks,
  ESendAPIBugZilla,
  EDialogWinAPIMSClassic,
  EDialogWinAPIEurekaLogDetailed,
  EDialogWinAPIStepsToReproduce,
  EDebugExports,
  EDebugTD32,
  EDebugJCL,
  EAppVCL,
  ExceptionLog7,
  Windows,
  Dialogs,
  Forms,
  MidasLib,
  UsersFORM in 'UsersFORM.pas' {UsersFRM},
  InvoiceFORM in 'InvoiceFORM.pas' {InvoiceFRM},
  wait in '..\Common\wait.pas' {waitForm},
  ImporterFORM in 'ImporterFORM.pas' {ImporterFRM},
  PreviewCSVFORM in 'PreviewCSVFORM.pas' {PreviewCSVFrm},
  ChangeLogFORM in 'ChangeLogFORM.pas' {ChangeLogFRM},
  VideoWindowFORM in 'VideoWindowFORM.pas' {VideoWindowFRM},
  Main in 'Main.pas' {MainFRM};

var
    Mutex : THandle;

{$R *.res}

begin
  Mutex := CreateMutex(nil, True, 'MyAppName');
  if (Mutex <> 0) and (GetLastError = 0) then begin

    Application.Initialize;
    Application.MainFormOnTaskbar := True;
    Application.Title := 'InvoiceX by Cybernate Software';
  Application.CreateForm(TMainFRM, MainFRM);
  Application.CreateForm(TwaitForm, waitForm);
  Application.Run;

    if Mutex <> 0 then
      CloseHandle(Mutex);

  end else
    MessageDlg('�� InvoiceX ���������� ���', mtError, [mbOK], 0);
end.
