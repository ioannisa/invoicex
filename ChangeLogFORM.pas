unit ChangeLogFORM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, Menus, StdCtrls, cxButtons, cxTextEdit, cxMemo,
  cxRichEdit, ShellApi, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint;

type
  TChangeLogFRM = class(TForm)
    rtfChangeLog: TcxRichEdit;
    Memo1: TcxRichEdit;
    procedure FormCreate(Sender: TObject);
    procedure rtfChangeLogPropertiesURLClick(Sender: TcxCustomRichEdit;
      const URLText: string; Button: TMouseButton);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ChangeLogFRM: TChangeLogFRM;

implementation

uses Main;

{$R *.dfm}

procedure TChangeLogFRM.FormCreate(Sender: TObject);
var F:TextFile;
begin

    AssignFile(F, ExtractFilePath(Application.Exename)+'\change_log.rtf');
    rewrite(F);
    write(F, Memo1.Lines.Text);
    closeFile(F);

    rtfChangeLog.Lines.LoadFromFile(ExtractFilePath(Application.Exename)+'\change_log.rtf');

    DeleteFile(ExtractFilePath(Application.Exename)+'\change_log.rtf');
end;

procedure TChangeLogFRM.rtfChangeLogPropertiesURLClick(
  Sender: TcxCustomRichEdit; const URLText: string; Button: TMouseButton);
begin
    ShellExecute(Application.Handle,
             PChar('open'),
             PChar(URLText),
             PChar(0),
             nil,
             SW_NORMAL);
end;

end.
