-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.1.38-community


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema invoices
--

CREATE DATABASE IF NOT EXISTS invoices;
USE invoices;

--
-- Temporary table structure for view `v_expensetype`
--
DROP TABLE IF EXISTS `v_expensetype`;
DROP VIEW IF EXISTS `v_expensetype`;
CREATE TABLE `v_expensetype` (
  `idDetail` int(10) unsigned,
  `descrDetail` varchar(85),
  `idMaster` int(10) unsigned,
  `descrMaster` varchar(45),
  `fullDescr` varchar(133)
);

--
-- Definition of table `expensemastertype`
--

DROP TABLE IF EXISTS `expensemastertype`;
CREATE TABLE `expensemastertype` (
  `idexpenseMasterType` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descr` varchar(45) NOT NULL,
  PRIMARY KEY (`idexpenseMasterType`),
  UNIQUE KEY `Index_2` (`descr`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `expensemastertype`
--

/*!40000 ALTER TABLE `expensemastertype` DISABLE KEYS */;
INSERT INTO `expensemastertype` (`idexpenseMasterType`,`descr`) VALUES 
 (1,'Άγνωστο'),
 (2,'Διασκέδαση'),
 (3,'Εκπαίδευση'),
 (4,'Ένδυση'),
 (5,'Επισκευές'),
 (6,'Ιατρικά'),
 (7,'Κατοικίδιο'),
 (8,'Λογαριασμοί'),
 (9,'Λοιπα Έξοδα'),
 (10,'Μετακινήσεις'),
 (11,'Οικιακός Εξοπλισμός'),
 (12,'Οπτικά'),
 (13,'Περιποίηση Σώματος'),
 (15,'Πρόστιμα'),
 (14,'Προσωπικά Έξοδα'),
 (16,'Ταξίδια'),
 (17,'Τρόφιμα'),
 (18,'Ψιλικά');
/*!40000 ALTER TABLE `expensemastertype` ENABLE KEYS */;


--
-- Definition of table `expensetype`
--

DROP TABLE IF EXISTS `expensetype`;
CREATE TABLE `expensetype` (
  `idexpenseType` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descr` varchar(85) NOT NULL,
  `idExpenseMasterType` int(10) unsigned NOT NULL,
  `included` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idexpenseType`),
  UNIQUE KEY `Index_2` (`idExpenseMasterType`,`descr`) USING BTREE,
  CONSTRAINT `FK_expensetype_1` FOREIGN KEY (`idExpenseMasterType`) REFERENCES `expensemastertype` (`idexpenseMasterType`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `expensetype`
--

/*!40000 ALTER TABLE `expensetype` DISABLE KEYS */;
INSERT INTO `expensetype` (`idexpenseType`,`descr`,`idExpenseMasterType`,`included`) VALUES 
 (3,'',1,1),
 (4,'CD, DVD, Video Club',2,1),
 (5,'Internet (Online Συνδρομές, Παιχνίδια)',2,1),
 (6,'Ελεύθερος Χρόνος (Χόμπυ, Φωτογραφία, Επιτραπέζια)',2,1),
 (7,'Εστιατόρια, Ταβέρνες, Μεζεδοπωλεία',2,1),
 (8,'Καφετέρια, Μπαρ, Πίστα, Club',2,1),
 (9,'Κινηματογράφος, Θέατρο, Γήπεδο, Καλωδιακή',2,1),
 (10,'Τυχερά Παιχνίδια, Τζόγος',2,1),
 (11,'Βιβία, Τσάντες, Γραφική Ύλη',3,1),
 (12,'Γυμναστήριο, Πολεμικές τέχνες',3,1),
 (13,'Ιδιαίτερα Μαθήματα',3,1),
 (14,'Ξένες Γλώσσες',3,1),
 (15,'Σεμπινάρια, Συνέδρια',3,1),
 (16,'Σχολικά Φροντιστήρια',3,1),
 (17,'Ωδείο, Χορός, Καλλιτεχνικά',3,1),
 (18,'Αξεσουάρ (Τσάντες, Ζώνες, Καπέλα)',4,1),
 (19,'Καλλυντικά',4,1),
 (20,'Παπούτσια',4,1),
 (21,'Ρούχα (Αγορά, Μοδίστρα, Καθαριστήριο)',4,1),
 (22,'Σπίτι (Υδραυλικά, Ηλεκτρικά, Χρώματα)',5,1),
 (23,'Εξετάσεις',6,1),
 (24,'Επεμβάσεις, Χειρουρία, Γέννες',6,1),
 (25,'Επισκέψεις Ιατρών',6,1),
 (26,'Θεραπείες (Spa, Μασαζ, Φυσιοθεραπείες)',6,1),
 (27,'Ορθοπεδικά, Ανατομικά, Βοηθήματα',6,1),
 (28,'Φάρμακα',6,1),
 (29,'Αξεσουάρ',7,1),
 (30,'Ιατρικά Έξοδα',7,1),
 (31,'Τρόφιμα, Καθημερινή Φροντίδα',7,1),
 (32,'Ασφάλειες Ζωής, Αυτοκινήτου, Σπιτιού',8,1),
 (33,'ΔΕΚΟ (ΕΥΔΑΠ, ΟΤΕ, ΔΕΗ)',8,1),
 (34,'Εναλλακτική Τλεφωνία (Forthnet, Tellas, On κ.λ.π.)',8,1),
 (35,'Ενοίκιο',8,1),
 (36,'Κινητή Τηλεφωνία',8,1),
 (37,'Κοινόχρηστα, Πετρέλαιο, Φυσικό Αέριο',8,1),
 (38,'',9,1),
 (39,'Αγορά Αυτοκινήτου, Μηχανής, Service',10,1),
 (40,'Βενζίνη, Πλυντήριο',10,1),
 (41,'Διόδια',10,1),
 (42,'Εισιτήρια, Κάρτες',10,1),
 (43,'Επισκευές (Αυτοκίνητο, Μηχανή, Σκάφος)',10,1),
 (44,'Πάρκινγκ',10,1),
 (45,'Ταξί, Ενοικιάσεις Μέσου',10,1),
 (46,'Είδη Υγιεινής',11,1),
 (47,'Επίπλωση, Σκεύη',11,1),
 (48,'Εργαλεία, είδη κήπου, άνθη, λιπάσματα',11,1),
 (49,'Ηλεκτρονικά (Η/Υ, TV, Stereo, DVD, Αναλώσιμα)',11,1),
 (50,'Καθημερινά Έξοδα (Απορρυπαντικά, Χαρτικά)',11,1),
 (51,'Λευκά Είδη (Πετσέτες, Σεντόνια, Χαλιά, Κουρτίνες)',11,1),
 (52,'Γυαλιά, Φακοί Επαφής, Αναλώσιμα',12,1),
 (53,'Αισθητικές Επεμβάσεις (Πλαστικές, Λιπαναρρόφηση)',13,1),
 (54,'Κομμωτήριο',13,1),
 (55,'Μανικιούρ, Πεντικιουρ, Αποτρίχωση',13,1),
 (56,'Προσωαπικά, Κοσμήματα, Δώρα τρίτων',14,1),
 (57,'Κλήσεις, ΠΑραβάσεις, Δικαστικά',15,1),
 (58,'Εισιτήρια (Αεροπορικά, Πλοίου, Τρένου)',16,1),
 (59,'Ξενοδοχεία',16,1),
 (60,'Συνάλλαγμα, Έξοδα στο εξωτερικό',16,1),
 (61,'Fast Food, Κυλικεία, Τυρόπιτες',17,1),
 (62,'Super Market',17,1),
 (63,'Αρτοποιεία, Ζαχαροπλαστείο',17,1),
 (64,'Κάβα, Ποτά, Ξηροί Καρποί, Καφές',17,1),
 (65,'Κρεοπωλείο, Μανάβης, Ιχθυοπωλείο, Λαϊκή, κλπ',17,1),
 (66,'Εφημερίδες, Περιοδικά',18,1),
 (67,'Λοιπά',18,1),
 (68,'Τσιγάρα, Πούρα, Αναπτήρες',18,1);
/*!40000 ALTER TABLE `expensetype` ENABLE KEYS */;


--
-- Definition of table `store`
--

DROP TABLE IF EXISTS `store`;
CREATE TABLE `store` (
  `idstore` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descr` varchar(45) DEFAULT NULL,
  `afm` varchar(9) DEFAULT NULL,
  `idExpenseType` int(10) unsigned zerofill DEFAULT NULL,
  PRIMARY KEY (`idstore`),
  UNIQUE KEY `Index_2` (`afm`),
  KEY `FK_store_1` (`idExpenseType`),
  CONSTRAINT `FK_store_1` FOREIGN KEY (`idExpenseType`) REFERENCES `expensetype` (`idexpenseType`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `store`
--

/*!40000 ALTER TABLE `store` DISABLE KEYS */;
INSERT INTO `store` (`idstore`,`descr`,`afm`,`idExpenseType`) VALUES 
 (1,'afsdfasdf','125552000',0000000013),
 (5,'skatoylhs','175552000',NULL),
 (7,'gatoylhs','55555555',NULL),
 (8,'dsdfadfasdf','177822121',NULL);
/*!40000 ALTER TABLE `store` ENABLE KEYS */;


--
-- Definition of table `usr`
--

DROP TABLE IF EXISTS `usr`;
CREATE TABLE `usr` (
  `idusr` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fullname` varchar(45) NOT NULL,
  `anualgoal` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`idusr`),
  UNIQUE KEY `Index_2` (`fullname`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `usr`
--

/*!40000 ALTER TABLE `usr` DISABLE KEYS */;
INSERT INTO `usr` (`idusr`,`fullname`,`anualgoal`) VALUES 
 (1,'ΓΙΑΝΝΗΣ ΑΝΥΦΑΝΤΑΚΗΣ',6000),
 (2,'ΓΙΩΡΓΟΣ ΑΝΥΦΑΝΤΑΚΗΣ',3000),
 (3,'ΣΤΕΛΛΑ ΑΝΥΦΑΝΤΑΚΗ',2500),
 (4,'   ',2500);
/*!40000 ALTER TABLE `usr` ENABLE KEYS */;


--
-- Definition of table `usrinv`
--

DROP TABLE IF EXISTS `usrinv`;
CREATE TABLE `usrinv` (
  `idusrinv` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idusr` int(10) unsigned NOT NULL,
  `invnum` varchar(25) NOT NULL,
  `invDate` date NOT NULL,
  `idstore` int(10) unsigned NOT NULL,
  `amount` double NOT NULL,
  `reason` longtext,
  `idExpenseType` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idusrinv`),
  KEY `FK_usrinv_1` (`idusr`),
  KEY `FK_usrinv_2` (`idstore`),
  KEY `FK_usrinv_3` (`idExpenseType`),
  CONSTRAINT `FK_usrinv_1` FOREIGN KEY (`idusr`) REFERENCES `usr` (`idusr`),
  CONSTRAINT `FK_usrinv_2` FOREIGN KEY (`idstore`) REFERENCES `store` (`idstore`),
  CONSTRAINT `FK_usrinv_3` FOREIGN KEY (`idExpenseType`) REFERENCES `expensetype` (`idexpenseType`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `usrinv`
--

/*!40000 ALTER TABLE `usrinv` DISABLE KEYS */;
INSERT INTO `usrinv` (`idusrinv`,`idusr`,`invnum`,`invDate`,`idstore`,`amount`,`reason`,`idExpenseType`) VALUES 
 (1,1,'45','2010-06-14',1,33,'test readon',5),
 (2,1,'1753','2010-05-25',5,17.32,NULL,8),
 (3,1,'45','2010-06-29',8,65,NULL,4);
/*!40000 ALTER TABLE `usrinv` ENABLE KEYS */;


--
-- Definition of view `v_expensetype`
--

DROP TABLE IF EXISTS `v_expensetype`;
DROP VIEW IF EXISTS `v_expensetype`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_expensetype` AS select `et`.`idexpenseType` AS `idDetail`,`et`.`descr` AS `descrDetail`,`emt`.`idexpenseMasterType` AS `idMaster`,`emt`.`descr` AS `descrMaster`,(case trim(`et`.`descr`) when '' then `emt`.`descr` else concat(`emt`.`descr`,' - ',`et`.`descr`) end) AS `fullDescr` from (`expensetype` `et` join `expensemastertype` `emt`) where (`et`.`idExpenseMasterType` = `emt`.`idexpenseMasterType`);



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
