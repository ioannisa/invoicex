unit InvoiceFORM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, cxGroupBox, DB, MemDS, DBAccess, MyAccess,
  StdCtrls, DBClient, cxLabel, cxCalendar, cxCurrencyEdit, Menus, cxButtons,
  ExtCtrls, GIFImg, functionalities, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, cxCheckBox;

type
  TInvoiceFRM = class(TForm)
    cxGroupBox1: TcxGroupBox;
    cxGroupBox2: TcxGroupBox;
    cbboxStoreAfm: TcxLookupComboBox;
    cbboxStoreName: TcxLookupComboBox;
    cbboxReason: TcxLookupComboBox;
    tblStore: TMyTable;
    dsStore: TMyDataSource;
    lblAfm: TcxLabel;
    lblBrand: TcxLabel;
    edtAmount: TcxCurrencyEdit;
    edtDate: TcxDateEdit;
    edtInvNum: TcxTextEdit;
    lblDate: TcxLabel;
    lblInvNum: TcxLabel;
    lblAmount: TcxLabel;
    lblReason: TcxLabel;
    edtComment: TcxTextEdit;
    cxLabel7: TcxLabel;
    btnOk: TcxButton;
    qr: TMyQuery;
    qrExpense: TMyQuery;
    dsExpense: TMyDataSource;
    btnCancel: TcxButton;
    timerFocus: TTimer;
    cxLabel1: TcxLabel;
    timerAFM: TTimer;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    lblWrong: TcxLabel;
    cxLabel5: TcxLabel;
    ckboxFixedDate: TcxCheckBox;
    procedure cbboxStoreAfmPropertiesNewLookupDisplayText(Sender: TObject;
      const AText: TCaption);
    procedure cbboxStoreAfmPropertiesChange(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btnCancelClick(Sender: TObject);
    procedure timerFocusTimer(Sender: TObject);
    procedure cbboxStoreAfmPropertiesCloseUp(Sender: TObject);
    procedure cbboxStoreNamePropertiesCloseUp(Sender: TObject);
    procedure edtDatePropertiesChange(Sender: TObject);
    procedure timerAFMTimer(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cbboxStoreAfmKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cbboxStoreNameKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtDateKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtInvNumKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtAmountKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cbboxReasonKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCommentKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    storeInsert: boolean;
    idINV, idUSR: integer;
    newStoreID: String;
    function checkAndPost: boolean;
    procedure init;
    function checkDuplicate: String;
    function cleanForm: boolean;
    procedure paintAFM;
    procedure clearAFM;
  public
    Constructor Create(AOwner: TComponent; usrID: integer; invID: integer=0); overload;
  end;

const
    CL_GREEN: integer=$00D5FFB9;
    CL_RED:   integer=$00BBBBFF;

var
  InvoiceFRM: TInvoiceFRM;

implementation

uses Main;

{$R *.dfm}

procedure TInvoiceFRM.btnOkClick(Sender: TObject);
begin
    if lblWrong.Visible then begin
        if MessageDlg('�� �.�.�. ��� ����������� ��� ����� ������� ����������� �� ����� �����!'+#13+#10+'���������� ���������� ��������� ��� ��� ���������� ���� �� ���� �� ��������;', mtWarning, [mbYes, mbNo], 0)=mrNo then
            exit;
    end;

    if Trim(edtInvNum.Text)='' then begin
        if MessageDlg('��� ������� ������ ���������.'#13#10#13#10'������ �� ������������ �������� ����� ������;', mtWarning, [mbYes, mbNo], 0)=mrNo then
            exit;
    end;

    if storeInsert then begin
        try tblStore.Post; except end;
    end;

    if checkAndPost then begin
        tblStore.Connection.Commit;
        init;
        if idInv>0 then
            close;
    end;
end;

procedure TInvoiceFRM.cbboxReasonKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if ((key=VK_RETURN) and (Shift=[])) then begin
        if not cbboxReason.DroppedDown then
            edtComment.SetFocus;
    end;
end;

procedure TInvoiceFRM.cbboxStoreAfmKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if ((key=VK_RETURN) and (Shift=[])) then begin
        if not cbboxStoreAfm.DroppedDown then
            cbboxStoreName.SetFocus;
    end;
end;

procedure TInvoiceFRM.cbboxStoreAfmPropertiesChange(Sender: TObject);
begin
    timerAFM.Enabled:=true;
end;

procedure TInvoiceFRM.cbboxStoreAfmPropertiesCloseUp(Sender: TObject);
begin
    if not cbboxStoreAfm.Focused then
        exit;

    cbboxStoreAfm.PostEditValue;

    if not VarIsNull(cbboxStoreAfm.EditingValue) then begin
        cbboxStoreName.EditValue:=cbboxStoreAfm.EditingValue;
        if tblStore.Locate('idstore', cbboxStoreAfm.EditingValue, []) then
            cbboxReason.EditValue:=tblStore.FieldByName('idExpenseType').AsVariant
        else
            cbboxReason.EditValue:=null;
    end;

    if Trim(cbboxStoreName.Text)<>'' then begin
        if tblStore.Locate('descr', cbboxStoreName.Text, []) then begin
            cbboxStoreAfm.EditValue:=cbboxStoreName.EditValue;
        end;
    end;

    paintAFM;
end;

procedure TInvoiceFRM.cbboxStoreAfmPropertiesNewLookupDisplayText(
  Sender: TObject; const AText: TCaption);
var canContinue: boolean;
begin
    canContinue:=false;

    if not storeInsert then begin
        try
            if Sender=cbboxStoreAfm then begin
                if VarIsNull(cbboxStoreAfm.EditValue) then begin
                    tblStore.Insert;
                    cbboxStoreName.EditValue:=null;
                    cbboxReason.EditValue:=null;
                    storeInsert:=true;
                    canContinue:=true;
                end else begin
//                    tblStore.Locate('idstore', cbboxStoreAfm.EditValue, []);
//                    tblStore.Edit;
                end;
            end else if Sender=cbboxStoreName then begin
                if VarIsNull(cbboxStoreName.EditValue) then begin
                    tblStore.Insert;
                    cbboxStoreAfm.EditValue:=null;
                    cbboxReason.EditValue:=null;
                    storeInsert:=true;
                    canContinue:=true;
                end else begin
//                    tblStore.Locate('idstore', cbboxStoreName.EditValue, []);
//                    tblStore.Edit;
                end;
            end;
        except
        end;


    end else begin 
        try
            if storeInsert then begin
                canContinue:=true;
                tblStore.Edit;
            end;
        except
        end;
    end;

    if canContinue then begin
        if Sender=cbboxStoreAfm then
            tblStore.FieldByName('afm').AsString:=Trim(AText)
        else
        if Sender=cbboxStoreName then
            tblStore.FieldByName('descr').AsString:=Trim(AText);

        try
            tblStore.Post;
        except
            on e:Exception do begin
                MessageDlg('������� ��� ���������� �� ���� �� ���', mtError, [mbOk], 0);
                tblStore.Cancel;
                cbboxStoreAfm.SetFocus;
            end;
        end;

        if storeInsert then begin
            newStoreID:=tblStore.FieldByName('idStore').AsString;
            tblStore.Filter:='idstore='+newStoreID;
            cbboxReason.EditValue:=null;
        end;

        cbboxStoreAfm.Properties.ListSource:=dsStore;
        cbboxStoreName.Properties.ListSource:=dsStore;
    end else begin
        //
    end;
end;

procedure TInvoiceFRM.cbboxStoreNameKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if ((key=VK_RETURN) and (Shift=[])) then begin
        if not cbboxStoreName.DroppedDown then
            edtDate.SetFocus;
    end;
end;

procedure TInvoiceFRM.cbboxStoreNamePropertiesCloseUp(Sender: TObject);
begin
    if not cbboxStoreName.Focused then
        exit;

    cbboxStoreName.PostEditValue;

    if not VarIsNull(cbboxStoreName.EditingValue) then begin
        cbboxStoreAfm.EditValue:=cbboxStoreName.EditingValue;
        if tblStore.Locate('idstore', cbboxStoreName.EditingValue, []) then
            cbboxReason.EditValue:=tblStore.FieldByName('idExpenseType').AsVariant
        else
            cbboxReason.EditValue:=null;
    end;

    if Trim(cbboxStoreAfm.Text)<>'' then begin
        if tblStore.Locate('afm', cbboxStoreAfm.Text, []) then begin
            cbboxStoreName.EditValue:=cbboxStoreAfm.EditValue;
        end;
    end;
end;

function TInvoiceFRM.checkAndPost: boolean;
var errMsg: String;
    reasonStr: String;
    ds: Char;
    tmp: String;
    invNum: String;
begin
    try
        ds:=DecimalSeparator;
        DecimalSeparator:=#46;

        errMsg:='';

        if lblAfm.Style.TextColor=clRed then
            errMsg:='��� ������� �.�.� �����������'
        else if lblBrand.Style.TextColor=clRed then
            errMsg:='��� ������� �������� �����������'
        else if lblDate.Style.TextColor=clRed then
            errMsg:='��� ������� ���������� ����������'
        else if lblAmount.Style.TextColor=clRed then
            errMsg:='��� ������� ���� ����������'
        else if lblReason.Style.TextColor=clRed then
             errMsg:='��� ������� ���������� ����������'
        else if length(cbboxStoreAfm.Text)<>9 then
             errMsg:='�� ��� ������ �� ����������� ��� 9 ������������ ����������'
        else if StrToInt(FormatDateTime('yyyy', edtDate.Date))<2000 then
             errMsg:='����������� ���������� ������ �� ���� ��������� ��� 2000 ��������� ����� �����������'
        else if edtDate.Date>now then
             errMsg:='����������� �� �������� �������� �� ���������� ���������� ����� ��� ���������� ���. '#13#10'����� ����� ����� ���������� ��� Windows;';

        if idInv=0 then begin
            if Trim(errMsg)='' then begin
                tmp:=checkDuplicate;
                if Trim(tmp)<>'' then begin
                    if Trim(edtInvNum.Text)<>'' then
                        errMsg:='����������� ����� �������! '#13#10#13#10'[���], '#13#10+'[��/��� ����������], '#13#10+'[��. ���������] '#13#10#13#10'�� �������� �������� ��� �� ���� ������������ �������� ��� �������� ['+tmp+']'
                    else
                        errMsg:='����������� ����� �������! '#13#10#13#10'[���], '#13#10+'[��/��� ����������], '#13#10+'[����] '#13#10#13#10'�� �������� �������� ��� �� ���� ������������ �������� ��� �������� ['+tmp+']';

                end;
            end;
        end;

        if Trim(errMsg)='' then begin
            if Trim(edtComment.Text)='' then
                reasonStr:='NULL'
            else
                reasonStr:=QuotedStr(Trim(edtComment.Text));

            try
                invNum:=Trim(edtInvNum.Text);
                invNum:=IntToStr(StrToInt(invNum));
                if invNum='0' then
                    invNum:='';
            except
            end;

            if idINV=0 then begin // new so insert
                qr.SQL.Text:=
                    'insert into usrinv(idusr, invnum, invDate, idStore, amount, reason, idExpenseType) '#13#10+
                    'values ('#13#10+
                    IntToStr(idUSR)+', '+QuotedStr(Trim(invNum))+', '+QuotedStr(FormatDateTime('yyyy-mm-dd', edtDate.EditValue))+', '+
                    VarToStr(cbboxStoreName.EditValue)+', '+FloatToStrF(edtAmount.EditValue, ffFixed, 15, 2)+', '+
                    reasonStr+', '+VarToStr(cbboxReason.EditValue)+')';
            end else begin // edit so update
                qr.SQL.Text:=
                    'update usrinv set '+
                    'invNum='+QuotedStr(Trim(InvNum))+', '+
                    'invDate='+QuotedStr(FormatDateTime('yyyy-mm-dd', edtDate.EditValue))+', '+
                    'idStore='+VarToStr(cbboxStoreName.EditValue)+', '+
                    'amount='+FloatToStrF(edtAmount.EditValue, ffFixed, 15, 2)+', '+
                    'reason='+reasonStr+', '+
                    'idExpenseType='+VarToStr(cbboxReason.EditValue)+' '+
                    'where idUsrInv='+intToStr(idINV);
            end;

            tblStore.Locate('idstore', cbboxStoreName.EditValue, []);
            try tblStore.Edit; except end;
            tblStore.FieldByName('idExpenseType').AsInteger:=cbboxReason.EditValue;
            tblStore.Post;

            try
                qr.Execute;
            except
                on e: Exception do begin
                    errMsg:='������ ���� ���������� ��� ������������, �� ������ ���������:'+#13+#10+''+#13+#10+e.Message;
                end;
            end;
        end;
    finally
        DecimalSeparator:=ds;
    end;

    if Trim(errMsg)<>'' then
        MessageDlg(errMsg, mtError, [mbOK], 0);

    Result:=(Trim(errMsg)='');
end;

function TInvoiceFRM.checkDuplicate: String;
var ds: Char;
    invNum: String;
begin
    try
        invNum:=Trim(edtInvNum.Text);
        invNum:=IntToStr(StrToInt(invNum));
        if invNum='0' then
            invNum:='';
    except
    end;

    if Trim(edtInvNum.Text)<>'' then begin
        qr.SQL.Text:=
            'select idusrinv, fullname from usrinv, usr where '+
            ' usrinv.idUsr=usr.idUsr '+
            ' and idstore='+VarToStr(cbboxStoreAfm.EditValue)+
            ' and trim(invnum)='+QuotedStr(Trim(InvNum))+
            ' and invDate='+QuotedStr(FormatDateTime('yyyy-mm-dd', edtDate.Date));
    end else begin
        ds:=DecimalSeparator;
        DecimalSeparator:=#46;

        qr.SQL.Text:=
            'select idusrinv, fullname from usrinv, usr where '+
            ' usrinv.idUsr=usr.idUsr '+
            ' and idstore='+VarToStr(cbboxStoreAfm.EditValue)+
            ' and trim(invnum)='+QuotedStr(Trim(InvNum))+
            ' and cast(usrinv.amount as decimal(18,2))='+FloatToStrF(edtAmount.EditValue, ffFixed, 15, 2)+
            ' and invDate='+QuotedStr(FormatDateTime('yyyy-mm-dd', edtDate.Date));

        DecimalSeparator:=ds;
    end;

    qr.Active:=true;

    if qr.RecordCount=0 then
        result:=''
    else
        result:=qr.FieldByName('fullname').AsString;
end;

function TInvoiceFRM.cleanForm: boolean;
begin
    if ((Trim(cbboxStoreAfm.Text)='') and
        (Trim(cbboxStoreName.Text)='') and
        (((Trim(edtDate.Text)='') and (not ckboxFixedDate.Checked)) or (ckboxFixedDate.Checked)) and
        (Trim(edtInvNum.Text)='') and
        (Trim(edtAmount.Text)='') and
        (Trim(cbboxReason.Text)='') and
        (Trim(edtComment.Text)=''))
     then
        Result:=true
     else
        Result:=false;
end;

procedure TInvoiceFRM.clearAFM;
begin
    lblWrong.Visible:=false;
    cbboxStoreAfm.Style.Color:=clWindow;
end;

constructor TInvoiceFRM.Create(AOwner: TComponent; usrID: integer; invID: integer=0);
begin
  inherited Create(AOwner);

    storeInsert:=false;
    tblStore.Open;
    qrExpense.Open;

    idUSR:=usrID;
    idINV:=invID;

    init;

    if invID>0 then begin
        qr.SQL.Text:=
            'SELECT idusrinv, idusr, u.idStore, invNum, invDate, amount, '#13#10+
            ' store.idStore, store.descr brand, afm, store.idExpenseType lastStoreExpenseType, '#13#10+
            ' u.idExpenseType descrDetail, idMaster, descrMaster, fullDescr '#13#10+
            ' FROM usrinv u, v_expensetype et, store where u.idExpenseType=et.idDetail and u.idStore=store.idStore '#13#10+
            ' and idusrinv='+intToStr(invID);

        qr.Active:=true;

        if qr.RecordCount>0 then begin
            cbboxStoreAfm.EditValue:=qr.FieldByName('idStore').AsInteger;
            cbboxStoreName.EditValue:=qr.FieldByName('idStore').AsInteger;
        end;

        qr.SQL.Text:='select * from usrinv where idusrinv='+intToStr(invID);
        qr.Active:=true;

        if qr.RecordCount>0 then begin
            edtDate.EditValue:=qr.FieldByName('invDate').AsDateTime;
            edtAmount.EditValue:=qr.FieldByName('amount').AsFloat;
            edtInvNum.EditValue:=qr.FieldByName('invNum').AsVariant;
            edtComment.EditValue:=qr.FieldByName('reason').AsVariant;
            cbboxReason.EditValue:=qr.FieldByName('idExpenseType').AsInteger;
        end;

        paintAFM;
    end;
end;

procedure TInvoiceFRM.edtAmountKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if ((key=VK_RETURN) and (Shift=[])) then begin
        cbboxReason.SetFocus;
    end;
end;

procedure TInvoiceFRM.edtCommentKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if ((key=VK_RETURN) and (Shift=[])) then begin
        btnOk.SetFocus;
    end;
end;

procedure TInvoiceFRM.edtDateKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if ((key=VK_RETURN) and (Shift=[])) then begin
        if not edtDate.DroppedDown then
            edtInvNum.SetFocus;
    end;
end;

procedure TInvoiceFRM.edtDatePropertiesChange(Sender: TObject);
begin
    if cbboxStoreAfm.Text<>'' then lblAfm.Style.TextColor:=clWindowText else lblAfm.Style.TextColor:=clRed;
    if cbboxStoreName.Text<>'' then lblBrand.Style.TextColor:=clWindowText else lblBrand.Style.TextColor:=clRed;
    if edtDate.Text<>'' then lblDate.Style.TextColor:=clWindowText else lblDate.Style.TextColor:=clRed;
    if edtInvNum.Text<>'' then lblInvNum.Style.TextColor:=clWindowText else lblInvNum.Style.TextColor:=clPurple;
    if edtAmount.Text<>'' then lblAmount.Style.TextColor:=clWindowText else lblAmount.Style.TextColor:=clRed;
    if cbboxReason.Text<>'' then lblReason.Style.TextColor:=clWindowText else lblReason.Style.TextColor:=clRed;

    {if Sender=cbboxStoreAfm then begin
        if not VarIsNull(cbboxStoreAfm.EditingValue) then
            cbboxStoreName.EditValue:=cbboxStoreAfm.EditValue;
    end else
    if Sender=cbboxStoreName then begin
        if not VarIsNull(cbboxStoreName.EditingValue) then
            cbboxStoreAfm.EditValue:=cbboxStoreName.EditValue;
    end;}

    paintAFM;
end;

procedure TInvoiceFRM.edtInvNumKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if ((key=VK_RETURN) and (Shift=[])) then begin
        edtAmount.SetFocus;
    end;
end;

procedure TInvoiceFRM.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    if not cleanForm then begin
        if MessageDlg('���������� �������/�������� ����� ����������'#13#10'���������� ����� ����� ����������;', mtConfirmation, [mbYes, mbCancel], 0) =mrYes then begin
            CanClose:=True;
            if qr.Connection.InTransaction then
                qr.Connection.Rollback;
            Close;
        end else
            CanClose:=true;
    end else begin
        qr.Connection.Rollback;
        CanClose:=true;
    end;
end;

procedure TInvoiceFRM.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if ((Key=VK_RETURN) and (Shift=[ssCtrl])) then
        btnOkClick(Self);
end;

procedure TInvoiceFRM.btnCancelClick(Sender: TObject);
begin
    if not cleanForm then begin
        if idINV=0 then begin
            if MessageDlg('� ����� ��� �������� ��������.'+#13+#10+'���������� ��������� ��� ������ ���;', mtWarning, [mbYes, mbNo], 0)=mrYes then
                init;
        end else begin
            if MessageDlg('���������� ������� ��� ��������� ���� �������� ����;', mtWarning, [mbYes, mbNo], 0)=mrYes then begin
                init;
                Close;
            end;
        end;
    end else begin
        init;
        close;
    end;
end;

procedure TInvoiceFRM.init;
begin
    if tblStore.Connection.InTransaction then
        tblStore.Connection.Rollback;
        
    cbboxStoreAfm.Clear;
    cbboxStoreName.Clear;

    if not ckboxFixedDate.Checked then
        edtDate.Clear;

    edtInvNum.Clear;
    edtAmount.Clear;
    cbboxReason.Clear;
    edtComment.Clear;

    cbboxStoreAfm.EditValue:=null;
    cbboxStoreName.EditValue:=null;
    cbboxReason.EditValue:=null;

    cbboxStoreAfm.Properties.ListSource:=dsStore;
    cbboxStoreName.Properties.ListSource:=dsStore;

    storeInsert:=false;

    clearAFM;

    try
        tblStore.Filter:='';
    except
        tblStore.Close;
        tblStore.Open;
    end;
    cbboxStoreAfm.Properties.DropDownListStyle:=lsEditList;
    cbboxStoreName.Properties.DropDownListStyle:=lsEditList;

    tblStore.Close;
    tblStore.Open;

    newStoreID:='';

    tblStore.Connection.StartTransaction;

    timerFocus.Enabled:=true;
end;

procedure TInvoiceFRM.paintAFM;
begin
    if checkAFM(cbboxStoreAfm.Text) then begin
        lblWrong.Visible:=false;
        cbboxStoreAfm.Style.Color:=CL_GREEN;
    end else begin
        if Trim(cbboxStoreAfm.Text)='' then begin
            clearAFM;
        end else begin
            lblWrong.Visible:=true;
            cbboxStoreAfm.Style.Color:=CL_RED;
        end;
    end;
end;

procedure TInvoiceFRM.timerAFMTimer(Sender: TObject);
var isFound: boolean;
begin
    isFound:=false;

    timerAFM.Enabled:=false;
    if ((Trim(cbboxStoreAfm.Text)='') and (Trim(cbboxStoreName.Text)='')) then begin
        tblStore.Cancel;
        tblStore.Filter:='';

        cbboxStoreAfm.Properties.ListSource:=dsStore;
        cbboxStoreName.Properties.ListSource:=dsStore;

        storeInsert:=false;
        if newStoreID<>'' then begin
            try
                if tblStore.Locate('idStore', StrToInt(newStoreID), []) then
                    tblStore.Delete;
            except
            end;
            newStoreID:='';
        end;
    end else begin
        
    end;

    edtDatePropertiesChange(Sender);
end;

procedure TInvoiceFRM.timerFocusTimer(Sender: TObject);
begin
    timerFocus.Enabled:=false;
    cbboxStoreAfm.SetFocus;

    paintAFM;
end;

end.
