//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "cxClasses.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxData.hpp"
#include "cxDataStorage.hpp"
#include "cxDBData.hpp"
#include "cxEdit.hpp"
#include "cxFilter.hpp"
#include "cxGraphics.hpp"
#include "cxGrid.hpp"
#include "cxGridBandedTableView.hpp"
#include "cxGridCustomTableView.hpp"
#include "cxGridCustomView.hpp"
#include "cxGridDBBandedTableView.hpp"
#include "cxGridLevel.hpp"
#include "cxGridTableView.hpp"
#include "cxStyles.hpp"
#include <DB.hpp>
#include <DBTables.hpp>
#include "GridStatus.h"
#include <DBClient.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TButton *Button1;
        TButton *Button2;
        TcxGrid *cxGrid1;
        TcxGridDBBandedTableView *cxGrid1DBBandedTableView1;
        TcxGridDBBandedColumn *cxGrid1DBBandedTableView1OrderNo;
        TcxGridDBBandedColumn *cxGrid1DBBandedTableView1CustNo;
        TcxGridDBBandedColumn *cxGrid1DBBandedTableView1SaleDate;
        TcxGridDBBandedColumn *cxGrid1DBBandedTableView1ShipDate;
        TcxGridDBBandedColumn *cxGrid1DBBandedTableView1EmpNo;
        TcxGridDBBandedColumn *cxGrid1DBBandedTableView1ShipToContact;
        TcxGridDBBandedColumn *cxGrid1DBBandedTableView1ShipToAddr1;
        TcxGridDBBandedColumn *cxGrid1DBBandedTableView1ShipToAddr2;
        TcxGridDBBandedColumn *cxGrid1DBBandedTableView1ShipToCity;
        TcxGridDBBandedColumn *cxGrid1DBBandedTableView1ShipToState;
        TcxGridDBBandedColumn *cxGrid1DBBandedTableView1ShipToZip;
        TcxGridDBBandedColumn *cxGrid1DBBandedTableView1ShipToCountry;
        TcxGridDBBandedColumn *cxGrid1DBBandedTableView1ShipToPhone;
        TcxGridDBBandedColumn *cxGrid1DBBandedTableView1ShipVIA;
        TcxGridDBBandedColumn *cxGrid1DBBandedTableView1PO;
        TcxGridDBBandedColumn *cxGrid1DBBandedTableView1Terms;
        TcxGridDBBandedColumn *cxGrid1DBBandedTableView1PaymentMethod;
        TcxGridDBBandedColumn *cxGrid1DBBandedTableView1ItemsTotal;
        TcxGridDBBandedColumn *cxGrid1DBBandedTableView1TaxRate;
        TcxGridDBBandedColumn *cxGrid1DBBandedTableView1Freight;
        TcxGridDBBandedColumn *cxGrid1DBBandedTableView1AmountPaid;
        TcxGridLevel *cxGrid1Level1;
        TDataSource *dsOrders;
	TClientDataSet *ClientDataSet1;
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
private:	// User declarations
        TcxGridStatus *FStatus;
public:		// User declarations
        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
