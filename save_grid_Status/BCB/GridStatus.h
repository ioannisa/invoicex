//---------------------------------------------------------------------------

#ifndef GridStatusH
#define GridStatusH
#include "cxGridDBBandedTableView.hpp"
//---------------------------------------------------------------------------
class TRecordInfo {
public:
    Variant KeyValues;
    int     ALevel;
};
//---------------------------------------------------------------------------
class TRecordInfos : public TList {
private:
    TRecordInfo*    __fastcall GetItem(int index);
protected:
    virtual void    __fastcall Notify(void* Ptr, TListNotification Action);

public:
    __property TRecordInfo* Items[int index] = {read=GetItem};
};
//---------------------------------------------------------------------------
class TcxGridStatus {
public:
    __fastcall TcxGridStatus(TcxGridTableView *AGridView);
    __fastcall ~TcxGridStatus();

    void    __fastcall Restore(TcxGridTableView *AGridView);

protected:

    TRecordInfos*   GroupRecords;
    TRecordInfos*   SelectedRecords;
    int             SelStartItem;
    int             SelEndItem;
    TRecordInfo*    AFocusedRow;
    TRecordInfo*    TopRow;

    TRecordInfo*            __fastcall SaveRecord(TcxGridTableView* AView, TcxCustomGridRecord* GridRecord);
    TcxCustomGridRecord*    __fastcall FindRecordEx(TcxGridTableView* AView, TRecordInfo* RecordInfo);

    void                    __fastcall SaveGridViewSelection(TcxGridTableView* AView);
    void                    __fastcall LoadGridViewSelection(TcxGridTableView* AView);
    void                    __fastcall SaveGridViewTopFocusedRecords(TcxGridTableView* AView);
    void                    __fastcall LoadGridViewTopFocusedRecords(TcxGridTableView* AView);
    void                    __fastcall SaveGridViewExpanded(TcxGridTableView* AView);
    void                    __fastcall LoadGridViewExpanded(TcxGridTableView* AView);
};
#endif
