//---------------------------------------------------------------------------


#pragma hdrstop

#include "GridStatus.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//       TRecordInfos
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void __fastcall TRecordInfos::Notify(void* Ptr, TListNotification Action) {
    if (Action == lnExtracted || Action == lnDeleted) {
        TRecordInfo* rec = (TRecordInfo*)(Ptr);
        if (rec != NULL) {
            delete rec;
            rec = NULL;
        }
    }
}
//---------------------------------------------------------------------------
TRecordInfo* __fastcall TRecordInfos::GetItem(int index) {
    return (TRecordInfo*)(TList::Items[index]);
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//       TcxGridStatus
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
__fastcall TcxGridStatus::TcxGridStatus(TcxGridTableView* AGridView) {
  GroupRecords = new TRecordInfos();
  SelectedRecords = new TRecordInfos();
  SaveGridViewSelection(AGridView);
  SaveGridViewExpanded(AGridView);
  SaveGridViewTopFocusedRecords(AGridView);
}
//---------------------------------------------------------------------------
__fastcall TcxGridStatus::~TcxGridStatus() {
    delete GroupRecords;
    delete SelectedRecords;
    delete AFocusedRow;
    delete TopRow;
}
//---------------------------------------------------------------------------
TRecordInfo* __fastcall TcxGridStatus::SaveRecord(TcxGridTableView* AView, TcxCustomGridRecord* GridRecord) {
    TRecordInfo* recInfo = new TRecordInfo();
    recInfo->KeyValues   = AView->DataController->GetRecordId(GridRecord->RecordIndex);
    recInfo->ALevel      = GridRecord->Level;
    return recInfo;
}
//---------------------------------------------------------------------------
TcxCustomGridRecord* __fastcall TcxGridStatus::FindRecordEx(TcxGridTableView* AView, TRecordInfo* RecordInfo) {
    int i,  ARecordIndex;
    TList*  AList;
    Variant AKeyValue;
    TcxCustomGridRecord* rec = NULL;

    i = 0;
    //TcxGridDBBandedTableView
    TcxGridDBDataController* ctrl = (TcxGridDBDataController*)(AView->DataController);
    if (ctrl == NULL) return NULL;
    ARecordIndex = ctrl->FindRecordIndexByKey(RecordInfo->KeyValues);
    AList = new TList();
    try {
    while (i < AView->ViewData->RowCount) {
        if (dynamic_cast<TcxGridGroupRow*>(AView->ViewData->Rows[i]) != NULL) {
            AList->Clear();
            AView->DataController->Groups->LoadRecordIndexesByRowIndex(AList, i);
            if (AList->IndexOf((void *)ARecordIndex) != -1) {
                if (AView->ViewData->Rows[i]->Level < RecordInfo->ALevel) {
                    AView->ViewData->Rows[i]->Expand(false);
                } else {
                    rec = AView->ViewData->Rows[i];
                    break;
                }
            }
        } else {
            AKeyValue = AView->DataController->GetRecordId(AView->ViewData->Rows[i]->RecordIndex);
            if (AKeyValue == RecordInfo->KeyValues) {
                rec = AView->ViewData->Rows[i];
                break;
            }
        }
        i++;
    }
    } __finally {
        delete AList;
    }
    return rec;
}
//---------------------------------------------------------------------------
void __fastcall TcxGridStatus::SaveGridViewSelection(TcxGridTableView* AView) {
    TRecordInfo* SelectRecordInfo;
    for (int i= 0;  i<AView->Controller->SelectedRecordCount; i++) {
        SelectRecordInfo = SaveRecord(AView, AView->Controller->SelectedRecords[i]);
        this->SelectedRecords->Add(SelectRecordInfo);
    }
	if (AView->Controller->SelectedColumnCount == 0) {
        SelStartItem = -1;
        SelEndItem = -1;
    } else {
        SelStartItem = AView->Controller->SelectedColumns[0]->Index;
        SelEndItem = AView->Controller->SelectedColumns[AView->Controller->SelectedColumnCount - 1]->Index;
    }
}
//---------------------------------------------------------------------------
void __fastcall TcxGridStatus::LoadGridViewSelection(TcxGridTableView* AView) {
    TcxCustomGridRecord* ARecord;
    AView->Controller->ClearSelection();

    for (int i=0; i<SelectedRecords->Count; i++) {
        ARecord = FindRecordEx(AView, SelectedRecords->Items[i]);
        if (ARecord != NULL) {
            ARecord->Selected = true;
        }
    }

    if (SelStartItem != -1) {
        AView->Controller->SelectColumns(AView->Columns[SelStartItem],
                                                    AView->Columns[SelEndItem]);
    }
}
//---------------------------------------------------------------------------
void __fastcall TcxGridStatus::SaveGridViewTopFocusedRecords(TcxGridTableView* AView) {
    if (AView->Controller->FocusedRow == NULL) {
		AFocusedRow = NULL;
    } else {
        AFocusedRow = SaveRecord(AView, AView->Controller->FocusedRow);
    }

    TopRow = NULL;
    if (AView->Controller->TopRowIndex != -1 &&
        AView->Controller->TopRowIndex < AView->ViewData->RecordCount) {
        TopRow = SaveRecord(AView, AView->ViewData->Records[AView->Controller->TopRowIndex]);
    }
}
//---------------------------------------------------------------------------
void __fastcall TcxGridStatus::LoadGridViewTopFocusedRecords(TcxGridTableView* AView) {
    TcxCustomGridRecord* ARecord;

	if (AFocusedRow != NULL) {
	  ARecord = FindRecordEx(AView, AFocusedRow);
	  if (ARecord != NULL) {
		ARecord->Focused = true;
	  }
	}
	if (TopRow != NULL) {
	  ARecord = FindRecordEx(AView, TopRow);
	  if (ARecord != NULL) {
		AView->Controller->TopRowIndex = ARecord->Index;
	  }
	}
}
//---------------------------------------------------------------------------
void __fastcall TcxGridStatus::SaveGridViewExpanded(TcxGridTableView* AView) {
    int i;
    TRecordInfo* GroupRecordInfo;

    for (int i=0; i<AView->DataController->RowCount; i++) {
//    with AView.ViewData.Records[i] do
        if (AView->ViewData->Records[i]->Expanded) {
            GroupRecordInfo = SaveRecord(AView, AView->ViewData->Records[i]);
            GroupRecords->Add(GroupRecordInfo);
        }
    }
}
//---------------------------------------------------------------------------
void __fastcall TcxGridStatus::LoadGridViewExpanded(TcxGridTableView* AView) {
    int i;
    TcxCustomGridRecord* ARecord;
    for (int i=0; i<GroupRecords->Count; i++) {
        ARecord = FindRecordEx(AView, GroupRecords->Items[i]);
        if (ARecord != NULL) {
            ARecord->Expand(false);
        }
    }
}
//---------------------------------------------------------------------------
void __fastcall TcxGridStatus::Restore(TcxGridTableView *AGridView) {
    SendMessage(AGridView->Site->Handle, WM_SETREDRAW, 0, 0);
    try {
        AGridView->ViewData->Collapse(true);
        LoadGridViewExpanded(AGridView);
        LoadGridViewSelection(AGridView);
        LoadGridViewTopFocusedRecords(AGridView);
    } __finally {
        SendMessage(AGridView->Site->Handle, WM_SETREDRAW, 1, 0);
        RedrawWindow(AGridView->Site->Handle, NULL, 0, RDW_FRAME | RDW_NOFRAME |
                                RDW_ALLCHILDREN | RDW_INVALIDATE);
    }
}

