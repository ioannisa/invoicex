unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, ADODB, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, StdCtrls, cxGridBandedTableView,
    cxGridDBBandedTableView, DBTables, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter;

type
  TRecordInfo = class
    KeyValues: Variant;
    ALevel: Integer;
  end;

  TRecordInfos = class(TList)
  private
    function GetItem(Index: Integer): TRecordInfo;
  protected
    procedure Notify(Ptr: Pointer; Action: TListNotification); override;
  public
    property Items[Index: Integer]: TRecordInfo read GetItem; Default;
  end;

  TcxGridStatus = class
  protected
    GroupRecords: TRecordInfos;
    SelectedRecords: TRecordInfos;
    SelStartItem: Integer;
    SelEndItem: Integer;
    AFocusedRow: TRecordInfo;
    TopRow: TRecordInfo;

    function SaveRecord(AView: TcxGridDBBandedTableView; GridRecord:
      TcxCustomGridRecord): TRecordInfo;
    function FindRecordEx(AView: TcxGridDBBandedTableView; RecordInfo:
      TRecordInfo): TcxCustomGridRecord;

    procedure SaveGridViewSelection(AView: TcxGridDBBandedTableView);
    procedure LoadGridViewSelection(AView: TcxGridDBBandedTableView);
    procedure SaveGridViewTopFocusedRecords(AView: TcxGridDBBandedTableView);
    procedure LoadGridViewTopFocusedRecords(AView: TcxGridDBBandedTableView);
    procedure SaveGridViewExpanded(AView: TcxGridDBBandedTableView);
    procedure LoadGridViewExpanded(AView: TcxGridDBBandedTableView);
  public
    constructor Create(AGridView: TcxGridDBBandedTableView);
    destructor Destroy; override;

    procedure Restore(AGridView: TcxGridDBBandedTableView);
  end;

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    cxGrid1: TcxGrid;
    cxGrid1Level1: TcxGridLevel;
    tblOrders: TTable;
    tblOrdersOrderNo: TFloatField;
    tblOrdersCustNo: TFloatField;
    tblOrdersSaleDate: TDateTimeField;
    tblOrdersShipDate: TDateTimeField;
    tblOrdersEmpNo: TIntegerField;
    tblOrdersShipToContact: TStringField;
    tblOrdersShipToAddr1: TStringField;
    tblOrdersShipToAddr2: TStringField;
    tblOrdersShipToCity: TStringField;
    tblOrdersShipToState: TStringField;
    tblOrdersShipToZip: TStringField;
    tblOrdersShipToCountry: TStringField;
    tblOrdersShipToPhone: TStringField;
    tblOrdersShipVIA: TStringField;
    tblOrdersPO: TStringField;
    tblOrdersTerms: TStringField;
    tblOrdersPaymentMethod: TStringField;
    tblOrdersItemsTotal: TCurrencyField;
    tblOrdersTaxRate: TFloatField;
    tblOrdersFreight: TCurrencyField;
    tblOrdersAmountPaid: TCurrencyField;
    dsOrders: TDataSource;
    cxGrid1DBBandedTableView1: TcxGridDBBandedTableView;
    cxGrid1DBBandedTableView1OrderNo: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1CustNo: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1SaleDate: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1ShipDate: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1EmpNo: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1ShipToContact: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1ShipToAddr1: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1ShipToAddr2: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1ShipToCity: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1ShipToState: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1ShipToZip: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1ShipToCountry: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1ShipToPhone: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1ShipVIA: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1PO: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1Terms: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1PaymentMethod: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1ItemsTotal: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1TaxRate: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1Freight: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1AmountPaid: TcxGridDBBandedColumn;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    FStatus: TcxGridStatus;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  cxVariants;

{$R *.dfm}

procedure TRecordInfos.Notify(Ptr: Pointer; Action: TListNotification);
begin
  if Action in [lnExtracted, lnDeleted] then
    FreeAndNil(TRecordInfo(Ptr));
end;

function TRecordInfos.GetItem(Index: Integer): TRecordInfo;
begin
  Result := TRecordInfo(inherited Items[Index]);
end;

function TcxGridStatus.SaveRecord(AView: TcxGridDBBandedTableView;
  GridRecord: TcxCustomGridRecord): TRecordInfo;
begin
  Result := TRecordInfo.Create;
  Result.KeyValues :=
    AView.DataController.GetRecordId(GridRecord.RecordIndex);
  Result.ALevel := GridRecord.Level;
end;

function TcxGridStatus.FindRecordEx(AView: TcxGridDBBandedTableView; RecordInfo:
  TRecordInfo): TcxCustomGridRecord;
var
  I, ARecordIndex: Integer;
  AList: TList;
  AKeyValue: Variant;
begin
  I := 0;
  ARecordIndex := AView.DataController.FindRecordIndexByKey(RecordInfo.KeyValues);
  Result := nil;
  AList := TList.Create;
  try
    while I < AView.ViewData.RowCount do
    begin
      if AView.ViewData.Rows[I] is TcxGridGroupRow then
      begin
        AList.Clear;
        AView.DataController.Groups.LoadRecordIndexesByRowIndex(AList, I);
        if AList.IndexOf(Pointer(ARecordIndex)) <> -1 then
        begin
          if AView.ViewData.Rows[I].Level < RecordInfo.ALevel then
            AView.ViewData.Rows[I].Expand(False)
          else
          begin
            Result := AView.ViewData.Rows[I];
            Break;
          end;
        end;
      end
      else
      begin
        AKeyValue := AView.DataController.GetRecordId(AView.ViewData.Rows[I].RecordIndex);
        if VarEquals(AKeyValue, RecordInfo.KeyValues) then
        begin
          Result := AView.ViewData.Rows[I];
          Break;
        end;
      end;
      Inc(I);
    end;
  finally
    FreeAndNil(AList);
  end;
end;

procedure TcxGridStatus.SaveGridViewSelection(
  AView: TcxGridDBBandedTableView);
var
  i: Integer;
  SelectRecordInfo: TRecordInfo;
begin
  with AView.DataController, AView.Controller do
  begin
    for i := 0 To SelectedRecordCount - 1 do
    begin
      SelectRecordInfo := SaveRecord(AView, SelectedRecords[i]);
      Self.SelectedRecords.Add(SelectRecordInfo);
    end;
    if SelectedColumnCount = 0 then
    begin
      SelStartItem := -1;
      SelEndItem := -1;
    end
    else
    begin
      SelStartItem := SelectedColumns[0].Index;
      SelEndItem := SelectedColumns[SelectedColumnCount - 1].Index;
    end;
  end;
end;

procedure TcxGridStatus.LoadGridViewSelection(
  AView: TcxGridDBBandedTableView);
var
  i: Integer;
  ARecord: TcxCustomGridRecord;
begin
  AView.Controller.ClearSelection;

  for i := 0 To SelectedRecords.Count - 1 do
  begin
    ARecord := FindRecordEx(AView, SelectedRecords[i]);
    if Assigned(ARecord) then
      ARecord.Selected := True;
  end;

  if SelStartItem <> -1 then
    AView.Controller.SelectColumns(AView.Columns[SelStartItem],
      AView.Columns[SelEndItem]);
end;

procedure TcxGridStatus.SaveGridViewTopFocusedRecords(AView: TcxGridDBBandedTableView);
begin
  if AView.Controller.FocusedRow = nil then
    AFocusedRow := nil
  else
    AFocusedRow := SaveRecord(AView, AView.Controller.FocusedRow);
  TopRow := nil;
  if (AView.Controller.TopRowIndex <> -1) and
    (AView.Controller.TopRowIndex < AView.ViewData.RecordCount) then
      TopRow := SaveRecord(AView, AView.ViewData.Records[AView.Controller.TopRowIndex]);
end;

procedure TcxGridStatus.LoadGridViewTopFocusedRecords(AView: TcxGridDBBandedTableView);
var
  ARecord: TcxCustomGridRecord;
begin
  ARecord := FindRecordEx(AView, AFocusedRow);
  if Assigned(ARecord) then
    ARecord.Focused := True;
  ARecord := FindRecordEx(AView, TopRow);
  if Assigned(ARecord) then
    AView.Controller.TopRowIndex := ARecord.Index;
end;

type
  TcxDataControllerGroupsAccess = class(TcxDataControllerGroups);
  TcxDataGroupsAccess = class(TcxDataGroups);

procedure TcxGridStatus.SaveGridViewExpanded(
  AView: TcxGridDBBandedTableView);
var
  i: Integer;
  GroupRecordInfo: TRecordInfo;
begin
  for i := 0 To AView.DataController.RowCount - 1 do
    with AView.ViewData.Records[i] do
      if Expanded then
      begin
        GroupRecordInfo := SaveRecord(AView, AView.ViewData.Records[i]);
        GroupRecords.Add(GroupRecordInfo);
      end;
end;

procedure TcxGridStatus.LoadGridViewExpanded(
  AView: TcxGridDBBandedTableView);
var
  i: Integer;
  ARecord: TcxCustomGridRecord;
begin
  for i := 0 to GroupRecords.Count - 1 do
  begin
    ARecord := FindRecordEx(AView, GroupRecords[i]);
    if Assigned(ARecord) then
      ARecord.Expand(False);
  end;
end;

constructor TcxGridStatus.Create(AGridView: TcxGridDBBandedTableView);
begin
  inherited Create;
  GroupRecords := TRecordInfos.Create;
  SelectedRecords := TRecordInfos.Create;
  SaveGridViewSelection(AGridView);
  SaveGridViewExpanded(AGridView);
  SaveGridViewTopFocusedRecords(AGridView);
end;

destructor TcxGridStatus.Destroy;
begin
  FreeAndNil(GroupRecords);
  FreeAndNil(SelectedRecords);
  FreeAndNil(AFocusedRow);
  FreeAndNil(TopRow);
  inherited;
end;

procedure TcxGridStatus.Restore(AGridView: TcxGridDBBandedTableView);
begin
  SendMessage(AGridView.Site.Handle, WM_SETREDRAW, 0, 0);
  try
      AGridView.ViewData.Collapse(True);
      LoadGridViewExpanded(AGridView);
      LoadGridViewSelection(AGridView);
      LoadGridViewTopFocusedRecords(AGridView);
  finally
    SendMessage(AGridView.Site.Handle, WM_SETREDRAW, 1, 0);
    RedrawWindow(AGridView.Site.Handle, Nil, 0, RDW_FRAME Or RDW_NOFRAME Or
      RDW_ALLCHILDREN Or RDW_INVALIDATE);
  end;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  FreeAndNil(FStatus);
  FStatus := TcxGridStatus.Create(cxGrid1DBBandedTableView1);
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
    with tblOrders do begin
        DisableControls;
        Close;
        Open;
        EnableControls;
    end;

  if Assigned(FStatus) then
    FStatus.Restore(cxGrid1DBBandedTableView1);
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  if FStatus <> nil then
    FreeAndNil(FStatus);
end;

end.
