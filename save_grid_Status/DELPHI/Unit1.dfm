object Form1: TForm1
  Left = 407
  Top = 201
  Caption = 'Form1'
  ClientHeight = 446
  ClientWidth = 688
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 128
    Top = 408
    Width = 75
    Height = 25
    Caption = 'Save'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 248
    Top = 416
    Width = 75
    Height = 25
    Caption = 'Load'
    TabOrder = 1
    OnClick = Button2Click
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 688
    Height = 337
    Align = alTop
    TabOrder = 2
    object cxGrid1DBBandedTableView1: TcxGridDBBandedTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsOrders
      DataController.KeyFieldNames = 'OrderNo'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsSelection.CellMultiSelect = True
      Bands = <
        item
        end>
      object cxGrid1DBBandedTableView1OrderNo: TcxGridDBBandedColumn
        DataBinding.FieldName = 'OrderNo'
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1CustNo: TcxGridDBBandedColumn
        DataBinding.FieldName = 'CustNo'
        Visible = False
        GroupIndex = 0
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1SaleDate: TcxGridDBBandedColumn
        DataBinding.FieldName = 'SaleDate'
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1ShipDate: TcxGridDBBandedColumn
        DataBinding.FieldName = 'ShipDate'
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1EmpNo: TcxGridDBBandedColumn
        DataBinding.FieldName = 'EmpNo'
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1ShipToContact: TcxGridDBBandedColumn
        DataBinding.FieldName = 'ShipToContact'
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1ShipToAddr1: TcxGridDBBandedColumn
        DataBinding.FieldName = 'ShipToAddr1'
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1ShipToAddr2: TcxGridDBBandedColumn
        DataBinding.FieldName = 'ShipToAddr2'
        Position.BandIndex = 0
        Position.ColIndex = 7
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1ShipToCity: TcxGridDBBandedColumn
        DataBinding.FieldName = 'ShipToCity'
        Position.BandIndex = 0
        Position.ColIndex = 8
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1ShipToState: TcxGridDBBandedColumn
        DataBinding.FieldName = 'ShipToState'
        Position.BandIndex = 0
        Position.ColIndex = 9
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1ShipToZip: TcxGridDBBandedColumn
        DataBinding.FieldName = 'ShipToZip'
        Position.BandIndex = 0
        Position.ColIndex = 10
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1ShipToCountry: TcxGridDBBandedColumn
        DataBinding.FieldName = 'ShipToCountry'
        Position.BandIndex = 0
        Position.ColIndex = 11
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1ShipToPhone: TcxGridDBBandedColumn
        DataBinding.FieldName = 'ShipToPhone'
        Position.BandIndex = 0
        Position.ColIndex = 12
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1ShipVIA: TcxGridDBBandedColumn
        DataBinding.FieldName = 'ShipVIA'
        Position.BandIndex = 0
        Position.ColIndex = 13
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1PO: TcxGridDBBandedColumn
        DataBinding.FieldName = 'PO'
        Position.BandIndex = 0
        Position.ColIndex = 14
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1Terms: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Terms'
        Width = 78
        Position.BandIndex = 0
        Position.ColIndex = 15
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1PaymentMethod: TcxGridDBBandedColumn
        DataBinding.FieldName = 'PaymentMethod'
        Visible = False
        GroupIndex = 1
        Position.BandIndex = 0
        Position.ColIndex = 16
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1ItemsTotal: TcxGridDBBandedColumn
        DataBinding.FieldName = 'ItemsTotal'
        Position.BandIndex = 0
        Position.ColIndex = 17
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1TaxRate: TcxGridDBBandedColumn
        DataBinding.FieldName = 'TaxRate'
        Position.BandIndex = 0
        Position.ColIndex = 18
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1Freight: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Freight'
        Position.BandIndex = 0
        Position.ColIndex = 19
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1AmountPaid: TcxGridDBBandedColumn
        DataBinding.FieldName = 'AmountPaid'
        Position.BandIndex = 0
        Position.ColIndex = 20
        Position.RowIndex = 0
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBBandedTableView1
    end
  end
  object tblOrders: TTable
    Active = True
    DatabaseName = 'DBDEMOS'
    TableName = 'orders.db'
    Left = 64
    Top = 72
    object tblOrdersOrderNo: TFloatField
      FieldName = 'OrderNo'
      DisplayFormat = #39'#'#39'0000'
    end
    object tblOrdersCustNo: TFloatField
      Alignment = taLeftJustify
      CustomConstraint = 'CustNo IS NOT NULL'
      ConstraintErrorMessage = 'CustNo cannot be blank'
      FieldName = 'CustNo'
      Required = True
      DisplayFormat = 'CN 0000'
      MaxValue = 9999.000000000000000000
      MinValue = 1000.000000000000000000
    end
    object tblOrdersSaleDate: TDateTimeField
      FieldName = 'SaleDate'
    end
    object tblOrdersShipDate: TDateTimeField
      FieldName = 'ShipDate'
    end
    object tblOrdersEmpNo: TIntegerField
      CustomConstraint = 'Value > 0'
      ConstraintErrorMessage = 'EmpNo cannot be 0 or negative'
      FieldName = 'EmpNo'
      Required = True
      DisplayFormat = 'Emp'#39'#'#39' 0000'
      MaxValue = 9999
      MinValue = 1
    end
    object tblOrdersShipToContact: TStringField
      FieldName = 'ShipToContact'
    end
    object tblOrdersShipToAddr1: TStringField
      FieldName = 'ShipToAddr1'
      Size = 30
    end
    object tblOrdersShipToAddr2: TStringField
      FieldName = 'ShipToAddr2'
      Size = 30
    end
    object tblOrdersShipToCity: TStringField
      FieldName = 'ShipToCity'
      Size = 15
    end
    object tblOrdersShipToState: TStringField
      FieldName = 'ShipToState'
    end
    object tblOrdersShipToZip: TStringField
      FieldName = 'ShipToZip'
      Size = 10
    end
    object tblOrdersShipToCountry: TStringField
      FieldName = 'ShipToCountry'
    end
    object tblOrdersShipToPhone: TStringField
      FieldName = 'ShipToPhone'
      Size = 15
    end
    object tblOrdersShipVIA: TStringField
      FieldName = 'ShipVIA'
      Size = 7
    end
    object tblOrdersPO: TStringField
      FieldName = 'PO'
      Size = 15
    end
    object tblOrdersTerms: TStringField
      FieldName = 'Terms'
      Size = 6
    end
    object tblOrdersPaymentMethod: TStringField
      FieldName = 'PaymentMethod'
      Size = 7
    end
    object tblOrdersItemsTotal: TCurrencyField
      FieldName = 'ItemsTotal'
    end
    object tblOrdersTaxRate: TFloatField
      FieldName = 'TaxRate'
      DisplayFormat = '0.00%'
      MaxValue = 100.000000000000000000
    end
    object tblOrdersFreight: TCurrencyField
      FieldName = 'Freight'
    end
    object tblOrdersAmountPaid: TCurrencyField
      FieldName = 'AmountPaid'
    end
  end
  object dsOrders: TDataSource
    DataSet = tblOrders
    Left = 24
    Top = 72
  end
end
