object MainFRM: TMainFRM
  Left = 286
  Top = 181
  Caption = 'InvoiceX - Cybernate Software'
  ClientHeight = 742
  ClientWidth = 1291
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 562
    Top = 5
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 481
    Top = 5
    Width = 75
    Height = 25
    Caption = 'Button2'
    TabOrder = 1
    OnClick = Button2Click
  end
  object dxStatusBar1: TdxStatusBar
    Left = 0
    Top = 720
    Width = 1291
    Height = 22
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        PanelStyle.Alignment = taCenter
        PanelStyle.Font.Charset = DEFAULT_CHARSET
        PanelStyle.Font.Color = clWindowText
        PanelStyle.Font.Height = -11
        PanelStyle.Font.Name = 'Tahoma'
        PanelStyle.Font.Style = [fsUnderline]
        PanelStyle.ParentFont = False
        Text = 'Cybernate InvoiceX'
        Width = 120
        OnClick = dxStatusBar1Panels0Click
      end
      item
        PanelStyleClassName = 'TdxStatusBarKeyboardStatePanelStyle'
        PanelStyle.CapsLockKeyAppearance.ActiveCaption = 'CAPS'
        PanelStyle.CapsLockKeyAppearance.InactiveCaption = 'CAPS'
        PanelStyle.NumLockKeyAppearance.ActiveCaption = 'NUM'
        PanelStyle.NumLockKeyAppearance.InactiveCaption = 'NUM'
        PanelStyle.ScrollLockKeyAppearance.ActiveCaption = 'SCRL'
        PanelStyle.ScrollLockKeyAppearance.InactiveCaption = 'SCRL'
        PanelStyle.InsertKeyAppearance.ActiveCaption = 'OVR'
        PanelStyle.InsertKeyAppearance.InactiveCaption = 'INS'
        Width = 124
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        PanelStyle.Alignment = taCenter
        PanelStyle.Font.Charset = DEFAULT_CHARSET
        PanelStyle.Font.Color = clWindowText
        PanelStyle.Font.Height = -11
        PanelStyle.Font.Name = 'Tahoma'
        PanelStyle.Font.Style = [fsUnderline]
        PanelStyle.ParentFont = False
        Text = 'MySQL Embedded'
        Width = 100
        OnClick = dxStatusBar1Panels2Click
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        PanelStyle.Alignment = taCenter
        PanelStyle.Font.Charset = DEFAULT_CHARSET
        PanelStyle.Font.Color = clWindowText
        PanelStyle.Font.Height = -11
        PanelStyle.Font.Name = 'Tahoma'
        PanelStyle.Font.Style = []
        PanelStyle.ParentFont = False
        Text = #941#954#948#959#963#951' 1.3.4'
        Width = 110
      end
      item
        PanelStyleClassName = 'TdxStatusBarContainerPanelStyle'
        PanelStyle.Container = dxStatusBar1Container4
        Width = 300
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
      end>
    PaintStyle = stpsUseLookAndFeel
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ExplicitLeft = -128
    ExplicitTop = 603
    ExplicitWidth = 1284
    object dxStatusBar1Container4: TdxStatusBarContainerControl
      AlignWithMargins = True
      Left = 468
      Top = 2
      Width = 302
      Height = 20
      object progBar: TcxProgressBar
        Left = 0
        Top = 0
        Align = alClient
        Properties.ShowTextStyle = cxtsText
        Properties.Text = #904#955#949#947#967#959#962' '#949#957#951#956#949#961#974#963#949#969#957'...'
        TabOrder = 0
        Transparent = True
        Visible = False
        Width = 302
      end
    end
  end
  object Panel5: TPanel
    Left = 0
    Top = 24
    Width = 1291
    Height = 696
    Align = alClient
    BevelOuter = bvNone
    Caption = 'Panel5'
    TabOrder = 4
    ExplicitLeft = 928
    ExplicitTop = 136
    ExplicitWidth = 185
    ExplicitHeight = 161
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 1291
      Height = 27
      Align = alTop
      TabOrder = 0
      object cxLabel1: TcxLabel
        Left = 8
        Top = 4
        Caption = #935#949#953#961#953#963#964#941#962
        Transparent = True
      end
      object btnAddUsr: TcxButton
        Left = 233
        Top = 2
        Width = 27
        Height = 22
        Hint = #928#961#959#963#952#942#954#951' / '#924#949#964#945#946#959#955#942' '#967#949#953#961#953#963#964#942
        OptionsImage.Glyph.Data = {
          B6040000424DB604000000000000360000002800000015000000120000000100
          18000000000080040000120B0000120B00000000000000000000C8D0D4C8D0D4
          C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
          D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
          C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D464686A00557F002A7F4040
          40565656ABB1B4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
          C8D0D4C8D0D4C8D0D4C8D0D464686A64686A002A3F0073DC00AAFF0055FF002A
          7F393055565656727475ABB1B4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
          C8D0D4C8D0D464686A6E2C4AFFB1B1FF6B6B00557F0073DC00AAFF0055FF002A
          7F6035CA21153B5656565656568F9395C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
          C8D0D4C8D0D47F5858DC5895FFB1B1FF6B6B00557F0073DC00AAFF0055FF002A
          7F6035CA18004B727475ABB1B4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
          C8D0D4C8D0D47F5858DC5895FFB1B1FF6B6B00557F0073DC00AAFF0049DC0024
          6D6035CA18004BC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
          C8D0D4C8D0D47F5858DC5895FFB1B1BF5050007FBF008EED00AAFF0079ED001E
          5C6035CA18004BC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
          C8D0D4C8D0D47F5858DC5895FFB1B17F464600AAFF00AAFF00AAFF007FFF004F
          ED2F1A650C0025C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
          C8D0D464686AFFB1B1FFB1B1FFB1B17F585800AAFF00AAFF00AAFF007FFF0055
          FF47357F240070969C9FC8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
          C8D0D464686AFFB1B1FFB1B1FFB1B17F585800AAFF00AAFF00AAFF007FFF0055
          FF47357F31009664686AC8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
          C8D0D464686AFFB1B1FFB1B1FFB1B17F585800AAFF3FBFFF9ADBFF75B8FF1A71
          FF47357F31009664686AC8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
          C8D0D464686AFFB1B1FFB1B1FFC4C47F6B6B5A9BBF556A95002A7F003FBF1A46
          7F8775BF31009664686AC8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
          C8D0D464686AFFC4C4FFC6C6FFAAAAFF9F9F5D4F5D5252527D7D7D3D3D3D8063
          A58247CA8247CA64686AC8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
          C8D0D4C8D0D47F6A6A5530302828285530303A3A3AD9D9D9DCDCDC7474740C0C
          0C21153B64686AC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
          C8D0D4C8D0D4000000989898BBBBBB5C5C5C3B3D3EB6B6B6CECECE5353539191
          915C5C5C64686AC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
          C8D0D4C8D0D4000000E9E9E9EFEFEF7D7D7D64686A999FA23F4142BFBFBFEFEF
          EF7D7D7D64686AC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
          C8D0D4C8D0D4C8D0D4000000000000000000C8D0D4C8D0D4C8D0D40000005858
          58515354C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
          C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
          D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = btnAddUsrClick
      end
      object cbboxUsr: TcxDBLookupComboBox
        Left = 60
        Top = 3
        DataBinding.DataField = 'idusr'
        DataBinding.DataSource = dsUsr
        Properties.DropDownListStyle = lsFixedList
        Properties.KeyFieldNames = 'idusr'
        Properties.ListColumns = <
          item
            Caption = #908#957#959#956#945
            SortOrder = soAscending
            FieldName = 'fullname'
          end>
        Properties.ListOptions.ShowHeader = False
        Properties.ListSource = dsUsr2
        Properties.OnChange = cbboxUsrPropertiesChange
        TabOrder = 0
        Width = 171
      end
      object cxLabel2: TcxLabel
        Left = 305
        Top = 4
        Caption = #924#951#957' '#958#949#967#957#940#964#949' '#960#972#963#959' '#963#951#956#945#957#964#953#954#972' '#949#943#957#945#953' '#957#945' '#954#961#945#964#940#964#949' '#945#957#964#943#947#961#945#966#945' '#945#963#966#945#955#949#943#945#962
        Enabled = False
        Style.TextStyle = [fsItalic]
        Transparent = True
      end
      object btnBackup: TcxButton
        Left = 641
        Top = 2
        Width = 82
        Height = 22
        Caption = 'BACKUP'
        OptionsImage.Glyph.Data = {
          F6020000424DF60200000000000036000000280000000E000000100000000100
          180000000000C0020000C40E0000C40E00000000000000000000C8D0D4000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000084840084840000000000000000000000000000000000
          00C6C6C6C6C6C600000000848400000000000000000084840084840000000000
          00000000000000000000000000C6C6C6C6C6C600000000848400000000000000
          00008484008484000000000000000000000000000000000000C6C6C6C6C6C600
          0000008484000000000000000000848400848400000000000000000000000000
          0000000000000000000000000000008484000000000000000000848400848400
          8484008484008484008484008484008484008484008484008484008484000000
          0000000000008484008484000000000000000000000000000000000000000000
          0000000084840084840000000000000000008484000000C6C6C6C6C6C6C6C6C6
          C6C6C6C6C6C6C6C6C6C6C6C6C6C6C60000000084840000000000000000008484
          000000C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C60000000084
          840000000000000000008484000000C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6
          C6C6C6C6C6C6C60000000084840000000000000000008484000000C6C6C6C6C6
          C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C600000000848400000000000000
          00008484000000C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C600
          00000000000000000000000000008484000000C6C6C6C6C6C6C6C6C6C6C6C6C6
          C6C6C6C6C6C6C6C6C6C6C6000000C6C6C6000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
          C8D0D4C8D0D4C8D0D4C8D0D40000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
          C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D40000}
        TabOrder = 2
        OnClick = btnBackupClick
      end
      object btnRestore: TcxButton
        Left = 729
        Top = 2
        Width = 91
        Height = 22
        Caption = 'RESTORE'
        OptionsImage.Glyph.Data = {
          A6020000424DA6020000000000003600000028000000100000000D0000000100
          18000000000070020000C40E0000C40E00000000000000000000C8D0D4000000
          000000000000000000000000000000000000000000000000000000000000C8D0
          D4C8D0D4C8D0D4C8D0D4C8D0D400000000000000848400848400848400848400
          8484008484008484008484008484000000C8D0D4C8D0D4C8D0D4C8D0D4000000
          00FFFF0000000084840084840084840084840084840084840084840084840084
          84000000C8D0D4C8D0D4C8D0D4000000FFFFFF00FFFF00000000848400848400
          8484008484008484008484008484008484008484000000C8D0D4C8D0D4000000
          00FFFFFFFFFF00FFFF0000000084840084840084840084840084840084840084
          84008484008484000000C8D0D4000000FFFFFF00FFFFFFFFFF00FFFF00000000
          0000000000000000000000000000000000000000000000000000C8D0D4000000
          00FFFFFFFFFF00FFFFFFFFFF00FFFFFFFFFF00FFFFFFFFFF00FFFF000000C8D0
          D4C8D0D4C8D0D4C8D0D4C8D0D4000000FFFFFF00FFFFFFFFFF00FFFFFFFFFF00
          FFFFFFFFFF00FFFFFFFFFF000000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4000000
          00FFFFFFFFFF00FFFF000000000000000000000000000000000000000000C8D0
          D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4000000000000000000C8D0D4C8D0D4C8
          D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4000000000000000000C8D0D4C8D0D4
          C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
          D4C8D0D4000000000000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
          D0D4C8D0D4000000C8D0D4C8D0D4C8D0D4000000C8D0D4000000C8D0D4C8D0D4
          C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D40000000000000000
          00C8D0D4C8D0D4C8D0D4}
        TabOrder = 3
        OnClick = btnRestoreClick
      end
      object btnClearDB: TcxButton
        Left = 826
        Top = 2
        Width = 192
        Height = 22
        Caption = #913#916#917#921#913#931#924#913' '#914#913#931#919#931' '#916#917#916#927#924#917#925#937#925
        OptionsImage.Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000C30E0000C30E00000000000000000000DCDCDCDCDCDC
          DCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDC
          DCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDCDCDCDCDCDCDCDCDCDCDCDCDCDCDC
          DCDCDC333333333333333333333333333333333333333333333333333333FFFF
          FFDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDC333333FFFFFFCCCCCCCCCCCC99
          9999999999999999666666333333FFFFFFDCDCDCDCDCDCDCDCDCDCDCDCDCDCDC
          DCDCDC333333FFFFFF999999CCCCCC666666999999333333666666333333FFFF
          FFDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDC333333FFFFFF999999CCCCCC66
          6666999999333333666666333333FFFFFFDCDCDCDCDCDCDCDCDCDCDCDCDCDCDC
          DCDCDC333333FFFFFF999999CCCCCC666666999999333333666666333333FFFF
          FFDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDC333333FFFFFF999999CCCCCC66
          6666999999333333666666333333FFFFFFDCDCDCDCDCDCDCDCDCDCDCDCDCDCDC
          DCDCDC333333FFFFFF999999CCCCCC666666999999333333666666333333FFFF
          FFDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDC333333FFFFFF999999CCCCCC66
          6666999999333333666666333333FFFFFFDCDCDCDCDCDCDCDCDCDCDCDCDCDCDC
          DCDCDC333333FFFFFFCCCCCCCCCCCC999999999999999999666666333333FFFF
          FFFFFFFFDCDCDCDCDCDCDCDCDCDCDCDC33333333333333333333333333333333
          3333333333333333333333333333333333FFFFFFDCDCDCDCDCDCDCDCDCDCDCDC
          333333CCCCCCCCCCCCCCCCCCCCCCCC9999999999999999999999996666663333
          33FFFFFFDCDCDCDCDCDCDCDCDCDCDCDC33333333333333333333333333333333
          3333333333333333333333333333333333DCDCDCDCDCDCDCDCDCDCDCDCDCDCDC
          DCDCDCDCDCDCDCDCDCDCDCDC333333333333333333DCDCDCDCDCDCDCDCDCDCDC
          DCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDC
          DCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDC}
        TabOrder = 4
        OnClick = btnClearDBClick
      end
      object Button3: TButton
        Left = 1224
        Top = 8
        Width = 75
        Height = 25
        Caption = 'Button3'
        TabOrder = 7
        OnClick = Button3Click
      end
    end
    object mainPageControl: TcxPageControl
      Left = 0
      Top = 27
      Width = 1291
      Height = 669
      Align = alClient
      TabOrder = 1
      Properties.ActivePage = tsInvoices
      Properties.CustomButtons.Buttons = <>
      Properties.Images = ImageList1
      OnChange = mainPageControlChange
      ExplicitLeft = -104
      ExplicitTop = -32
      ExplicitWidth = 289
      ExplicitHeight = 193
      ClientRectBottom = 665
      ClientRectLeft = 4
      ClientRectRight = 1287
      ClientRectTop = 25
      object tsInvoices: TcxTabSheet
        Caption = #922#945#964#945#967#974#961#953#963#951' '#945#960#959#948#949#943#958#949#969#957' - '#931#964#945#964#953#963#964#953#954#940
        ImageIndex = 58
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object gridInput: TcxGrid
          Left = 0
          Top = 55
          Width = 1283
          Height = 585
          Align = alClient
          TabOrder = 1
          LevelTabs.Images = ImageList1
          RootLevelOptions.DetailTabsPosition = dtpTop
          OnActiveTabChanged = gridInputActiveTabChanged
          object bvInput: TcxGridDBBandedTableView
            PopupMenu = popupManagement
            Navigator.Buttons.CustomButtons = <>
            Navigator.Buttons.Insert.Visible = False
            Navigator.Buttons.Delete.Visible = False
            Navigator.Buttons.Edit.Visible = False
            Navigator.Buttons.Post.Visible = False
            Navigator.Buttons.Cancel.Visible = False
            Navigator.Buttons.Refresh.Visible = False
            Navigator.Buttons.GotoBookmark.Visible = True
            Navigator.InfoPanel.DisplayMask = '[RecordIndex] '#945#960#972' [RecordCount]'
            Navigator.InfoPanel.Visible = True
            Navigator.Visible = True
            FilterBox.Visible = fvAlways
            OnCellDblClick = bvInputCellDblClick
            DataController.DataSource = dsCusInv
            DataController.Filter.DateTimeFormat = 'dd/mm/yyyy'
            DataController.KeyFieldNames = 'invDate;idusrinv'
            DataController.Summary.DefaultGroupSummaryItems = <
              item
                Format = ',0.00 '#8364';-,0.00 '#8364
                Kind = skSum
                Position = spFooter
                Column = bvInputamount
              end
              item
                Format = ',0.00 '#8364';-,0.00 '#8364
                Kind = skSum
                Column = bvInputamount
              end
              item
                Format = ',0'
                Kind = skCount
                Position = spFooter
                Column = bvInputinvNum
              end
              item
                Format = ',0'
                Kind = skCount
                Column = bvInputinvNum
              end>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = #902#952#961': ,0.00 '#8364';-,0.00 '#8364
                Kind = skSum
                Column = bvInputamount
              end
              item
                Format = #931#965#957': ,0'
                Kind = skCount
                Column = bvInputinvNum
              end
              item
                Format = #924#927': ,0.00 '#8364';-,0.00 '#8364
                Kind = skAverage
                Column = bvInputamount
              end>
            DataController.Summary.SummaryGroups = <>
            DataController.OnGroupingChanged = bvInputDataControllerGroupingChanged
            DateTimeHandling.Filters = [dtfRelativeDays, dtfRelativeDayPeriods, dtfRelativeWeeks, dtfRelativeMonths, dtfRelativeYears]
            DateTimeHandling.MonthFormat = 'mm'
            DateTimeHandling.YearFormat = 'yyyy'
            DateTimeHandling.DateFormat = 'dd/mm/yyyy'
            DateTimeHandling.Grouping = dtgByDate
            DateTimeHandling.HourFormat = 'hh'
            OptionsBehavior.ImmediateEditor = False
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
            OptionsCustomize.BandMoving = False
            OptionsCustomize.BandsQuickCustomization = True
            OptionsCustomize.BandsQuickCustomizationReordering = qcrEnabled
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsSelection.MultiSelect = True
            OptionsView.Footer = True
            OptionsView.FooterAutoHeight = True
            OptionsView.FooterMultiSummaries = True
            OptionsView.GroupFooters = gfVisibleWhenExpanded
            OptionsView.GroupRowStyle = grsOffice11
            OptionsView.GroupSummaryLayout = gslAlignWithColumnsAndDistribute
            Bands = <
              item
                Caption = #914#913#931#921#922#913' '#931#932#927#921#935#917#921#913
                Styles.Header = cxStyle1
              end
              item
                Caption = #922#913#932#919#915#927#929#921#917#931' '#913#915#927#929#937#925
                Styles.Header = cxStyle1
              end
              item
                Caption = #919#924#917#929#927#923#927#915#921#913#922#913' '#931#932#927#921#935#917#921#913
                Styles.Header = cxStyle1
              end>
            object bvInputinvNum: TcxGridDBBandedColumn
              Caption = #913#961'. '#913#960#959#948'.'
              DataBinding.FieldName = 'invNum'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taRightJustify
              HeaderAlignmentHorz = taCenter
              Width = 70
              Position.BandIndex = 0
              Position.ColIndex = 4
              Position.RowIndex = 0
            end
            object bvInputinvDate: TcxGridDBBandedColumn
              Caption = #919#956'/'#957#943#945
              DataBinding.FieldName = 'invDate'
              PropertiesClassName = 'TcxDateEditProperties'
              Properties.SaveTime = False
              Properties.ShowTime = False
              OnGetFilterValues = bvInputinvDateGetFilterValues
              HeaderAlignmentHorz = taCenter
              Options.CellMerging = True
              SortIndex = 0
              SortOrder = soDescending
              Width = 83
              Position.BandIndex = 0
              Position.ColIndex = 1
              Position.RowIndex = 0
            end
            object bvInputyearDate: TcxGridDBBandedColumn
              Caption = #904#964#959#962
              DataBinding.FieldName = 'yearDate'
              HeaderAlignmentHorz = taCenter
              Options.CellMerging = True
              Width = 50
              Position.BandIndex = 2
              Position.ColIndex = 4
              Position.RowIndex = 0
            end
            object bvInputmonthDate: TcxGridDBBandedColumn
              Caption = #924#942#957#945#962
              DataBinding.FieldName = 'monthDate'
              HeaderAlignmentHorz = taCenter
              Options.CellMerging = True
              Width = 50
              Position.BandIndex = 2
              Position.ColIndex = 3
              Position.RowIndex = 0
            end
            object bvInputdayDate: TcxGridDBBandedColumn
              Caption = #919#956#941#961#945
              DataBinding.FieldName = 'dayDate'
              HeaderAlignmentHorz = taCenter
              Options.CellMerging = True
              Width = 50
              Position.BandIndex = 2
              Position.ColIndex = 1
              Position.RowIndex = 0
            end
            object bvInputweekOfDate: TcxGridDBBandedColumn
              Caption = #917#946#948#959#956#940#948#945
              DataBinding.FieldName = 'weekOfDate'
              HeaderAlignmentHorz = taCenter
              Options.CellMerging = True
              Width = 55
              Position.BandIndex = 2
              Position.ColIndex = 2
              Position.RowIndex = 0
            end
            object bvInputweekdayDate: TcxGridDBBandedColumn
              Caption = #919#956'. '#917#946#948'.'
              DataBinding.FieldName = 'weekdaynum'
              PropertiesClassName = 'TcxImageComboBoxProperties'
              Properties.Alignment.Horz = taLeftJustify
              Properties.Items = <
                item
                  Description = #916#917#933
                  ImageIndex = 0
                  Value = 0
                end
                item
                  Description = #932#929#921
                  Value = 1
                end
                item
                  Description = #932#917#932
                  Value = 2
                end
                item
                  Description = #928#917#924
                  Value = 3
                end
                item
                  Description = #928#913#929
                  Value = 4
                end
                item
                  Description = #931#913#914
                  Value = 5
                end
                item
                  Description = #922#933#929
                  Value = 6
                end>
              HeaderAlignmentHorz = taCenter
              Options.CellMerging = True
              Width = 50
              Position.BandIndex = 2
              Position.ColIndex = 0
              Position.RowIndex = 0
            end
            object bvInputamount: TcxGridDBBandedColumn
              Caption = #928#959#963#972
              DataBinding.FieldName = 'amount'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.Alignment.Horz = taRightJustify
              Properties.DisplayFormat = ',0.00 '#8364';-,0.00 '#8364
              HeaderAlignmentHorz = taCenter
              Width = 130
              Position.BandIndex = 0
              Position.ColIndex = 5
              Position.RowIndex = 0
            end
            object bvInputstoreName: TcxGridDBBandedColumn
              Caption = #922#945#964#940#963#964#951#956#945
              DataBinding.FieldName = 'storeName'
              HeaderAlignmentHorz = taCenter
              Width = 190
              Position.BandIndex = 0
              Position.ColIndex = 3
              Position.RowIndex = 0
            end
            object bvInputstoreAfm: TcxGridDBBandedColumn
              Caption = #913'.'#934'.'#924'.'
              DataBinding.FieldName = 'storeAfm'
              HeaderAlignmentHorz = taCenter
              Width = 69
              Position.BandIndex = 0
              Position.ColIndex = 2
              Position.RowIndex = 0
            end
            object bvInputexpenseDescr: TcxGridDBBandedColumn
              Caption = #917#943#948#959#962' '#913#947#959#961#940#962
              DataBinding.FieldName = 'expenseDescr'
              HeaderAlignmentHorz = taCenter
              Width = 200
              Position.BandIndex = 1
              Position.ColIndex = 1
              Position.RowIndex = 0
            end
            object bvInputexpenseMasterDescr: TcxGridDBBandedColumn
              Caption = #922#945#964#951#947#959#961#943#945
              DataBinding.FieldName = 'expenseMasterDescr'
              HeaderAlignmentHorz = taCenter
              Width = 117
              Position.BandIndex = 1
              Position.ColIndex = 0
              Position.RowIndex = 0
            end
            object bvInputFullname: TcxGridDBBandedColumn
              Caption = #935#949#953#961#953#963#964#942#962
              DataBinding.FieldName = 'fullname'
              HeaderAlignmentHorz = taCenter
              Options.CellMerging = True
              Width = 133
              Position.BandIndex = 0
              Position.ColIndex = 0
              Position.RowIndex = 0
            end
          end
          object tvAnual: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            Navigator.Buttons.Insert.Visible = False
            Navigator.Buttons.Delete.Visible = False
            Navigator.Buttons.Edit.Visible = False
            Navigator.Buttons.Post.Visible = False
            Navigator.Buttons.Cancel.Visible = False
            Navigator.Buttons.Refresh.Visible = False
            Navigator.Buttons.SaveBookmark.Visible = False
            Navigator.Buttons.GotoBookmark.Visible = False
            Navigator.InfoPanel.DisplayMask = '[RecordIndex] '#945#960#972' [RecordCount]'
            Navigator.InfoPanel.Visible = True
            Navigator.Visible = True
            DataController.DataSource = dsAnual
            DataController.Filter.DateTimeFormat = 'dd/mm/yyyy'
            DataController.Summary.DefaultGroupSummaryItems = <
              item
                Format = ',0'
                Kind = skSum
                Position = spFooter
                Column = tvAnualqty
              end
              item
                Format = ',0'
                Kind = skSum
                Column = tvAnualqty
              end
              item
                Format = ',0.00 '#8364';-,0.00 '#8364
                Kind = skSum
                Position = spFooter
                Column = tvAnualtotal
              end
              item
                Format = ',0.00 '#8364';-,0.00 '#8364
                Kind = skSum
                Column = tvAnualtotal
              end
              item
                Format = ',0.00 '#8364';-,0.00 '#8364
                Kind = skSum
                Position = spFooter
                Column = tvAnualfinal
              end
              item
                Format = ',0.00 '#8364';-,0.00 '#8364
                Kind = skSum
                Position = spFooter
                Column = tvAnualanualGoal
              end
              item
                Format = ',0.00 '#8364';-,0.00 '#8364
                Kind = skSum
                Column = tvAnualanualGoal
              end
              item
                Format = ',0.00 '#8364';-,0.00 '#8364
                Kind = skSum
                Position = spFooter
                Column = tvAnualdesired
              end>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = #915#961#945#956#956#949#962': ,0'
                Kind = skCount
                Column = tvAnualfullname
              end
              item
                Format = #913#960#959#948#949#953#958#949#953#962': ,0'
                Kind = skSum
                Column = tvAnualqty
              end
              item
                Format = #902#952#961': ,0.00 '#8364';-,0.00 '#8364
                Kind = skSum
                Column = tvAnualtotal
              end
              item
                Format = ',0.00 '#8364';-,0.00 '#8364
                Kind = skSum
                Column = tvAnualfinal
              end
              item
                Format = ',0.00 '#8364';-,0.00 '#8364
                Kind = skSum
                Column = tvAnualanualGoal
              end
              item
                Format = ',0.00 '#8364';-,0.00 '#8364
                Kind = skSum
                Column = tvAnualdesired
              end>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.ImmediateEditor = False
            OptionsBehavior.IncSearch = True
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.Footer = True
            OptionsView.GroupFooters = gfVisibleWhenExpanded
            OptionsView.GroupRowStyle = grsOffice11
            OptionsView.GroupSummaryLayout = gslAlignWithColumnsAndDistribute
            object tvAnualfullname: TcxGridDBColumn
              Caption = #935#949#953#961#953#963#964#942#962
              DataBinding.FieldName = 'fullname'
              HeaderAlignmentHorz = taCenter
              Width = 221
            end
            object tvAnualqty: TcxGridDBColumn
              Caption = #928#959#963#972#964#951#964#945' '#913#960#959#948#949#953#958#949#969#957
              DataBinding.FieldName = 'qty'
              HeaderAlignmentHorz = taCenter
              Width = 148
            end
            object tvAnualtotal: TcxGridDBColumn
              Caption = #931#973#957#959#955#959' '#913#960#959#948#949#943#958#949#969#957
              DataBinding.FieldName = 'total'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.DisplayFormat = ',0.00 '#8364';-,0.00 '#8364
              HeaderAlignmentHorz = taCenter
              Width = 126
            end
            object tvAnualanualGoal: TcxGridDBColumn
              Caption = #917#964#942#963#953#959' '#917#953#963#972#948#951#956#945
              DataBinding.FieldName = 'anualGoal'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.DisplayFormat = ',0.00 '#8364';-,0.00 '#8364
              HeaderAlignmentHorz = taCenter
              Width = 110
            end
            object tvAnuala: TcxGridDBColumn
              Caption = #922#955#943#956#945#954#945' "'#913'"'
              DataBinding.FieldName = 'a'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.DisplayFormat = ',0.00 '#8364';-,0.00 '#8364
              HeaderAlignmentHorz = taCenter
              Width = 122
            end
            object tvAnualb: TcxGridDBColumn
              Caption = #922#955#943#956#945#954#945' "'#914'"'
              DataBinding.FieldName = 'b'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.DisplayFormat = ',0.00 '#8364';-,0.00 '#8364
              HeaderAlignmentHorz = taCenter
              Width = 106
            end
            object tvAnualdesired: TcxGridDBColumn
              Caption = #917#960#953#952#965#956#951#964#941#962' '#913#960#959#948#949#943#958#949#953#962
              DataBinding.FieldName = 'desired'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.DisplayFormat = ',0.00 '#8364';-,0.00 '#8364
              HeaderAlignmentHorz = taCenter
              Width = 120
            end
            object tvAnualfinal: TcxGridDBColumn
              Caption = #917#960#953#963#964#961#959#966#942'(+) / '#934#972#961#959#962'(-)'
              DataBinding.FieldName = 'final'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.DisplayFormat = ',0.00 '#8364';-,0.00 '#8364
              HeaderAlignmentHorz = taCenter
              Width = 129
            end
            object tvAnualsetYear: TcxGridDBColumn
              Caption = #904#964#959#962
              DataBinding.FieldName = 'setYear'
              HeaderAlignmentHorz = taCenter
            end
          end
          object ch1: TcxGridDBChartView
            DataController.DataSource = dsCusInv
            DiagramArea.Values.CaptionPosition = ldvcpAbove
            DiagramBar.Values.CaptionPosition = cdvcpOutsideEnd
            DiagramColumn.Active = True
            DiagramColumn.Values.CaptionPosition = cdvcpOutsideEnd
            DiagramLine.Values.CaptionPosition = ldvcpAbove
            Title.Text = #915#961#945#966#953#954#940' '#946#940#963#949#953' '#964#973#960#959#965' '#949#958#972#948#969#957' - '#954#940#957#964#949' zoom '#963#964#953#962' '#956#960#940#961#949#962
            ToolBox.Border = tbNone
            ToolBox.CustomizeButton = True
            ToolBox.DiagramSelector = True
            object ch1DataGroup1: TcxGridDBChartDataGroup
              DataBinding.FieldName = 'expenseMasterDescr'
              DisplayText = #914#945#963#953#954#942' '#922#945#964#951#947#959#961#943#945
            end
            object ch1DataGroup7: TcxGridDBChartDataGroup
              DataBinding.FieldName = 'expenseDescr'
              DisplayText = #933#960#959#954#945#964#951#947#959#961#943#945
            end
            object ch1Series1: TcxGridDBChartSeries
              DataBinding.FieldName = 'amount'
              DisplayText = #928#959#963#972
              ValueCaptionFormat = ',0.00 '#8364';-,0.00 '#8364
            end
            object ch1Series2: TcxGridDBChartSeries
              DataBinding.FieldName = 'dayDate'
              DisplayText = #928#959#963#972#964#951#964#945
              GroupSummaryKind = skCount
              ValueCaptionFormat = ',0'
            end
          end
          object ch2: TcxGridDBChartView
            DataController.DataSource = dsCusInv
            DiagramArea.Values.CaptionPosition = ldvcpAbove
            DiagramBar.Values.CaptionPosition = cdvcpOutsideEnd
            DiagramColumn.Active = True
            DiagramColumn.Values.CaptionPosition = cdvcpOutsideEnd
            DiagramLine.Values.CaptionPosition = ldvcpAbove
            Title.Text = #915#961#945#966#953#954#940' '#946#940#963#949#953' '#951#956'/'#947#953#945#954#974#957' '#963#964#959#953#967#949#943#969#957' - '#954#940#957#964#949' zoom '#963#964#953#962' '#956#960#940#961#949#962
            ToolBox.Border = tbNone
            ToolBox.CustomizeButton = True
            ToolBox.DiagramSelector = True
            object ch2DataGroup1: TcxGridDBChartDataGroup
              DataBinding.FieldName = 'yearDate'
              DisplayText = #904#964#959#962
            end
            object ch2DataGroup2: TcxGridDBChartDataGroup
              DataBinding.FieldName = 'monthDate'
              DisplayText = #924#942#957#945#962
            end
            object ch2DataGroup3: TcxGridDBChartDataGroup
              DataBinding.FieldName = 'invDate'
              DisplayText = #924#941#961#945
            end
            object ch2Series1: TcxGridDBChartSeries
              DataBinding.FieldName = 'amount'
              DisplayText = #928#959#963#972
              ValueCaptionFormat = ',0.00 '#8364';-,0.00 '#8364
            end
            object ch2Series2: TcxGridDBChartSeries
              DataBinding.FieldName = 'amount'
              DisplayText = #928#959#963#972#964#951#964#945
              GroupSummaryKind = skCount
              ValueCaptionFormat = ',0'
            end
          end
          object lvlInput: TcxGridLevel
            Tag = 1
            Caption = #922#945#964#945#967#969#961#951#956#941#957#945' '#963#964#959#953#967#949#943#945
            GridView = bvInput
            ImageIndex = 57
          end
          object lvlAnual: TcxGridLevel
            Tag = 2
            Caption = #917#964#942#963#953#945' '#963#965#947#954#949#957#964#961#969#964#953#954#940
            GridView = tvAnual
            ImageIndex = 59
          end
          object lvlCh1: TcxGridLevel
            Tag = 3
            Caption = #915#961#945#966#953#954#941#962' '#960#945#961#940#963#964#945#963#949#953#962' '#946#940#963#949#953' '#954#945#964#951#947#959#961#943#945#962' '#949#953#948#974#957
            GridView = ch1
            ImageIndex = 52
          end
          object lvlCh2: TcxGridLevel
            Tag = 4
            Caption = #915#961#945#966#953#954#941#962' '#960#945#961#945#963#964#940#963#949#953#962' '#946#940#963#949#953' '#951#956#949#961#959#955#959#947#953#945#954#974#957' '#963#964#959#953#967#949#943#969#957
            GridView = ch2
            ImageIndex = 52
          end
        end
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 1283
          Height = 55
          Align = alTop
          TabOrder = 0
          object btnPrint: TcxButton
            Left = 270
            Top = 6
            Width = 98
            Height = 25
            Caption = #917#954#964#973#960#969#963#951
            OptionsImage.Glyph.Data = {
              76030000424D7603000000000000360000002800000011000000100000000100
              1800000000004003000000000000000000000000000000000000C8D0D4C8D0D4
              C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
              D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4000000000000000000000000
              000000000000000000000000000000000000000000C8D0D4C8D0D4C8D0D4C8D0
              D400C8D0D4000000C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6
              C6C6C6000000C6C6C6000000C8D0D4C8D0D4C8D0D40000000000000000000000
              0000000000000000000000000000000000000000000000000000000000C6C6C6
              000000C8D0D4C8D0D400000000C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C600
              FFFF00FFFF00FFFFC6C6C6C6C6C6000000000000000000C8D0D4C8D0D4000000
              00C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6848484848484848484C6C6C6C6
              C6C6000000C6C6C6000000C8D0D4C8D0D4000000000000000000000000000000
              00000000000000000000000000000000000000000000000000C6C6C6C6C6C600
              0000C8D0D400000000C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6
              C6C6C6C6C6C6C6000000C6C6C6000000C6C6C6000000C8D0D400C8D0D4000000
              000000000000000000000000000000000000000000000000000000C6C6C60000
              00C6C6C6000000000000C8D0D400C8D0D4C8D0D4000000FFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000C6C6C6000000C6C6C6000000C8D0
              D400C8D0D4C8D0D4C8D0D4000000FFFFFF000000000000000000000000000000
              FFFFFF000000000000000000000000C8D0D4C8D0D400C8D0D4C8D0D4C8D0D400
              0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000C8D0D4
              C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4000000FFFFFF00000000
              0000000000000000000000FFFFFF000000C8D0D4C8D0D4C8D0D4C8D0D400C8D0
              D4C8D0D4C8D0D4C8D0D4000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFF000000C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
              D4000000000000000000000000000000000000000000000000000000C8D0D4C8
              D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
              D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400}
            TabOrder = 2
            OnClick = btnPrintClick
          end
          object rgUsrStats: TcxRadioGroup
            Left = 374
            Top = 3
            Caption = #928#961#959#946#959#955#942' '#945#960#959#948#949#943#958#949#969#957' '#947#953#945' '#967#949#953#961#953#963#964#942
            Properties.Columns = 2
            Properties.Items = <
              item
                Caption = #917#960#953#955#949#947#956#941#957#959
              end
              item
                Caption = #908#955#959#965#962
              end>
            Properties.OnChange = rgUsrStatsPropertiesChange
            ItemIndex = 0
            TabOrder = 3
            Height = 49
            Width = 176
          end
          object lblChartInfo: TcxLabel
            Left = 965
            Top = 5
            AutoSize = False
            Caption = 
              #927#953' '#947#961#945#966#953#954#941#962' '#960#945#961#945#963#964#940#963#949#953#962' '#960#945#961#959#965#963#953#940#950#959#965#957' '#945#960#959#964#949#955#941#963#956#945#964#945' '#946#940#963#949#953' '#964#951#962' '#949#960#953#955 +
              #959#947#942#962' '#964#969#957' '#966#943#955#964#961#969#957' '#960#959#965' '#959#961#943#950#959#957#964#945#953' '#963#964#959' tab "'#922#945#964#945#967#969#961#951#956#941#957#945' '#963#964#959#953#967#949#943#945'"'
            Enabled = False
            Style.TextStyle = [fsItalic]
            Properties.WordWrap = True
            Transparent = True
            Visible = False
            Height = 44
            Width = 244
          end
          object lblAnualInfo: TcxLabel
            Left = 965
            Top = 5
            AutoSize = False
            Caption = 
              #927' '#965#960#959#955#959#947#953#963#956#972#962' '#941#964#959#965#962' '#947#943#957#949#964#945#953' '#947#953#945' '#964#959#957' '#949#960#953#955#949#947#956#941#957#959' '#967#949#953#961#953#963#964#942' '#963#964#959' '#949#960#953#955 +
              #949#947#956#941#957#959' '#941#964#959#962' '#949#954#964#974#962' '#945#957' '#949#960#953#955#949#947#959#973#957' "'#972#955#959#953'" '#963#964#945' "'#931#964#945#964#953#963#964#953#954#940' '#947#953#945' '#967#949#953#961#953#963 +
              #964#942'"'
            Enabled = False
            Style.TextStyle = [fsItalic]
            Properties.WordWrap = True
            Transparent = True
            Visible = False
            Height = 44
            Width = 244
          end
          object btnNewInvoice: TcxButton
            Left = 6
            Top = 6
            Width = 107
            Height = 25
            Caption = #925#941#945' '#913#960#972#948#949#953#958#951
            OptionsImage.Glyph.Data = {
              76030000424D7603000000000000360000002800000011000000100000000100
              18000000000040030000120B0000120B00000000000000000000DCDCDC000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000DCDCDCDCDCDCDCDCDC00000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000DCDCDCDCDCDCDCDC
              DC00000000FFFFFFFF0000FF0000FF0000FFFFFFFF0000FF0000FF0000FFFFFF
              FF0000FF0000FFFFFF000000DCDCDCDCDCDCDCDCDC00000000FFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000
              DCDCDCDCDCDCDCDCDC00000000FFFFFFFF0000FF0000FF0000FFFFFFFF0000FF
              0000FF0000FFFFFFFF0000FF0000FFFFFF000000DCDCDCDCDCDCDCDCDC000000
              00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000
              0000000000000000DCDCDCDCDCDCDCDCDC00000000FFFFFFFF0000FF0000FF00
              00FFFFFFFF0000FF0000FF000000000000FF0000FF0000FF00000000DCDCDCDC
              DCDCDCDCDC00000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FF00000000FF0000FF0000FF00000000DCDCDCDCDCDCDCDCDC00000000FFFFFF
              FF0000FF0000FF0000FFFFFF00000000000000000000000000FF0000FF0000FF
              0000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              00000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF000000
              000000000000000000000000000000000000000000000000FF0000FF0000FF00
              00FF0000FF0000FF0000FF0000FF0000FF0000000000000000FFFF00FFFF00FF
              FF00FFFF00FFFF0000000000FF0000FF0000FF0000FF0000FF0000FF0000FF00
              00FF0000FF000000000000000000000000000000000000000000000000000000
              000000000000000000FF0000FF0000FF0000000000000000000000000000DCDC
              DCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDC00000000FF0000
              FF0000FF00000000DCDCDCDCDCDCDCDCDC00DCDCDCDCDCDCDCDCDCDCDCDCDCDC
              DCDCDCDCDCDCDCDCDCDCDCDCDC00000000FF0000FF0000FF00000000DCDCDCDC
              DCDCDCDCDC00DCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDC
              DC000000000000000000000000000000DCDCDCDCDCDCDCDCDC00}
            TabOrder = 0
            OnClick = btnNewInvoiceClick
          end
          object btnExport: TcxButton
            Left = 119
            Top = 6
            Width = 145
            Height = 25
            Caption = #917#958#945#947#969#947#942' '#963#964#959#953#967#949#943#969#957
            DropDownMenu = popupExport
            Kind = cxbkDropDownButton
            OptionsImage.Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000000000000302
              02035E5D5A7497908AE59B7F75F49C7D70F09A7969F09B7969F09C786CF0B299
              8FFB74736F910000000000000000000000000000000000000000000000003838
              3645988B86F9BD6B51FFF78F68FFFA956EFFF38B64FFEFA077FFF6AD86FFF0AB
              82FFCB9981FF76736F94000000000000000000000000000000000F0F0E129C99
              93E4B56E57FFFF9E74FFF17F57FFEF7A54FFFE9E76FFFFB793FFFFE1C3FFFFE7
              CDFFFDDBC2FFC5A08EF4282725320000000000000000000000006663607C9B7F
              71FFEF9672FFEF845DFFE0613BFFDF5E37FFEA7B53FFFFA681FFFFE6D7FFFFFF
              FBFFFFFFFFFFEDCFC6FF7F726AA0000000000000000000000000ABA49EE8B579
              62FFFDA57AFFED895FFFEF8962FFC68A74F2B2816EFBF19A71FFFFD0BEFFFFFF
              FFFFFFFFFDFFFFF9F4FFB99E91E00808080B0000000000000000AEA7A0E2D58E
              71FFF59C73FFF4AA81FFD89B7EFB2422212E8A8781C3DD9270FFFFC1A2FFFFF6
              EDFFFFF7F0FFFFFEF6FFE3C2B0FF4C4945650000000000000000101110145E58
              5376604C4383614B418738343149000000009E9E99CDCA8F73FFFFBB97FFFFEB
              DAFFFFF3E6FFFFF6E7FFF1CFBDFF51433E6B0000000000000000000000000000
              000000000000000000004E4B4860AAA59FE0AEA7A3FFB3836DFFFFBE94FFFFDF
              CAFFFFEEDCFFFFEFDAFFF7D4B9FF91796EB85658556E10100F14000000000000
              0000000000000101010197938CD7B6826CFFCD8F73FFDE9878FFFFC099FFFFDA
              C0FFFFE9D1FFFFE9D1FFFDD9BFFFCA8A73FFB18A7AFFAD9F94E2000000000000
              000000000000000000009D9993D0D69574FFFFBD93FFFFDDC2FFFFE1C8FFFFE0
              C7FFFFE2C7FFFFE1C6FFFFE2C4FFFFE2C3FFFACDACFFCA907EE8000000000000
              000000000000000000002C2B2934AC9388F0E58E6AFFFFC0A0FFFFE2C6FFFFDF
              C0FFFFDFBFFFFFDEBDFFFFDDBDFFFFE1C0FFEDBA9AFF6657517C000000000000
              000000000000000000000000000072706C8DBE836DFFF18E63FFFFD0AAFFFFDF
              BEFFFFDAB8FFFFD9B7FFFFDABAFFFBC6A1FF917769B003030304000000000000
              000000000000000000000000000000000000908C87C7CC6F50FFF79A71FFFFD7
              B5FFFFD7B4FFFFD8B5FFFFD4AFFFD7A488F32F2C2A3D00000000000000000000
              00000000000000000000000000000000000021222129A48378E8DA603CFFFBB1
              88FFFFDCB3FFFFDBB2FFF3B690FF55453E6C0000000000000000000000000000
              000000000000000000000000000000000000000000006264617FC16E54FFE961
              38FFFFCA9EFFFECDA1FFB89280D90807070A0000000000000000000000000000
              00000000000000000000000000000000000000000000000000005E58537DC07F
              6AF9D18869F0DEA188FF504E4A64000000000000000000000000}
            TabOrder = 1
            OnClick = btnExportClick
          end
          object cbboxGroup: TcxComboBox
            Left = 178
            Top = 33
            Hint = 
              #924#960#959#961#949#943#964#949' '#957#945' '#959#956#945#948#959#960#959#953#949#943#964#949' '#956#949' '#964#959' '#960#959#957#964#943#954#953' '#972#960#969#962' '#949#963#949#943#949#962' '#952#941#955#949#964#949' '#947#953#945' '#963#973 +
              #957#952#949#964#949#962' '#959#956#945#948#959#960#959#953#942#963#949#953#962'.  '#917#948#969' '#949#960#953#955#941#947#949#964#949' '#964#953#962' '#963#965#957#951#952#941#963#964#949#961#949#962' '#959#956#945#948#959#960#959#953#942#963 +
              #949#953#962' '#947#953#945' '#957#945' '#956#951' '#967#940#957#949#964#949' '#967#961#972#957#959
            ParentShowHint = False
            Properties.DropDownListStyle = lsFixedList
            Properties.Items.Strings = (
              #935#969#961#943#962' '#959#956#945#948#959#960#959#943#951#963#951
              #917#943#948#959#965#962' '#945#947#959#961#940#962
              #922#945#964#951#947#959#961#943#945#962' '#949#958#972#948#959#965
              #922#945#964#963#964#942#956#945#964#959#962
              #924#942#957#945' '#941#964#959#965#962
              #917#946#948#959#956#940#948#945#962' '#941#964#959#965#962
              #919#956#941#961#945#962' '#949#946#948#959#956#940#948#959#962)
            Properties.OnChange = cbboxGroupPropertiesChange
            ShowHint = True
            TabOrder = 6
            Text = #935#969#961#943#962' '#959#956#945#948#959#960#959#943#951#963#951
            Width = 190
          end
          object lblGroup: TcxLabel
            Left = 6
            Top = 34
            Caption = #932#945#967#949#943#945' '#959#956#945#948#959#960#959#943#951#963#951' '#960#943#957#945#954#945' '#946#940#963#949#953' '
            Transparent = True
          end
          object cbboxYearStats: TcxRadioGroup
            Left = 556
            Top = 3
            Caption = #928#961#959#946#959#955#942' '#945#960#959#948#949#943#958#949#969#957' '#947#953#945' '#941#964#959#962
            Properties.Columns = 3
            Properties.Items = <
              item
                Caption = #908#955#945
              end
              item
                Caption = #934#941#964#959#962
              end
              item
                Caption = #928#941#961#965#963#953
              end>
            Properties.OnChange = cbboxYearStatsPropertiesChange
            ItemIndex = 1
            Style.TextStyle = []
            TabOrder = 8
            Height = 49
            Width = 173
          end
          object rgIncluded: TcxRadioGroup
            Left = 733
            Top = 2
            Caption = #928#959#965' '#960#961#959#963#956#949#964#961#972#957#964#945#953' '#963#964#959' '#945#966#959#961#959#955#972#947#951#964#959
            Properties.Columns = 3
            Properties.Items = <
              item
                Caption = #908#955#945
              end
              item
                Caption = #925#945#953
              end
              item
                Caption = #908#967#953
              end>
            Properties.OnChange = cbboxYearStatsPropertiesChange
            ItemIndex = 1
            Style.TextStyle = []
            TabOrder = 9
            Height = 49
            Width = 212
          end
        end
        object memo_grid_by_weekday: TMemo
          Left = 928
          Top = 236
          Width = 331
          Height = 89
          Lines.Strings = (
            '[Main]'
            'Version=2'
            ''
            '[bvInput: TcxGridDBBandedTableView]'
            '='
            'Footer="True"'
            'GroupByBox="True"'
            'GroupFooters=1'
            'NewItemRow="False"'
            'Version=1'
            ''
            '[bvInput/Bands: TcxGridBands]'
            '='
            ''
            '[bvInput/Bands/Band0: TcxGridBand]'
            '='
            'Width=0'
            'Visible="True"'
            'Index=0'
            'BandIndex=-1'
            'ColIndex=0'
            ''
            '[bvInput/Bands/Band1: TcxGridBand]'
            '='
            'Width=0'
            'Visible="True"'
            'Index=1'
            'BandIndex=-1'
            'ColIndex=1'
            ''
            '[bvInput/Bands/Band2: TcxGridBand]'
            '='
            'Width=0'
            'Visible="False"'
            'Index=2'
            'BandIndex=-1'
            'ColIndex=2'
            ''
            '[bvInput/0: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=4'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=70'
            'AlignmentHorz=1'
            'Index=0'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/1: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=1'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=83'
            'AlignmentHorz=0'
            'Index=1'
            'Visible="True"'
            'SortOrder="soDescending"'
            'SortIndex=0'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/2: TcxGridDBBandedColumn]'
            '='
            'BandIndex=2'
            'ColIndex=4'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=50'
            'AlignmentHorz=0'
            'Index=2'
            'Visible="False"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/3: TcxGridDBBandedColumn]'
            '='
            'BandIndex=2'
            'ColIndex=3'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=50'
            'AlignmentHorz=0'
            'Index=3'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/4: TcxGridDBBandedColumn]'
            '='
            'BandIndex=2'
            'ColIndex=1'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=50'
            'AlignmentHorz=0'
            'Index=4'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/5: TcxGridDBBandedColumn]'
            '='
            'BandIndex=2'
            'ColIndex=2'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=55'
            'AlignmentHorz=0'
            'Index=5'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="True"'
            ''
            '[bvInput/6: TcxGridDBBandedColumn]'
            '='
            'BandIndex=2'
            'ColIndex=0'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=0'
            'Width=50'
            'AlignmentHorz=0'
            'Index=6'
            'Visible="False"'
            'SortOrder="soAscending"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="True"'
            ''
            '[bvInput/7: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=5'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=130'
            'AlignmentHorz=1'
            'Index=7'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/8: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=3'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=190'
            'AlignmentHorz=0'
            'Index=8'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/9: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=2'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=69'
            'AlignmentHorz=0'
            'Index=9'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/10: TcxGridDBBandedColumn]'
            '='
            'BandIndex=1'
            'ColIndex=1'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=200'
            'AlignmentHorz=0'
            'Index=10'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/11: TcxGridDBBandedColumn]'
            '='
            'BandIndex=1'
            'ColIndex=0'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=117'
            'AlignmentHorz=0'
            'Index=11'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/12: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=0'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=133'
            'AlignmentHorz=0'
            'Index=12'
            'Visible="False"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            '')
          ScrollBars = ssBoth
          TabOrder = 7
          Visible = False
          WordWrap = False
        end
        object memo_grid_by_week: TMemo
          Left = 918
          Top = 221
          Width = 331
          Height = 89
          Lines.Strings = (
            '[Main]'
            'Version=2'
            ''
            '[bvInput: TcxGridDBBandedTableView]'
            '='
            'Footer="True"'
            'GroupByBox="True"'
            'GroupFooters=1'
            'NewItemRow="False"'
            'Version=1'
            ''
            '[bvInput/Bands: TcxGridBands]'
            '='
            ''
            '[bvInput/Bands/Band0: TcxGridBand]'
            '='
            'Width=0'
            'Visible="True"'
            'Index=0'
            'BandIndex=-1'
            'ColIndex=0'
            ''
            '[bvInput/Bands/Band1: TcxGridBand]'
            '='
            'Width=0'
            'Visible="True"'
            'Index=1'
            'BandIndex=-1'
            'ColIndex=1'
            ''
            '[bvInput/Bands/Band2: TcxGridBand]'
            '='
            'Width=0'
            'Visible="False"'
            'Index=2'
            'BandIndex=-1'
            'ColIndex=2'
            ''
            '[bvInput/0: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=4'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=70'
            'AlignmentHorz=1'
            'Index=0'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/1: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=1'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=83'
            'AlignmentHorz=0'
            'Index=1'
            'Visible="True"'
            'SortOrder="soDescending"'
            'SortIndex=0'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/2: TcxGridDBBandedColumn]'
            '='
            'BandIndex=2'
            'ColIndex=4'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=50'
            'AlignmentHorz=0'
            'Index=2'
            'Visible="False"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/3: TcxGridDBBandedColumn]'
            '='
            'BandIndex=2'
            'ColIndex=3'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=50'
            'AlignmentHorz=0'
            'Index=3'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/4: TcxGridDBBandedColumn]'
            '='
            'BandIndex=2'
            'ColIndex=1'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=50'
            'AlignmentHorz=0'
            'Index=4'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/5: TcxGridDBBandedColumn]'
            '='
            'BandIndex=2'
            'ColIndex=2'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=0'
            'Width=55'
            'AlignmentHorz=0'
            'Index=5'
            'Visible="False"'
            'SortOrder="soAscending"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="True"'
            ''
            '[bvInput/6: TcxGridDBBandedColumn]'
            '='
            'BandIndex=2'
            'ColIndex=0'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=50'
            'AlignmentHorz=0'
            'Index=6'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/7: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=5'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=130'
            'AlignmentHorz=1'
            'Index=7'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/8: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=3'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=190'
            'AlignmentHorz=0'
            'Index=8'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/9: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=2'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=69'
            'AlignmentHorz=0'
            'Index=9'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/10: TcxGridDBBandedColumn]'
            '='
            'BandIndex=1'
            'ColIndex=1'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=200'
            'AlignmentHorz=0'
            'Index=10'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/11: TcxGridDBBandedColumn]'
            '='
            'BandIndex=1'
            'ColIndex=0'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=117'
            'AlignmentHorz=0'
            'Index=11'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/12: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=0'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=133'
            'AlignmentHorz=0'
            'Index=12'
            'Visible="False"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            '')
          ScrollBars = ssBoth
          TabOrder = 5
          Visible = False
          WordWrap = False
        end
        object memo_grid_by_month: TMemo
          Left = 910
          Top = 207
          Width = 331
          Height = 89
          Lines.Strings = (
            '[Main]'
            'Version=2'
            ''
            '[bvInput: TcxGridDBBandedTableView]'
            '='
            'Footer="True"'
            'GroupByBox="True"'
            'GroupFooters=1'
            'NewItemRow="False"'
            'Version=1'
            ''
            '[bvInput/Bands: TcxGridBands]'
            '='
            ''
            '[bvInput/Bands/Band0: TcxGridBand]'
            '='
            'Width=0'
            'Visible="True"'
            'Index=0'
            'BandIndex=-1'
            'ColIndex=0'
            ''
            '[bvInput/Bands/Band1: TcxGridBand]'
            '='
            'Width=0'
            'Visible="True"'
            'Index=1'
            'BandIndex=-1'
            'ColIndex=1'
            ''
            '[bvInput/Bands/Band2: TcxGridBand]'
            '='
            'Width=0'
            'Visible="False"'
            'Index=2'
            'BandIndex=-1'
            'ColIndex=2'
            ''
            '[bvInput/0: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=4'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=70'
            'AlignmentHorz=1'
            'Index=0'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/1: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=1'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=83'
            'AlignmentHorz=0'
            'Index=1'
            'Visible="True"'
            'SortOrder="soDescending"'
            'SortIndex=0'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/2: TcxGridDBBandedColumn]'
            '='
            'BandIndex=2'
            'ColIndex=4'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=50'
            'AlignmentHorz=1'
            'Index=2'
            'Visible="False"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/3: TcxGridDBBandedColumn]'
            '='
            'BandIndex=2'
            'ColIndex=3'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=0'
            'Width=50'
            'AlignmentHorz=1'
            'Index=3'
            'Visible="False"'
            'SortOrder="soAscending"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="True"'
            ''
            '[bvInput/4: TcxGridDBBandedColumn]'
            '='
            'BandIndex=2'
            'ColIndex=1'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=50'
            'AlignmentHorz=1'
            'Index=4'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/5: TcxGridDBBandedColumn]'
            '='
            'BandIndex=2'
            'ColIndex=2'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=55'
            'AlignmentHorz=1'
            'Index=5'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/6: TcxGridDBBandedColumn]'
            '='
            'BandIndex=2'
            'ColIndex=0'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=50'
            'AlignmentHorz=0'
            'Index=6'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/7: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=5'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=130'
            'AlignmentHorz=1'
            'Index=7'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/8: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=3'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=190'
            'AlignmentHorz=0'
            'Index=8'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/9: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=2'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=69'
            'AlignmentHorz=0'
            'Index=9'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/10: TcxGridDBBandedColumn]'
            '='
            'BandIndex=1'
            'ColIndex=1'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=200'
            'AlignmentHorz=0'
            'Index=10'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/11: TcxGridDBBandedColumn]'
            '='
            'BandIndex=1'
            'ColIndex=0'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=117'
            'AlignmentHorz=0'
            'Index=11'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/12: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=0'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=133'
            'AlignmentHorz=0'
            'Index=12'
            'Visible="False"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            '')
          ScrollBars = ssBoth
          TabOrder = 4
          Visible = False
          WordWrap = False
        end
        object memo_grid_by_store: TMemo
          Left = 902
          Top = 193
          Width = 331
          Height = 89
          Lines.Strings = (
            '[Main]'
            'Version=2'
            ''
            '[bvInput: TcxGridDBBandedTableView]'
            '='
            'Footer="True"'
            'GroupByBox="True"'
            'GroupFooters=1'
            'NewItemRow="False"'
            'Version=1'
            ''
            '[bvInput/Bands: TcxGridBands]'
            '='
            ''
            '[bvInput/Bands/Band0: TcxGridBand]'
            '='
            'Width=0'
            'Visible="True"'
            'Index=0'
            'BandIndex=-1'
            'ColIndex=0'
            ''
            '[bvInput/Bands/Band1: TcxGridBand]'
            '='
            'Width=0'
            'Visible="True"'
            'Index=1'
            'BandIndex=-1'
            'ColIndex=1'
            ''
            '[bvInput/Bands/Band2: TcxGridBand]'
            '='
            'Width=0'
            'Visible="False"'
            'Index=2'
            'BandIndex=-1'
            'ColIndex=2'
            ''
            '[bvInput/0: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=4'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=70'
            'AlignmentHorz=1'
            'Index=0'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/1: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=1'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=83'
            'AlignmentHorz=0'
            'Index=1'
            'Visible="True"'
            'SortOrder="soAscending"'
            'SortIndex=1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/2: TcxGridDBBandedColumn]'
            '='
            'BandIndex=2'
            'ColIndex=4'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=50'
            'AlignmentHorz=1'
            'Index=2'
            'Visible="False"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/3: TcxGridDBBandedColumn]'
            '='
            'BandIndex=2'
            'ColIndex=3'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=50'
            'AlignmentHorz=1'
            'Index=3'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/4: TcxGridDBBandedColumn]'
            '='
            'BandIndex=2'
            'ColIndex=1'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=50'
            'AlignmentHorz=1'
            'Index=4'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/5: TcxGridDBBandedColumn]'
            '='
            'BandIndex=2'
            'ColIndex=2'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=55'
            'AlignmentHorz=1'
            'Index=5'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/6: TcxGridDBBandedColumn]'
            '='
            'BandIndex=2'
            'ColIndex=0'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=50'
            'AlignmentHorz=0'
            'Index=6'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/7: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=5'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=130'
            'AlignmentHorz=1'
            'Index=7'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/8: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=3'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=0'
            'Width=190'
            'AlignmentHorz=0'
            'Index=8'
            'Visible="False"'
            'SortOrder="soAscending"'
            'SortIndex=0'
            'WasVisibleBeforeGrouping="True"'
            ''
            '[bvInput/9: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=2'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=529'
            'AlignmentHorz=0'
            'Index=9'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/10: TcxGridDBBandedColumn]'
            '='
            'BandIndex=1'
            'ColIndex=1'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=200'
            'AlignmentHorz=0'
            'Index=10'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/11: TcxGridDBBandedColumn]'
            '='
            'BandIndex=1'
            'ColIndex=0'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=117'
            'AlignmentHorz=0'
            'Index=11'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/12: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=0'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=133'
            'AlignmentHorz=0'
            'Index=12'
            'Visible="False"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"')
          ScrollBars = ssBoth
          TabOrder = 6
          Visible = False
          WordWrap = False
        end
        object memo_grid_by_itemCategory: TMemo
          Left = 886
          Top = 175
          Width = 331
          Height = 89
          Lines.Strings = (
            '[Main]'
            'Version=2'
            ''
            '[bvInput: TcxGridDBBandedTableView]'
            '='
            'Footer="True"'
            'GroupByBox="True"'
            'GroupFooters=1'
            'NewItemRow="False"'
            'Version=1'
            ''
            '[bvInput/Bands: TcxGridBands]'
            '='
            ''
            '[bvInput/Bands/Band0: TcxGridBand]'
            '='
            'Width=0'
            'Visible="True"'
            'Index=0'
            'BandIndex=-1'
            'ColIndex=0'
            ''
            '[bvInput/Bands/Band1: TcxGridBand]'
            '='
            'Width=0'
            'Visible="True"'
            'Index=1'
            'BandIndex=-1'
            'ColIndex=1'
            ''
            '[bvInput/Bands/Band2: TcxGridBand]'
            '='
            'Width=0'
            'Visible="False"'
            'Index=2'
            'BandIndex=-1'
            'ColIndex=2'
            ''
            '[bvInput/0: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=4'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=70'
            'AlignmentHorz=1'
            'Index=0'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/1: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=1'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=83'
            'AlignmentHorz=0'
            'Index=1'
            'Visible="True"'
            'SortOrder="soAscending"'
            'SortIndex=1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/2: TcxGridDBBandedColumn]'
            '='
            'BandIndex=2'
            'ColIndex=4'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=50'
            'AlignmentHorz=1'
            'Index=2'
            'Visible="False"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/3: TcxGridDBBandedColumn]'
            '='
            'BandIndex=2'
            'ColIndex=3'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=50'
            'AlignmentHorz=1'
            'Index=3'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/4: TcxGridDBBandedColumn]'
            '='
            'BandIndex=2'
            'ColIndex=1'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=50'
            'AlignmentHorz=1'
            'Index=4'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/5: TcxGridDBBandedColumn]'
            '='
            'BandIndex=2'
            'ColIndex=2'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=55'
            'AlignmentHorz=1'
            'Index=5'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/6: TcxGridDBBandedColumn]'
            '='
            'BandIndex=2'
            'ColIndex=0'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=50'
            'AlignmentHorz=0'
            'Index=6'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/7: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=5'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=130'
            'AlignmentHorz=1'
            'Index=7'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/8: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=3'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=190'
            'AlignmentHorz=0'
            'Index=8'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/9: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=2'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=69'
            'AlignmentHorz=0'
            'Index=9'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/10: TcxGridDBBandedColumn]'
            '='
            'BandIndex=1'
            'ColIndex=1'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=200'
            'AlignmentHorz=0'
            'Index=10'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/11: TcxGridDBBandedColumn]'
            '='
            'BandIndex=1'
            'ColIndex=0'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=0'
            'Width=117'
            'AlignmentHorz=0'
            'Index=11'
            'Visible="False"'
            'SortOrder="soAscending"'
            'SortIndex=0'
            'WasVisibleBeforeGrouping="True"'
            ''
            '[bvInput/12: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=0'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=133'
            'AlignmentHorz=0'
            'Index=12'
            'Visible="False"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '')
          ScrollBars = ssBoth
          TabOrder = 2
          Visible = False
          WordWrap = False
        end
        object memo_grid_by_item: TMemo
          Left = 878
          Top = 159
          Width = 331
          Height = 89
          Lines.Strings = (
            '[Main]'
            'Version=2'
            ''
            '[bvInput: TcxGridDBBandedTableView]'
            '='
            'Footer="True"'
            'GroupByBox="True"'
            'GroupFooters=1'
            'NewItemRow="False"'
            'Version=1'
            ''
            '[bvInput/Bands: TcxGridBands]'
            '='
            ''
            '[bvInput/Bands/Band0: TcxGridBand]'
            '='
            'Width=0'
            'Visible="True"'
            'Index=0'
            'BandIndex=-1'
            'ColIndex=0'
            ''
            '[bvInput/Bands/Band1: TcxGridBand]'
            '='
            'Width=0'
            'Visible="True"'
            'Index=1'
            'BandIndex=-1'
            'ColIndex=1'
            ''
            '[bvInput/Bands/Band2: TcxGridBand]'
            '='
            'Width=0'
            'Visible="False"'
            'Index=2'
            'BandIndex=-1'
            'ColIndex=2'
            ''
            '[bvInput/0: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=4'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=70'
            'AlignmentHorz=1'
            'Index=0'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/1: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=1'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=83'
            'AlignmentHorz=0'
            'Index=1'
            'Visible="True"'
            'SortOrder="soAscending"'
            'SortIndex=1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/2: TcxGridDBBandedColumn]'
            '='
            'BandIndex=2'
            'ColIndex=4'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=50'
            'AlignmentHorz=1'
            'Index=2'
            'Visible="False"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/3: TcxGridDBBandedColumn]'
            '='
            'BandIndex=2'
            'ColIndex=3'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=50'
            'AlignmentHorz=1'
            'Index=3'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/4: TcxGridDBBandedColumn]'
            '='
            'BandIndex=2'
            'ColIndex=1'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=50'
            'AlignmentHorz=1'
            'Index=4'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/5: TcxGridDBBandedColumn]'
            '='
            'BandIndex=2'
            'ColIndex=2'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=55'
            'AlignmentHorz=1'
            'Index=5'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/6: TcxGridDBBandedColumn]'
            '='
            'BandIndex=2'
            'ColIndex=0'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=50'
            'AlignmentHorz=0'
            'Index=6'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/7: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=5'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=130'
            'AlignmentHorz=1'
            'Index=7'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/8: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=3'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=190'
            'AlignmentHorz=0'
            'Index=8'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/9: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=2'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=69'
            'AlignmentHorz=0'
            'Index=9'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/10: TcxGridDBBandedColumn]'
            '='
            'BandIndex=1'
            'ColIndex=1'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=0'
            'Width=200'
            'AlignmentHorz=0'
            'Index=10'
            'Visible="False"'
            'SortOrder="soAscending"'
            'SortIndex=0'
            'WasVisibleBeforeGrouping="True"'
            ''
            '[bvInput/11: TcxGridDBBandedColumn]'
            '='
            'BandIndex=1'
            'ColIndex=0'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=117'
            'AlignmentHorz=0'
            'Index=11'
            'Visible="True"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '[bvInput/12: TcxGridDBBandedColumn]'
            '='
            'BandIndex=0'
            'ColIndex=0'
            'RowIndex=0'
            'LineCount=1'
            'GroupIndex=-1'
            'Width=133'
            'AlignmentHorz=0'
            'Index=12'
            'Visible="False"'
            'SortOrder="soNone"'
            'SortIndex=-1'
            'WasVisibleBeforeGrouping="False"'
            ''
            '')
          ScrollBars = ssBoth
          TabOrder = 3
          Visible = False
          WordWrap = False
        end
      end
      object cxTabSheet2: TcxTabSheet
        Caption = #917#960#953#967#949#953#961#942#963#949#953#962
        ImageIndex = 67
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 1283
          Height = 61
          Align = alTop
          TabOrder = 0
          object cxLabel14: TcxLabel
            Left = 6
            Top = 2
            Caption = 
              '- '#917#948#974' '#960#961#959#946#940#955#955#959#957#964#945#953'  '#959#953' '#949#960#953#967#949#953#961#942#963#949#953#962' '#956#949' '#964#951#957' '#949#960#969#957#965#956#943#945' '#954#945#953' '#964#959' '#913'.'#934'.'#924 +
              '. '#964#959#965#962' '#954#945#953' '#964#959#957' '#964#949#955#949#965#964#945#943#959' '#964#973#960#959' '#945#947#959#961#940#962' '#960#959#965' '#949#943#967#949' '#947#943#957#949#953' '#963#949' '#945#965#964#941#962' '#964#953#962 +
              ' '#949#960#953#967#949#953#961#942#963#949#953#962'.'
            Enabled = False
            Style.TextStyle = [fsItalic]
            Transparent = True
          end
          object cxLabel16: TcxLabel
            Left = 6
            Top = 22
            Caption = 
              '- '#915#943#957#949#964#945#953' '#941#955#949#947#967#959#962' '#913#934#924' '#946#940#963#949#953' '#964#959#965' '#945#955#947#959#961#943#952#956#959#965' '#960#959#965' '#948#953#945#964#943#952#949#964#945#953'  '#945#960#972' '#964 +
              #959' '#945#961#956#972#948#953#959' '#965#960#959#965#961#947#949#943#959'.  '#913#934#924' '#960#959#965' '#941#967#959#965#957' '#963#942#956#945#957#963#951' '#955#940#952#959#965#962' '#945#960#959#954#955#949#943#949#964#945#953' '#957 +
              #945' '#949#943#957#945#953' '#963#969#963#964#940'.'
            Enabled = False
            Style.TextStyle = [fsItalic]
            Transparent = True
          end
          object cxLabel17: TcxLabel
            Left = 6
            Top = 40
            Caption = 
              '- '#917#960#953#967#949#953#961#942#963#949#953#962' '#960#961#959#963#964#943#952#949#957#964#945#953' '#945#965#964#972#956#945#964#945' '#954#945#964#940' '#964#951' '#948#951#956#953#959#965#961#947#943#945' '#957#941#945#962' '#945#960#972 +
              #948#949#953#958#951#962'.  '#917#948#974' '#956#960#959#961#949#943#964#949' '#957#945' '#956#949#964#945#946#940#955#949#964#949' '#963#964#959#953#967#949#943#945' '#942#948#951' '#954#945#964#945#967#969#961#951#956#941#957#951#962' '#949 +
              #964#945#953#961#943#945#962' '#942' '#954#945#953' '#957#945' '#960#961#959#963#952#941#963#949#964#949' '#966#965#963#953#954#940' '#956#953#945' '#940#955#955#951'.'
            Enabled = False
            ParentFont = False
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.TextStyle = [fsBold, fsItalic]
            Style.IsFontAssigned = True
            Transparent = True
          end
        end
        object gridCMP: TcxGrid
          Left = 0
          Top = 86
          Width = 1283
          Height = 554
          Align = alClient
          TabOrder = 1
          object tvCMP: TcxGridDBTableView
            PopupMenu = popupCMP
            Navigator.Buttons.CustomButtons = <>
            Navigator.Buttons.Insert.Visible = False
            Navigator.Buttons.Delete.Visible = False
            Navigator.Buttons.Edit.Visible = False
            Navigator.Buttons.Post.Visible = False
            Navigator.Buttons.Cancel.Visible = False
            Navigator.Buttons.Refresh.Visible = False
            Navigator.Buttons.SaveBookmark.Visible = True
            Navigator.Buttons.GotoBookmark.Visible = True
            Navigator.InfoPanel.DisplayMask = '[RecordIndex] '#945#960#972' [RecordCount]'
            Navigator.InfoPanel.Visible = True
            Navigator.Visible = True
            FilterBox.Visible = fvAlways
            DataController.DataSource = dsStore
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = #931#969#963#964#940' '#913#934#924': ,0'
                Kind = skSum
                Column = tvCMPcorrectAFM
              end
              item
                Format = ',0'
                Kind = skCount
                Column = tvCMPidstore
              end>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.FocusFirstCellOnNewRecord = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsBehavior.ImmediateEditor = False
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnGrouping = False
            OptionsCustomize.ColumnHorzSizing = False
            OptionsCustomize.ColumnMoving = False
            OptionsCustomize.DataRowSizing = True
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            OptionsView.Indicator = True
            object tvCMPidstore: TcxGridDBColumn
              Caption = #913'/'#913
              DataBinding.FieldName = 'idstore'
              Visible = False
              HeaderAlignmentHorz = taCenter
            end
            object tvCMPdescr: TcxGridDBColumn
              Caption = #917#960#969#957#965#956#943#945
              DataBinding.FieldName = 'descr'
              HeaderAlignmentHorz = taCenter
              SortIndex = 0
              SortOrder = soAscending
            end
            object tvCMPafm: TcxGridDBColumn
              Caption = #913'.'#934'.'#924'.'
              DataBinding.FieldName = 'afm'
              HeaderAlignmentHorz = taCenter
              Width = 90
            end
            object tvCMPcorrectAFM: TcxGridDBColumn
              Caption = #931#969#963#964#972' '#913'.'#934'.'#924'.'
              DataBinding.FieldName = 'correctAFM'
              PropertiesClassName = 'TcxImageComboBoxProperties'
              Properties.Alignment.Horz = taLeftJustify
              Properties.Images = ImageList1
              Properties.ImmediateDropDownWhenKeyPressed = False
              Properties.Items = <
                item
                  Description = #925#913#921
                  ImageIndex = 0
                  Value = 1
                end
                item
                  Description = #927#935#921
                  ImageIndex = 39
                  Value = 0
                end>
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Options.Focusing = False
              Width = 115
            end
            object tvCMPidExpenseType: TcxGridDBColumn
              Caption = #917#958#39#959#961#953#963#956#959#973' '#954#945#964#951#947#959#961#943#945' '#949#958#972#948#969#957' '#947#953#945' '#964#951#957' '#949#960#953#967#949#943#961#953#963#951
              DataBinding.FieldName = 'idExpenseType'
              PropertiesClassName = 'TcxLookupComboBoxProperties'
              Properties.KeyFieldNames = 'idDetail'
              Properties.ListColumns = <
                item
                  Caption = #932#973#960#959#953' '#917#958#972#948#969#957
                  SortOrder = soAscending
                  FieldName = 'fullDescr'
                end>
              Properties.ListSource = dsExpenseType
              HeaderAlignmentHorz = taCenter
              Width = 350
            end
            object tvCMPColumn1: TcxGridDBColumn
              Caption = #933#960#949#961#954#945#964#951#947#959#961#943#945
              DataBinding.FieldName = 'idExpenseType'
              PropertiesClassName = 'TcxLookupComboBoxProperties'
              Properties.KeyFieldNames = 'idDetail'
              Properties.ListColumns = <
                item
                  FieldName = 'descrMaster'
                end>
              Properties.ListSource = dsExpenseType
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Options.Focusing = False
              Width = 168
            end
            object tvCMPColumn2: TcxGridDBColumn
              Caption = #933#960#959#954#945#964#951#947#959#961#943#945
              DataBinding.FieldName = 'idExpenseType'
              PropertiesClassName = 'TcxLookupComboBoxProperties'
              Properties.KeyFieldNames = 'idDetail'
              Properties.ListColumns = <
                item
                  FieldName = 'descrDetail'
                end>
              Properties.ListSource = dsExpenseType
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Options.Focusing = False
              Width = 250
            end
          end
          object lvlCMP: TcxGridLevel
            GridView = tvCMP
          end
        end
        object cxDBNavigator1: TcxDBNavigator
          Left = 0
          Top = 61
          Width = 1280
          Height = 25
          Buttons.CustomButtons = <>
          Buttons.Images = ImageList1
          Buttons.First.Visible = False
          Buttons.PriorPage.Visible = False
          Buttons.Prior.Visible = False
          Buttons.Next.Visible = False
          Buttons.NextPage.Visible = False
          Buttons.Last.Visible = False
          Buttons.Insert.Hint = #917#953#963#945#947#969#947#942' '#957#941#945#962' '#949#960#953#967#949#943#961#951#963#951#962
          Buttons.Insert.ImageIndex = 50
          Buttons.Append.Visible = False
          Buttons.Delete.Hint = #916#953#945#947#961#945#966#942' '#949#960#953#967#949#943#961#951#963#951#962
          Buttons.Delete.ImageIndex = 49
          Buttons.Edit.Hint = #924#949#964#945#946#959#955#942' '#949#960#953#967#949#943#961#951#963#951#962
          Buttons.Edit.ImageIndex = 64
          Buttons.Edit.Visible = True
          Buttons.Post.Hint = #913#960#959#952#942#954#949#965#963#951' '#960#961#959#963#952#942#954#951#962'/'#956#949#964#945#946#959#955#974#957
          Buttons.Post.ImageIndex = 0
          Buttons.Cancel.Hint = #913#954#973#961#969#963#951' '#960#961#959#963#952#942#954#951#962'/'#956#949#964#945#946#959#955#942#962
          Buttons.Cancel.ImageIndex = 39
          Buttons.Refresh.Visible = False
          Buttons.SaveBookmark.Visible = False
          Buttons.GotoBookmark.Visible = False
          Buttons.Filter.Visible = False
          DataSource = dsStore
          InfoPanel.Font.Charset = DEFAULT_CHARSET
          InfoPanel.Font.Color = clDefault
          InfoPanel.Font.Height = -11
          InfoPanel.Font.Name = 'Tahoma'
          InfoPanel.Font.Style = []
          InfoPanel.ParentFont = False
          Align = alTop
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
      end
      object cxTabSheet1: TcxTabSheet
        Caption = #932#973#960#959#953' '#949#958#972#948#969#957
        ImageIndex = 60
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 1283
          Height = 145
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object cxLabel3: TcxLabel
            Left = 24
            Top = 0
            Caption = 
              #917#948#974' '#956#960#959#961#949#943#964#949' '#957#945' '#960#961#959#963#952#941#963#949#964#949'/'#956#949#964#945#946#940#955#949#964#949'/'#945#966#945#953#961#941#963#949#964#949' '#964#953#962' '#948#953#954#941#962' '#963#945#962' '#954 +
              #945#964#951#947#959#961#943#949#962' '#954#945#953' '#965#960#959#954#945#964#951#947#959#961#943#949#962' '#949#958#972#948#969#957'.'
            Transparent = True
          end
          object cxLabel15: TcxLabel
            Left = 24
            Top = 16
            Caption = 
              #916#949#957' '#941#967#949#964#949' '#964#951' '#948#965#957#945#964#972#964#951#964#945' '#956#949#964#945#946#959#955#942#962' '#942' '#945#966#945#943#961#949#963#951#962' '#942#948#951' '#965#960#940#961#967#959#965#963#945#962' '#954#945#964 +
              #951#947#959#961#943#945#962'/'#965#960#959#954#945#964#951#947#959#961#943#945#962' '#949#958#972#948#969#957'.'
            Enabled = False
            Transparent = True
          end
          object cxLabel18: TcxLabel
            Left = 23
            Top = 39
            Caption = 
              #932#959' '#963#973#963#964#951#956#945' '#967#961#951#963#953#956#959#960#959#953#949#943' '#964#959' '#963#965#957#948#965#945#963#956#972' 2 '#967#961#969#956#940#964#969#957' '#947#953#945' '#957#945' '#963#945#962' '#946#959#951#952#942 +
              #963#949#953' '#963#964#953#962' '#949#957#941#961#947#949#953#941#962' '#963#945#962'.'
            Transparent = True
          end
          object cxLabel19: TcxLabel
            Left = 60
            Top = 55
            Caption = 
              '1) '#922#972#954#954#953#957#959' '#967#961#974#956#945' '#941#967#959#965#957' '#959#953' '#949#947#947#961#945#966#941#962' '#960#959#965' '#945#957#942#954#959#965#957' '#963#964#959' '#963#973#963#964#951#956#945' '#954#945#953' '#948 +
              #949' '#948#941#967#959#957#964#945#953' '#945#955#955#945#947#941#962' ('#945#957' '#949#960#953#967#949#953#961#942#963#949#964#949' '#945#955#955#945#947#942' '#948#949' '#952#945' '#947#943#957#949#953' '#964#943#960#959#964#945').'
            Transparent = True
          end
          object cxLabel20: TcxLabel
            Left = 60
            Top = 68
            Caption = '2) '#924#945#973#961#959' '#967#961#974#956#945' '#941#967#959#965#957' '#959#953' '#949#947#947#961#945#966#941#962' '#960#959#965' '#960#961#959#963#964#941#952#951#954#945#957' '#945#960#972' '#949#963#940#962'.'
            Transparent = True
          end
          object cxLabel22: TcxLabel
            Left = 23
            Top = 104
            Caption = 
              '"'#915#953#945' '#945#966#959#961#959#955#972#947#951#964#959'" '#948#951#955#974#957#949#953' '#972#964#953' '#959' '#964#973#960#959#962' '#949#958#972#948#959#965' '#960#941#961#957#949#953' '#956#941#961#959#962' '#963#964#945' '#941#958 +
              #959#948#945' '#960#959#965' '#948#953#954#945#953#959#955#959#947#959#973#957#964#945#953' '#947#953#945' '#964#959' '#945#966#959#961#959#955#972#947#951#964#972' '#956#945#962
            Style.TextStyle = [fsBold]
            Transparent = True
          end
          object cxLabel23: TcxLabel
            Left = 23
            Top = 120
            Caption = 
              #913#957' '#948#949#957' '#949#943#957#945#953' '#964#963#949#954#945#961#953#963#956#941#957#959' '#964#972#964#949' '#952#949#969#961#949#943#964#945#953' '#969#962' '#964#973#960#959#962' '#949#958#972#948#959#965' '#960#959#965' '#948#949' ' +
              #963#967#949#964#943#950#949#964#945#953' '#956#949' '#964#959' '#945#966#959#961#959#955#972#947#951#964#959' ('#960'.'#967'. '#916#917#922#927').'
            Style.TextStyle = [fsBold]
            Transparent = True
          end
        end
        object Panel7: TPanel
          Left = 378
          Top = 145
          Width = 400
          Height = 495
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object cxDBNavigator3: TcxDBNavigator
            Left = 0
            Top = 0
            Width = 400
            Height = 25
            Buttons.CustomButtons = <>
            Buttons.Images = ImageList1
            Buttons.First.Visible = False
            Buttons.PriorPage.Visible = False
            Buttons.Prior.Visible = False
            Buttons.Next.Visible = False
            Buttons.NextPage.Visible = False
            Buttons.Last.Visible = False
            Buttons.Insert.Hint = #917#953#963#945#947#969#947#942' '#957#941#959#965' '#967#949#953#961#953#963#964#942
            Buttons.Insert.ImageIndex = 50
            Buttons.Append.Visible = False
            Buttons.Delete.Hint = #916#953#945#947#961#945#966#942' '#967#949#953#961#953#963#964#942
            Buttons.Delete.ImageIndex = 49
            Buttons.Edit.Hint = #924#949#964#945#946#959#955#942' '#967#949#953#961#953#963#964#942
            Buttons.Edit.ImageIndex = 64
            Buttons.Edit.Visible = True
            Buttons.Post.Hint = #913#960#959#952#942#954#949#965#963#951' '#960#961#959#963#952#942#954#951#962'/'#956#949#964#945#946#959#955#974#957
            Buttons.Post.ImageIndex = 0
            Buttons.Cancel.Hint = #913#954#973#961#969#963#951' '#960#961#959#963#952#942#954#951#962'/'#956#949#964#945#946#959#955#942#962
            Buttons.Cancel.ImageIndex = 39
            Buttons.Refresh.Visible = False
            Buttons.SaveBookmark.Visible = False
            Buttons.GotoBookmark.Visible = False
            Buttons.Filter.Visible = False
            DataSource = dsExpenseDetailType
            InfoPanel.Font.Charset = DEFAULT_CHARSET
            InfoPanel.Font.Color = clDefault
            InfoPanel.Font.Height = -11
            InfoPanel.Font.Name = 'Tahoma'
            InfoPanel.Font.Style = []
            InfoPanel.ParentFont = False
            Align = alTop
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
          end
          object gridExpenseType: TcxGrid
            Left = 0
            Top = 25
            Width = 400
            Height = 470
            Align = alClient
            TabOrder = 1
            object tvExpenseType: TcxGridDBTableView
              Navigator.Buttons.CustomButtons = <>
              OnCustomDrawCell = tvExpenseTypeCustomDrawCell
              DataController.DataSource = dsExpenseDetailType
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsBehavior.ImmediateEditor = False
              OptionsBehavior.IncSearch = True
              OptionsView.GroupByBox = False
              OptionsView.Indicator = True
              object tvExpenseTypeidexpenseType: TcxGridDBColumn
                DataBinding.FieldName = 'idexpenseType'
                Visible = False
              end
              object tvExpenseTypedescr: TcxGridDBColumn
                Caption = #933#960#959#954#945#964#951#947#959#961#943#945' '#949#958#972#948#969#957
                DataBinding.FieldName = 'descr'
                HeaderAlignmentHorz = taCenter
                Width = 274
              end
              object tvExpenseTypeidExpenseMasterType: TcxGridDBColumn
                DataBinding.FieldName = 'idExpenseMasterType'
                Visible = False
              end
              object tvExpenseTypeincluded: TcxGridDBColumn
                Caption = #915#953#945' '#913#966#959#961#959#955#972#947#951#964#959
                DataBinding.FieldName = 'included'
                HeaderAlignmentHorz = taCenter
                Width = 94
              end
            end
            object lvlExpenseType: TcxGridLevel
              GridView = tvExpenseType
            end
          end
        end
        object Panel8: TPanel
          Left = 0
          Top = 145
          Width = 345
          Height = 495
          Align = alLeft
          BevelOuter = bvNone
          Caption = 'Panel7'
          TabOrder = 2
          object cxDBNavigator2: TcxDBNavigator
            Left = 0
            Top = 0
            Width = 345
            Height = 25
            Buttons.CustomButtons = <>
            Buttons.Images = ImageList1
            Buttons.First.Visible = False
            Buttons.PriorPage.Visible = False
            Buttons.Prior.Visible = False
            Buttons.Next.Visible = False
            Buttons.NextPage.Visible = False
            Buttons.Last.Visible = False
            Buttons.Insert.Hint = #917#953#963#945#947#969#947#942' '#957#941#959#965' '#967#949#953#961#953#963#964#942
            Buttons.Insert.ImageIndex = 50
            Buttons.Append.Visible = False
            Buttons.Delete.Hint = #916#953#945#947#961#945#966#942' '#967#949#953#961#953#963#964#942
            Buttons.Delete.ImageIndex = 49
            Buttons.Edit.Hint = #924#949#964#945#946#959#955#942' '#967#949#953#961#953#963#964#942
            Buttons.Edit.ImageIndex = 64
            Buttons.Edit.Visible = True
            Buttons.Post.Hint = #913#960#959#952#942#954#949#965#963#951' '#960#961#959#963#952#942#954#951#962'/'#956#949#964#945#946#959#955#974#957
            Buttons.Post.ImageIndex = 0
            Buttons.Cancel.Hint = #913#954#973#961#969#963#951' '#960#961#959#963#952#942#954#951#962'/'#956#949#964#945#946#959#955#942#962
            Buttons.Cancel.ImageIndex = 39
            Buttons.Refresh.Visible = False
            Buttons.SaveBookmark.Visible = False
            Buttons.GotoBookmark.Visible = False
            Buttons.Filter.Visible = False
            DataSource = dsExpenseMasterType
            InfoPanel.Font.Charset = DEFAULT_CHARSET
            InfoPanel.Font.Color = clDefault
            InfoPanel.Font.Height = -11
            InfoPanel.Font.Name = 'Tahoma'
            InfoPanel.Font.Style = []
            InfoPanel.ParentFont = False
            Align = alTop
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
          end
          object gridExpenseMasterType: TcxGrid
            Left = 0
            Top = 25
            Width = 345
            Height = 470
            Align = alClient
            TabOrder = 1
            object tvExpenseMasterType: TcxGridDBTableView
              Navigator.Buttons.CustomButtons = <>
              OnCustomDrawCell = tvExpenseMasterTypeCustomDrawCell
              DataController.DataSource = dsExpenseMasterType
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsBehavior.ImmediateEditor = False
              OptionsBehavior.IncSearch = True
              OptionsView.GroupByBox = False
              OptionsView.Indicator = True
              object tvExpenseMasterTypeidexpenseMasterType: TcxGridDBColumn
                DataBinding.FieldName = 'idexpenseMasterType'
                Visible = False
              end
              object tvExpenseMasterTypedescr: TcxGridDBColumn
                Caption = #914#945#963#953#954#942' '#954#945#964#951#947#959#961#943#945' '#949#958#972#948#969#957
                DataBinding.FieldName = 'descr'
                HeaderAlignmentHorz = taCenter
              end
            end
            object lvlExpenseMasterType: TcxGridLevel
              GridView = tvExpenseMasterType
            end
          end
        end
        object Panel9: TPanel
          Left = 345
          Top = 145
          Width = 33
          Height = 495
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 3
        end
      end
    end
  end
  object imgDirections: TcxImage
    Left = 46
    Top = 210
    AutoSize = True
    Picture.Data = {
      0B546478504E47496D61676589504E470D0A1A0A0000000D49484452000001F0
      0000015C08060000003772EC73000000017352474200AECE1CE9000000046741
      4D410000B18F0BFC6105000000206348524D00007A26000080840000FA000000
      80E8000075300000EA6000003A98000017709CBA513C0000912049444154785E
      EDBD099CA45575BF5FC68D24C410356A3031B8249818232EB8241A51D0686212
      7EA88989FF1850B3A85141718BDB60340A6EE01A37545C7101850184111C711C
      159B65D847B6615F0710646453FEE769FA4B0E877BDFAAEA7AABFB7DBBCF7CE6
      7EAAABEA7DEF7BB73ACF3DE79E7BEEE0965B6E19F83498FB77D04107CD1C7CF0
      C1B369E5CA95B3E990430EC9946D906320C74027C78089AEBB5BBA9BA5BB5ABA
      B3A55FB17427C9345EA3BCCBF7B797FFD91EFD6A8F3B0CE8B9C17E2781FBD043
      0F9D39ECB0C3660E3FFCF099238E386266D5AA5599B20D720CE418E8DC1830D9
      751F4BF7B4740F4BBF360773407E1BC41350FD0254F657737FD500FE2B68DC02
      F791471E39B37AF5EA99A38F3E7A66CD9A3599B20D720CE418E8DC1830503FCC
      D21F5A7A80A57B59DA7C0EE268E2B3FF120809F0A534066A00BF339A375AF751
      471D35FB433DE69863668E3DF6D899134E386166DDBA7599B20D720CE418E8CC
      18402E199F9F61E92F2C3DD2D203E7208E268E169E000FCBA54B0964CBB52E35
      80DF15ED1BCD1B781F77DC7133A79C72CACC19679C3173F6D967CF6CD8B02153
      B6418E811C039D1903C82523F47F5AFA674B7F3507713471CCE9AC8927C013E0
      4BCE025303F8DDD0BE319BA379036F7E20175D74D1CC65975D36B371E3C64CD9
      063906720C74660C20978CD07B5A7AED1CC4D1C431A7B3268E635B023C01BE6C
      007E771CD658F3C66C8EE60DBC01F7B5D75E3BB369D3A64CD9063906720C7466
      0C20978CD0FBCE411C4D1C733A6BE238B6E19D9E004F802F1B806F86B739E673
      D696D0BE99E1F223B9E1861B666EBEF9E64CD9063906720C74660C20978CD05F
      B3F4E1392DFC6FEDF51196EE6B69B304783AAF2DC575F29A09FD3680E3B0C69A
      37DA379A37F0B686C8946D906320C74067C60072C9207DB0A54F58FA2F4B3B59
      C299ED7712E009EFA5086FEA94004F21DC19219C13C39C18CF770C24C013D24B
      15D24DF54A8027C013E039067A3F0612E009F004382AF9ADFFD2849E42BDF742
      7DBEDA5CDED73F4B40023C019E004F8027B472E29263A0876320019E004F8027
      C05378F75078A7C6DC3F8DB9ED3E4B8027C013E009F00478023CC7400FC74002
      3C019E004F80A7F0EEA1F06E5B9BCBFCFAA7D127C013E009F004F8BC006E27B7
      AD5FBB76EDA929F8FB27F8B3CF96469F25C013E009F079007CB7DD76BB088049
      1002B2F819DFF1D9F6DB6F7FD5521298D475CB2DB7BCC1BCF671DFBF85BFBB58
      3FFA67DB6DB7BDA6ED49C6E5975F7E3CF9EEB7DF7E6776B1DE0B5926DA80B6A0
      4D16F2B9F9AC5B272009F00478027C1E00075CC0991FD1E9A79F7EE2E69B6F7E
      F3D65B6FBDC90B32FEE6F37DF6D9E79CA52470A827D0A6DED4AB6D40B6D5563B
      EDB4D315C0A5ADFC94CF1E7BEC711EF54F68DD32C358A09DDB6EE3CC6F340B41
      023C019E009F00E008718418A016D0247C788FB05F6AC288BAEEB2CB2E9776BD
      5EB4FD3426174CC8BCF5A5EBED30CDF23189CD89CC68B09D463F24C013E009F0
      09008EF601D02228009CCCCB32E5CA9CCEF5FEC78C9688991D41487E324D3331
      E01E5EF5995E65B6E43EFF59092C2A0BD79117664F3EA31C315FDE4BA32ADD47
      B9F5B9960C24C0019B4CEBBCFAA503FF1DCF95F582BFA396ECAD1BB576A41CB1
      BD7C5DF8DEE7E3AF95599DEFA3752496DBE7431F73AF6F6F2669B2C0A84EEA5B
      DA996BE913F5AB871D79952C04BE6F3406C8DB2F5BF8BA9207CFF0CFA25DF52C
      CAC7779AD0281FAED1B8237FAE51D94B75557E3C4F9AB7CAE12774C3FA8536D7
      D8A32CBA97329426C23C8BA4BE881ABFEA3D0D40763DCF0478023C013E4F804B
      1046C12D61C82B421301236821BC78EFD74F25C474ADD657B996EBA4F109AE08
      6B84B29E2301CEFDD10AA0E709DCCA8B573D8F6708000847F2A8DD8740D3B512
      DE9A34A89EE4AB7273BD4046DE5CAB49079FAB0ECA43B0F1DF919F00E8E14FBD
      79AFF24BD0AB0D3CC0753FF5F293A438E1D18441B0E255A057B979E53ECAC5F7
      022E7F93F84EF75326AE078871C25002B8DA9631E1FB9ECFD5563C97F7AA2F79
      7B80AB0D352EA9AF4CFE9A805066E5491F458073BD1F337EA2A536E233DF371A
      D34DFDA2B26969C98F136FD9E13A4D8A5537AED584D94F3412E07998C97284D8
      72AEF3C4B1D0052F844E5C0FE5BDD7AC0443AF754A8B9040E31A04968462C9C3
      DB03496BCF4DD74B4BA53CD296BDA540E592308C1A6BE93E951728C5C9027542
      E853760F31AE8D8E6E5ED3952648395426F28EED28F0521701D12F5168A22090
      F8FAF0B7D7129557047804ADF21424051E4DA228B32C287E72A37C3D187D9D35
      3188933F2DC968B2E3CBE721ADE76A9CC5EF043A59076469902544931E4D347C
      39BDE5406343D0F613478D255F178D0FDFD6B14DE59BA071526B37FF1B525FC6
      3ED62474B99AF153034F0D7C3982BC15804B8B897088A08DE65CAF45C86C1805
      2979349996255825784BD70FBB266A5DB1DCA5BCF5594960CA692F6AE6D4239A
      3DBDF6E9F3F49F37B56304566912A2FBBD06A7EB6AF7F33D9310B53D930F996F
      054DD54FF0F5E514AC4A00F7F5F4139568A68D160E7DAF7BBC29BB0670819449
      439C3CF96580A672FA72F9F68AE3C68FEF51FA456DA4761C05E071D945E34916
      8EAE9BBAA755BE0478023C013E4F13BA04B8B4136F4AF6C08A1AB8849CD60275
      ADB4A3A8D996E0246DD57B839784849E11354DAE054E252D47792334E37D12DE
      B18CD21CA34619355495D16BD7DEE4EBCDCC5A32880093E6164DD251D31358FD
      1AB0F292E6566A177D57D21C0146ACBBD7C0758FAC001A1B3CD75B5BA27541E5
      9215A654AE084E3FD92981D32F1B287F3E23C50998CFBB3449539B780DDC8F37
      B575C932E2DB5F93BCE8DC59B220F889879F2469C9A26912362D60762DDF0478
      023C013E21C0114AD274F81B30CAB14DF08E1A32D7738D77DAF1EB93E423B3AD
      07B8DFAA26E736049AD65EA3E0579E7256F279C62D405E48D6EE93D0D4FAA8CA
      26D326F592795966DB989760E01DC854173FA1A8B5A39CE3FCD63DEAAFB557C1
      89FAE85AF50F30F1CE79A509474D7B8D9AB79639BC395EF550FFFBE5014D66E4
      C055F290F77E0D5A07F66BCBBE7D4A00F7F551BB6B8228B8CA6F807622EF6816
      57193481D3A442CF6ED2C0A9A36F6B4D14693BEDCAE06FDA897CE56F21333CFD
      A5B124CB569C6CF97AA84C729C5B6EA6F40478023C013E21C0BD09103848E049
      E0CB24CB7B01CF7B727B0846CF70BF9628812C21EDBD79C9DB3B01458D4E6591
      50F4DAA0AE8D9A64AD2C0858EF11AD498537CD7A212DD0F87BA2495D10F4501B
      D68EF17BAD4F7B2D5FE000B6BE3E5AC6F0E671AF5DC961CD4F88FC9282DA9B3E
      D07ABDEACC6711E4D18A10EBEFFB2B7A9B0B52B17FF95CE5577F6A8C69A2A576
      95D5C03BF071AD9CE50449EAA87AFA6BB94E7D330CE05CE7EBE0FB45961A3F1E
      55073FD965DC5326F5596C2FE5AF32C5B5F1AE69CAD32A4F023C019E009F07C0
      47F9417A271D69C8C3EEAB69D2DCA7EFBC09579F95B439FF2C5F96616518F53E
      CA210F670F209EE5352140A38988B4D6588612147C9D05D2523BAA6E51FBA27C
      1EC0B1ADBCA7F3A86D526BEF5AB9E6AB11CEA7BF7C7D346992C9DA0350ED1297
      026439F17D11DBB0D64E257F855ABF90872C41A53EF363799C3EAA2D3D8DDAB7
      7DBC2E019E004F804F09E07D1408E396194D7558B01A6FEAADE55F03F8B8E5C9
      EBFF6F9B9F262FDE837C94F6994F5F7827B3519E91D7B413FC25019E004F8027
      C0E775988984F0302D3301DE8EB01E157ADAA6A5EB4B4E7C4D79C5ED85A33C77
      943E1E259FBC66BCB192004F8027C013E013013C85EE784237DB2BDBABAD3190
      004F8027C013E009F03C0F3CC7400FC740023C019E004F80A7F0EEA1F06E4B8B
      CB7CFA6B11488027C013E009F00478023CC7400FC740023C019E004F80A7F0EE
      A1F04ECDB9BF9A735B7D97004F8027C013E009F004788E811E8E810478023C01
      9E004FE1DD43E1DD961697F9F457934F8027C013E009F00478023CC7400FC740
      023C019E004F80A7F0EEA1F04ECDB9BF9A735B7D97004F8027C013E009F00478
      8E811E8E810478023C019E004FE1DD43E1DD961697F9F457934F8027C013E009
      F00478023CC7400FC740023C019E004F80A7F0EEA1F04ECDB9BF9A735B7D9700
      4F8027C013E009F004788E811E8E810478023C019E004FE1DD43E1DD961697F9
      F457934F8027C013E009F00478023CC7400FC740023C019E004F80A7F0EEA1F0
      4ECDB9BF9A735B7D97004F8027C013E009F004788E811E8E810478023C019E00
      4FE1DD43E1DD961697F9F457934F8027C013E009F00478023CC7400FC740023C
      019E009F10E0A79F7EFA89975F7EF9F1F3D164D6AE5D7B2A693EF74EEB9EC52A
      13EDB8DB6EBB5D44DA638F3DCEE3BDAF23EF57AE5CB93ED6DBDFB7CF3EFB9C33
      DFBE98567B2E957C6BEDBF54EAD7C77A24C013E009F079021CD06CBEF9E6370F
      06835B485B6EB9E50DFBEDB7DF99E30802EE279F71EE99E6B5C06F31CA041C78
      2E6D487B6CBBEDB6D7C489CDD65B6FBD89CF63FDB987F6DF7EFBEDAFE26F5EA7
      D946CB35EF6CDBEE99DA13E009F004F83C00BECB2EBB5C0A34800DA041330430
      7C362AC4D1168156973446CA84A05EE832D18EB45DD4BA054B3EAF7D4FBB77AD
      1D971AE419E3B471AD7F965A7DFB529F0478023C013E26C01166C00488FB1F3A
      D0037E24FF39D797CCC22593A4AE952999572607A5240D957C3039938699E34B
      F9707F349BFBB2698252BB577565E24279874D60C847D769A240D969D352F9B9
      8689452D6FF2930524DEAF3245533C7991AFEAE9272CA57EE119316F3EE3DADA
      F5D4897C4925D33F652AF5DDB0F6F6CF8BE5D2B3541FD5DF3F5FE5F66DA23E89
      4B10BA56E392EF23DCF88EBAFA31ABEBD4777E6CC632929FEFE3D856AAEFA8E3
      AF5607957BB19688A63129488027C013E06302BC495BD47712F60813C1050D86
      24C0E95AFDB0310F732DAF32CDA35DEEB4D34E57C84CEF5FF95CF0E27359004A
      429667A82C312F99ACBD795AD76AB2527A3E9F49584A0B561D785FD2E2551799
      CB79950057BD79551DA4F9715D296FBED7A48957AE1300A4B5979628B856667A
      D543FD10FB85CF31CB470B802C30F17AD551E5D2FB28C06B7DE7976562BB0B94
      7C4E7ED4C14F187996AC111A0F1A77EA5F955BE551F9FCF5D2B4FDB8D25205D7
      C7B6E25E7DCF2B935B2D8BF83C3421E533F553EC638D093D43E52A8D414DC474
      8DC61079E837C8677E59A5D4BFD380EB42E499004F8027C0E709F0D20F544259
      9A0B4247022F6AE85E90480315DCB516ED8525CF8BC237DE271836090F5F462F
      C43DC0056E09D928F47C39040D094CDD1BCBCEB310AC085B014290971097F017
      C02590759DCA2EC1CDF59A2C08D80298DA46F70A1E94C37FA73C7C5B442B8AF2
      96D545EF65191050553EF2273F0FC7D8274D7DD7D4DEFE3B3D4F1329EA2E6D57
      7E01AABF5E7DDF69A2E6274C7ECC0A8C9A04F9898CFAD94F90B89EE4DBD87F2F
      EDDB035C63A269D946E5541BFA3AC449B37E3B1ACFBCFAB15D1AFF0B01DB693C
      23019E004F80B70870002CE124C1E20593FFCC0BE2A849F1630716511B890097
      7688D024BF9AF6ED85474980490BF7D745487B21E8BFF3025FF773AD04B9CF53
      A0066C3C532097461C853879C7A50A39B3A9ADBDC95EBE09B403B0F165F065F6
      6D2090FAB294261F9AA8501F01451EF0027849BBAB39DF918F80E72717EA7B3F
      892801CBB7B59CFFFC3DF2B1505B97E017DB48E349F934C1AF34BEBDE5461398
      3829F36DAFBF872DBB344D686219D57EDE4A91004FD02D47D02DD53A637FBC5D
      B21F3BFF365BB56AD5CC9A356B66D6AD5B37B361C386998D1B37CE6CDAB46986
      99AE0460D37AAD0002884AC2DC0BB808F0E8615DBA3F029CE77808B405706998
      D2C0A3908C008F666A69CE51EB8850F1A6DA92A9BB346151594AFD407BE89EA6
      32972C25E41701549AF8001B0F65DF4FA53E2B01867C63DFF9B5DFA6B2C76778
      53BCEF7F392432267DDBFA362D95CD4F54460178D3E490BCE2E4B204F09A8F80
      F2A60EB5C958A90E4DBFADD4C013E64B156CCBA55E13015C708BC09576A9CF25
      44BC76E185A3173232B57A0DD49B23BDF6E43552AD3D8EE335EECDAECA97B278
      6DDF6B985C13B796790844C73D2D15C4ED5C2593ABAF576C4F3DD77FEE97164A
      F5F05AB9D710F5EC68DED5044216831A6C554E590ABC09B80470E5AB762C5923
      D4BFA5BE93D65AD29A4B9304AD3F7BE735CA58D26C23C0B9D73BE8F9ED624D00
      57DD4A1610CA210B52F45CF7006D1A137E62D034A1619CC55D08FA5D9047B406
      69921D27977D7C9F26F49C8C2C1768FB7A4E04706F424538C8B148CE42125812
      B432030BD2D286F43D82D0030681A8B5C62880C98B2481ABEB04589EA1F5D79A
      409296EAB7054918939F602E70FAF5DE1250044DEEE56FEEF380D33DDE214ECF
      E15A7962738F4CC1F221501B296F39CBA9FE7AAFE503ADA7F24C3D4FFBC3FD7E
      7139C5A96CDEC1AFC9935F7DE82727B204C8718B7A502E81427D16FB52ED44B9
      C8437D37ACBDD5261A475E03D767BE3E9483A4FE94B31FCF5119347EE2B86B02
      B8266ADCEB7D0134C1F40E8B7CC67B5EE3644AD7E9B7546AFF6889292D87D0E6
      FEB7236B84FA87BAA94FE2B24C1FE14D9913E009F004F8986BE0FAB14B58482B
      431095D6BB11B01284DE94C9E7081D091ADE6B4D92EB4BDA13F7FB7B781ECF95
      9681601A668EA4FC5C47B9FD3394379FFBBA08CABE6E3CCF9B6B750D65A30EB5
      32C4EBB4D58A32718F4CAE08684D44780ECF9366E805BCEA2F6D36F601F70AA6
      72A052197D7DF89B6794CCF85EB897D67D0121796ADD5C75543B525EC1C5E7C5
      7DA5BED378F19AAB6F6FE5E71DFD6873EF81CE7328AB6F37B5279F4BEBD6B894
      B93D8E3BC15FE58EE667D5414B0ABEFD4A6353E34D932A3F81E2D99A04FB76A2
      BE7CE7FB3DAEEBABCDF45B8CBF1D8D6D3F16FA0A6D5FEE0478023C013E4F800F
      13002553E7B07BF2FB5BF74603F0687AED42DBC8DAD085B268D2236B47D3B6B9
      5A79872D19C4FBA6BD7E0C984B0E845D69EFAE9523019E004F8027C03B759085
      B4BAAE094B0FCCAE942D02386AE1C3CA392EC0E3F6C261F98FFBFDA816A471F3
      5DAAD70F03F81A9BDD65CA36E8DA1898731A9FFF4B9CB58CE3853EAA30405B19
      66921D35AFBCAE1B71A8015897FA34024F939F51AD1798CE47D9B910971146CD
      3FC7ED74C76D023CE1DC35388F529E1AB9EDDE07587AF4D183C18B0F190C7EC3
      AEBB5348B7DEBA10004FE1355DE195ED9BEDBBDCC740023C013E0A303B74CD3D
      E700FD0283F4AB2D7DFC7B83C151964EB2CF6F21D9DFE71C38186C6F98FED5B9
      74777BBDABA53BCFC13C01BEDC055FD63FE1BF14C640023C01DE2138B35CF3AB
      7380DEC9E0FCD239401F64503ED63EBF5990AEBDAE1A0CBEBEDD60F00706EADF
      B1745F4BF7B2F49B967E6D0EE2BF320BF1D4C053802F05019E7558DEE338019E
      005F04806F63607EA6A57FB7B48FA5FDE7007DF53040D7BEB73CAE7DFF60F03A
      83F323E7D29FDAEB432D6D3507F27BD8EB6696EED22AC0590B1C65DBD6B405ED
      E51B7F76FCCA55A7ADE755CF3AFDCCCB4EE4335ED7FEF8DC53F9DB273EE35A5E
      F5B72F279F716F2CBBCF63DAF59A4FFEF409EBBA7AD5F631FAC9276D23F3DBB9
      D8DE34EE9AF07CCA38ED7B58ABEFC2B8EC5B3DFDD6B7B85592BA682CC5E03BFE
      3E8DA171822B0D6BA73876757D023C013E6580DFD9E0FA7C7BC68CA58BE70BE8
      A6FB0E32F3F99F0F062F3230EF38979E65AF4FB3F467961E6EE9F7E7347169E1
      ED69E0DAEB3BEC0738EDEF81EA9DEEFBFA5B789D153406F287FED97B37FDC683
      DE7233207EEC5F7EE81ABEF789CFB856DFE95E9595CF5FF5969517E9FD3E9FF8
      FE39E4E7F3B8FF23FEE78612FCA75DDFA6FC151044C1627CA013DC1F94043805
      36513092A500F018C96D31FB639ACF6EFBF7A7BDE80ACE148322F13E467DA37E
      DA32AA603594AB2D473F1FD887B1EBC767023C013E6580DB901BDCE9DB83C1A3
      560F069FB667DDD016C46D6270F33B0603F3551BEC6E09EDFBB5965E65E93F2C
      3DDFD25F597A9C254CEAF7B1F4EB96580B6F07E00A9DDAE64C7BBEC22E027C87
      E77CE22A402BB80263013B3E430007C6FE3B0F70E54FBED2F2F90CA093BCE63F
      DF3AB4719FB639F943659AF62E8F7A98461B655BA83CA85309320BF5FC857ACE
      347E7F00B2699781268777B04ACD9D493F8D3DEC3A3F4063DACB9B0478027C01
      00CEBA33E0FCB5FF1C0CFEF01B83C19E06DF8D9380FC9B83C1554F1A0CCC576D
      F0A9B9F4717BFD80A5FF9903F92EF6FAD796B6B5F4A0390D1CA7B6764CE83A85
      0A2D4FC7374A83F33F6E8481343D5ED9F6A37B745DDC9AA48350F89C3CA316C0
      67D13CEA01FEC2577EF552E08DC6ECB5E926800BE25EE3F600E7EF12A8F5DC3D
      DEF3EDD9E33363A2AC8A36A63DC7B481DA8AF6F149EDAA36F0266D6DCBF3022C
      6ED553FC6D85CFA43C4D009776334CF0F24CF25438CE18EED37F4F7DBCF615FB
      4B75547BE855FDED352C5D1BF353C093180DCFBFD7B88A13CC98A7DA5AF7F2BD
      22B0F11DE0501D627FA9AE7A551B29C29ECAE0DB4BCFD758A9FD2EFCF7A5FA96
      B6692A242DCF88CFF16353CF245F2DB7F0BD42EC964CE4BEFF4BCB1344796302
      C018F1F7AB0DA2464E7E3E3A5E297A9CA213D2BE0A47EBEB91004F804F13E0B3
      DAEEADDEDF7882B30E8D73D9D6F71E0C9EF8F6C160AFC307830BC705B9DD77FE
      E683C16ACBE7484BE6B736AB857FD5D2272DBDCB125AF83F5A7AAA254CE8F7B7
      84231B65B8D513DD7E04F33E8D8C1F223F74FD5811C012047C2641A878D41288
      BA5EB19F75C0450930E4AF53A4B82F86D58CC01148056F0F6299C931A9FBF56B
      AD6F0367EE7BF60B3F7745D4DA950F9F734D09D280BD3439503D79558C6D8581
      15B4143A93F70AC14A7DF5B98422024CDAB587548C76C73524DA4BD050FB222C
      FD5AE26D4B0336C9D033152FDCD71361ACF2280C29D7FBB3DB15935D076B70BD
      9EEF43B4AA2C0AE9E9F3A5CC4DB1C7299362972BB639E5505CEFD816DEF4EAEB
      E3DB91BAF9F8F21ADB3A235D07D5C834ACD0AC3C5765F5B1F5F5B95ED576BE5D
      FDE44AB1C90568EEF307BFA84E0A15DC545FB58DC69ADEC7312B4D96BC14BA57
      7D29EB85CAAF7BD546BEDDA5A9FB36547DB85E1057D94B935B9EC333636859AE
      E53B8D7BF2A2DFE2C422019E005F2080A3FDDED3D25666CB7EDC5B0683FFFEBA
      DBFA350AC40F1B0C6E7ABAC1DBF238CDD2A996D659FAA12520FE654B68E0FF65
      69674BCFB0F4289E67094F74D6BFD1BEB1064CB68D4C42AE646A532C667E805A
      9F2B69791262D26CF443D68F5CC251F76AA62E6119B572015CEBD351232EAD81
      0BCE32AF03740F63AF81936F9C143469F782846FA35294B0082CD51701A8498B
      3F954D6089EDD464B6ADAD81738FCCB03A50A4A4596922A2F2D007FE0436096D
      015BE343932C0F707F4A969E4FDFFBB8E61E767E7227F0F8098C60256DF3D6A1
      FD7F96104120B68F40180F87F17D2430C53C4A2182B986C988DACF9FF8A5EBD5
      3EFE9432EEF39351FD2EA8ABC6BA3F78A456DFD8369A3CFBF3D1E3842DF6B507
      B1B79C292F7FBDCA21E735EAE1EBA7368BBF57C5F557593451D733FC4422CA84
      D2184F8027C01708E09B7D703078BC399C7DE2BB83C14F4701B6BFC6C87C8381
      FF0A1BD3975ABAC8D2D9964EB264D6F881CD05061F76F0FECB39783FD05E4DD9
      BF6DEDFBD62D649302DC9B652380F443940043B0D64CB8FA9197BEF7F97A8079
      ADFE7626C1392736B468D6A901B1F7201FB6062E0D1AF0CBFC3E2AC0593B8F1A
      78E9D08F61F5547D689778C88AC024014FBBD6263351D03599D0A5F934F93194
      C28D7A8895BEF79F79809780AAC355A461D7005E02A7AF5B0DACA58926E550F2
      E66D1D4AA236541BFB3C6ACFF1A01550A535022A815A13DB52BFF8E795BEAFD5
      B75426FAB66451A16EB294F0BDCAED7FB37E0C6902E73FF33E16BE6E5C23AB03
      6D19EB10CBE9C789CF47939CA68929DF25C013E0D30438D1D0560F062F34CAFE
      B0066D83FA35B5EFBE6372C66CE13799AC6179E95A4B1B2DA1859F6CC9E60283
      FD2DD90EB2C1AB2D3DCFD276961E6609B3F91696FE6FEBD82CB9E7FED9E09FB7
      099D1F8ECCC232CBFA1F9ACCBE12589302DC43BB048BD90985F342C7A10C8063
      326FD2926BDF711F50E6555AB7DE4781C224A1A49D4F0A70AFED78A1E72D12A5
      6794045E13C06BEDE9F35908800B344D26F4B601AE6520AF01FBB3C005A338E9
      1865A210DB5C9ABDC6B2B6B99177D484E3C436FA3CE81E5F8E52996A7D2BAB8B
      7C31FC528737DF6B0CA8EC3580539E783CA92686C300EE276FDEFA21CD3C019E
      809E26A01BF27EAC7DF7314BD796E06C5EE997ED3518ACDCCACCDE1F332097AE
      F9BCFD2E1E3C18FCC27E1F375ABACE121A38CB95C759FA96A54F5BB225F159AF
      F3BFB584C7F9432CE1716ECBE483BB59FA3FADBB2D807B335FE907A619BB770A
      E2475E3315CADC2CB378CD6C2EB35DC9CC1BBDD0F7FBCA71677AB08EAA81531F
      3CD7658A17C0798D8E714C14649A8FFBC5A5A17853BFDAC50B64ADA9FACFA250
      2F692D32FD96B4CB7135F012444A1A5834A14BD8530704AEAC0671C9C46B79DE
      F4EE2D396A27B587E0A9755C399251D69A095DE35240F2B08C6D2248AA6D3536
      A599D21FDE84EEC7AEEEF17E1911F2115C1A0F7E82D0B4B4C4F5CAC33B85F9E5
      0C3F8153DDF5DBD0772513BAFA203A22EA9EB88D30E6ED3578FD4D9E1AC3D2C0
      556E015E4B2FEAC3F83B577D359EA2D39BC684FF3C35F0047C8B80BFA769DA2F
      B7FC4E2C01D982B5DC68DEE347BCF2D675EA7FB6B4BB79987DCAEEF965BC7E57
      DB22661E6780FB7A4BD758BAC4D27A4B6B2D1D60E943965E3F97CF0EF6FA084B
      0FB0F45B96E46D7EEB7A77E9DF241AB8778291990E01CE0F4C3F4ADE7BE1E19D
      75F4B737EF790F56ED4BF650F38E39452D33EC03E71ABF954C5EE402B9077A09
      EE256738C11ACD9CEFD1CA23D4A3E6AA898BCA1F812B81EC35986100179846DD
      2AE51DC7A495F12AEF63B53DAF942F7A036B4D9BE7F1BDAE172C0459D555FD27
      41ABEF0186C68E77628B0E4FE423212E2737DA8FB6D5B3B59B4170D0D8D3F5D2
      E66A26640F5C814BD624BE93AF017F4B53577D0434AFADFAFCFCC4C44F369597
      0750D3EF42FDA6DF92DA516345E340A0F40E91AA03AFD15F44634EFD453DF88C
      DF9B9EA1FEE119D1494DD708F4DE8F82B654FF6B82E09D56E5E5AE3AF1EADB83
      7B635BFADF944CEB920D09F004780B007FAAE5F1054BC53DDE470D0667EE3B18
      BCCF42A3FD8D8D4D3465E0FD724B6F312DFC040FEF95066E8BBEF253FB0E6863
      32BFD2D279964EB484D7B929E6833DE7EEDFC95E2D86CB606B4BF7B38497FBFF
      799ADB9BEABF49002E01E5CDE8FEC7CA8F50C2C7CFE6F91B61538AF6C50F521E
      AEBCC6D9776D2B8A7EDC68C068C95E134643E633B471B694F1774CDCCF777ECB
      D9EC4C7FEEDE18DC85EB5867D7FA7829529B1738DA7A435B4530CE6AFB063505
      5CD17D08415FFFA881D35625D0D6CC8D72F0E21E9F3C5CE81B0947C1D2E7471E
      B5FED1A480FCB4CDCC979F7A738D3EE33ADA83CFB4DD4ACF92B64E39B9465EF5
      821675E71EAE2BB529D7D7F2F6F5217F5F7FBFA549F5D0AE00F2E4795EC3E419
      512BF7F995962DBCB9D897A5F6BB501EEABFF8BB9103992685BE8FD4AEDC531A
      777E5CFA6D64948BEB696F6F2D89ED1EAD604DE343E35593049EA1ED6ED10AC0
      B3A94FCD2783EBBD4C498027C0E709F0FB99E6FC26BBF7EC92B66DDF5DF70DD3
      944DDBFE57832851D180ED0B2CBDD4D2EEC0FBB70DEAB6C67DA3EE372FB48DE6
      2E8E87F9B9962EB674A1A5332CFDC8D2C196CCDA3E78B3A5175A9297398E6AF2
      3267AF79D9641E493E29C04BB08826316F0EADC1253F1F1ECBBAB4BE39AD7643
      684713EAB0678DB28E3E2C0F0FF0D20462D4FBBB72DDA80E8B4DE56DF25D5888
      7A6A82B510CF9AEF3312E009F071006E60FE3BBBFE9B35A7335BDB3EF1A383C1
      3BB6B258E7C64C429AB21FFBC5968CE5B326EFB75AFA6F4BEF788369D4E4B3DA
      9CD46C11FB07F69959D8678F9F677BD829968EB7645FCF6E117B9F25F677FF83
      A5275B92A31AFBBB7154FBBF93C622AC4BEFA70170EF84E2CD88E30261BE3FE6
      A57ADF42023C6AC4A3B469DB00AF99BD47294B57AE590A008F9A7957DAD69723
      019E001F01E00FB46BDE69A918CBDCA07EF5D7CCB46DEA351A365BB89E6DE95F
      2CBDCCD26B2CBDC9D2DB2CE17006BC5798AAFC26F342BBCA3CD02F7CCCADFBB7
      DF63E93396D0B4319593ECEBD9486BDCFBEF9630C13FD692F9B60D4C811FE2A8
      D604F269001CA115CD7500219AC9BA2808BA5C266DC5EB6A1965E66EA37C6DE6
      D54679E69B0726607E0FDE143C6EDD4A79CCB73C4BF5BE047802BC02F0BB1998
      9FCF59DB356DDBA2A8FDF0BD66D236AF318BAF32BBB6FD4F96FECDD2AE964CC1
      1EAC980336E006C298BF01FACB2CA4EA87CC0BED207315C71C0E9CD1CE2DBB59
      607FCE922D9BCF6ADDC437FFFF2C71BE37278CFD9E25EFA876EBBEEE71FF4D03
      E04B554864BD869BF9B38DB28D16630C24C013E001E00FB7F71FB4548C556E1B
      AF2FFF92056331DBF8738D99AC43F34ADC7163F22C6C81B4D7B6319903674CE8
      AC876352FF9B3FB9F5E4B0ED2CE141FEFF2C719A180792CC9AD72DBDD112DBC3
      F88E53C5FED0128E6ABF61693447B526A827C053E02E86C0CD67E6B86B730C24
      C013E006EBCD4DDBFE777BFD5145DBBED9C2981E6564C5F90C4DF8EF2C71DA17
      9A33EBD26C0B5B61094D9BB48725CCE65C8FD31AA6759CD8D0D481315BBE38AB
      9BB48DA5BFB0B4A3A59DE7F264CD9CA02C38BF71BEF75696C67754B39BAAFF12
      E02948DB14A499578EA7C5180309F0650DF02718B039E2B3186CC53CC4CFFFAC
      AD4FDB3E2DE0CAD19C7F6F0993375BC0D0962DA47951DB7E857D8E460D843911
      0CA7B347CF019BBDDAF7B574EFB957BCC8013AD7F00CAE07DC4FB0F44796B6B4
      74BB8348B0184CFC2F019E027731046E3E33C75D9B632001BEEC004EB095571B
      044F2969DBB6E6FD73F31C3BC46CDE98C465DE66DF36E66CC29562DA46C3F6DA
      369FF11DD7702D666FEE7DBC254E03C3E90C10DFD31211D2F01A27D80AFBB681
      39DFB3BE0DE41F65090FF3AD2CE1A8A633BC67B787C9DC9F002F1CDDD9A660C8
      BC12343906BA3F0612E0CB06E04F33F87DD5522DD8CA7A3B507BCFB9602B68C1
      68CF68D168D37EFB971CD2D0BED1C2D1C6D1CAD1CED1A0ED98EE5908135C0587
      33200CA80136FBB4D9EE058CEF32F7D916F60AC4B9F6F72DFDEEDC7B3EBFEDFC
      EEE86897004F80DFE1ECF1044EF781937DD46E1F25C09734C0EF6FDAF65B0D7E
      E796B46DFBEE9A030DEAB648CD9A33EBD331D80AEBD85EDB5E61EF59EFDECD12
      EBDFAC83B31ECEBA3871C8CD376D80499C33BFF114477BC6E10C58DFA641DBDF
      3A239C58E5401AC734804DE26F8EFEE4BBD9BDDD11DE69424F7827BC730CE418
      C8D3C866A3862CB1746703F34E56A7951587B45B2CD8CAF1B6F17A8FAD6E0DB6
      C2DE6A3CC3F110DFD512DBBF146C45DA369EE578986356C7E35C1EE84FB4BFB7
      B18487389A33EBDA5EDBBE9DD9DBB7B383385A39B006F4BCDEA6A537F58B5D37
      D9BF5C036F571348CD2ADB33C7C0C28F81D4C0970CC0B736E0116CE5B28AB6BD
      F1AB83C17EA632A335136CE5399676B6A4602B71FBD78A39980375F676B3C79B
      BDDE4FB1B4ADA53FB6B49525B6766D61096D5B5A7315DC6E0D1B2D5C89EB67EF
      511A36A99A8CDE7677027CE1854D0AF86CF31C03ED8E810478AF01FEABA66DBF
      C060B7BAA66D5BB4B3B57660C8EB5DB015000E9031839782AD68FB1760279A1A
      51D5003E8786E02DFE0796386B9B6D5D98BB7148BB2D06F930F0B6F57D023C4D
      A86942CD31B0ECC74002BC97007FB48190602B5797C06DC1562EF9821DFC618B
      DA68D998C9C709B6C27A382675E2986F67E93196D8CE858319CE665B58BADD1A
      355A735B601E359F04780AEF652FBC539B6D579BED637B26C07B03F0DF346DFB
      A506B8998AB67DF32183C1B7CDE38CC02A3895ED688910A46CEDAA055BD1F6AF
      97D8350AB6A23DD86CEB2A6DFFBA9D43DAA8C06DFBBA0478023C019E6360D98F
      810478E701FE64831FC1563695C06DC156CE26D88A2D4AB33ECD362E4EEBD2F6
      2F055B213C290E69BC6AFB9782AD703DDBC68886E683ADDCC7DE1340052FF146
      8FF0B6E13C4A7E09F014DECB5E78F75163CC32B76B3548807712E0BF6DDAF66B
      0D64A797A03D176CE5203B158430A568CC044E21800A9A7453B0153471347234
      7334F41D2C116C85ED5F0FB2C4F62F055BB9C3F6AF51C0BA50D724C013E009F0
      1C03CB7E0C24C0BB037083F6B30C80045BB9B9046EDBFE75B2055BD9CB0288A3
      69B3464DB015D6ACE359DB68DB6F9BD3B6D9FEE583ADB0264EB015628C37055B
      59F075ED71E09F004FE1BDEC85776AB3ED6AB37D6CCF04F8A203FCFE06AE3D2C
      5D508236C1560E180CF6B7A829ECBF26D80A5EE17887A37DEF6E69D4602B4FB5
      6B394B9B30A5045B61FB5735D8CA38305D8C6B13E009F004788E81653F0612E0
      8B0270CEDA7EAE99C2BF557148BBE588C1E01882AD984D9B233B09B6C23EEC61
      C156386B9B602B3B5B7A8E25EE55B015B67F29D8CAA26DFF6A0BF609F014DECB
      5E78F75163CC32B76B3548802F28C009B6F25E4BD5602BFB0F069FB13D5FC01A
      F80261605C0BB642C434F672EF3A0777EE03F631D84A69FBD7D0602B6DC1761A
      F924C013E009F01C03CB7E0C24C0A70E70CEDADEC520B6B6A66DDB59DBDF25D8
      8A4544C1A9CC9FB54DB09561676D2BD88A3F6BFB21765F0CB67217FBACD7D0F6
      138104780AEF652FBC539B6D579BED637B26C0A7067082AD7CCC5235D8CAE707
      838F9AAACC01223896F9B3B6713CAB9DB58DC31A8E6B38B09582AD68FBD7A207
      5B9986E6AD3C13E009F004788E81653F0612E0AD029CB3B65F6E9039B1A46DDB
      9AF78D166CE5702333266FB4ED1D2DF9602B4D676D2BD80A5BC6146CA574D676
      A7B77FB505F504780AEF652FBCFBA8316699DBB51A24C05B0138C156BE60A91A
      6C65DFC1E07D73C156089A329F602B6C1D23D80A676DDB4EB2C1032C71D6B682
      ADE814AF4E6FFF4A80277813BC3906720CB4340612E0F306F8FD4CDB7E8301E9
      ECCAF6AFEBBE39187CC316B1099C82C6ACB3B6156CA5B6FD2B065BF1676D2BD8
      4A6FB77F25C05BFAE1A626D3AE2693ED99EDD9C73190001F0FE073C156BE6920
      AA065BF9E860F00E8B9082A6CD1A350783B0663D2CD88A3F6BDB075BE1ACEDDF
      9BD3B6FD59DB77B6CF9685B65D827E9AD07322905A5C8E81653F0612E02301FC
      810691B75BBAB8A26D5FFDB5C1E04B2FBC75CB17476F2AD88AB67FA16D1319ED
      ED736985BDE25D8E9739477B72C427B1CC9754B095B6B4ED04780AEA652FA8FB
      A81D6699A76FD5488057014EB095E799E3D9AADAF62F82ADBCCF22A1CD055B01
      C0ECC306C8BB5A2A9DB5FD66FB9C602B801DD83FC712FBBD396B7B1B4B4B2AD8
      4A023CC19BE0CD319063608A6320017E07803FDCC0B3B7A58D2570DB59DB977F
      7930D877C75BB77101DF51CEDAF6C15630A98F1A6CE54E76EDB23593374D00AC
      5D26FB67DAC1C0A7B9DC365BB56AD5CC9A356B66D6AD5B37B361C386998D1B37
      CE6CDAB469861F4A6A14D3D728B28DB38D730C8C3E0612E0B30027D8CA8BEDB5
      31D88A9DC5B9BB055BC1216D3EC156706253B015CEDA7E88A5251D6C2535F029
      CEBC53C88D2EE4B2ADB2AD96EA1858E6007F8241E69396AE2D69DB76D6F6F99F
      B7602B66DF06D86CE3F2C1564A676D13DAF4F59662B09527DB673A6BFBF7EDEF
      65116C25019E004FAB458E811C03531C03CB10E0045B79B5C1A51A6C65E56070
      98455479858176074B3A6B9BED604D676DF31D5BC438973B83AD582324C0A7F8
      C35DAA1A45D62BB5E51C03A38F816504F0A71950BE6CE98692B67DD460B09E60
      2B66DB667D3A065B41AB46BB362BFAAC2739AF843A450BF7676DFB602B9CB5AD
      602BB9FDAB65A04FB6006E77E71AF8E84222056AB6558E816E8E81250E7082AD
      BCD5805D0DB6F28DC1E00053B5F11C677D5AC156869DB54DB0153BA27B360C2A
      E6F50CB6D232A08769EF09F0D4EED3349B6360D98F812508F03B1BB4FFCE00B0
      B2B6FDEBDB83C1F1045BD9EAD64344D0B8F10C1F76D636078CC4602B9CB5FD48
      4B045BD159DBA96D2F00CC13E029BC97BDF04EADB89B5AF142F6CB1202F8430C
      D8EFB4D4146CE5F32F180CECFF6CB015F660EF6CA974D63666F21596D8FEA560
      2BECF18EC1566C0E30B89FA52D2CFDBAA5BB592242DA9239B6739826BC58DF27
      C013E009F01C03CB7E0CF41CE0BF6ADAF6F30D22AB6BDAF6B76C6BD87B078337
      5BF0704CE43ED84AE9AC6DA2A59582AD007C82AD3CC212C156E2F62F0E12596E
      D0A6EDDF4A3C784BFFBEDA62BE9BD7FE0E66DDD896F441DB9A37D726B3ED426A
      13F609F014DECB5E782FA4A697CFEAA6B6DF53806F6330F8A0A56AB0952FDAF6
      300B444E9015CCE4F30DB6B29DDDFB184B7F6489ED5FF79DD3B697F459DBA382
      D60E6BB9B745AA3BB03679D2E706F875968E2319E43F65B0FFA4A58F18E85F4A
      3207C29DED98D5C71D3E183CD65E71FEBBCD8A51037F023C019E00CF31B0ECC7
      408F00FE9B687A0685990A306E3E6C30386A8FC10007339CCA702EC3C90C6733
      3E23F6F80A4B98C74976E98018E5BB5BC2690DD37A29D8CA96F6F9BD2CA1515A
      1C97C15D2C2D376DFB0E5BC200EB1C68EF66EDFEEF06F29F0D03F9B8DF5B9E97
      0BFC1601EF30E06FE9DD9FBAD5BB1FAB077DE1613F3AD7DBF042BFFCCAEB8EDF
      E3A3DF3B6FB7BD565DB4F2BB67ACF75ACADA132E3895CF62E21E52E93BEE218F
      A6EF4A9AD0E9E75C7122F790AFFFBE940F9FE939BA96F7D481BA9057D333E277
      E417EFE1BD9E516B07AEA9B583DA52DFF3CCA6F6AC6987B5B2C5BED2737CDF34
      D5B3D6D6BE5DFDD8889F97CAA57E57DBC53EF26DE1CBA6BE9FCF381B259F61FD
      58EBAB5AF9A947D398505F6B4C95AED538F7FD167F7B4DE34B79D77E1F7E1CF0
      77531B2FB665A207007FA209FE4F5BAA065BF9EC60F001B36FEF68D27B9C602B
      ECF37E912542A2B26DCC075B0110C5602BA36AA74BFDBA3980035026355BD826
      F8C79B367DDCB8901EF5FAD583C199FB0F06AF7DC2AD96907B5BE21C74FC0EEE
      EE275523137C52802358367FECBB6F1EFCC93B6E51DAF2A9FBDC20E1B2EDF3F6
      BDC67FA7BF25344ADF710F02A1E9BB92C0D869D7AF5DC13DFB7CFE9873FCF7A5
      7CF84CCFE15AC01DAFE3B3F89CED5FF485ABB86EBF834E3A333E235ECF7B3DA3
      D60E9AF4D4CA28A0F13D7F37B567A94DE81FEEDDFA59FFBBC97FAFFA7A88F337
      D7FABE8979F2BDEAA96B75CD2E6F3AF852DFFEA5B1413934361827BE0FC887C9
      93F2E53BC6969F18A98CB15CA5FE8B75896D1C9F5D1B077EACD4DA5FFDE3CB57
      6A63DA883AD1064D63427DADB62E5DABB6F4FDE6DB857B9AC697EFC761BF519E
      D1D4C609F062C08FDF9E0BB6727A49C09B66F6F383068343EC54103467429B2A
      D88ACEDAB6382CB31AB6D7B6F98C602B046451B0951DECEFC75B7AB8A5075BB2
      734906F7B484B67D3B302C75208F5BBF3980A30503D1DFB6F4206BB4C77CDCB4
      64EB9F5F8C0AE661D79949FD04734AE00098C7597A94A587CDF595FC10BCF320
      5681D1FE4D0A70840802891FB73401DE4B384A2047E04530950400797B813F4C
      48F05CA00064C781AB04206596562918C572F30C129385719ED154F69A00D63D
      1E0A4DED597A86EA8180F6209430F6109B04E04C9A7806CF5339E80B922654B4
      A56F3BDDA33666FCF8EF052DDFD63580934769F236CA388B139B12D8758DA058
      9A0C959E45FD357952D9D51EE435EAB3FC736927B51DE36118C0E977DA35FE2E
      54873811F375F379AB8D79E6B0DFE2427FDF250DDCA0FD4C13E85FB5543C6B9B
      602B06883DE782AD70D636DAF38B2D95CEDA6E0AB6F2A43918B0DE9A676D5B23
      CC13E068E000F43EB616F1FFEC64B6B77F7230F8A6390E5E3A0CCC4DDF3301B0
      A03AC79839E55D9637DBFB58DE208C2D7DBE9DA5C75862EB1ECB1B5B58D2D2C6
      C200BC2638A2604670444D6A14C12A0D2EC2B2241C541601CE9BD1BDD6A87BBD
      764CF910B4315F84A6D75CBD00A33E8B01709E596BCF52BB502FEA4A79BDE0D5
      671E7AF305B8B47CDF4F6AABB8ACA0FE916999F655DBAB4CBA471AB89F7CD400
      4EDD793EF58C66EBA67B629BF971516ACF7101EEA14D5D3DB0E70B703F99A0CE
      C300EE7F6B25F88E0A70F2295915161AD6A5E77500E0F79F0BB6724149A8DB77
      D71C6850B785EC5D4C502BD8CABFD8DFC382ADE065CEFA37676DC7602B0FB4CF
      D8FE65CEE97734C38E0BB36576FD03D9676FE94D96BE64E9E44960EDEF3513FC
      8D16DEEE54B3917FD5FAC5E602830F5BB24D04B367A9A38513700787442C2768
      E30FB284399D49847602D89F23FCB31FC3BC4F23D38FB9F483F2D004A6256D7A
      14C13AEAAC1FCD4FDA7AD4C486015C1684923097695480E01900269AD1792F61
      2A1334EF9B34ACA861C735E99206CE67BE3D9B84A7CA09D0288B9F8C0854944F
      002D013CAE8F4613BAB4EA682921FFD2A448651248FCC42BF60365A35FBDA9BD
      69CC4C32CE4A13BB36004E1EDEFCED271793009CBAD2175E03D7DFEA33FAC42F
      EBD4E03B0EC0551F2D037401DE946191004EB095E79AA6F5AD9AF027D8CA07CC
      0CBED5ADEBDAF30DB6C2719F045BD9C6522DD8CAB27748AB4C4036B7CF9F80E3
      A0A58F5B5FFDA0E6873029C0CD9BFD469B915D6026F863AD9FD65A5A6DC914F9
      C10196CCC56160BBD26621BEAB2526646CEBE3701826623818B22B006BC0C268
      E04DDA8A34BF0821AF0148180F5B7BD6BA73744EF3C2C33F0FC1E5CD85C3005E
      FA9EBCA316E9B5589EE135CED21AA25F3BADD5D56B47B535DA12B43CF86A4294
      B6164465159176ABBE93F61C4DB1B5F24680CBFF21C2BA69F212DB5B80F3BE13
      0205E554D905263FA98A75577D3CB8461D675EC3AC8DC9713570F25499E2646E
      983F837F96604FBD646990656294BE523B798B873EAB8D5D3F36FDE45293879A
      497E31A0BEC000DFDA84FD5E962EAB68DB1B4DF5DACF6CA5444603BECFB1B4B3
      A552B0154CE42B2CE9AC6DB433055B798AFDBDADA53FB6B495256DFFCA602BD6
      180560FFB141FA79F6F9DB0DD407593A675C281F31186CF8D260F0FD770E0607
      EF3E18ACB2FC7E392C0FF326BFC9487C8D15E9124B17583ADBD2A996662CADB6
      64AE0E83FD2CED6D891D035854F077600984BE651D1C67364CE878A32F1CC0A3
      739417145E88CAECEB211C353E09686F76D5FA9D5F5BAD096D049BC08450D2B3
      E60B700F4EFD2D0D0AE07933FA300D9C7A50B698BC908C1A94F7C68FD02AB567
      6C17096B9EA9B5704DA0FCE44B40D084C583729806AE494D6C639E57B33EC46B
      05B8E87CE80126F3F330AB8DEA52727C8B7529ED34A05D7856EDDAF9005CCE95
      7E29401394D2B3BC03A877629325423097EF809FCCF972470DBC66CDF29ABCBF
      BF067069F271A96231C0AD672E00C009F8B18B09F3EFD5043AC156F6B26338E7
      82AD0C3B6B1B4D0C618E4915B0634E7FB6251F6CE52173C21DEDEC37E6043C1A
      DA72D7B6EF67DBB19E8E83A0A5CF18A88F1D06D9F8BD51F50A732CFB91EDB3FF
      CA7B6C4B97A9C32F376FBF9DAC6D993CFDBBD9B1DF68DEE21735E5FB7683BB79
      0DDE6CD75F6FE96796AEB474B1A5B32C9D688939C64A4BC0FBFD9670406482C6
      73FEC2124E8778A3D3BFDE7CBE300097661435636FDEE30716D7C46BA6E19200
      10849AB46FEFA8E5B509016118C0A336EDCDA902A74CB9FA4ED091101DF68C26
      E1565BC3ACB5D3288E6C325547ED4A132E0F705D2B8DB049D38D1AB8DA5890F2
      1A7EF413A03EA5E5073E2F99713D2CD5467A4EA93D6B801A067D9FD7B4D6C035
      E16B730D3C8E8FB804131DDFBC73A9AFF3382674B5A59F9C2F26B81700E08F36
      29FC114B579784B981E4922F0C061F335519A1EC83AD70BA1771C739ED0B589B
      BC9F3DFD2B9EB58D96EE9D9A7CB015B4B2E51C6CE5CED6E68F3648BFD8D23E06
      EAA36A568F1A6871245B65DAB059440EFA9099B0CD4B7077731EA4CD77B4C43A
      34A6EC9DE7C04A8CF857015AF3182F46C5B325915FDA16809B2D603CE0BEC9D2
      CF2DA17D5F66E95C4B68DE3FB464F15C06A6CCCFAE7FB39380F1C0E4008B0C3B
      06E8671C0FB5375FE16B4787B7DD3CD1696412C6513B1650F9BEC9037698601D
      D561A604606F461F0657394F798D82727B8F76FE8EF5E43399D1873DA32D80AB
      3D8739F609F2FEB9FA4CD6000F13B5B58479AD6F22C0050D954B796A6C44412F
      0D394EC886015C1AAB2624B13D271967D304B85FBF8F13D949D6C07D996B1340
      0FF026C7C751014E9F79AFFA2E807B4A0027D8CACB0D0AC757C070B3696FDF5E
      71EB76AE1DE66040B015B676CD02604E68036D9282AD703D5BC4146C85AD63B6
      2578604C99DDFE8537F23D2D2DC7602B0FB136DFC9124E65FB1B784F1D57ABB6
      E028171C3C181C6D8BCD9FB599D2DBCC468DB68BDF01960D3CFD69F717CDF501
      FBE7E90F8E52A5BF986859970EDEB69D6D21B332DC6E0781F5F78D3B9B866D33
      2AB46CB4EDEB2C5D6D09B3F939964EB2F47D4B76E9E07396F6B644385BFAFB1F
      2CE1B8F8584B809BFDF9F7B67407AB8A7D36DE3FFB01CCDB898D1F8F60AD3568
      5E1108829DD6AF79EFCDC75ECBAB9996C94766407F8D171C358D4EE592352082
      C46B5ADABE2433B8B43C79344BDBA62EBE1CDA42270DB2E919A53AF2996F072D
      01F86B153C439680A6F6F4EDC275718DD23B90454DD3B7C1381AB8D7FA340990
      55A236364ADADB2800573F94D6C0F5ECF98CB308708DE5D272C73826F4D204D4
      5B94E440189FE3B79979137A4DEB1D06707DAFB2EB79DE42551A7B71294513C0
      786DD4FC1703EC2D99D09F6CD0D8CFD2A6123C0C10677FDA04B32D4A138B9CA0
      29086680000C00011040CB96B6CD7B3E57B015AEE73ECCA7382F3D744E982BD8
      CAAFDAFBDB3431ECAF4B30FDA6D5E98906C897CE39951D536BEF1AC0EDBE6B6D
      B9E27833711F6076E9F7985BFFCBAD01595326B14D8BC914DEFEAC35A355E3C9
      CF52057E062C5B60056152856584FE22F1F71E46D4B7DB82F56DBE0D5F3353BB
      A9EB33D629C0F9344BE75BC24C7EA1A5332D9D60C90C3103F3611BD8F0189845
      7EC0B9EB6C1B63E2F0544BF435CE87AC756B82C6FE7CD6BB6797432CCDEFDFA4
      0017C47D3017AFA9D61C7504AF26079AA6EF24244A9A26DFF975D551B463AEF7
      6545884B23F7DA69A94C32DD3601BC569761ED1061DAD49ED12C5ADA32A4B5E4
      92A9D803675C0D5CCFD6DE6FBDF7CB1B356FFF514CE8CAAF6642AFF5D1B0F68D
      B069EA6B59014AFDAC4985DAAD6629F1EBD5B5BE9415233AB14D0AF03806F59C
      51C626E56E6AE3C580B67FE6040027D8CA1B0C16679480619A20C1560E0EC156
      D0E486055B4113472307263B5A4253C774FA27961E646939045B79B8B52D87B4
      BCD3DAF1107B3D775CADDA4CD667980BF7E1B66FFE6336137AA3CD7C687BFC0B
      9E63099334BE03C092ED78ECA3DFDD1200BD9D566DEF6F076A7B0FC4D190094D
      CB5207FDF57233B363A6BFE5CB660130FAB2050C20B3868D37F931964E9C4B3F
      B657DBD23FF8BAA54F5832BFB7D9673371A07C4CD2B6B1846585BE66ABDFED96
      43989C4DFCAF0D802FF68F379FDFCD0326B25FB25F166A0C8C0B7003CBB34C80
      1E60A9186CC5C071F2472D008745484163668D7A94602B8080B5CE175A421364
      4D1C4FE3475A5ACAC156D803FF4C4BAFB5F43983F57C9CCA2E5D695BBC3E3F18
      7CD91C01F7B4590F9A33EDBE93252C17449DA35D9914D1C60097F606C080B8A6
      55AFB0EFB0840074C00E64013DC07FB1252600AC893FC76606AFB3E71F610FE5
      D93C130D1E8DDDB83E30857F70A82573549F754C33C60F3E62898901F9315163
      ACFCB9259CD3D81AC6AE01FC18B0AC687FF7ED4E33B3CF27FB97004F21BB5042
      369F93636D5A636044803FC080BD87A5DA59DBD798B6B7FF5CB015BCC13181C6
      602B985A31919356580220986811F60AB682D994F5CE87CD09F2A5126CE56ED6
      763895B1A7FA4306EAEFDAFBE2496A354DDBEEB971CEA9EC1B1FB4E50823E5AB
      6D511858EE68494E6568B1AC5FE39DBFABA591CCDF761D1007B898CAB9877E01
      C6F40D79025926563C0BD8E283B09D25A0BBAD99E1E933265B6CDD33BFC4592D
      9F32606EFF802533040CFED7D2FB2C3169E03B26768C95C759624B18EBDBBF6D
      E91E9614596DD64C8EC61D937D34D9BF04780AD56909D5CC37C7D6428D811AC0
      4DED7D80D93EFFD9C0B1AA061553A98ED9DB84BFD939F110C6E909D32C00D9D5
      12305861496BDB001CE10D2080C3CE9610F43ED8CA1FD87B7354BE9DA3525103
      2B09F58E7CB6B5411AA7B2B75ADB7DCDD2FA71CDDF665F3ECF961F56DBC2F0A7
      AD01F730CD168D17FF01264668BD98C3F98CE5889A53995FABA6ED794F7FD007
      68D5F818E08CC6FDE4834F02F902569E437FD2374CAAB08600DA4759622903AB
      08CB1940170742265ABCF2192007F08C05FA99E7A0C13329A0CCF4F9F6961E33
      970FFD7D2F4B3820B2BE7D174B5570AB8FED9AC9FE25C053C82E9490CDE7E458
      9BD618880037F5EA155F3173AE01E8AA1278ECF38D6613FD8CA97CDADA8340DE
      794E58036700216D1B78AFB0A4602BACB9721F70405343638BC156FC7AE75041
      BE88D0BEA73DFBC9D61E2FB744A432CCDF378C036BBBEF6A9B241D6B36E5AFBD
      D742861A3D5F6676E31DAD4DBC5319E66FEF5486F99BF66C327F036BCCDF68D5
      327FEF6A7FA3F9D207F417FD80E6CEBAF35F59DAC1D2932DE1DD0F5C1F6109CF
      EF8758DACA128E6498B681ED1696F004A7AFD0963175A339F33D4E67F82CB00C
      C2D860C2C173E48408E0C913E0B3BECD3EEEB11D10ED9EC9FE25C053A84E4BA8
      66BE39B6166A0C007093C487996A74B4790E579DA50E33B3EFFF983665D21AED
      6AD4602BBBDBB5AC992AD80A5B82FECC1270408803058070876D418B08E69207
      FB3606DB175899F6325013FEB518B7BD09DE66FE5E6FCB0C87991DF9A336CB79
      8335026669DA11B801D39D2DB5E6546679B1E60DFC5903C7D4CEA480B571FA80
      C913E66F4CDFDB5862C902F0B2FECC1E6B9CC730670358C00C6481341A321611
      EF058E2738EFF9FE9E96D0CAD1D2C9FB8973CF61A2C667E48FB6BE8525E0AFBC
      8A66F2A63160F74EF62F019E4276A1846C3E27C7DA34C6C0D5DFF9CE69A73DEF
      79979BC9F6A6127C56DBA9529F37E098B4DFC9A4251A15EBA040C1075B91899C
      D7186C05732CD0D8CE125A1D1ADDEF5BD2F6AFDB7917DBE7630BF29641FF0003
      F5B32CBDC1124E65278CA35173AD6D99BB78A52DD97E6E30F8A2B957BFD31A80
      090C1607DA90F600A898AB012CE6EB5759C2CC2CA7B215F677347F63D1E073EF
      5486F91B2730CCDF98A66F732AB3BF31B7D35F98AAF1EA46237EB4259CC41E6A
      090F6FFA012D98BE601285D318666CFA44A0BECD9C6D9F55FBC6BE03E0DCC344
      0CF003719EC192086675CCE47E7D7BE26511CB6FB27F9300BC161E545B77F463
      658FB1B62DB1ADC8074C612F6A69BB53ED73F2E4B98AB5CDBDB5908EE451DB7F
      CD7D4DCF6DAA5B0CF5AAB0A13CCB97455B6F62196A213C4B65D53EF0D2773EF4
      686CE3DADEDCF9B62BCF2F1D095BFABCD6762A2F65F37FC7ED51BA3FB6531C57
      8C05B5710483F2F09FD3372A6FAD6F54AE52BD7CB9952FD7B3DF9EF1C8DF3E40
      CDB03E9B06CC964B9E375E7AE909E7EDB1C779337FF007C53DDB06AE9BCCA4FB
      032334E6574CAB3B5A023A0007689482ADF019DF0113D651D1F6D0D431C7020D
      8439B040431B6BADB365404BBBFE55CBF7B156579CCA3E6AA05E63EF8B11E36A
      00B7FBAE371F8093BF3A187C636F0BF7698DB39B914A8E5EBCE298D7B65319FE
      05E449DE25A732AC1B68BB5838589A00A05B5902A068BEF7B6E4B56ACCDF98AF
      01F56D5AF5B86D6EF7B2D40194D1C28138CF6052407F6F318D3EB73C27FB3709
      C0B5A7B5B48F544259D19BB8863DC2DA2F2E41A97DDC1E38B5285E08271D31AA
      202F7A7609E2B57DB6DA171E8387F8E736D54D65F565F175D3F7B57DB325B8D6
      F6E236EDBFD53E5E9DC215DBB8344151FB47380E6BD7528C717FC8880747ADED
      545EBF075D6DE42723A5C024DA6B1F63EFEBFE08AEB88F5D6DA403536A7DE3F7
      46C713B7E2DE791FF08772D1FE3EBA5DA94F6BF1E1970B7827ADE7C66F7E73FD
      A97FF3371BBF7FD7BB160F9938D0C25A9A2AB8C654A7779964443364DD555A22
      EBA868D75EDB461B4473E45A344ACCB4001F8DEF519670749267B1DF12042816
      52D3C6A9ECB906A53D0CD4075A3A735CADFA483BA8E3A0C1E0C87D2DD2982D3C
      BFD55CA7A92F5AAED678BD53195A319319DA2CEEA94693A60D79F54E6568DFB4
      25DA38ED49BBF30C264E68ED68EFDEA90CD3344E65AC2747A732CCDF68D5C053
      5A75D5FC3D2EAC4BD7D39F96E85726024C08981890BCC99D6B5AEB77CB6BB27F
      6D003C02C907A090E0F7025A824FD0F5E126F9812B4255E9C7AE508E7A6614CC
      3501510AE6D2F4DCA640267A36E544C8FBBAE9330039CE79E64DE12C8709BD51
      DA983C0441EA1DE11EDB95EB15698E36D6E4C68770F5A1664B00AF95BB04707F
      125909E03A0E9576F2938F5101AE7EF1C1799A602AF8FAA044BEDC2A23DF4BEB
      F69F51F7D2A467585FE6F7775C66B8FEBCF3D69DF3DAD75E78CCFDEF7F7D095A
      DFDF6CB35F7CFFA94FBDCA162BD7993464AFEE172C7DC092597F6721B3C212A0
      79FB5CE26FA00468800CEBB83B5AC2540B508009265305E060ED7464CFE20961
      F2DB76FF530DD6BB5AFA94811AA7B2E25EF51AC0BF6BE13EAD117EFCC5B9833A
      702A330A016A2C0A4C50A8EF0B2D31B999CF9E6AEF54A63DD525A732CCDF3B58
      6232841503F3F79F5A925319E6EF26A7B289B5EA71FBC2CA330B684B68E34AAD
      42DB97C99E31D9BF6903BC044E85ED140CA4C9F12A215832D7D6BE135CE26956
      5E1896CAE16353C7BC8701BCF6CCDA412E4D65A39C9300BC5437F2F4B1DA0514
      A015CF068F07B3A8DD621D054B99A7234C755FADEDF47D04B8C2EFCA6A5302B8
      4E3DF347BA92DF2800D784D18FA9A84D47704A9BF675F4F760362F9DC407D075
      884BED7CF284F468BE00977DFEF3679E6460AE816AED431EB2E9E897BDECA243
      BEFCE533BEF0852F9C6192F0384BC6BCD9401BB67C3BBB67F7FD96D0C6D118D1
      B6F17E065A987071BC02326C2F420BC4010AE727AD73A2F1CDDB41690478B0A7
      7A174BEFB342B3CDADB83FBD49D3664FF5D7ADBE1FB143336C36F23A23A4F6A2
      47A7323CB781EDEE96D0AA8130EDB1C292346AAF5563A9C0431C4F71DA6CB7B9
      7603FAC09F49C0B84E6598BF47722A1BA1ED964498596B8FC9FE4D13E025412C
      E1150F7290D6DD74C0432D6CAA00580B35D9F47DEDB9C300AEEF4BA7A445CD54
      9A58D3F18B80A2769C6893C0177C4B66F9D896825F3CE7BCA95DB9475AA8265E
      3A18A3D6DEBEDFF95B499A7304B826153259C771A3F7F27DF0E01C06704DD262
      5979DF749427FD413B51264D347DB9B9B774C4AD9EA7BE1EC70A9360BF6566D3
      69A79D74E64B5F7AF10FEF75AF1B8BDAF6E69BDFFCFD673D6BE3AA0F7DE89CAF
      7DED6B3FF9F4A73F7DD63EFBEC73CEDBDFFEF6734D129E6A69C692F9B30DCC19
      FDB65098800A00B1F60AE0D0469F6A090FE352B015697E68606D984B1F6890FE
      3B4B1CD4F14583F549E39ABFCDA94C07757CFE1D83C13B8CCE580E4A4E65A53D
      D580185047A73299BFE55416F754BFD8EEC1ACCE16AA67CFB5DB33EC154B0593
      1E9CCA5866F04E652C37C8A9CC9BBFC7762A5B0E109F8CDE76F74200BC04B9A8
      01F975D31AE86AC25A66F76126D112706ACF1D06F071CA22AD3C1E2E122D04B5
      58D59A80C4EF05C69AF6EECB28D0AB6DBD263B4E5D746D5C1F2E99D06379D5FE
      25802B7638508C00E733413B4E3E86015C65886363987F82DAD4FB68F872D7AC
      1EA549AB3F9D2F217D47EDFB17D75F7FECC51FFDE8D9EB1EFFF86BAADAF6C31E
      F6B3EFEEBEFB85077FE52B3FF9BC69E61FB5EBDFFDEE776F58B162C5F9AF7FFD
      EB2FD875D75D2F3271F6134BC7CF01FC4BF6BAB725D66531EFB2CE0B7CD876B4
      8D251CA3DA0EB6B2B995FF0906699CCAD853FD037B7FED38B0B6FBAEFB96C5DB
      C6A9CC4C07EFB182BFD20A093C77B4149DCA0830C2C4C476740D3FA8C3AE01D4
      68DE68D5DCB3AB253473EF5486E6CEB3F003C0818F3DD538953DC6529353D916
      F63D8E5F2C37B4E2549600B7961CF66F21005ED20EE5B9ED059A0EDA685A3B45
      7096BE2F9D1235CC84AEEF4BC7910E03B834AD5A593C309A8E72D4FDC33470AF
      C9EA6F26064D560E0F9C789EB937010F0378D434296B49FB8C26F458E6260DDC
      9BC3A3739C0E48A14DE52CA635FC6100E79ED239E1A368E01AB7F295F0EDA913
      CB62FFAB3FBC065E83FD7287F9B53FFAD129EB5FF0824B7F708F7B14B77F7D7F
      8B2D6E5AF39CE75C71C4A73E7516DAF6A7EC75EFBDF73EE71DEF78C779FFF55F
      FF75C1EE06F457BEF29517BDFCE52F9F4D26EBD65B3AC61226F48FCE810A0DD2
      FCB586065B19779DF38F0DB6CF33C8BCDD407D90A573C60135D79A53D959DF30
      D3F927EC300C9B65BCD9D45ACCFADEA98C2D5568C16CE11AC7A94CE66F262F98
      BF59EB67D900AD1DF3B7F654CBA98CF6C12251732AF37BAABD563D55A7B204F8
      307ADBF7D304B834CFD2FAAF3FAFBB66561F261CF57D5C772E09C626215A82FF
      3080D7D68D79B63F752B9E015D13DA93AE81D74E1E93D61FCF3357B9FC71A5D1
      F2516BD761401A770DDC7BD3D3765A13271FB5B39603B4E4218D7C18C065FD91
      139BDE8FB2062E806B02E097246A6BE07E39A2CBE7672FD6E4E1A68D1B8FBF60
      AFBD361CF7F087FFACAA6D3FE631D7AE7ED39BCEFFE60107ACFFDCE73E77E647
      3EF291B3F7DA6BAF73DFFAD6B79EFFBAD7BDEE82DD76DBED366803F0D7BCE635
      17BEF18D6F3CDFC4D94996565BC2890DF3F0CE7360C24C3EDF602BF733A7B0A7
      1BAC5F6DE933734E65B78C036B2BD015870C063FFA7C7B0775B0664D5A61C9EF
      A9667DDB3B95310120B80AD60782ADB4E95436EE846749AC59B73DA91801D1CD
      974C13E0326D2390BD162E2117C1DEB4FE2D81838047784B9B4348CA49A9B4BF
      DA6BB8B535DBF9009C7CE5B95D2A0BE09159B8C974EECB57B2548C2268A5E1D7
      DAB834D988706EAA4B6CD769019CBA6A62A1094DB41CF86BBC335D6DB2A7CFD5
      06A5F5ECDA84CFB72763C46F135339FDC489EBFD9AB94CE74DBE0FA3F4EF52B8
      E6AA55AB4E3BEDD9CFBE62EDAFFDDA2F8A6BDBF7BDEF0D6BFEF99F2FFBD67EFB
      9DB9FFFEFB9FF1C94F7EF2ACF7BFFFFD1B6C6D7B56DB7EF5AB5F7DE12B5EF18A
      5970F3FAAA57BDEAC237BCE10D17BCED6D6F3BEFBDEF7DEF064CEA26E9A47D7F
      784EE3C40CCCFA2CA1316F176CA52088EF6C9FE154F6624BFB18A83956F2B673
      A14701B6DDF30B9CCACCFC7DD08706830F1A497737976BD68F77B484699AF577
      261598AC315DEF6A293A95F975EAE854E6CDDF98CE31A1E35486E68E53192676
      3CCDFFDAD2D32D6D67892503F6546F6389890C4B070FB454DB538D568D0F809C
      F7DAF20348805BA3C671671F4DF66FDA00F7FB8BA53D21084B501B05E0D28694
      87F6353739B02100DBD6C0C9D3AF9F0BA2DEBC2CED504B03AA5FC922C17DF13A
      AEA7FDB4DCE0B550FE96195B130501461AAC345B9523DEAFC9107519A75D4705
      787C9E2F4FE96F81CA6BE0A5E50D3FF918A6817BF809A8F41BF751FF5A9B468B
      88FADA2F8D08EA32B1730F796AC2A3F7F1195A2E68F2D9580AD0BEE1C20B4F30
      B5F9FC99073FF8E74500DEE52EBF5CFBA427FDF43BFFFDDFE71D78E0813FF9EC
      673F7BE687CC39ED5DEF7AD7B96F79CB5BCE7FAD6D1D636D5B2672FEE633BEE3
      1AAEE59E030E38E027AB6C82609270B5A5FD2DE1758EE91C78B15589A02B1E44
      0F31487350074E65FB1B784F1D05D0FE1AF392E3A08EEF7CDA0EEA30CABECDA8
      0994712A63BD9A3DCF3C9F3DD025A732797F8FEB54467EE44BFE25A732B6C0C9
      A98CFDEB0FB654732AF3E6EFBBA87DECB50DA7BD847501D625ED7D327ADBDD93
      001CB8083251507A48719D008740AEC11601DBB4B6AA67001BAF110DDBA2254B
      40ED3A9E19BF53DDA2208DF545B0D7EA469E258895CA51BA6E548053C6521BCB
      5C5C9B00E899BA6ED476E5BEA63657DBD500CEBDEA67FF77EC5FDA963C4A9609
      3469F55B0984A5FEA39EB2B6D4FA46E5AA8D6B3F3E15FD4EFBE519075EDBAEF5
      A9AC084B15E05718504F7EC633AEAC055B59FBFBBFFFF3EFFDEBBF5E72E817BF
      78E6976D0BD8C73EF6B1B3D1A2D1A6D1AAD1AEBDB68DF68D168E368E568E768E
      967EE8A1879EFEBDEF7DEFD4134E38E1E4D34F3FFD441367875BFA8C25D6809F
      6B1E574FFEA069BFA615BF62CEA9EC1813A2C5C86D3580DB7DD79A53D9F1362B
      3800A73253755F6ED145D0724968BC68BE68C068C2E33895ADB0EBE554C6B62E
      34F15D2DC9A96C67FB1B8DDD3B95ED60EF712A634F75C9A94C7BAAEF6DDF6F61
      09A7322C0FE954362250DB36910FCB6F5101BE14B484ACC3687B72B39DDA6D27
      2610A32CADF4A5DD7F7EF6D9EBCE36D01EF33BBF533CC96A36D8CAD39F7ED591
      EF7EF7B95FFFFAD77FF299CF7CE6CC0F7CE003E7BCF39DEF3CF7CD6F7EF3F9AC
      637B8734B46DD6BB59F766FD9B7570D6C3BF6991D88E3AEAA8D38E39E698534E
      3DF5D493CE3DF7DC75975D76D9F1171D79E46946BAE38DDCEB6C03F8FA6F0F06
      978FAB55DB3D671C6093808F0F061FB3FD546FB4E82368BBAC1DB386CC5A326B
      CA78B4CBA94CE6EF6191CAC6712A23E63A4E654FB18453195BDDD8A38EF99B7D
      EA98BFA353D93DEC33AC0CE954D65150D7409E0037EDB32F422ECB997DA531D0
      14C3BF2FE3E49737DD74ECA506E293B6DBAE1A7F7BEDD65B5F77F42B5E71F14A
      D398BFF8C52F9EF1BFFFFBBF67BFE73DEFD9B087C53167FB97D7B601389EE568
      DB789AE3718EE7F957BEF29533BEF5AD6F9DBE66CD9A538F3BF4D0F5277FF8C3
      E79C6C803FE1AFFF7AE38F1FFAD0EBC605F56A3BDC64A56DF132D0FB833A0027
      5ED90427C14B1B6F6DBCB6478954864359DC532DAD5A4E652F9E9B00B026CE84
      006FF3510EEAF091CA1452B4A455A75359CFE00DD427FE378909BD2F8226CB99
      E0CC31D0DE18F89999AC6783ADFCD66F95B77F59B0953516AF5CC156F6DD77DF
      D9602BFFF33FFF73EE9BCCBB3C6EFFC2AB1C6D9B3DDDECEDC621ED0B16A0E550
      D3BCBF6BCF596B815B7E685EEBDFFF8DDF182BA4A899BF6F32F3F74FBE3C181C
      6CA6F4BDCDBEFD6A8BE109A077B4348A53199AF3A44E653C0BA732F6546F6749
      07756C637F2FD8411DC34CB9F9FD1D1DCC16A24D12E0A981A70522C7C0D4C7C0
      CD3FFBD9711799D3D809DB6E5B0FB6F2C847FEECBBE664769099C809B682D95B
      C1564ADBBF003940DFDB34EFFD6C3FF8417FF557571EF9E8475F7BF4FDEE5734
      C3376ADA5B6E79C3FB6CBB966D785E6F843ED2428A5A74D1D93565345F4CE124
      FE9E8F531991CC3093FB4865F1A00E9CCAFC411D442A9353593CA8A314A92C9D
      CA7AA8414F0AF904780AEFA90BEFD45EDBD35EFBD6963F3DFAE8536783ADA055
      9BCF6B4CB3C156EC2CEEC3CDB10C53B782ADC4ED5FFFF99FFF79F16BCC71ED5D
      CF79CEE51FB7F8E65F7DC423AE3DC2D6CBBF77E73B174F15ABC2DACAF1BD3FFA
      A3EB563FFDE9571EF1E2175F729069F55FB6AD671652F54C1386C75AB2A8A303
      0B0F3E1B071D6F74B468029A107D0C10D7B4EA15F65DDC53CD0400A7B2D2411D
      6C518B4E65FEA08EADECFB4E1DD431296CF2FEF6B5F40478023C019E63A0D531
      40B095F3CDB9ECB8873DACBCBEFC2BBFF2CBB58F7DEC35ABCDC10CA7B2FD0CA0
      68DB7BEEB9E76CB015B67AEDF18FFF78F9FBB7DFFEAA7D1FF1889F7DFDF77EEF
      FA6FFFFAAF8F65FE06E0DF336FF5EF3EE949571FF94FFF74D9A16662FFBAAD7D
      E3C8C62401333B6679D6D3718433F3FB79260C8985CE5E703BE27A402855206E
      8AF9ECA964A5833A62A4B2490EEAE0E4B23CA863196AD1934C6C161DE00AA4C2
      FE5752DC4A238D6358E42B6DF52A6DBD895BCB7000E2BA1863BDB4A737062151
      084D05DCD0B61FAEE3FE780A9AFF9C72C42D70DA8E54DA6E36AC2EDCAB2D486C
      8BF2DBA5946F2C7F697B13CF2E6DC1ABE511AFF57DA8433A4AF1EB9BB68751FE
      52FBF9AD60C3F6B3530F9EABE02D8A1E570BD033CA56B0521FF8AD5E8CA55A1F
      A84F35064A5BE79ACAAB6D79711B9C3E6F0A3CB4189AFA95871CB27E36D8CADD
      EF5E0EB68287F9BFFCCBA50AB6F2894F7CE2AC0FBFF18D177CD460FD69DBCFFD
      E5ADB7DE7448E5109226F3F7D1F7BCE74DDFF9D33FFDD9AA673DEBCA436DCDFB
      40DBE3CDE96278AAB3C58CFDDEEF7BDFFB363041F86FDB33CE24014737CCF298
      E17184C301CE8421B1D08FB3C461265F9903F8BBE7B46E9CCA5E6D09A732CCE8
      32AD7BA7B2610775B0A75CE6EF7BD9DFDEA94C6746DFC53E9FDD734E9A44B8E7
      BDED6BBC5D6BD34507B8A2A029E0860292440028E04AE99850092BEE55500C09
      DE786637D72A1E7614A831188A26157E3FB4826B506EF2E61A09749EE9CFA5D6
      A482CFF4770CF0510B24A2F2ABAC7AAFC9883F144465216F7F5E35EFFDF9DB94
      A1167255F97BC11FE386EB3B7FAD8EBC54DBEBE4B4D251991EE03E588CF6852B
      66B92F03DF292FB5AF876AEC5F955947A12AD84CED401C05AFF179FABDDC4D00
      5760174DE6E278F01EE31A5B11AC4DE5D5E96D71BF77694C2F06B07926C15636
      D83AF48FB7DAAA1C6CE56E77FBC5DA273FF9EAA30C9C07DB3EEC6F9836FC4D3B
      74E4B0DFFDDDEB57DB77E378807FEFAE77FDE5B74D1B3FF4094FF8E937CCECBE
      BF6D1DFBEC873E74364E6E801A2DDEF6799FC3BA390E6F78AAB3468EB73ADBCC
      0035DBCB8035FBC449FCCD67962E346178FA9C067E98BDEE3BA7797368874E21
      63DFF68E964A4E654D0775FC96DDC356AD3CA82335EC5683D42C2AC0755EB4D7
      4A1579CD7FE66349472079C1558AF01535773D13A118F7D146B8F910A2FA1BA1
      2D18F8F39A2987A26DA9EC7AAF8902CFF4E134B9A7E920109FA7AFA7EA405B78
      3049135480109EE5A13E0EC015AD8C3C238C3DC055FE1878C747942BC1A554EF
      D85EF150120551F1F9F9FE2D85276D3A8E7358DB3741B1D4071A0F9AB0E97E45
      C8A34DBCD63C4A79631BA8CC8BAD7D5FF6A52F9D71F2D39E76D51A338717D7B6
      EF739F1BBFB7CD36D7AE3613F8EA7BDFBB78B46713BC0FBFD7BD6E3AC0B6787D
      F1294FB9EA53FFF44F977FC4B46582B0B0358C5769D406EAF30035D1D5D0AA31
      BF13B805CF740F6B819ACFD1BAD1BEB91E6D1CAD1C33BA09C37596580337C7F3
      C15E73DA361A369A751ED491F06D15BE6D68F38B0A70C5348FDA1160F2D09079
      95EBD1766A82751480930702569302FFEC08F01872B3F46CE52333A7345045ED
      F2DA93B4499E234BC23088940EF6A8B59BAE256FE51BB5B55135700F8E081E0F
      F0D2A132F48FDAA1D6574D960701309E8B3E0CE05E5BF7CF95A61CCB32ACED9B
      00AE7BE3D88DE341A0579C731FFBBC76A0492C97FA501AF928D106A7A191136C
      E52CD3567F6470F6F0B550A2B79066617EA73B8D7550C7B7CDDCFE358B63BEEF
      1FFFF1757BDB7EF0FF3673FAEB5EF39A59B8026500CBF630D6A8792F8DDA403D
      1BDF3C6AD5809A04A8F91EA0139D8D602F446A03D4AC7BB3FECD3A38EBE1AC8B
      13D1CD84E10F2C1D6C89B56F346F346E8BC7321B0405F3F7A84E6569FE4ED82F
      08EC1715E0082A1D54C2DF24C05112600A5CE1E11785D4280097A95E70F69AA3
      879B3FE882BF11B6A5D09512D0CA47EFB93EC25200922978BE1AB8D6E14B425A
      6DE035635F8E51014EF965A1F0677F6B29406D516A73AEF1A76A8DAA817B8B83
      AC155ED31C06F0DA119D11AA2A8FDA48634FAF9A8CE9A4357FACA9962834A98C
      75D3B89215C64F3A23B06B61816379F55E21774BCB01D300B6CFF3EAD5AB4FFB
      81EDDBBE0DD6058FF261E6F003CDE3FC930F78C0F57BDA9EECD79873D9BFEDB4
      D315FF626BE22F366FF097BCE42517BFEC652FBB589AB28179F6C01140CCDF73
      66EEA2F99BEB3091632A07FC009F402E4460435BC7B4CE7A3B71CFBF64960322
      B91D7CF0C1EB0F3FFCF0D3BFF39DEF9CF6FDEF7FFFD41FD9D1A4260C59FB46FB
      DED31281583822F3A1962CFAE9ACF9BB18A9AC0D4D2AF358FAEBD5D3E8E34507
      B884B2CCBDFE3D0244A66B0972AD6D36C1AB6662155CA5FD7A48C9BCAC72F8F2
      44680D9B3848E38BE67ED5CD9B8A876981250DBCC9A12F025C6597563B0AC0E3
      E4265A44BC065E03F8284782726FA91FE517111DFE86017C5859A23398DA3EF6
      B99E5BFA5E139792CF80EAE2CBE1C798AC1AFEB09258473F89299D66563AF675
      DAF0262EF93038FBEFBF655BB5BE60FBAADFFF877FB8E90D8F7AD435BBD81AF8
      B3CCC1EC6FFFF66F37EEB8E38E573CC7B682FDC33FFCC3E5CF7FFEF32F03E0BB
      ECB2CB2CC4FFD5B68919C86F83B90E2161AD1A807BAD5A1A3AE66F0E262116FA
      073FF8C17388D4665BC2CE621F396780E3E57ED861879D7EA4854A25EE39A03E
      FEF8E34F3EE594534E3AE38C334EDCB061C3BA0B2EB8E0840B6D2DDF84216BDF
      9FB1C4563102B6700A176147895C965A756AD50BA2558F03FA450778C92CED35
      4CC15002538E65F301B804A8F2922395341A042FF09026E61DE66A1AB860E7AF
      15BC22303C80A44DD5CCBBAA5F0DE02527314D42547E01D24F184601B8343E81
      26BE1F05E0BA675C13BA0758699740049E9FCCD4345AD53F1ECB396CF2D404C6
      9A06AE3E00B47122E49764B84E1EFBF139A5F20E6BCF6941FC97BFFCE5CC59A6
      0597007EF4DDEEF6CB43CCB37C3F5BABDEF3718FBBE695DB6D77D5B39FF9CC2B
      9F61C07FDAD39E76E50E3BEC7095BD5EF5977FF997573ED33EFF1B8BAE6600DF
      F86CF354F7007FE10B5F380BEFFFF88FFFB8042D5CC77DB2562DF337EBDC38A6
      E1A086F91BAD1AF3375BD038988413C50E312FF8238E38E2F4EF7EF7BBA7FEE0
      073F38E5D8638F3DF924FBB77EFDFA13CF3AEBAC13CF3BEFBC75175D74D10997
      5F7EF9F1575E79E5F1D75C73CD71D75D77DD71D75F7FFDB13FFFF9CFD903BED2
      D2272D117485B0A8C410677BD766E308D5BC36B5E9851A038B0AF0A891480879
      586AFD4F5012F04ADEE8C34CE832617A9329F7C8FC5D839B370947A8A80E1E0E
      A3005CD60079D7D7047009E0B576F3CF8D7092D97E1480478D3B82C703BCA689
      CA337E1280C77B8769E0F46F7420230FC64C6DA258B3020C0362CD0F41A0653C
      A89FFC98F166F45A1B95CA3BCCA231ACBCF3FD1E805FF8F18F9F75CC831FBCE9
      C706E4A3EDCCEDAF1B64DF6B5007B6CF336F70D3AE376E6F7BB69F6C9AF693CC
      344EE2EFA73CE529570371800EBC773293F9DFFFFDDFCF6ADE68DD40FBA5B6ED
      0BB339EBD5C09A756EB46AB67C61FE660B18E66FB68461FE46AB3EE8A083D613
      DB9C4349886FCEC1249C28C6E124679E79E6891C5082467DC925979CB0D1F6A4
      FFF4A73F3DEE6716090E48DF70C30DC7DE6431D87FF18B5FCC5037126D73F3CD
      37CF983064FDFB1396D8DF9D004F8DBB731A779C182C2AC00506849A849C4CE6
      083FFD1D35596DDB1966CAE67B6968A5356F69415AEB6D02B8DFD2A3F250469D
      09EDCB320AC0B95EA6E226889404B7CA8206A7B2F0CA7B012C025C138651004E
      9DA2A6EBC1E3A12D48713DE5F21EF025F3B0DAA949FBADC16A18C0751F65A5BE
      2A0BF529397E4DA28197FA80B6A0FD651D2939A909FC5A5FA73F4AE58DCB2F8B
      05704087966A66E675C71D77DCC90014B812190D4D1A403FF1894FFCE9E31FFF
      F89F3EE1094FB8E6CFFFFCCF7F0ABC013AE0C66CFEDCE73E7716DAA6695FCA5A
      37C0C6090DC73222AE49ABC6A98C2D61389511950DF337477E62FE3EDA22BAFD
      F0873F3C8532D8BF937EF2939F9C78CE39E7AC3BFFFCF3D75D7CF1C5B769D5D7
      5E7BED6D5AF58D37DE380B6B0FEADA4426019E5AF34269CD6D3E6751012EC022
      C404650FC49AD779CDC3B94903AF69AD5EA036019CB22A0FAE93F9DDEF038F66
      EF26133AD70A02E3029C7BA5E9D5CA528293260CB15CE42728FB7CE3DA30EF15
      94C43BF4295F2D6F701D106B72B69A06C0E3A4487D542B4B6D0D5CFD51AABF1F
      63C3C643ED7E3ED75AB69FC4359577B1000E0081A2C1F244D6904D133E0B2D19
      18A3556F679EE3A671FF14AD1B8DDB43FB051642F5DFFEEDDF2E01D838986106
      C7B18C6340D9B78D568DF7374E652B57AE9C752A5B6DCE723895FDF8C73F3E05
      F3F769A79D7692377F73F4275A35E66FB46ACCDF51AB9E8FB56118C027169499
      41B640175BC07E2C039FE6CAB8D9AA55AB66CCBC35B36EDDBA19731499B11FDD
      CCA64D9B664D55F107A688586822FC2DC1CFDFA5E85568E608DF5294B40827DE
      9307AF250720799BFBD72601403E5AFF44AB2BEDC7ADE5A572F8FCB5E5ABF64C
      E555FA9E76A8958572460DB8A98E6A6BDD173DB3F55EA6E1D82FBCD7D2C7288E
      56A5F2A98EB53A97DA4FFDEBDB2796A5369168AAAB2697A576F063AC361E5487
      5A3BFAF61BA5BC4DE3603EC01AF51E000E2C71F6C25CCD9A33DBB16CBDFA62D6
      B1D1B00139DAF83FDA16B017BDE84597B0868D391C5338CE6568D67880036B69
      D598BFE55486F93B3A9561FEBEE28A2BEE60FE46AB8EE6EF51EBD2745D02BC8B
      74C9324DBD05DA00781B3FC0CC63F91E98917D3FBDBE079668BA66AA3E616666
      E6E4030F3CF0271FB698E2ECCBC62B7CCE437C36300A6027C00ADA355EE03896
      E101CE56ADB56BD7DE66FEC6A94CE66FEF5456327F0BD6D3EEE304F8D451910F
      E8620B24C0A7273CA72DB432FFECBB61630080E2FC85E736DBAE306FE344C63A
      35CE656CDB62DD9AD8E306F7F56679C3043E0B6B69D5722A8BE6EF26A7B261E5
      6AFBFB047817E992659A7A0B24C013026D0BD3CCAF3B630A0730CCD668C766D6
      3E1E2F6F4CDEECA7C6A4CE762DD3CC67D7AB712C03D668D5327F739F403D2DF3
      771BE325013E7554E403BAD80209F0EE08DB360459E691FD19C7005A388E6278
      A35F7DF5D5C7A18D0373D2A5975E7A3CEFB5550BA732797F2F94F9BB8D319B00
      EF225DB24C536F8104780AFC360468E6D1DD71A46D580019A73600AD54DA53DD
      C7BE4C804F1D15F9802EB64002BC7B82174FF1C53EADAA8F423CCBDCBDB1BC50
      7D9200EF225DB24C536F814900AEBDADA54342D82BEBE34E97F6D436050AD161
      227E0B51E91C65EDC5F6FB71E3B3540E9D30A6EFD95F5C3B9F5C752B95DB6F43
      A20E8AC616E3BCFBA0201264BEBC51B8016EED25D6FEF0D29E6F1D3B3A4C386A
      1FBEDFA7EFEFF175D3FE7D45A4F3FB966BFBADB9A6D64EB479531B2A40CF28E3
      A2E9194D7BC1755F53F856CA49DDFD385BAC3DDBC3FA33BFAF4F5012E0534745
      3E60915A2032DABFBFDD1E70BE18671FB817AC5EB8F8F3BFF95C6140E39EDA12
      9CB85E51D710AC71BFAD0F9DCAB502A8DFB7AB93B0F43CE501F834B1006E12DE
      31C636F97AE11F43B7AADC0A2203747986C2C42A6A980285F83A0838250D5BE5
      6332A22871112E3E225B6DF241F9F5EC58361F218CB6F0B1E3B956CF2B019C7A
      F9B6F0D1C862DFF2FCD8273A6D8C6BC97FD471A1887E8A95EEFBD5EF058F617B
      D556C3001E030825C0FBA7C98F0AF0266198DFDD3E2648B647F7DBA335807B98
      F8034C04F092965ED328FCF18B0A93AA6B7DB8510474296C68294A9880E6CBC9
      44C1E7E7CB5313E23EDA1BCF8EE513D40579414A13935279F9AE7464A6B4751F
      4654E13EC93786EBF4E557D9BC66A93650D962E43A7FDA9BAF3F7994AC097EA2
      334C3B94C6EDAF6B3A112CE6A73EAD4DFA34CE22AC9BA2FBE91E5928D45609F0
      0478C2ABFBF0CA3E32853B36C27C347004B1878C84FD28473796043F60525437
      846BC9BC098C7498448CD455027829AEB5D754A3163E0CE0FABEA4BD5366B587
      AE5379A346ADFACB9210DB239EA2C67BEA5D0B53EBA15A829DB46EAE1B15E05C
      2B2D38C6241F157693025CB0ADB5DF2400D792842644A3D669D8A425BF5FB889
      406AE009DCE508F456000E4C740084B43804BD07B8D658A575F15A821F424FA6
      73E515C37EFA93B94AA15A4B008F930C095709EB08BB610097365B12D251B31C
      565EF2507B799330CFF060D7D202DABA805A32A33701281E254ADF713DF9E888
      D49A665DB262E859BE5FF93BF64B0DE0E38C0B59026AD69CD26129F225F0E5F3
      BE14FADC2F6D24C0170EBC6D4D7212E009F004F83CD7C0051380ABB5652FB0B5
      D61C857C09E0F1EC6A2606D14CEDD7444B02A006F09AD35CD4449B4CC3BAB6F4
      0C9525025C1391DA19E0DC274D5B71E535E1E0393AA14C0095C5213ACDC549C9
      B0C94593035909627D05B8C0AEB573DF3FFADB4F0E12E009F0E50883AC73FF26
      41AD68E05E6B0644403C027CD435F09A3779349397B42D6F8ED689541EAA7122
      D0646E1EA681EBFBD2411B803AAE4F3795977278507BF07A4B863CF3A3F77804
      B5CA567294A31CFEF85569E0DCE3AF8FF597BF416CC35161D786095DA7D8D5B6
      D8D534F0614E6C1A9B9AA0344DCEDAD218339F762709A981F70F3E396198BCCF
      5A0338C255F0D56963F359039756091864DA8D9EE7B5F5CE26806BCDB8B45E8E
      19B706C1F8B9347069D5F1E4AEDAE7C3002E7844383119D0BAB3CEC556DB9434
      62CA2B537B04572C5BC9F250D3E2B50C10DB6FA1002ECB4CD3496993029CBACB
      133F4E0013B8ED02B7EDF64C804F0E83046AFFDAB03580FB6D4BD228C705B84C
      E3D1B48E508D9ADFB81AB834486DABE2BD261CD1316B14133AD748230494804D
      1EE22547AB610097C739C056FDD5A6BC8F4B0B12803A873D0A445FB75AD94605
      78F4ACF7CF5A0880D7B4FF58E73600AEFA24C0BB0DECD8F709F0FEC127270C93
      F7596B0097D627D37134A197D65BA3591D9096B4616D4BF3DADFB800E707AFBD
      D5BE2C00B364061F664297A6ABBDDBCA93F297D6F687015CE5533ECA57930BDA
      B5B486AE49441468DA2217CDEDDEE96D5480CBA41CFB50FBB94B7D1B015833A1
      8F322E3C54E3F5BEDE6D005CD69D0478023C01333960B20DA7DB8613011C4878
      EF6DBF8E8AD62490F12AB3AF7F8DA0E37D097E7A8E076D53B85105182999E9E4
      6807509A02A1C4BA79D37204BEBCC5D1546BEBB3A38647F55EE8BE2D6A6DA3BA
      96262194993A52D752D97856ED3E5F7F3D23F6219FEBBA52FFFAF6F7E3419F8F
      3A2E467D46A98DE3DA3ECF8E63338EB9A6F1D3B6E937F36B67A2901AF8744191
      20EE66FB4E04F0143EED089F6CC76CC71C03938D810478370193E09F6EBF24C0
      4D234BE1996D9063A0DF6320013E5D502488BBD9BE09F004784E60720CF47E0C
      24C0BB099804FF74FB25019EC2BBF7C23BB5E77E6BCF6DF45F027CBAA0481077
      B37D13E009F004788E81DE8F810478370193E09F6EBF4C04F061DEC135EF65EF
      FDACBF6B9EBFF22CAEE5250F62EF591CAF8D337CEFADDCE41D5FF26A2E7953B7
      A141641EA945E61898FF1848804F171409E26EB6EF44001FB63FB7B67FD8EF3F
      D6DF0A54E20FC15060176DD36ADA33ECF719979EEBF77BFBFDC24D7996F61597
      F633A7E09DBFE0CDB6CBB66B630C24C0BB099804FF74FBA51580D7F600033BC0
      E9BF8F51BD3CCC63B84EEE55B01741393E4B1A7804B87FAEC2A82AC88C073381
      5762199567023CE1D2065C328FE98FA304F874419120EE66FBB602F09A802A69
      ABD2DA1500C6035C1AB7028FF09D07745374AC08F05294379DEAE5C1ACF29462
      6C27C0A72F78136ED9C66D8C810478370193E09F6EBF740AE0FC90154E15CDD8
      1FC631EC84A86100D7719D3C2382D96BF75E9824C0132E6DC025F398FE384A80
      4F171409E26EB66F2B008FEBC8DEEC1D35E1260D1C41A798EAF14090A678DCDC
      17018EB6ADF39F75B087B4EC1298F92C3E93CF4689D59D027AFA023ADB38DBB8
      690C24C0BB099804FF74FBA515800B947A9523DAB826747EA0DE31CEC75917C0
      E3B374CD30273660ACB8DF11E03A092C4E3678AF6BF55C7D964049A0E418E8CE
      1848804F171409E26EB66F2B006F6B0D9C7CD09CE5CCA6356B69D893AC813799
      C631AF8F7A04687AA17747682740B32F340612E0DD044C827FBAFDD229804BCB
      D62955005BEBE093AE81D7002EAFF8D2C964B9069E80C849423FC640027CBAA0
      481077B37D5B0178691B1966E9714CE80448410BD6F9D7084EAF19D7B691D54C
      E8D11C5E0378691B99F21C0670CAEC1DED52D8F743D8673F2DBD7E4A80771330
      09FEE9F64B2B002F397AE95CEB519DD84A4E640092BC71426B0A0A5372621B15
      E0A5B2CB543F0CE05AAF4F202C3D20649FF6AB4F13E0D3054582B89BED3B11C0
      9B42A9F25D29ECA8EE91439942A9F2CAF55170624E5748D392A62F6D398652D5
      FEF192201E164A55790E0BA5AABAA4B0EF97B0CFFE5A7AFD9500EF266012FCD3
      ED9789009E8270E909C2ECD3ECD33E8E8104F874419120EE66FB26C0F324AADE
      9F44D547E06499DB9D2825C0BB099804FF74FB25019E004F80E718E8FD184880
      4F171409E26EB66F023C8577EF85776AB3ED6AB37D6CCF0478370193E09F6EBF
      24C013E009F01C03BD1F0309F0E9822241DCCDF64D80A7F0EEBDF0EEA3C69865
      6ED76A9000EF266012FCD3ED970478023C019E63A0F76320013E5D502488BBD9
      BE09F014DEBD17DEA9CDB6ABCDF6B13D13E0DD044C827FBAFD92004F8027C073
      0CF47E0C24C0A70B8A047137DB37019EC2BBF7C2BB8F1A6396B95DAB4102BC9B
      8049F04FB75F12E009F004788E81DE8F8104F874419120EE66FB26C05378F75E
      78A736DBAE36DBC7F64C8077133009FEE9F64B023C019E00CF31D0FB3190009F
      2E2812C4DD6CDF04780AEFDE0BEF3E6A8C59E676AD0609F06E0226C13FDD7E49
      8027C013E039067A3F0612E0D3054582B89BED9B004FE1DD7BE19DDA6CBBDA6C
      1FDB3301DE4DC024F8A7DB2F09F00478023CC740EFC740027CBAA0481077B37D
      13E029BC7B2FBCFBA8316699DBB51A24C0BB099804FF74FB25019E004F80E718
      E8FD1848804F171409E26EB66F023C8577EF85776AB3ED6AB37D6CCF04783701
      93E09F6EBF24C013E009F01C03BD1F0309F0E9822241DCCDF64D80A7F0EEBDF0
      EEA3C698656ED76A9000EF266012FCD3ED970478023C019E63A0F76320013E5D
      502488BBD9BE09F014DEBD17DEA9CDB6ABCDF6B13D13E0DD044C827FBAFD9200
      4F8027C0730CF47E0C24C0A70B8A047137DB37019EC2BBF7C2BB8F1A6396B95D
      AB4102BC9B8049F04FB75F12E009F004788E81DE8F8104F874419120EE66FB26
      C05378F75E78A736DBAE36DBC7F64C8077133009FEE9F64B023C019E00CF31D0
      FB3190009F2E2812C4DD6CDF04780AEFDE0BEF3E6A8C59E676AD0609F06E0226
      C13FDD7E498027C013E039067A3F0612E0D3054582B89BED9B004FE1DD7BE19D
      DA6CBBDA6C1FDB3301DE4DC024F8A7DB2F09F00478023CC740EFC740027CBAA0
      481077B37D13E029BC7B2FBCFBA8316699DBB51A24C0BB099804FF74FB25019E
      004F80E718E8FD1848804F171409E26EB66F023C8577EF85776AB3ED6AB37D6C
      CF0478370193E09F6EBF24C013E009F01C03BD1F0309F0E9822241DCCDF64D80
      A7F0EEBDF0EEA3C698656ED76A9000EF266012FCD3ED970478023C019E63A0F7
      6320013E5D502488BBD9BE13017CE5CA95EB2FBFFCF2E3798D89CFA7A965F0BC
      D34F3FFDC4693E639A79D33E3BEDB4D315DB6EBBED35BCEEB3CF3EE74CBBCD86
      D567EDDAB5A792865D97DFDFAA3D32FEF41BC8366957A31EB73D13E0DD044C82
      7FBAFD3211C00783C12D08305E63E2F3717F84A35EBFDF7EFB9DC9F3FA0C1BA0
      4D1D78256DBDF5D69B161BE09B6FBEF9CDBBEDB6DB45A3F6C372BF4EFDB6DCDB
      A10BF54F804F171409E26EB6EFC400E7C70B881652F00B7A5D101CF32D832621
      BBECB2CBA5F3CDA3CDFBB0006CB9E59637B499E752CF4B13D8A55ECF3ED42F01
      DE4DC024F8A7DB2F13015C5A7609E068937BECB1C77980DD274021F3239F6342
      F609AD9A6B745D141E7C0EC0011F10C48C493E5EE3E76F4D28F4FDF6DB6F7F15
      E551FEB15CBA9EEFF9DB5FAF3234D549CFE479948D3252BE9AF0232F80292DDC
      3F83E7FBE501CA4D5E7A86D7D4F9CCB795DA877AA8EEBC96FA426DC62B65569D
      C97F9CBA729FEAAA32C6F6557D543E2D81C4899FCAECAD2BBE1EB413F76AD981
      F7B1BD744DA98FFD5851BD95976F73DF26712CD236E4C37D5A42F2D7A8BCA5B1
      AD76A5CD5496A6362B5DEFC754ED5EAEF1F7D2BFBEBF7976D338EA03B47D1913
      E0D3054582B89BED3B11C0F5031284FC3A380258028CEF8115EF1174D23E798F
      60C1742BF32DC24FF7452182D0212FAE450092A79EE3B547CCD1E4415E5CCB3D
      5CAFF566E5AFE7AA1C5CAFA5005DEF3564045EA94E5C03205536CA223897204E
      3E9491E7732FF7F1DEB727F9E93D65E17BB59B8705F75326AE95599EEB556769
      89BEDC6A1F4D94B8C6F705DFFB7EF0FD37ACAE949BBC547F3D977E8AE5E33BF2
      8EFD4CBB70AD3ED7328D262B2A2FD7298FB8644339A987AED575948B7AFBA51F
      B5AFDA9CEFD5276A4B9587BEF3E34665501F0C1BDB2A13AF7AAEFA30B699C689
      2F8B1F27B44FED5EBEF3634CF5F7BFBFDA38EA1BBC296F02BC9B8049F04FB75F
      5A03785C03F742206AE8827914D0122835800B7671ED5BC2D80B6669784DEBBA
      F139E44B593D3C6BC22CD6496540284B83D54423E6C1357C270D4890D07363DE
      9AB8900F6DC7F5CA5320886517842220A3D957008A7DE1DB603E75F565266F95
      4F1A77AD7C9A88F8F6F179F9F292A72668259F0B3DC34F8CF4FCD26448D7C57E
      D3F3795E1C839A9408E09ABCD5C6B6264FA57115DB4CFDADBC9BC01AEF55DD7D
      BB900F756B1A477D8477027CBA90480877B77D5B0378D31A780440C9E4EE415B
      03383F54C14FDABC048EB43E69D9FA5C9A7129CFD267FEFAA6F5E91AD4A2C02C
      095FEEF5794BF3D7B54D00174080BFEE43584B807BB3A8AE8D93299551508D26
      E238C19A4F5D2350D4AEDE4C5D2A9F87BDCA152729BEBC027109E0B10CB11EA5
      BEF6DABEDACD4F8E4A6386F6527F96C636DF7B0D9EF702B99F8C9600AE6797AE
      F7FD1AEFE57982B5AEF36D551B4709F0EE0AEB0469F64D1C03BD0238C24CC252
      EBDF51D341800A1232416A6D370AF9288C75BDD6449BBCDCE703B59226E82720
      35806B9921C20DA12D4DB524FCBB04F05127186A0F591A3C50F5B75F966802B8
      CCE8A576AFF57513C099389400EE279F3580CB6A421EC058EBDF7EC255EA4379
      BA97AE6F02B826BAFE1ADF567EF2A6F1B5D8BB2026993CA4093DE1B61C27380B
      0670BFA65933DB0E33A1739FD667E38F1DE1A33568092209C49A3399B476E525
      537693F3998781D7AE4B82BF6649909955E5D4BAAAD7E27C7B95CCE632874693
      B4DAD09BA8BD608E8019D5843E6E5DD5F69A34091E719D99F294C0A1FB7D1BAA
      8F05436F422FF519F7FAE5065F77012E4EEAE25208FDED4DE83299ABCC7E5D5F
      A669EF8FA1FCE44019AD2F25087B87BFA6EB4BF7AA5CB22ED44CE8DC2BAD7E29
      6C1F4C8027C013E0B7B09D7BF6DF66AB56AD9A59B366CDCCBA75EB66366CD830
      B371E3C6994D9B36CD3A8B44782264647695C0F5664EAD534AC80A2E5CABD9BF
      DF072D879BA801EB73EFC42680F12A27A0D23AAB9CD2BC7015B0054E093DEA23
      07A3E834E435660F8471002E984913A34CD449825B9A33F595D39317C4DE61CE
      C34F4E4B828ADA23AE67FB75F0617D417DD5EE6AD751EAAA6B2883EAA5BAA8DD
      352E4ACB2FDEA1D0D7DDB78D9CC9A84F6DA9C2E72DA741EAA47C7C5F9387776E
      93331DD7684225CD5566708D7D5EF9AEE4C4A6B1ED9D2A3529E3191AE7EA578D
      39DF065A46F0E3A40470B5437494541FFADFA59E4759FAAC7DD30E09F0047802
      7C9E004748C6E4852E424DC2D19BBC15890C4112B7B4707D0478DC1643BEDE61
      8C3268ED4FF9710D9F95B68591BF3E57B9B4CDA974BD1798BA579392B8D549E0
      2B8185EF681F791053EE2840E5CD4DD9A396C8B532A9FA32F1B9F2549BFAED56
      5C3B6E5FC86C4F7B48F88F5A57CAE835E051CAE7EB431D4A0E856A1BEAAA2D72
      D15FA1544679DDC7BEF6DBC8A2C7BBAF771CBBDA3EA7B1A0F14A1EB5B1AD6D86
      8C8BD887B44F8C7120C7B9D2F5B1EFE3BDFABDC82F248EA3D216CC49CCD88B79
      6F023C019E009F27C017F3872B13A7D682A5B53439D52D54796B26F4857AFE42
      3E67A9D4B5645D58C876CC67CD2F246B023C019E00EF21C0B57EEC811DB7692D
      96509416B658CF5FC8E74687B1857C769BCF4A80CF0FA06DF6C17CF24A8027C0
      13E03D057834C12BDADA62AFEB610D286D6F9A8F80EAFA3D4BA5AE8C19FA6CB1
      C74ED7FBBB6BE54B8027C013E03D0478D7044996A79F1A5CF65BBFFB2D019E00
      4F8027C07B7F2E7282A8DF20CAFE9B5FFF25C013E009F0047802DC3CD51322D9
      067D1B0309F00478027C0A0067DBD228B1C5DB16183C57FBA07580857F06EB9C
      A5ED5B8AF6C6BE5EF6DE3685536DBBCCA3E6A77DCFA53DF7A3E6C175715BD538
      F72EE56B59FFD6BE698541F521606B75679C6BAB9D0ED95170211F986729B7DD
      62D52D019E004F804F01E08B11E5495BC90030A006E6714F399EEA7E8FB2048F
      0F788140EE22C0A913A0A06D7522DBB882536D344AD4B971F3EEFBF58ABC46DF
      2B2EC030802BC08B9C16159C47C7C0C678F37D6FA3AE953F019E004F80B70C70
      05B858688FDE51B60201EA92505624AD2E6B4C8AE8E5A3D78D2B5015E066DCFB
      96C3F5F3695F00ED277BA598E8CBA1ED16AB8E09F00478027C0C8023E4B4CF59
      A65C8546D5A10FFA31CB9CA8ED5DFE7AFFB7F6720BFCC34CC48A5CA5EB641297
      002E411AB82BAC2ADF470DD4C7D7D6D1A0AA47ED7932477B8D5E752E4D1278AE
      D7C84AED250B81DACCC3412156FDC488EF754DCC4FEF5557DA497D257371D410
      F5BD17C82A77ADBC8AB6C7F7D1BA11DB5D7D46997CD9791E75F663ABB4154F65
      E03AF50B9FD59EE3EBD3D48F3C5F61481526D55B6FE2B8D7988D75E07968E1A5
      58EB1A7F3E0AA0B47DB577AC47E9601D45A48BBF27D5C14783D378AC8DE5BE5B
      0812E009F004F8180057384F7FE8047F23084AA72A4923914952F1A5FDFD082D
      AFFD20DC1537BB14594DDF29A6B807AE8F6BAE70A632730ADCBA5FC24BDAB704
      ABD6C125F4146BDCD75179C73AEBE08DD2FA7FD4CEFCBDBEFE8AD7ED8FF754EC
      F208061F094D75D7C4C2C7F2D6B374CA55ED3CED781889AF8F2FAF02E9C85FC0
      C7C5F7F097CF81E2B4F34A3ED4D7975D1328F2A31E5C13E1A5FA295EBDEAC0E7
      7A8E1F5F3A3C86B6F331C27D3F6AC2C1F78A73AE650A1FAF5C4B2CBCF23CDD17
      23D169AC510E92A0AAB2D2B7D44D932EEE8FB1E6F50CB5936F4F8D0DFDE674D0
      8FC685C62ADF6BA9259683CFD5C67D8F5790004F8027C0C700B817264D40D275
      7E2D5CA0D477FEFE78DA13D720C8E2D9C67CAE8350A296EB418C6012004A11DA
      10743AE0249E832DC0CB3989727A8D5D2023FFA6494B342B36B5578CE5EE0F1B
      211FCAE40F6451DE1E205A421044E249707AEF35CD52BC751DE6C133D4A6B1AE
      F19CEF58DE58F7D2F2862FFB2851F4FC096AA53AC4F5689F7F6902A4BE9376ED
      0F0CD1F502356D573AFCC33F234E42D5B62A6BCDF74200F763B26492D664CAF7
      ABFA8757FDED1DEA3471D0EF4687AF2C96C9BBEDE726C013E009F029025C5A32
      3FDC083B0FB4A8C994AE8F665D69395E00964E116BCA5B42DF9B12E5E885C02D
      01DAC3A82D8097CAE8BDE14BDF6B92236B80CA154FE92AD5B1C95F405A9E4CCE
      9A38F8BACA9C1FFB44651917E07ED25013F2710214EB10DFFB361BD68FF1E856
      CAA0DD0C2A8FDEFB53DDFC337CBBC53AF813C6A411ABFF347E87AD9FD7FA4CF7
      95EA18FBACD63F6D8375A1F24B8027C013E05304B8174A0838AF45468047F896
      04128201D872AD3C7DA589EBB8D1283C4AF093B02DC14DC25BE65ECAE9D79D9B
      002E2DA964FA8F025A658820E67D8CF53E0EC0E3F283EAE8CDFA35933CCF166C
      A465AB2EBE3FA2454026E8F902BC04D061160C699CD2745527F55509E0BE1FFD
      FD3AB6D33F53961E3ED3D203CFF22157A315C1C359794903D70973DEEAE14DE8
      DCDBB4FB41E3CE4F36FD7853FFF83A6A2C688CD58EC95D28E0B6FD9C0478023C
      013E01C0BD4090401428040099B21156DE8CE8CDEBA5B5D768DAD68F5FEBAD12
      840047DA22DFC5F56799E7550EADB7AA2C5AFB94E01308E41C15C1EBCDBDBAB6
      56672FB0BC664C5BC9BCA9A32665DEF4E6610151758875E37B997625E079A626
      070282D64AC95B652EB595D736B586AA76F12667F5AD5F5F56BF7880283F95CD
      975F6517209BCAA34995AF6B3C57DC03B7669DF0132B2DD1F8B1A331A20994A0
      4BD918EBFA5E63CEB7BFDA550E76AABBDACDFB6A947E132A4FD3BAB4EF47F2F7
      CB39FAFDA98E1A33945D478896DA98BAF6755B61023C019E009F27C0FD5AB184
      15424EC28E57BFEE1A8587077A841682CA3B117910729FCCE77A9E0E36D1775A
      2F8D8091900404D2DC0553DDE3CBEDE1A2354ABE9716E4CBADB2D4B6A2719FEA
      C5DF9A08C859494092A956F949E3F375E35E3EF760F700F7E5E673C1C5E72D40
      97B422E5EB05BBA02D40A97CCA53E52B01485600DFA77EFCD08EA5FAF9B20950
      B10EF293885AB4D6AD9587EA34AC1F4B634475D5244566740154D0D4C4499335
      9D5DAEBEF5FDAF4986F2A28D7C9BD23ED1CFC3F76369BCA90DE3188A50D758F7
      6368D89EF7B6B5E736F24B8027C013E0F304B866F55E604BC3436078E1CFE7DE
      F4C88F1781153D8DE5212CF0D67EE4E42D4FDC2878780F6C1062DE24D994B7EE
      89E5F61A24DF916F7C1E7593576F932683A0E67BCAE11D8DBCB393F2918627B8
      AB1CDE998CBF7936D7A86D635F78673EFE569BA93FBC39D6B77569F942F52C95
      576529B58FF2559F4943D7F8F1ED19EB17FB5FDF6BC2C67B69F594CBE7C5DF71
      32A136A8F563D31851F979551D7CFBABACFA0D783F01FFBBF065527D7CB98741
      D58FD5F8FBA10CAA839E232F7D3F86A2052535F004E17204615FEBCCFE96DB25
      830BFF365BB56AD5CC9A356B66D6AD5B37B361C386998D1B37CE6CDAB4698699
      6E1B33E685CC4326E9857C66D3B3A2297EBEE54240973CD2E79B5FE9BE9AFF41
      9BCFA8E5B510F55B887A00F0D276C2519ECD7DD35CAF9645A4B4DC314AF9BA72
      4D6AE039F1E82B842729F7B20078D35AE26208A0B6008ED02D695E6DD6693101
      BE10F56BB3AD6A79C92A319F67A1654FD3A4BD54DA38019E009F04847DBD7759
      007C3E8233EFC913B9720CF4670C24C013E07D85F024E54E80E7F199BD5B1249
      B0F607AC0BD55709F004F82420ECEBBD09F00478023CC740EFC740023C01DE57
      084F52EE04780AEFDE0BEF85D2F2F239DDD5FC13E009F04940D8D77B13E009F0
      04788E81DE8F81047802BCAF109EA4DC09F014DEBD17DEA9197757335EA8BE49
      8027C02701615FEF4D8027C013E039067A3F0612E009F0BE4278927227C05378
      F75E782F949697CFE9AEA69F004F804F02C2BEDE9B004F8027C0730CF47E0C24
      C013E07D85F024E54E80A7F0EEBDF04ECDB8BB9AF142F54D023C013E0908FB7A
      6F023C019E00CF31D0FB3190004F80F715C293943B019EC2BBF7C27BA1B4BC7C
      4E7735FD0478027C1210F6F5DE0478023C019E63A0F76320019E00EF2B842729
      77023C8577EF85776AC6DDD58C17AA6F12E009F04940D8D77B13E009F004788E
      81DE8F81047802BCAF109EA4DC09F014DEBD17DE0BA5E5E573BAABE927C013E0
      9380B0AFF726C013E009F01C03BD1F0309F004785F213C49B913E029BC7B2FBC
      5333EEAE66BC507D93004F804F02C2BEDE9B004F8027C0730CF47E0C24C013E0
      7D85F024E54E80A7F0EEBDF05E282D2F9FD35D4D3F019E009F04847DBD37019E
      004F80E718E8FD18488027C0FB0AE149CA9D004FE1DD7BE19D9A717735E385EA
      9B0478027C1210F6F5DE0478023C019E63A0F76320019E00EF2B84272977023C
      8577EF85F7426979F99CEE6AFA09F004F82420ECEBBD09F00478023CC740EFC7
      40023C01DE57084F52EE04780AEFDE0BEFD48CBBAB192F54DF24C013E09380B0
      AFF726C013E009F01C03BD1F0309F004785F213C49B913E029BC7B2FBC174ACB
      CBE77457D34F8027C02701615FEF4D8027C013E039067A3F0612E009F0BE4278
      927227C05378F75E78A766DC5DCD78A1FA26019E009F04847DBD37019E004F80
      E718E8FD18488027C0FB0AE149CA9D006F10DE679D75D6A9A485D222F239A949
      E61898DF18488027C02701615FEF9D08E0C0ED5DEF7AD73531EDB7DF7E97D604
      D18A152B36357DDF2501F6B18F7DEC8A97BCE425B7FCF0873F3CB3542EEACFF7
      975C72C9890B5D6EDA9C67937876A91F34F9F8EA57BF7AD1EB5FFFFA1B76DD75
      D79BB9EEA4934E5AAFF252B7BDF7DEFB2ABEE31ABEA73ECA9B7B17BA6EC39EA7
      F2E5E46A7EB053FBF25BEC62FF0EEBFFD2F709F004785F213C49B92702382040
      D0F3AA04F48040E94786E00514D75E7BEDF1F3F9912EC63DD407B0959ECD778B
      3519A18D11C082B180EB5FF90E011DBF3BEAA8A3CEA13EBCEA3BFA85A40981FA
      B68B02FE90430E398FB69FF678A0EEB5B13CED674F3B7F263F7DFB2D36B54902
      3C013E0908FB7A6F2B00F73F2C2FF400846001BCF90E6D0F012CAD155008FAD2
      22D10AB986EB95B80EF0F33979F0B9D77C1148FE7A7DAF7BC81BD8C6C903F7C5
      6771AF2F77D4C295B7B45769E8AAA3DA43F992572C9BAEE13BB5896F477D4EDB
      94344DB595EEF1DAB8CF479A3ACFE073EA4F1A45CB264F0F70D5CFB7BBDA82F2
      6A3241FEB4B5DA3BF695CA2110733DF7936FA91FB95F79D3D6B409F5F19611FF
      0C3EE7BDFA9A7C35D9F2E34E7D4C7974BFEF433F96F53DE5E0BE5847DFE67ABE
      EF73F225D126F4097557F97CDBC531E4C789B79CC4F292179FA9CD3579D3FDBE
      EF7C9FAA3E9AD0C567E8BDCAAE76E5B5F4DBF19343DA9CE4F36CFA0D4F326949
      8027C0FB0AE149CA3D55807BC8A0C50204048CA0240D50C21E8DC0C3947B4812
      E0086DF2906088025CA6629E2B0183962A5321AFE4E7214E19642A97C0E61912
      443C43CF948051B9C84FE044C0496B45B0713FF5E16FD55940941094C99AEFB9
      565A25AFBCD78487FB22C46B00F7A674CAABBAA90E12A66AFB9A75817B23C0F9
      8CEB554EDA511618B539D7A8CDE92FF57BA9FCD451E67BFE1674C84BFDAEB616
      6834866833DF4F2A87EAA5FE53DBD296948BE708665EBB56F9D58782BAAEA11C
      8C4D8D07F251FF6B72A4F1C133799EDA5CB0557D79AFE50CEEF16DA776F7D06B
      BA46E5F5E5213F2DEFA8DDFDD8F4E38976A21EB56768AC2A7FDFA7A5FEA27D7C
      9BEB37ABDFAA7E839A6C4C026D7F6F023C013E0908FB7AEF82015C5042084AA8
      2014BD099A1FBBD78A22A4B85760ABAD7F7A6128612EAD4B5AA77F86340FF295
      D0961999F79449825AE0E7197C2E6D82EB299B84AFEAEACBE8C1C075AA8BD7C2
      C8A754460F4D09AD1AC0BDB95C9AAE5F2F17F4049826137109E06A53CA2D98F1
      B7DA3DB6B926105ED80A2E8294CAA2F7D2B005150F513F5EFCF28660E9A1AE09
      86FA5BEF3549F475275FFAB406F00878598434364A20D2A482EFC8DF4F96D476
      F223F15A31796A3CF9FEF6D7286F9557D7FBF2F8DF17CF17A87DB97C7D4B1309
      9E49DEDCA3B11AFBD45B2A4A133FFD5EE2D8680BDEE493004F80F715C293947B
      C1002EA1ED9DD84AA6E9A815F9F708106902B5B5672F84A4917B415182523495
      0AE0DE598AE70A04354127E1C5FD512B8B008F42CFAF39478D9BBC22684735A1
      ABEE5AF3246F04BB8469D33A68A9ADC84F96125942BCF616DB3CD69B6B4B9F79
      A8F8F6F5F5F4708E932D4D5C185F028D9EE32D2ECA3BB6BF4CDBA3025CED2A10
      0F0378EC2F8D159E17C753C9B9ACA6A5C7B6F4EF01AF260DB220F8E7C6BEA88D
      EBDA58559D4B008FA678E5DD34619C04E809F004F82420ECEBBD0B027039AF45
      CD376AE09AA9D7B4CC513CD8C943701F55030790320B4B1B14202400F97E98A9
      51C2319AC4BDA0943540A06B5B0347702AC9AC8F20172804704D86784F9BF9FB
      D4FE3580AB2D4AD0F51A3AF9485BF3101564BCC6EDF38A93A5260DDC6B95B26A
      08E27A8ECCB75103F71AB14CF69AB4691D9B67ABADB456ED2D34A5C9886F3F3D
      7B98065E5A96F040A31C5AFED16498BC9B00AEBA683CAA0FFCC4D94FB8788696
      18742FDFCBE42FFF0959743CC0D50F4D1AB834F969EC1E488027C0FB0AE149CA
      3D5580CB942C2183E090A9528E5DFA4C82DECFDCB5CE2921282F69697EDEA4E8
      A12F2184C041E8F935702F68748FD74EBC46E527184D264F0F3A81481AAFCAE8
      2D105E7BA4AC12B09A78685D586BE05A4B8F02DD6B33DE74AEBFFD9ABEFF5E65
      126CE3BDC3005EF20BF080F1EBA55A778E2661AD856A4D95F7FC1DB78895AC23
      F299F02052BB0BB6DE1CCE779A74C8E2A0FEE03AB5B726570299EAA435FD92F5
      6018C0194372608B6BE0B2D2086C327397C6A8A0A9ED80F29F6802B82C237122
      26BF02BF56EDAFF5CF50F9B5664FD9D43EEA53955FE3CA8F67AD81FB25078DF9
      DAF6CCF968E209F004F82420ECEBBD1301BCE431CB0F559A3642524299CFE5CC
      258F637EA8DEDBDAC39BEFE43D1CBDBCE5B55D9AC993879E491EF21646E878CF
      5F2F24E464A467CA71C77F2EE8AA7EBEAC5E40FA7BA4C5794DC56BFAD27CF92C
      028EF7B22678AF6F3F5191D6A8B2495BD22B6594A7306D42FB97DA581305BE57
      396489284D92A206CDF3E591CFDF3CD3D7494E7DBECDA357F3285ED2FE39D1BB
      D9B7BB3CD9BD05267AA12B2F4D06FD58926F83261F1A0F7E7CA82E4D00579BA8
      FFE4D058DA75A0DD15F2228F10632CF83AC8EB3CFE064B408F7DA8B62FF99CE8
      1994956768BCC63EA56EDEB211C7B07ED771CCA96D3509980FAC4BF724C013E0
      7D85F024E59E08E06DFDF8FA9E4F5C3B1CA53E71DD70947BFC355AB39DD69AA2
      D66CE3B287CAE04DDEE3967DA1AE17CC4A13A051CA304A1F35017C94678C7ACD
      A8636C92F28CFA8C51CBDC745DDB7BF913E009F04940D8D77B13E02DC4C19E8F
      E01B050E6D08CA69E5D10780FB6D6EF3698751FA6812608E53A651C758DC3E36
      CE33B4FD6D9C7BE67B2D7D33DF89556AE009EBBE02B7ED7227C05B00F87C8558
      DE375928D06CBF6C3F8D81D4C013EA6DC3B10FF925C013E0BD3F892A419E204F
      8027C0FB00DCB6CB98004F8027C0730CF47E0C24C013E06DC3B10FF92D5980B3
      7E398DFDA64B55DBC3A3B8E6B0B654EB9CF55A3A9A7B023C01DE07E0B65DC625
      09701C8B96D2494B0B011ADAABCD7DB90B51E67CC6D201F0A47D99004F80B70D
      C73EE43711C0F124F5FB8A15F94B3F467DC7E7FE50859AF7A98F06A6C00F7A25
      4FE5139F13CBC11615054569BA56F7A93C311F9EA9E797BE53997CB97D5D15C0
      230A27DA22EEC7D635B5EFE2F37DBB702FF795EAE1CBC375CAC79759FBF57D39
      7DBDA32543FBCB4B4257E557FEB532F93110FB6152619EF72F3FB027C013E07D
      006EDB659C08E07E0B8D228F11D001E1AC53A878F5B1BC4BE139B957D1A1146D
      8D6D33F1D00B3444698AE42308F872E830073D5F51C3044CCAA7002A7A962264
      F97C808A02792872948F90A52D4A318A1966681D3EA2EFFC611ACAB37440492C
      BB4257FA13C5B84FEDE5F7806B9B91FAC1474353300E0536F151BDE2011EBEDE
      8A44A7E7286F45EAF22677EE53700ED58357AE2995C94783A3ACEAAF5CF6587E
      F06D63C295004F80B70DC73EE4D70AC025A025D075829722390132858D2C01DC
      5FAFF0A702AEDF6FAC309B9A2048CB16A4F5BD80E34363F299BE276F857F54E4
      3169818A73EDE33F7BF04561538A7CA5D09DFA4E31B77D3D633E94CD1F31A9B8
      D53A714B30D78448790B780A07AB002CE42FF02B4A19E5527D35D1909541F5F6
      CF8BF5A67C9A0CC4F22B0EB9CA435D05E951CBD48620CF3C96E70420019E00EF
      0370DB2E632B00D7119F129EF1BD3F5DA904F0F899427B925F0C18A2F7FE7008
      7FFCA4C0AC13B73CA0E2A95ED244A5597B182B8CA9EAA4C867A3025CD7A93CAA
      4B93A358A9EC3180870F2E220D59960A4D50FC33D4F6BE1C2A9BDA32D6DB87DE
      F4160B6FF588EDE021AD36D711AD3E6639DF35952901BC3C013C69BF27C013E0
      6DC3B10FF9B502709985A57D79804BA316144A008FC0F7B0F4C2DF834A7F7B13
      B9CCBF3A7DCBAFB30AD6D2EC811CE5D6894E511355EC690996D2919EF19E61EF
      A94B0DE0B5B24780FBB6D171917E02E39FE1DB3E9E12A60985DA4A1A789CB8E8
      F08AA8F147811BDBC71F0AE3FBD097299E333DA910CFFB972FFC13E009F03E00
      B7ED32B60270996B756A964E9802B27E3D54D0886765EBD432395C79A0CB64CC
      77CA57825A79CB642E20E8301320E2CDE85E9BD77A2DF7504E7F34237FEB4425
      1DE2C0BD25CF76814DA6E99A49DDB7113095893C6AEAB1EC9A78A89CBE6DA471
      53364D9EB89E76F16DEF970FE2B1A972F2531EEA0B5F6F1D63A993DD4ADEEA9A
      20F09D2623EA179DE6C5B37C9F69D9803AFBB56F7F78484279F942799CBE4F80
      27C0DB86631FF26B0DE0328D22A0BD131B2094A01684A5EDE9072A0728397D01
      11014E5BC2F84E0E5C117AD2AA633E0297F2F05A229F79473B3D530E6D3E2F41
      95BA95BCC705A5611AB87C02E43CE74F20AB955DA6699595F71EFE5A6F579BC8
      C14D960ED59DEF81AC77DC933540FD2370AA3E724854BDE5ADAE7E8A6DA1E35F
      F95E4E883C5765D2E4433E136A2F3D4F13ACD8CFE308F2BC7679023F019E00EF
      0370DB2EE34400EF8BB09459D83BB12D54D9A346EEAD07A3ECBB1E7688C5B0EF
      A7514F79E0472BC224CFD2846A923CF2DEE5096FFA3D019E006F1B8E7DC86FC9
      035C0095295D4E590B25EC6B00AF7973C7720D03F4B0EFA7514FDAB2744EF824
      CFA29D3212DCF205F0246327019EF0EE036CA751C6250F7060E3C180F9762141
      510B0033AAC0F2015A4AF70CFB7ED4E7E47509CF3E8F81D4C013E2D30064D7F3
      5CF200EFB350CAB22754730C8C360612E009F0AEC3761AE54B80E74954BD3F89
      2A21371AE496723B25C013E0D30064D7F34C8027C013E039067A3F0612E009F0
      AEC3761AE54B80A7F0EEBDF05ECA9A65D66D34EB42023C013E0D40763DCF0478
      023C019E63A0F76320019E00EF3A6CA751BE04780AEFDE0BEFD45247D3529772
      3B25C013E0D30064D7F34C8027C013E039067A3F0612E009F0AEC3761AE54B80
      A7F0EEBDF05ECA9A65D66D34EB42023C013E0D40763DCF0478023C019E63A0F7
      6320019E00EF3A6CA751BE04780AEFDE0BEFD45247D35297723B25C013E0D300
      64D7F34C8027C013E039067A3F0612E009F0AEC3761AE54B80A7F0EEBDF05ECA
      9A65D66D34EB42023C013E0D40763DCF0478023C019E63A0F76320019E00EF3A
      6CA751BE04780AEFDE0BEFD45247D35297723B25C013E0D30064D7F34C8027C0
      13E039067A3F0612E009F0AEC3761AE54B80A7F0EEBDF05ECA9A65D66D34EB42
      023C013E0D40763DCF0478023C019E63A0F76320019E00EF3A6CA751BE2AC007
      83C17D2D3DC2D2DF5A7AADA50F5BFA9AA56F5B5A9329DB20C7408E810E8D01E4
      12F2093985BC426E21BF90639B599AFD370D219A79E6E461B1C6400DE077B7B1
      7E1F4B0FB3F40C4BFF69694F4BFBCEFD480EB6D74CD9063906720C74650C006F
      E413720A7985DC427E21C7906709F05B12B48B05DA693DB706F0BBD968BFA7A5
      3FB4F41796FED912B35A7E1CCC703F9129DB20C7408E810E8D01E412F2093985
      BC426E21BF9063C8B30478027CC959606A00BFAB8DF67B587A80A5475AFAABB9
      1F05335B7E20FF9529DB20C7408E810E8D01E412F2097823AF905BC82FE418F2
      2C019E005F3600BFB38DF65FB3742F4B0F9CFB3130A3C52CC5DAD24E99B20D72
      0CE418E8D018402E219F9053C01BB985FC428E21CF12E009F06503F05FB1D18E
      D969F3B91F013359CC51AC29E118C20F2453B6418E811C035D1903C825E41372
      0A7905BC915FC831E459023C01BE6C007EA7B9592B839F192C6628D6927008C1
      ABF37732651BE418C831D0A131805C423E21A79057C82DE417DA37F22C019E00
      5F360067B033E899B9F203600D891F03DE9C6CC9C8946D906320C740D7C600F2
      093985BC426E21BF6E8337426D5ADEC0996F7AB82FC618F8FF013EB950B9231E
      32B10000000049454E44AE426082}
    Style.Edges = []
    TabOrder = 9
    Transparent = True
    Visible = False
  end
  object panelCredits: TPanel
    Left = 329
    Top = 240
    Width = 545
    Height = 297
    TabOrder = 3
    Visible = False
    OnClick = Panel3Click
    object Panel3: TPanel
      Left = 16
      Top = 16
      Width = 513
      Height = 265
      BevelOuter = bvNone
      Color = clWhite
      ParentBackground = False
      TabOrder = 0
      OnClick = Panel3Click
      object Image1: TImage
        Left = 41
        Top = 36
        Width = 313
        Height = 89
        Cursor = crHandPoint
        Picture.Data = {
          0A544A504547496D616765E9A10000FFD8FFE000104A46494600010201004800
          480000FFE10F224578696600004D4D002A000000080007011200030000000100
          010000011A00050000000100000062011B0005000000010000006A0128000300
          00000100030000013100020000001B0000007201320002000000140000008D87
          69000400000001000000A4000000D00000001B000000010000001B0000000141
          646F62652050686F746F73686F702043532057696E646F777300323030363A30
          313A33302031353A35323A3531000000000003A00100030000000100010000A0
          0200040000000100000129A00300040000000100000053000000000000000601
          0300030000000100060000011A0005000000010000011E011B00050000000100
          00012601280003000000010002000002010004000000010000012E0202000400
          00000100000DEC0000000000000048000000010000004800000001FFD8FFE000
          104A46494600010201004800480000FFED000C41646F62655F434D0001FFEE00
          0E41646F626500648000000001FFDB0084000C08080809080C09090C110B0A0B
          11150F0C0C0F1518131315131318110C0C0C0C0C0C110C0C0C0C0C0C0C0C0C0C
          0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C010D0B0B0D0E0D100E0E10140E0E
          0E14140E0E0E0E14110C0C0C0C0C11110C0C0C0C0C0C110C0C0C0C0C0C0C0C0C
          0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0CFFC0001108002D00A003012200
          021101031101FFDD0004000AFFC4013F00000105010101010101000000000000
          00030001020405060708090A0B01000105010101010101000000000000000100
          02030405060708090A0B1000010401030204020507060805030C330100021103
          04211231054151611322718132061491A1B14223241552C16233347282D14307
          259253F0E1F163733516A2B283264493546445C2A3743617D255E265F2B384C3
          D375E3F3462794A485B495C4D4E4F4A5B5C5D5E5F55666768696A6B6C6D6E6F6
          37475767778797A7B7C7D7E7F711000202010204040304050607070605350100
          021103213112044151617122130532819114A1B14223C152D1F0332462E17282
          92435315637334F1250616A2B283072635C2D2449354A317644555367465E2F2
          B384C3D375E3F34694A485B495C4D4E4F4A5B5C5D5E5F55666768696A6B6C6D6
          E6F62737475767778797A7B7C7FFDA000C03010002110311003F00F542604AE5
          ECFAECFC9C9B29E83D36DEACCA0EDB725AF6D346EFDDAAEB41F5558FAF99B661
          FD57CE7D44B6CB1ADA411E16B9B5BFFF0003DE8DF57BA5D189D3317158D0194D
          4DD07773807DAFFEB3EC729E1184717B928F19948C21124C63E91C5394B83D5F
          A71535707EB8EECFAFA7757C1B7A4E56418C6F55C2CAAC3FB95E4D70DF51748D
          707090B2BACFD5EC1EAD8EDC7C96B8D6C78B5858ED8F6BDBC398F1F456767F55
          EA3D37EB374DC7B6C07A67526BE92C2D12DC96FB98FF0057E9FE966BF620630C
          95ED8E0970CA5285DC7F57EAFD5FCD2F97F464A7A758F7FD61655F58A9E85E8B
          8BAEC7393EBEE1B4005FFA3D9F4BFC1AA5F5A7ACF50C7774DE9FD2EC1566F52C
          80CF50B5AFDB4B06EC8B363E5BF9CD587D72DCFF00F9ED8AEE9CC63B25F8258C
          377D0634BADDF91606FD3F49BFE0FF00C227E1C1C42E55EAC79270B35C3EDFE9
          CBFABC4A7D01A41129D70B9BD47EB3FD5DF43A866E78EA9D39D636ACBADD4B29
          7561FF0046DA5D57FDF96975FEBDD4BEDD8BD13A2160CECA61BECCAB06F65343
          7FC28AFF003ECB36FB777FE8C4DFBBCAE3C328CA32123EE0E2108FB7FCE71F1C
          6338F07F754F4F213AE1F3B33EB67D5EA9BD4F233BF6B6054E68CBA2CA1B4D8D
          638EDF5687D5FF007E56BEB47D62EA5876747774970B5B9B690EA486FE99A454
          FA6BF55E1DE86FF53F9C6A4396948C44251989F170CC1223C58E3C728CB8E319
          454F5B21290B8ACACCFAD5D33A3753EA39BD42AB6E0C63F19B55405741DE1B6B
          5BEA33F4CCD8FDADF537A58EEFAE9D530EBCC6F52AFA73DF535D4E3D74B2CDFE
          DDCDB72AEB03BD37E47D2F4E966CA1897DDF4E2F771887170717AFE6E18CFF00
          738BF494F6CB33AB75DC6E9AEAE8D8FC9CDC89FB3E2523758F8E5FAFB6AA9BF9
          F6D8A8FD50EBD7F59E8F5E46500DC963DD4DDB4402E647E9037F337B1EB2313A
          8329C0EA3F5A321A6EBB2AC7B696707D163FECD8788C77E631F67BEDDAA0CC25
          8E6612D251E2E23D843E6932E1C625648E2A31846234E3C993E48DBAFF00B5BE
          B311EA7ECEC5039F48E59DFF00D5DE28F477FF00695CE95D769CF7D98F656FC4
          CDA0037625D1BC03F46DADCCF65F43BFD2D6B9FCA1F5B28C0BF36CEA1431F5D4
          FB1D8CDC71B190DDDB2ABCFBFD4AFF0035CFFCF4F9D63D98DD17ADCCE454FC76
          5CFE0BEACA6866431DB7FE11DEA28B888FDED05913E1F97FC165F6A32000F6FD
          44C632C4726992B88465EEFCD193D4F51EAB81D32965F9D70A6AB2C6D2C7104C
          BDFF00419EC0EFA5B55A6BA570BFE305B9EEC7C4B1B90D6E18C8AABFB3967BBE
          D1367A795EB7FA36B7FC0ABFD47AD756E8BD268A5EF6751EB39979C6C67067A6
          C73DC677BAA69FA14D7B7FE31EAEFDDEE188C640CF2194787FBBFE0FE87F94F5
          355EAE4275C5DF4FD7AC4C7766D7D5599B7520D9661BF19ADA9E1BEEB2BAAD60
          6DBDBD9FCDEF5D0F40EB15759E998F9F50D82F6CB984C96B81D96D73F9DB1ED5
          1CF15478E338E48DF0930E2F4CBC78E3053FFFD0EF7EB5F4BB3AB742CBC1ABF9
          DB59354E80D8C22DADB3FCB73362A1F543EB16266E0578D73DB4F50C568A72B1
          AC219635F58F4F7EC7EDF6BF6FF616A75BEBDD2BA354C7F51B7D31738B6A6B5A
          E7B9C47D2DB5D41CEF6FE72E4BA9F58FA81D52DF5736A37DBFE91D8B707C0FDE
          B2B6B1CFFEDAB586139E2E138F24A1C5C709E38F1F0CBE59FF007E32FEF29DAF
          AC9F5A9F8776374DE8A69CBEAD956B582974BDACAFFC25B77A2F66CFF3BF969B
          EBA61599BD06EBF1FF00A4E0B9B998CE1CEFA4EF76DFEBD5EA2C8E9BD77EA2F4
          C9FB0B1D8FBB473998B76E23F75D63DAEB36FF00255FCCFAFF00D0FEC8F660B2
          FCBCA2D2DA31C5160DEF77B58C2E7B0376FEFA78C59633C67161C83DB37C5923
          C2721FEBFE8C20A6A744C96FD60FAC76F59683F66C3C66636302222DB87AD96E
          FEC6E7D69755BF1B13EBFE13F26C6D359C03587BC86B773DD68ADBB9DED6EF5A
          FF0053FA2BBA574AA31AD03D783664471EABF57B7FEB4DD94FF615EEA1F57FA6
          E7E47DA72716ABEEF4FD1DF63771F4F5FD1FBBFAC992CD8C66981671084B0438
          77E0AE1E2FF0BF9C5381FE3032F1EDE8F5749A1CDB73BA85D5328A58439C435D
          B9CF86CFB3F356575EC1C5AFEB2E13FA93DF560E5630C66E432C3506DD54B5AD
          7DEDFA0C7FB7FCF5D5F4DFAA5D1BA6DE6FC2C3AE8B488F504B9C01E431D639FB
          3FB0B4337A56266E33B1B26965F4BBE957600E698E0FF5BF948C3998E3E18C38
          B8009F14B4864BCDC3EA87CFC3C1EDC14F15D63A27D55E9987EB675F95636C21
          8CA2BCA75B65849E2BA777BD17AFD0DC7CCFAAD43039ACAB29AC635E65C1AD6D
          0D6B6C3FE91ADFA6B7707EA6F43C0C81918B835557335659AB8B4F8B3D573F63
          968E4F47C2CA7D165F4B6DB315DEA63BDDCB1FA7BD9FCAF6B52FBC81281E2C99
          0478ECCFBE487B71E18714BFE9A9C8FAE8D03EA7F5081FE0DBFF009F2B57FEAF
          347EC9C231FF0069E9FF00A862BB97D3F1F3311F89955B6EA2C00595BF56B803
          BBDDFDA6A9E362331E96D3500C656D0D63470D6B46D635BFD56A80CC7B231D6A
          26677D3D518C7FEE54F23FE2EFFE48BCFF00DDCBA7FF000341A702DC8E859FD0
          1843737A7DEE15871891EA7DAF0ACFF8BBDBB98D72EB7A7F48C2E9CC755874B3
          1EA738BCB1820173BE93FBFB9DB553EADD0864E4333716D76267D4D2C664D603
          A587FC06452FF66451FC8FCC4DE6A432E49E400D4B8B4FD2E19EECD872701A27
          8758CE32F9847262F93887EE7AA6E3E475BCECFE979180CE9198DCDC8A5F5D9B
          DA19535CE6963DDEBBDDEFFF008367F8455D9998FD5707A5749C476FB9AFC7B3
          35B041A6BC503D6F5E47E8EC75CDF4EB67E7ABF96FFAC5878B7645EFC03563B0
          BDF60AF20BB6B7F3850D7EDDDFC9DEAEE1754C16E28C8EA160C520B5AFB2DDAD
          AEC716FA86DC7B6A7DD4DD4FD37FB2E7FD9FFC3A8A38A73D7D47F4758EFF00DD
          A6439210038631D0F14382723C32ADE5C71F97F75CBFF180437A2E23DFED1F6F
          A1CE27B0FD238AADF5F6961674ECFB77BB0B1B25C32DD518736BBB645AC7B3E8
          FF0037F4D6D67F52FABFD4EFB7A2E73439BEA0A40B4431F6345567E8DED3BAB7
          35D93536AB3F47EA59FCD258957D5BE9D80EC4A2DA2BC325F34871B1AE2E73E9
          B9BB1FEABACFD2D16B1ECFF82B15EC790E3188F04F8B1CA476F4CE19870FCFFA
          33FF0005AAE1DFD03EAC5180ECFBB32FFB286978B1B98E70708D056DDDFA47D9
          F9B5ADFF00AA146155D268774F65D5625DBAEA9991FCE43CFD276AEF6BE37D7F
          C8599FB03EA2D3906D6D587EA336D901C5E00B00B2A70A773EB736C63BD4AFD9
          F417578ADAFD26D953858C780E63DA4169691ED730B7DBB36A6E7C84C0478B2C
          B5E2BCBE91E151B9FF008EA7FFD1EB3EB7D39EDCFE91D4B1316CCD674FBAD7DF
          55247A9B6C60ADAEADAEFA480DFACD98047EC2EA87CF6B3FF24BABBBD28FD220
          7EA8A6F723C1013C625C20F04899C6E3C5297FD3E353CEFF00CE7CCFFCA1EA9F
          E6B3FF00249BFE73E67FE50F54FF00359FF925D1FEA897EA887B987FCCC7FC69
          FF00DF29C167D6DCD6086FD5FEA51FD467FE4D4BFE78E7FF00F3BFD4BFCC67FE
          4D6E7EA897EA88FB98FF00CC8FF1A7FF007CA70FFE78E7FF00F3BFD4BFCC67FE
          4D2FF9E39FFF00CEFF0052FF00319FF935B9FAA25FAA25EE63FF00323FC69FFD
          F29C3FF9E39FFF00CEFF0052FF00319FF934BFE78E7FFF003BFD4BFCC67FE4D6
          E7EA897EA897B98FFCC8FF001A7FF7CA70FF00E78E7FFF003BFD4BFCC67FE4D2
          FF009E39FF00FCEFF52FF319FF00935B9FAA25FAA25EE63FF323FC69FF00DF29
          C3FF009E39FF00FCEFF52FF319FF00934BFE78E7FF00F3BFD4BFCC67FE4D6E7E
          A897EA897B98FF00CC8FF1A7FF007CA79DC9FAD19B9143E91D0FAAD05E205D53
          58D7B4F67B0EE737FCEF62C86646656F75F5749EA75649739DBABC7A5B543D83
          1EDFD5773AADF631AC7FABFE919FE8BF42BB9FD512FD513E3980078708AEBACC
          FDAA788F5EC6B3D3AFA37566D70E6C6CA890C7D14E16D6B9C7E9D3F65A2FA6DF
          F48A2DB6EAEB736BE8FD59BB9D53C835D65A7D363ABB2B7B377BAABDF65997FF
          00873F4BFC85DCFEA897EA88FBC6BF99D3CF229E11AFB598ACA1BD1BAA1752EA
          6CA9EEAAA700EA69FB0FB99B9BBD9651F4FF00E1176FD05ADAFA4E254DA9F406
          D4DFD0D8035ECFE458C67B18F6FF002113F545629F4E3D9C2666C8640030E0FF
          001BFEE94FFFD9FFED13D650686F746F73686F7020332E30003842494D040400
          00000000071C020000020002003842494D0425000000000010460CF28926B856
          DAB09C01A1B0A790773842494D03ED0000000000100047FFB4000200020047FF
          B4000200023842494D042600000000000E000000000000000000003F80000038
          42494D040D000000000004000000783842494D04190000000000040000001E38
          42494D03F3000000000009000000000000000001003842494D040A0000000000
          0100003842494D271000000000000A000100000000000000023842494D03F500
          0000000048002F66660001006C66660006000000000001002F6666000100A199
          9A0006000000000001003200000001005A000000060000000000010035000000
          01002D000000060000000000013842494D03F80000000000700000FFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF03E800000000FFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFF03E800000000FFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFF03E800000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFF03E800003842494D04080000000000100000000100000240000002
          40000000003842494D041E000000000004000000003842494D041A0000000003
          4D00000006000000000000000000000053000001290000000C006C006F006700
          6F0032002D006D00650072006700650064000000010000000000000000000000
          0000000000000000010000000000000000000001290000005300000000000000
          0000000000000000000100000000000000000000000000000000000000100000
          00010000000000006E756C6C0000000200000006626F756E64734F626A630000
          0001000000000000526374310000000400000000546F70206C6F6E6700000000
          000000004C6566746C6F6E67000000000000000042746F6D6C6F6E6700000053
          00000000526768746C6F6E670000012900000006736C69636573566C4C730000
          00014F626A6300000001000000000005736C6963650000001200000007736C69
          636549446C6F6E67000000000000000767726F757049446C6F6E670000000000
          0000066F726967696E656E756D0000000C45536C6963654F726967696E000000
          0D6175746F47656E6572617465640000000054797065656E756D0000000A4553
          6C6963655479706500000000496D672000000006626F756E64734F626A630000
          0001000000000000526374310000000400000000546F70206C6F6E6700000000
          000000004C6566746C6F6E67000000000000000042746F6D6C6F6E6700000053
          00000000526768746C6F6E67000001290000000375726C544558540000000100
          00000000006E756C6C54455854000000010000000000004D7367655445585400
          000001000000000006616C74546167544558540000000100000000000E63656C
          6C54657874497348544D4C626F6F6C010000000863656C6C5465787454455854
          00000001000000000009686F727A416C69676E656E756D0000000F45536C6963
          65486F727A416C69676E0000000764656661756C740000000976657274416C69
          676E656E756D0000000F45536C69636556657274416C69676E00000007646566
          61756C740000000B6267436F6C6F7254797065656E756D0000001145536C6963
          654247436F6C6F7254797065000000004E6F6E6500000009746F704F75747365
          746C6F6E67000000000000000A6C6566744F75747365746C6F6E670000000000
          00000C626F74746F6D4F75747365746C6F6E67000000000000000B7269676874
          4F75747365746C6F6E6700000000003842494D042800000000000C000000013F
          F00000000000003842494D04140000000000040000001C3842494D040C000000
          000E0800000001000000A00000002D000001E00000546000000DEC00180001FF
          D8FFE000104A46494600010201004800480000FFED000C41646F62655F434D00
          01FFEE000E41646F626500648000000001FFDB0084000C08080809080C09090C
          110B0A0B11150F0C0C0F1518131315131318110C0C0C0C0C0C110C0C0C0C0C0C
          0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C010D0B0B0D0E0D100E0E
          10140E0E0E14140E0E0E0E14110C0C0C0C0C11110C0C0C0C0C0C110C0C0C0C0C
          0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0CFFC0001108002D00A0
          03012200021101031101FFDD0004000AFFC4013F000001050101010101010000
          0000000000030001020405060708090A0B010001050101010101010000000000
          0000010002030405060708090A0B1000010401030204020507060805030C3301
          0002110304211231054151611322718132061491A1B14223241552C162333472
          82D14307259253F0E1F163733516A2B283264493546445C2A3743617D255E265
          F2B384C3D375E3F3462794A485B495C4D4E4F4A5B5C5D5E5F55666768696A6B6
          C6D6E6F637475767778797A7B7C7D7E7F7110002020102040403040506070706
          05350100021103213112044151617122130532819114A1B14223C152D1F03324
          62E1728292435315637334F1250616A2B283072635C2D2449354A31764455536
          7465E2F2B384C3D375E3F34694A485B495C4D4E4F4A5B5C5D5E5F55666768696
          A6B6C6D6E6F62737475767778797A7B7C7FFDA000C03010002110311003F00F5
          42604AE5ECFAECFC9C9B29E83D36DEACCA0EDB725AF6D346EFDDAAEB41F5558F
          AF99B661FD57CE7D44B6CB1ADA411E16B9B5BFFF0003DE8DF57BA5D189D33171
          58D0194D4DD07773807DAFFEB3EC729E1184717B928F19948C21124C63E91C53
          94B83D5FA71535707EB8EECFAFA7757C1B7A4E56418C6F55C2CAAC3FB95E4D70
          DF51748D707090B2BACFD5EC1EAD8EDC7C96B8D6C78B5858ED8F6BDBC398F1F4
          56767F55EA3D37EB374DC7B6C07A67526BE92C2D12DC96FB98FF0057E9FE966B
          F620630C95ED8E0970CA5285DC7F57EAFD5FCD2F97F464A7A758F7FD61655F58
          A9E85E8B8BAEC7393EBEE1B4005FFA3D9F4BFC1AA5F5A7ACF50C7774DE9FD2EC
          1566F52C80CF50B5AFDB4B06EC8B363E5BF9CD587D72DCFF00F9ED8AEE9CC63B
          25F8258C377D0634BADDF91606FD3F49BFE0FF00C227E1C1C42E55EAC79270B3
          5C3EDFE9CBFABC4A7D01A41129D70B9BD47EB3FD5DF43A866E78EA9D39D636AC
          BADD4B297561FF0046DA5D57FDF96975FEBDD4BEDD8BD13A2160CECA61BECCAB
          06F653437FC28AFF003ECB36FB777FE8C4DFBBCAE3C328CA32123EE0E2108FB7
          FCE71F1C6338F07F754F4F213AE1F3B33EB67D5EA9BD4F233BF6B6054E68CBA2
          CA1B4D8D638EDF5687D5FF007E56BEB47D62EA5876747774970B5B9B690EA486
          FE99A454FA6BF55E1DE86FF53F9C6A4396948C44251989F170CC1223C58E3C72
          8CB8E319454F5B21290B8ACACCFAD5D33A3753EA39BD42AB6E0C63F19B554057
          41DE1B6B5BEA33F4CCD8FDADF537A58EEFAE9D530EBCC6F52AFA73DF535D4E3D
          74B2CDFEDDCDB72AEB03BD37E47D2F4E966CA1897DDF4E2F771887170717AFE6
          E18CFF00738BF494F6CB33AB75DC6E9AEAE8D8FC9CDC89FB3E2523758F8E5FAF
          B6AA9BF9F6D8A8FD50EBD7F59E8F5E46500DC963DD4DDB4402E647E9037F337B
          1EB2313A8329C0EA3F5A321A6EBB2AC7B696707D163FECD8788C77E631F67BED
          DAA0CC258E6612D251E2E23D843E6932E1C625648E2A31846234E3C993E48DBA
          FF00B5BEB311EA7ECEC5039F48E59DFF00D5DE28F477FF00695CE95D769CF7D9
          8F656FC4CDA0037625D1BC03F46DADCCF65F43BFD2D6B9FCA1F5B28C0BF36CEA
          1431F5D4FB1D8CDC71B190DDDB2ABCFBFD4AFF0035CFFCF4F9D63D98DD17ADCC
          E454FC765CFE0BEACA6866431DB7FE11DEA28B888FDED05913E1F97FC165F6A3
          2000F6FD44C632C4726992B88465EEFCD193D4F51EAB81D32965F9D70A6AB2C6
          D2C7104CBDFF00419EC0EFA5B55A6BA570BFE305B9EEC7C4B1B90D6E18C8AABF
          B3967BBED1367A795EB7FA36B7FC0ABFD47AD756E8BD268A5EF6751EB39979C6
          C67067A6C73DC677BAA69FA14D7B7FE31EAEFDDEE188C640CF2194787FBBFE0F
          E87F94F5355EAE4275C5DF4FD7AC4C7766D7D5599B7520D9661BF19ADA9E1BEE
          B2BAAD606DBDBD9FCDEF5D0F40EB15759E998F9F50D82F6CB984C96B81D96D73
          F9DB1ED51CF15478E338E48DF0930E2F4CBC78E3053FFFD0EF7EB5F4BB3AB742
          CBC1ABF9DB59354E80D8C22DADB3FCB73362A1F543EB16266E0578D73DB4F50C
          568A72B1AC219635F58F4F7EC7EDF6BF6FF616A75BEBDD2BA354C7F51B7D3173
          8B6A6B5AE7B9C47D2DB5D41CEF6FE72E4BA9F58FA81D52DF5736A37DBFE91D8B
          707C0FDEB2B6B1CFFEDAB586139E2E138F24A1C5C709E38F1F0CBE59FF007E32
          FEF29DAFAC9F5A9F8776374DE8A69CBEAD956B582974BDACAFFC25B77A2F66CF
          F3BF969BEBA61599BD06EBF1FF00A4E0B9B998CE1CEFA4EF76DFEBD5EA2C8E9B
          D77EA2F4C9FB0B1D8FBB473998B76E23F75D63DAEB36FF00255FCCFAFF00D0FE
          C8F660B2FCBCA2D2DA31C5160DEF77B58C2E7B0376FEFA78C59633C67161C83D
          B37C5923C2721FEBFE8C20A6A744C96FD60FAC76F59683F66C3C66636302222D
          B87AD96EFEC6E7D69755BF1B13EBFE13F26C6D359C03587BC86B773DD68ADBB9
          DED6EF5AFF0053FA2BBA574AA31AD03D783664471EABF57B7FEB4DD94FF615EE
          A1F57FA6E7E47DA72716ABEEF4FD1DF63771F4F5FD1FBBFAC992CD8C66981671
          084B043877E0AE1E2FF0BF9C5381FE3032F1EDE8F5749A1CDB73BA85D5328A58
          439C435DB9CF86CFB3F356575EC1C5AFEB2E13FA93DF560E5630C66E432C3506
          DD54B5AD7DEDFA0C7FB7FCF5D5F4DFAA5D1BA6DE6FC2C3AE8B488F504B9C01E4
          31D639FB3FB0B4337A56266E33B1B26965F4BBE957600E698E0FF5BF948C3998
          E3E18C38B8009F14B4864BCDC3EA87CFC3C1EDC14F15D63A27D55E9987EB675F
          95636C218CA2BCA75B65849E2BA777BD17AFD0DC7CCFAAD43039ACAB29AC635E
          65C1AD6D0D6B6C3FE91ADFA6B7707EA6F43C0C81918B835557335659AB8B4F8B
          3D573F63968E4F47C2CA7D165F4B6DB315DEA63BDDCB1FA7BD9FCAF6B52FBC81
          281E2C990478ECCFBE487B71E18714BFE9A9C8FAE8D03EA7F5081FE0DBFF009F
          2B57FEAF347EC9C231FF0069E9FF00A862BB97D3F1F3311F89955B6EA2C00595
          BF56B803BBDDFDA6A9E362331E96D3500C656D0D63470D6B46D635BFD56A80CC
          7B231D6A26677D3D518C7FEE54F23FE2EFFE48BCFF00DDCBA7FF000341A702DC
          8E859FD01843737A7DEE15871891EA7DAF0ACFF8BBDBB98D72EB7A7F48C2E9CC
          755874B31EA738BCB1820173BE93FBFB9DB553EADD0864E4333716D76267D4D2
          C664D603A587FC06452FF66451FC8FCC4DE6A432E49E400D4B8B4FD2E19EECD8
          72701A278758CE32F9847262F93887EE7AA6E3E475BCECFE979180CE9198DCDC
          8A5F5D9BDA19535CE6963DDEBBDDEFFF008367F8455D9998FD5707A5749C476F
          B9AFC7B335B041A6BC503D6F5E47E8EC75CDF4EB67E7ABF96FFAC5878B7645EF
          C03563B0BDF60AF20BB6B7F3850D7EDDDFC9DEAEE1754C16E28C8EA160C520B5
          AFB2DDADAEC716FA86DC7B6A7DD4DD4FD37FB2E7FD9FFC3A8A38A73D7D47F475
          8EFF00DDA6439210038631D0F14382723C32ADE5C71F97F75CBFF180437A2E23
          DFED1F6FA1CE27B0FD238AADF5F6961674ECFB77BB0B1B25C32DD518736BBB64
          5AC7B3E8FF0037F4D6D67F52FABFD4EFB7A2E73439BEA0A40B4431F6345567E8
          DED3BAB735D93536AB3F47EA59FCD258957D5BE9D80EC4A2DA2BC325F34871B1
          AE2E73E9B9BB1FEABACFD2D16B1ECFF82B15EC790E3188F04F8B1CA476F4CE19
          870FCFFA33FF0005AAE1DFD03EAC5180ECFBB32FFB286978B1B98E70708D056D
          DDFA47D9F9B5ADFF00AA146155D268774F65D5625DBAEA9991FCE43CFD276AEF
          6BE37D7FC8599FB03EA2D3906D6D587EA336D901C5E00B00B2A70A773EB736C6
          3BD4AFD9F417578ADAFD26D953858C780E63DA4169691ED730B7DBB36A6E7C84
          C0478B2CB5E2BCBE91E151B9FF008EA7FFD1EB3EB7D39EDCFE91D4B1316CCD67
          4FBAD7DF55247A9B6C60ADAEADAEFA480DFACD98047EC2EA87CF6B3FF24BABBB
          D28FD2207EA8A6F723C1013C625C20F04899C6E3C5297FD3E353CEFF00CE7CCF
          FCA1EA9FE6B3FF00249BFE73E67FE50F54FF00359FF925D1FEA897EA887B987F
          CCC7FC69FF00DF29C167D6DCD6086FD5FEA51FD467FE4D4BFE78E7FF00F3BFD4
          BFCC67FE4D6E7EA897EA88FB98FF00CC8FF1A7FF007CA70FFE78E7FF00F3BFD4
          BFCC67FE4D2FF9E39FFF00CEFF0052FF00319FF935B9FAA25FAA25EE63FF0032
          3FC69FFDF29C3FF9E39FFF00CEFF0052FF00319FF934BFE78E7FFF003BFD4BFC
          C67FE4D6E7EA897EA897B98FFCC8FF001A7FF7CA70FF00E78E7FFF003BFD4BFC
          C67FE4D2FF009E39FF00FCEFF52FF319FF00935B9FAA25FAA25EE63FF323FC69
          FF00DF29C3FF009E39FF00FCEFF52FF319FF00934BFE78E7FF00F3BFD4BFCC67
          FE4D6E7EA897EA897B98FF00CC8FF1A7FF007CA79DC9FAD19B9143E91D0FAAD0
          5E205D5358D7B4F67B0EE737FCEF62C86646656F75F5749EA75649739DBABC7A
          5B543D831EDFD5773AADF631AC7FABFE919FE8BF42BB9FD512FD513E39800787
          08AEBACCFDAA788F5EC6B3D3AFA37566D70E6C6CA890C7D14E16D6B9C7E9D3F6
          5A2FA6DFF48A2DB6EAEB736BE8FD59BB9D53C835D65A7D363ABB2B7B377BAABD
          F65997FF00873F4BFC85DCFEA897EA88FBC6BF99D3CF229E11AFB598ACA1BD1B
          AA1752EA6CA9EEAAA700EA69FB0FB99B9BBD9651F4FF00E1176FD05ADAFA4E25
          4DA9F406D4DFD0D8035ECFE458C67B18F6FF002113F545629F4E3D9C2666C864
          0030E0FF001BFEE94FFFD93842494D042100000000005300000001010000000F
          00410064006F00620065002000500068006F0074006F00730068006F00700000
          001200410064006F00620065002000500068006F0074006F00730068006F0070
          00200043005300000001003842494D04060000000000070008000000010100FF
          E11933687474703A2F2F6E732E61646F62652E636F6D2F7861702F312E302F00
          3C3F787061636B657420626567696E3D27EFBBBF272069643D2757354D304D70
          43656869487A7265537A4E54637A6B633964273F3E0A3C783A786D706D657461
          20786D6C6E733A783D2761646F62653A6E733A6D6574612F2720783A786D7074
          6B3D27584D5020746F6F6C6B697420332E302D32382C206672616D65776F726B
          20312E36273E0A3C7264663A52444620786D6C6E733A7264663D27687474703A
          2F2F7777772E77332E6F72672F313939392F30322F32322D7264662D73796E74
          61782D6E73232720786D6C6E733A69583D27687474703A2F2F6E732E61646F62
          652E636F6D2F69582F312E302F273E0A0A203C7264663A446573637269707469
          6F6E207264663A61626F75743D27757569643A39303336316636652D39313937
          2D313164612D613063652D613133666132396632383264270A2020786D6C6E73
          3A657869663D27687474703A2F2F6E732E61646F62652E636F6D2F657869662F
          312E302F273E0A20203C657869663A436F6C6F7253706163653E313C2F657869
          663A436F6C6F7253706163653E0A20203C657869663A506978656C5844696D65
          6E73696F6E3E3239373C2F657869663A506978656C5844696D656E73696F6E3E
          0A20203C657869663A506978656C5944696D656E73696F6E3E38333C2F657869
          663A506978656C5944696D656E73696F6E3E0A203C2F7264663A446573637269
          7074696F6E3E0A0A203C7264663A4465736372697074696F6E207264663A6162
          6F75743D27757569643A39303336316636652D393139372D313164612D613063
          652D613133666132396632383264270A2020786D6C6E733A7064663D27687474
          703A2F2F6E732E61646F62652E636F6D2F7064662F312E332F273E0A203C2F72
          64663A4465736372697074696F6E3E0A0A203C7264663A446573637269707469
          6F6E207264663A61626F75743D27757569643A39303336316636652D39313937
          2D313164612D613063652D613133666132396632383264270A2020786D6C6E73
          3A70686F746F73686F703D27687474703A2F2F6E732E61646F62652E636F6D2F
          70686F746F73686F702F312E302F273E0A20203C70686F746F73686F703A4869
          73746F72793E3C2F70686F746F73686F703A486973746F72793E0A203C2F7264
          663A4465736372697074696F6E3E0A0A203C7264663A4465736372697074696F
          6E207264663A61626F75743D27757569643A39303336316636652D393139372D
          313164612D613063652D613133666132396632383264270A2020786D6C6E733A
          746966663D27687474703A2F2F6E732E61646F62652E636F6D2F746966662F31
          2E302F273E0A20203C746966663A4F7269656E746174696F6E3E313C2F746966
          663A4F7269656E746174696F6E3E0A20203C746966663A585265736F6C757469
          6F6E3E32372F313C2F746966663A585265736F6C7574696F6E3E0A20203C7469
          66663A595265736F6C7574696F6E3E32372F313C2F746966663A595265736F6C
          7574696F6E3E0A20203C746966663A5265736F6C7574696F6E556E69743E333C
          2F746966663A5265736F6C7574696F6E556E69743E0A203C2F7264663A446573
          6372697074696F6E3E0A0A203C7264663A4465736372697074696F6E20726466
          3A61626F75743D27757569643A39303336316636652D393139372D313164612D
          613063652D613133666132396632383264270A2020786D6C6E733A7861703D27
          687474703A2F2F6E732E61646F62652E636F6D2F7861702F312E302F273E0A20
          203C7861703A437265617465446174653E323030362D30312D33305431353A35
          323A35312B30323A30303C2F7861703A437265617465446174653E0A20203C78
          61703A4D6F64696679446174653E323030362D30312D33305431353A35323A35
          312B30323A30303C2F7861703A4D6F64696679446174653E0A20203C7861703A
          4D65746164617461446174653E323030362D30312D33305431353A35323A3531
          2B30323A30303C2F7861703A4D65746164617461446174653E0A20203C786170
          3A43726561746F72546F6F6C3E41646F62652050686F746F73686F7020435320
          57696E646F77733C2F7861703A43726561746F72546F6F6C3E0A203C2F726466
          3A4465736372697074696F6E3E0A0A203C7264663A4465736372697074696F6E
          207264663A61626F75743D27757569643A39303336316636652D393139372D31
          3164612D613063652D613133666132396632383264270A2020786D6C6E733A73
          745265663D27687474703A2F2F6E732E61646F62652E636F6D2F7861702F312E
          302F73547970652F5265736F7572636552656623270A2020786D6C6E733A7861
          704D4D3D27687474703A2F2F6E732E61646F62652E636F6D2F7861702F312E30
          2F6D6D2F273E0A20203C7861704D4D3A4465726976656446726F6D207264663A
          7061727365547970653D275265736F75726365273E0A2020203C73745265663A
          696E7374616E636549443E757569643A39303336316636612D393139372D3131
          64612D613063652D6131336661323966323832643C2F73745265663A696E7374
          616E636549443E0A2020203C73745265663A646F63756D656E7449443E61646F
          62653A646F6369643A70686F746F73686F703A36636631333631332D39313937
          2D313164612D613063652D6131336661323966323832643C2F73745265663A64
          6F63756D656E7449443E0A20203C2F7861704D4D3A4465726976656446726F6D
          3E0A20203C7861704D4D3A446F63756D656E7449443E61646F62653A646F6369
          643A70686F746F73686F703A39303336316636642D393139372D313164612D61
          3063652D6131336661323966323832643C2F7861704D4D3A446F63756D656E74
          49443E0A203C2F7264663A4465736372697074696F6E3E0A0A203C7264663A44
          65736372697074696F6E207264663A61626F75743D27757569643A3930333631
          6636652D393139372D313164612D613063652D61313366613239663238326427
          0A2020786D6C6E733A64633D27687474703A2F2F7075726C2E6F72672F64632F
          656C656D656E74732F312E312F273E0A20203C64633A666F726D61743E696D61
          67652F6A7065673C2F64633A666F726D61743E0A203C2F7264663A4465736372
          697074696F6E3E0A0A3C2F7264663A5244463E0A3C2F783A786D706D6574613E
          0A20202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          20202020200A2020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          202020202020202020200A202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020200A20202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          20202020202020202020202020202020202020200A2020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          202020202020202020202020202020202020202020202020200A202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020200A20
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020200A20202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          20202020202020200A2020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          202020202020202020202020200A202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020200A20202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          20202020202020202020202020202020202020202020200A2020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          202020202020202020202020202020202020202020202020202020200A202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          200A202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020200A20202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          20202020202020202020200A2020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          202020202020202020202020202020200A202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020200A20202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          20202020202020202020202020202020202020202020202020200A2020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          202020202020202020202020202020202020202020202020202020202020200A
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          202020200A202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020200A20202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          20202020202020202020202020200A2020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          202020202020202020202020202020202020200A202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020200A20202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          20202020202020202020202020202020202020202020202020202020200A2020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          20200A2020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          202020202020200A202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020200A20202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          20202020202020202020202020202020200A2020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          202020202020202020202020202020202020202020200A202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020200A20202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          0A20202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          20202020200A2020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          202020202020202020200A202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020200A20202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          20202020202020202020202020202020202020200A2020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          202020202020202020202020202020202020202020202020200A202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020200A20
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020200A20202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          20202020202020200A2020202020202020202020202020202020202020202020
          2020202020202020202020202020202020202020202020202020202020202020
          0A3C3F787061636B657420656E643D2777273F3EFFE20C584943435F50524F46
          494C4500010100000C484C696E6F021000006D6E74725247422058595A2007CE
          00020009000600310000616373704D5346540000000049454320735247420000
          000000000000000000000000F6D6000100000000D32D48502020000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000001163707274000001500000003364657363000001840000
          006C77747074000001F000000014626B707400000204000000147258595A0000
          0218000000146758595A0000022C000000146258595A0000024000000014646D
          6E640000025400000070646D6464000002C400000088767565640000034C0000
          008676696577000003D4000000246C756D69000003F8000000146D6561730000
          040C0000002474656368000004300000000C725452430000043C0000080C6754
          52430000043C0000080C625452430000043C0000080C7465787400000000436F
          70797269676874202863292031393938204865776C6574742D5061636B617264
          20436F6D70616E79000064657363000000000000001273524742204945433631
          3936362D322E3100000000000000000000001273524742204945433631393636
          2D322E3100000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000058595A20000000000000
          F35100010000000116CC58595A20000000000000000000000000000000005859
          5A200000000000006FA2000038F50000039058595A2000000000000062990000
          B785000018DA58595A2000000000000024A000000F840000B6CF646573630000
          00000000001649454320687474703A2F2F7777772E6965632E63680000000000
          0000000000001649454320687474703A2F2F7777772E6965632E636800000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000064657363000000000000002E4945432036313936362D
          322E312044656661756C742052474220636F6C6F7572207370616365202D2073
          52474200000000000000000000002E4945432036313936362D322E3120446566
          61756C742052474220636F6C6F7572207370616365202D207352474200000000
          00000000000000000000000000000000000064657363000000000000002C5265
          666572656E63652056696577696E6720436F6E646974696F6E20696E20494543
          36313936362D322E3100000000000000000000002C5265666572656E63652056
          696577696E6720436F6E646974696F6E20696E2049454336313936362D322E31
          0000000000000000000000000000000000000000000000000000766965770000
          00000013A4FE00145F2E0010CF140003EDCC0004130B00035C9E000000015859
          5A2000000000004C09560050000000571FE76D65617300000000000000010000
          00000000000000000000000000000000028F0000000273696720000000004352
          542063757276000000000000040000000005000A000F00140019001E00230028
          002D00320037003B00400045004A004F00540059005E00630068006D00720077
          007C00810086008B00900095009A009F00A400A900AE00B200B700BC00C100C6
          00CB00D000D500DB00E000E500EB00F000F600FB01010107010D01130119011F
          0125012B01320138013E0145014C0152015901600167016E0175017C0183018B
          0192019A01A101A901B101B901C101C901D101D901E101E901F201FA0203020C
          0214021D0226022F02380241024B0254025D02670271027A0284028E029802A2
          02AC02B602C102CB02D502E002EB02F50300030B03160321032D03380343034F
          035A03660372037E038A039603A203AE03BA03C703D303E003EC03F904060413
          0420042D043B0448045504630471047E048C049A04A804B604C404D304E104F0
          04FE050D051C052B053A05490558056705770586059605A605B505C505D505E5
          05F6060606160627063706480659066A067B068C069D06AF06C006D106E306F5
          07070719072B073D074F076107740786079907AC07BF07D207E507F8080B081F
          08320846085A086E0882089608AA08BE08D208E708FB09100925093A094F0964
          0979098F09A409BA09CF09E509FB0A110A270A3D0A540A6A0A810A980AAE0AC5
          0ADC0AF30B0B0B220B390B510B690B800B980BB00BC80BE10BF90C120C2A0C43
          0C5C0C750C8E0CA70CC00CD90CF30D0D0D260D400D5A0D740D8E0DA90DC30DDE
          0DF80E130E2E0E490E640E7F0E9B0EB60ED20EEE0F090F250F410F5E0F7A0F96
          0FB30FCF0FEC1009102610431061107E109B10B910D710F511131131114F116D
          118C11AA11C911E81207122612451264128412A312C312E31303132313431363
          138313A413C513E5140614271449146A148B14AD14CE14F01512153415561578
          159B15BD15E0160316261649166C168F16B216D616FA171D17411765178917AE
          17D217F7181B18401865188A18AF18D518FA19201945196B199119B719DD1A04
          1A2A1A511A771A9E1AC51AEC1B141B3B1B631B8A1BB21BDA1C021C2A1C521C7B
          1CA31CCC1CF51D1E1D471D701D991DC31DEC1E161E401E6A1E941EBE1EE91F13
          1F3E1F691F941FBF1FEA20152041206C209820C420F0211C2148217521A121CE
          21FB22272255228222AF22DD230A23382366239423C223F0241F244D247C24AB
          24DA250925382568259725C725F726272657268726B726E827182749277A27AB
          27DC280D283F287128A228D429062938296B299D29D02A022A352A682A9B2ACF
          2B022B362B692B9D2BD12C052C392C6E2CA22CD72D0C2D412D762DAB2DE12E16
          2E4C2E822EB72EEE2F242F5A2F912FC72FFE3035306C30A430DB3112314A3182
          31BA31F2322A3263329B32D4330D3346337F33B833F1342B3465349E34D83513
          354D358735C235FD3637367236AE36E937243760379C37D738143850388C38C8
          39053942397F39BC39F93A363A743AB23AEF3B2D3B6B3BAA3BE83C273C653CA4
          3CE33D223D613DA13DE03E203E603EA03EE03F213F613FA23FE24023406440A6
          40E74129416A41AC41EE4230427242B542F7433A437D43C044034447448A44CE
          45124555459A45DE4622466746AB46F04735477B47C04805484B489148D7491D
          496349A949F04A374A7D4AC44B0C4B534B9A4BE24C2A4C724CBA4D024D4A4D93
          4DDC4E254E6E4EB74F004F494F934FDD5027507150BB51065150519B51E65231
          527C52C75313535F53AA53F65442548F54DB5528557555C2560F565C56A956F7
          5744579257E0582F587D58CB591A596959B85A075A565AA65AF55B455B955BE5
          5C355C865CD65D275D785DC95E1A5E6C5EBD5F0F5F615FB36005605760AA60FC
          614F61A261F56249629C62F06343639763EB6440649464E9653D659265E7663D
          669266E8673D679367E9683F689668EC6943699A69F16A486A9F6AF76B4F6BA7
          6BFF6C576CAF6D086D606DB96E126E6B6EC46F1E6F786FD1702B708670E0713A
          719571F0724B72A67301735D73B87414747074CC7528758575E1763E769B76F8
          775677B37811786E78CC792A798979E77A467AA57B047B637BC27C217C817CE1
          7D417DA17E017E627EC27F237F847FE5804780A8810A816B81CD8230829282F4
          835783BA841D848084E3854785AB860E867286D7873B879F8804886988CE8933
          899989FE8A648ACA8B308B968BFC8C638CCA8D318D988DFF8E668ECE8F368F9E
          9006906E90D6913F91A89211927A92E3934D93B69420948A94F4955F95C99634
          969F970A977597E0984C98B89924999099FC9A689AD59B429BAF9C1C9C899CF7
          9D649DD29E409EAE9F1D9F8B9FFAA069A0D8A147A1B6A226A296A306A376A3E6
          A456A4C7A538A5A9A61AA68BA6FDA76EA7E0A852A8C4A937A9A9AA1CAA8FAB02
          AB75ABE9AC5CACD0AD44ADB8AE2DAEA1AF16AF8BB000B075B0EAB160B1D6B24B
          B2C2B338B3AEB425B49CB513B58AB601B679B6F0B768B7E0B859B8D1B94AB9C2
          BA3BBAB5BB2EBBA7BC21BC9BBD15BD8FBE0ABE84BEFFBF7ABFF5C070C0ECC167
          C1E3C25FC2DBC358C3D4C451C4CEC54BC5C8C646C6C3C741C7BFC83DC8BCC93A
          C9B9CA38CAB7CB36CBB6CC35CCB5CD35CDB5CE36CEB6CF37CFB8D039D0BAD13C
          D1BED23FD2C1D344D3C6D449D4CBD54ED5D1D655D6D8D75CD7E0D864D8E8D96C
          D9F1DA76DAFBDB80DC05DC8ADD10DD96DE1CDEA2DF29DFAFE036E0BDE144E1CC
          E253E2DBE363E3EBE473E4FCE584E60DE696E71FE7A9E832E8BCE946E9D0EA5B
          EAE5EB70EBFBEC86ED11ED9CEE28EEB4EF40EFCCF058F0E5F172F1FFF28CF319
          F3A7F434F4C2F550F5DEF66DF6FBF78AF819F8A8F938F9C7FA57FAE7FB77FC07
          FC98FD29FDBAFE4BFEDCFF6DFFFFFFEE000E41646F626500644000000001FFDB
          0084000101010101010101010101010101010101010101010101010101010101
          0101010101010101010101010101020202020202020202020203030303030303
          0303030101010101010101010101020201020203030303030303030303030303
          0303030303030303030303030303030303030303030303030303030303030303
          03030303FFC00011080053012903011100021101031101FFDD00040026FFC401
          A20000000602030100000000000000000000070806050409030A0201000B0100
          000603010101000000000000000000060504030702080109000A0B1000020103
          0401030302030303020609750102030411051206210713220008311441322315
          09514216612433175271811862912543A1B1F02634720A19C1D13527E1533682
          F192A24454734546374763285556571AB2C2D2E2F2648374938465A3B3C3D3E3
          293866F3752A393A48494A58595A6768696A767778797A85868788898A949596
          9798999AA4A5A6A7A8A9AAB4B5B6B7B8B9BAC4C5C6C7C8C9CAD4D5D6D7D8D9DA
          E4E5E6E7E8E9EAF4F5F6F7F8F9FA110002010302040403050404040606056D01
          0203110421120531060022134151073261147108428123911552A162163309B1
          24C1D14372F017E18234259253186344F1A2B226351954364564270A73839346
          74C2D2E2F255657556378485A3B3C3D3E3F3291A94A4B4C4D4E4F495A5B5C5D5
          E5F52847576638768696A6B6C6D6E6F667778797A7B7C7D7E7F7485868788898
          A8B8C8D8E8F839495969798999A9B9C9D9E9F92A3A4A5A6A7A8A9AAABACADAEA
          FAFFDA000C03010002110311003F00DFE3DFBAF75C1E4541C903FD736F7EEBDD
          27727B96831704D5355534F4D4D4F1992A2A6A668A9E9A9D05C979EA26758614
          007259801EF68AD23AC71A16958D02804B13E800A93F90EB4485059880BF3E80
          EACF955D134553352D4773754C13C123453412F616D44961950D9A3910E56EAC
          A7EA3D88D39379C6545923E52DCD91854116D3508F97674D7D4DBFFBFD7F6F42
          5603B2F6EEE4A48B2183CD627398E974E8C861B254595A17D5C8D35743354404
          906F6D40FB22BAB4BBB198DBDF5A4B05C0E2B223237FBCB007A715D1C551C11F
          235E9794B9082A5414756BFF0088FF001FF1E4DBDB1D5BA70F7EEBDD7BDFBAF7
          5EF7EEBDD42ABAC8E9519E4655550599999554002E4B3310140FCDFE9EFC0124
          0033D7B871E9820DDB8AAA94C34B90C7D548BCB2D2D7D1D4BA8FA5D920964651
          7E2E4017F6E3C53462B242EA3FA4AC3FC207550E8D85704FC88E9474F50B3A82
          A4107FA1FF000BFB6FAB7527DFBAF75EF7EEBDD7BDFBAF75EF7EEBDD7BDFBAF7
          5EF7EEBDD7BDFBAF75EF7EEBDD7BDFBAF75EF7EEBDD7BDFBAF75EF7EEBDD7177
          5417240FF5CDBDFBAF74C190CFD250C72C924D1C71C28D24B248EB1471225CBC
          92C921091C6AA2E5988007BF70049E03AF0A92140AB134006493E80799E8A7EF
          1F9DBF16B646524C2EE6F903D4D87CAC5ABCB4336F0C5CF3C454E92B2FD9CB52
          91B83C59883EC3379CE9CA1B7CC6DEF799ACA398791956BFC89E879B6FB5BEE5
          6F16CB79B5F226EB35A9E0C2DDC03F66A02BD66D93F37BE35F61E486276677AF
          56EE2CA16455C750EEFC5255C8CE6C8B04355353B4ECC78B26A3FE1EDCB0E6EE
          55DD25F036FE63B29A6FE15956BF902457F2E99DDFDB5F70F6083EAB79E48DCE
          DEDBF8DA07D229EA541A7E74E8CFD06E7A6ABD0352DDC060750F503F420FD0A9
          1C82383EC45D020107874A68A649402A41BFF43EFDD6FAC8CEABF523FDB8F7EE
          BDD6313C6780C2E3FC47FC57DFBAF7594107906FEFDD7BAEFDFBAF75EF7EEBDD
          7BDFBAF75EF7EEBDD7BDFBAF75FFD0DFD98E9524F161EFDD7BA29DF2D7E5175E
          FC51E99DE9DD1D915B243B776950868F1F4853F8A6E0CD55B1A7C3EDDC4239D2
          F91CA56304527D31A06722CA6E2FE43E49DEBDC4E69DAF94F618C1BEB96CB37C
          1146B9795E9F8507971268A38F5491CA0145AB93403D4FFAB24F90EB4E3ACDFF
          00F3EFF9CB763E7AA62DE157D53F1EB05957A538EA0AEC9E2F616DBA69599E97
          18F4D8F961ADDF9BBDE92C65690B589B9F1A103DF45AEA0F657EEA5CBF666E2C
          05FF0039CF1D5495592EE76186605BB6DE00787051C3B8F542891300EA26BCA5
          73F0A0FF0020FC8B1E8D5623F907750CB8E84E5BBB3B4EBB2A63BD656D16136F
          D2D1CB395BB49053D449533AC77FA0790B1FC9F70FDC7DF8F9A0CEE6CB912C12
          D2BDA1E790B53E65500AFD829D381EE683BA31F2D24FFCFC3A2B9DB7F13BE697
          F2B4A81DF1F1C3BAF70EE9EB4C1D640DB929E35AC846328E69D638A2DEFB267A
          9ACC3E5B0350C56392A61B88D985F4120FB95793BDDAF69BEF208DC93CF1CA71
          5AF31CA87C357D275902A5ADAE005759145484346A70D401EA9447755B88C248
          4D1644C67D0F98AFA1A83C3AD917F96B7F305C1FCD3E9AA4DE5253D2603B036D
          D5C3B7BB2B6A534CF25362F39E01353E5319E4BCBFC133D0299600C58C6C1E32
          4E904E15FBE1ED1DEFB43CDC76912BCFCBF74A64B499BE264068D1BD31E24468
          18E35021A993D6D0BABBC32D3C55F31C181E0C0797A11E47E54EADA31F97A6A9
          851848A75007823FE35EE19E9CE9DD65461706E3FDBFFBD5FDFBAF75D3CF1C62
          ECD6FF005EC3FDEFDFBAF745F7BEB3A29BACBB204331473B037A2A323157573B
          6726159594EA0EA79041041FA7B37E5E00F30F2F8231F5F6FF00F5793A6AE3FB
          09FF00D21FF075A7DFFC27F779E761F919DF5264F3B98AF87FD1AD2C491E4B2B
          5F5D146EDBC52EE91D5D44C88FA56D7001B71EFA25F7D9B6B68790793DA1B68D
          18EE9C55554FF60DE8075B9238D26B3291AA9F0DB8003C97D3ADD07696E1A7A8
          A589A4996E507D4FFADF4B9BFD3DF35BA73A5EC790A694D91D4FFACC0FBF75EE
          A5EB5B5EFEFDD7BA8F2D64117EB751FD6E40F7EEBDD45FE2F444FF009D5E7FC4
          7D7FD7BFBF75EEA5C7570CA2E8E0FF00AC41F7EEBDD7369E241766B0FF00123F
          E2BEFDD7BA8CD91A55BDE45E3FDA85FF00DB73EFDD7BAE1FC568FF00E3A0E7FC
          47BF75EEB9FF0012A5FF008EA9FF00250F7EEBDD636CB51AFD655FF6E3F1EFDD
          7BAC91E46965364707E9F91F9F7EEBDD4CF22DAE0DFF003FEF8FD3DFBAF745EF
          E41F7EEC0F8F9D6BBA7B4BB1F371E1369ED4A26AAAEA8E1EAEB2A1EF1D162719
          4C086ABCA64EA488A0887D58DCD9549055BE6F7B6F2EED579BCEED7023B0816A
          C7CC9F2551E6CC7007AFCBA1172A72AEF9CEDCC1B6F2C72E5999F76BA7D28BC0
          28196773F8510658FA7CC8EB5A1977C7CBEFE6C1B8B2D95A8DD997F8F3F1071B
          989E8F1D8AC13D4D3E5B77C3048CA2232C7253CDB9F2BE13FBF2C8EB8FA566D2
          AAC458E38DA49CF5EF6DD4D32DE3ED3C84921002D43CA01E15C191A9F112422D
          69D66EEE11FB49F755DBED2D9B6D8B987DDD9A10CC64A18E02471A104431D7E1
          00195C0AD69D1C5D83FCAE7E1B6D5C5A5154F53A6F8AD654FBACEEF7CC64B299
          3AB940BBCA569A6A2A4A72EDC9548EC2FF0053EE4CDB7D93F6E76F804726C7F5
          52D32F33B3313EB8200FC875046F9F7A8F7B379BB33C1CD42C2DFF000C56D122
          228F4AB0666A7A93D43EC7FE55BF0FF76E3258F0FD7755D6B9A8E26FE1FB8B61
          E6ABE82B286A17D5154BD1D64F574557E36E74B05240FD43EBEDADDBD90F6F77
          184ADAED4D6572076C90BB020F91A3120FF2FB7A51CBDF7ADF79B65B957BFDFE
          3DD2C49EF86EA256561E60320565AFAE7ECE8ABED0EF1F92DFCADBB176D6D0EE
          6DDB9BEF0F893BB32231985DE5502A6AF3DB26777002A4B552D4D4D15550C444
          92D03C8F4F53102D0B061EE3E8B78E6FF65F75B3DBF996F64DCB912E1B4C7364
          BC47ED3520A8C98C92AC32A7A992E396BDB7FBD172EEE7BD723ED90EC7EED59C
          7AE5B61A563B81F30A02BAB9C2CC00646A0714EB662EB5EC9C2EF3C0E173F84C
          A5265B0B9CC651E5B1193A3944D4D90C764204A9A5AB81C1B3472C3203FD41E0
          F23DE4BDB5CC1796F05DDACAB25B4A819181A865615047DA3AC15BEB2BCDB2F6
          EF6EDC2DDA1BFB791A391185191D090CA47A823F3E3D317C8CEDFAAEA0E95ED5
          ECDC5515265727D7FD7FBA37863F155D24B151E46B3038C9EBE0A1AA929D9678
          E9EA1E1D2CC8430078F628E50D961E64E6CE5BE5EB899A382FAF62819D685944
          8C14B2D71515C571D209DD92291D29A80C57871A75579FCAD3F99E6F9F9E983E
          D4CD6FDEBFDA9D793F5FE6B6CE2F1B4FB5F2597AF8B2699CA1AAACA896ACE5A5
          9648DE99A9C0509C104DF9F7357DE17D94DA3D99BCE58B6DA77AB9BC5BF49998
          CCA834F865400BA00E3AB35EB74952778A47565D008A0A71247A9F4EAED30F93
          8AB20570E0DD6F7B81FD3FA9F78E7D39D39495F4D17EA9147FC8407BF75EEA38
          CB5193612A927E9C81FEF67DFBAF75323A98A5174607FD620FD7E87FD6F7EEBD
          D48F7EEBDD7BDFBAF75FFFD1DFAB21308699DCF002B727E97B71FEC7DFBAF75A
          6EFF00C28CFBD739B83B0BA37E33E16AE618DA7A097B0F3D430C8C23C867B3B9
          13B7F6BC5530A9FDCFB1A586578C1BD9A7240F7D12FB92F2A5ADBECDCDFCFB75
          18FA8925FA58D88F862897C49483E5A98807FD28EAB150DC4B237C3127F322A4
          FEC007E7D5C0FC23F8E386E99E90EB0EADC351C50260F6EE36AF393A2059327B
          A72D4B0E437064AA8E90649E6AE999016B958D157E83DE18FBB1CE979EE07B83
          CCDCCB7529689EE5E3845709044C52255F4141A8D38B313D521074788C3BDFB8
          FE7E5F90A01D59FE1FAD68928D03428491F5B01F41F4F71DF4EF412F6F745613
          7AED3DCFB43318F8AB709BAB0795DBD97A492349527A0CB51CF47500A382ACC8
          B2EA5BFD1941FC7B32D9B75BCD8B77DAF7BDBE5297D69711CD19069468D830C8
          F234A1F913D52441246E84F11FB3D0FE473D5637C21FE5AFB77E07EEBDF79ED8
          FD8FBE376D0EFCC6506372582DCB438BA5C7D2B62EBA4ACA1AEA67A06323D5C1
          1C8D082DC146FEBEE6DF78BDFDDCFDE4DAF67DBF76E57B4B39ACA73224B148EC
          C752E96421940D27078F11D534CED2C72CB2A9A29185A56B4E26A7D3A2E1DCDF
          CD5FE4A7C64FE61FB6FE3876461FAF4FC7EDCBBA768AE3B714783AAA5DD2DB1B
          7888A8E9327FC5BEF9A97EF71195959273E2D2DE222C2FEE52E4DFBBFF002273
          F7B1D73CF5CBD717A39D22B49B547E286885D4152C9A34D74B815515C061D343
          C7114D299AAD1B9AAE9195AD78F1F84F1F51D6C7DB6B75A57C5ABC8ADFD0AF2A
          C2C406520F2ADC11FE1EF0B87CC50F4AFE6387554BFCDEFF0098BEEEF82BD47B
          1F2FD5B4DB6B25DA1D81BC9F1587A0DD5472E47174FB6F0F432566E1C94D4105
          453C9248249A9E189B500ACC7EBEF227EEE5ECEEDBEEEF32EF36DBFBDC272FD9
          5A867685B4399646A46A18838A066229E9D36C249258A18DF4D4124D2B8181C7
          D49FE47A8FD29DE5DCBDD5F0BB1DD93DED4D81C6F61F60F596F0DCD5789DB58B
          97118BC6E132787CA1DBD08A49EA2A261553E2C2CD2333FD6400003D85B9EB62
          E55E56F78C72D726C933ECD61B9DAC3AE57F119A55953C5EE000A2B1D207C8F4
          9CEB36D74CF26A5EED2680600A797A904F5A95FF002EFF00977B4FE1F6F5EEAD
          E5B830B99DC99BDCDB3D703B1F6F61E26639ADCF16E36A9A5A0ADAA50CD43472
          03769155DC81A554B11EFA4FEFEFB51B9FBB9B3F28EC5637D15AD9DBEE226B99
          5F8A422265628BC19EA40009005751C0A74B278E56FA7923D3DA8464D00A85C9
          F90A756703E55FF3D3DE18F9FB076275B53ECDDA15901C86236AD26CFDAA6BE1
          C62AF9A15871DB8E79771D6CB2436E5D4492FE17DC0A9C8BF733D86E23E5DDD3
          9962B8DDA36D0F2BDD4C6AFC0EA78A90AE7C81A2F9F49AB152A6E676FE92AD17
          F2ED3FE5E8DEFF002E4FE74DBC7B6BB4697E387CB3DBD8BD97DAD5B593E1F6D6
          EDA1A3976E506677052165936AEE6DBD564AE1B3F52C8453BC656291C68650CC
          0FB037BE3F75FDBF95B97A5E7DF6D2F65BAE5E8D04935BB378AC911CF8D04A32
          F1A8CB29A9D3DC0D011D5CEB88248650F6CDC1BCC578569820F0AE2878F5B165
          776660F0BB7F25B833795A2C4E1F0B8DACCAE5F27909929A8F198DC7C1254D6D
          7564EE42434F4D4F1B3B93F85FEBC7BC32B5B5B9BEB9B6B2B281A5BC99D52345
          156776345551E649200E9D6608ACCC68A38F5AB5FC80FE77FF0026FBF3B6721D
          3DFCBABAC4E531F4B3D4D241BDF2DB70EE5DCDB8129A56825CDD162EA1D707B5
          B6FDD75452D5179196CCC56FA467DF29FDD6790793796E3E69F7CB9896392819
          A1137830455C84671FA9349E442D057001E3D32C1B42C97131891B8281573F6F
          1CFC80C799E824DCDF333F9EBF46E38F62F62ED3C76E0DA187226CD521D91B47
          3B451522DA4965C943B4E6FE2F474CA8A434EA42C7F93ECD76BF6F7EE73CEB74
          360E5DDE5537697FB3D3733C6E4F0A278DD8C7FA3424FA755FD0AD3EA2643E45
          80D3FCD69FB69D5C6FF2D8FE6BFB37E6E61F27B732F878F60773ED2A08ABF736
          CD1586AB1597C6B4829E4DC1B4AA6A0AD5CD4314E556A69E5065A62E09665371
          8DBEF9FB01BBFB3F736DB8DB5E35F7285CC9A229CAD2489E9511CE076D48AE87
          140D4A501E2ED648E4114C06A22A18706FCBC98798FCC7429FF343F9ABD93F11
          3E2CD7F71F51D3EDAAFDDF4BBDB68EDE48375E3E5C9E21B1D9D7AF4AE76A582A
          29A43509F6C9E36D761CFB26F603DBCD87DCFF00705395F98DE75DB4D94D2D61
          6D0FAA3D1A73438EE35C75A7D66482347D3A9A84D01C6927CFE63AA4A8BF9FB7
          73EE3F8F9B7B0FB3B63E33787CC1DD79FCFE3EA71DB776B6565DA1B476F41240
          B83CBD2E0E17ABA8CFE6AB6277B421FC3194D525EE17DE51BFDCF7966D39E2F6
          F373DDE5B5F6BEDADE260649944D3CC7578AAD21D2228968B9F89AB45A509EB4
          639E32EB2CE162AFC642834A0ED03856B5CF90F23D1C2FE591F303E77F69D477
          84DF2F6873F414BB776FE1EBBAFD737D7D06C68E5C8C9FC4A5C92C0D051519C9
          A247045A9496D00FF8FB89BEF13C85ED072AC1C95FEB6735BBCB7578D1DCF857
          46E084A2E9AD5DB4649A1C54FD9D325D1656105D338113135CD08228780E8937
          4DFF00C2847B671DBD3B129FE456D5DB392DB387C36E1A6D8986EB6C05563B39
          9CDEB4796FB3C26332592AAAEAB869317534C8FE69BC6CCAC3D2A4903DCC7CD9
          F731E5EB8DAF65FEA2EE5710EE324F11B892EA5D71C76E549919102AEA901A69
          5A81EA40E9E10DD048644983D572080A32306A3341FE0E9D327F33FF009E6772
          C72F64F57F56D175D6C1C940F5BB776BC5B5B6C1AE38B2A65A69557774EDB832
          7533436B3B22094FE85B103D96A7B7DF73DE519579739979AD2F37D8982CB23D
          C4B87E043781FA518078AD7B7CCF540636CB5C4CC7D5168BF960D7F69E868FE5
          C3FCE7FB7F7FF7751FC67F97F88C5E377A66B2151B7B6AEF1A0C37F762B61DDD
          45E5F26D8DE583256922A8AF3134704F12C6566B2B290D7041EF97DD8B97762E
          529BDC0F6C2E646DB608C4B35BB48664680D3F56DE4CB7683A994920AD48208E
          9C606158E512F896CC40A9A545781A8E209C1C5475B3F63B73C7558EF26A17D0
          7EB716201FEA4720FBC17E9DEB595FE67FBD3737CB9F9AFD45F0776CE56A69B6
          3ED69283757648A677581F215907F14C955D5AA1B49FC0B6C288E00DC2CF507F
          27DE31FBA93DE73C7B83CBBEDA58CC56C232B25C538548D4C4FF00A48C62BE6D
          D679FDDF6CF6DF6A3D9AE72F7CB75B657DDA75786CEBC74A9D08AB5E1E2CE6AD
          4E2ABF2EAC2B78EF9EA4F89DD2926E8CF2C3B5FADFAE70D4588C1E1319147F75
          56F1C7F6F8AC162293D02AB2D94992E493CB1791CD813EE74DD774E5FE40E5AF
          AAB8020D9ACE30888A0558F05451E6EC789FB58F5897CBDB0738FBC5CF62C2D1
          CDD732EE5334B2CAE4E9404D5E590FE18E306800F2A2AE48EAA7E97F984FF310
          EF7ACAEDC7F197E3EE3B1FD6D4F512D36366ADDB9FC7E6AA48D8A87AACF65AAB
          1D455759A7F5A52AF8D0F1EE008FDD3F76F9AE496F793394C2ECEA68A4C464AF
          DAEC554B7A85141D663CDF77EFBB97B7B05BEDBEE77B84CDCC8CA0B8F1C4207F
          A58A30CCABE85CD4F41DF767F328FE605D6FB7F6E6D8EC1EABC47526F193348F
          3EF69F6B34D41B9F185550E2A9B1F5AF5B848A78DDB5BCB4F2EBB0B69039F651
          CCBEEF7BADB25AD9D8EEDB1A6DFB8992A66316245FE10AD54AF992A6BD08B91F
          EEDDF779E69BFDC775E5DE6A9379D97C1A0B65B81AA07AFF00685D34CA41180A
          EB4F3AF571BDC1D538CF925F1E331B1B7353C15136F5D898EC85354F892F8FDD
          8D85A7CA62F2B44A411049065987E923F69D97E87DE487306C50F39726CFB55F
          28325CDA2BA9A7C33680EAC3D3BBD3C891D610F26F36DD7B63EE75AF306D2ECB
          0D8EE5246EB53DF6DE298E48DBD418F39FC401E3D162FE4C5DE19FAFEA0DE7D2
          BBAAB27933DD17BD2AB034A950C5E5836F64E7AC6828AEFEBF163B2B49511A83
          C2AB81F4F719FB03BEDC5E72E6E5CB97CE4DDED7705057888D89A0FB158301F6
          F53A7DF1394ACB6DE76D8B9DF698C0DBF7EB31231030668C2D5BD2AF1B213EA4
          57A8FF00CE33E497CC4D87B7A7D8BD19D609BB7A637BF516E78BB737AFF756BB
          373ED315553518EAD0993A6A84A7C5AAE15BC9AA48DF45CB7BE9A7DD87933DAD
          DFEEE0DEF9BF9845B737D9EED17D141F50B1F8C555593F4CE64AC9514073C3AC
          3762AF70D0CB3158B4AE34E0E4D7BA98E03CFAD7BFF978FC87F99BD1F49D8143
          F147A9E2ED0C7E7721B7EAF76ACFB5EBF704789ADA3866A7C6BAD45155520A67
          A9A595EE8C4EA02E3E9EF323DF3E45F68F9C66E5E97DD0E661B74D6EB28B7FF1
          94835862A5F0C0EBA103870FCFA557622570E6729269A6175540E1E47CFADE07
          11DF98AEBCE96FF4A1DB594A4DA58EDB7B0E83756FAABABBC14D86A88F114D59
          98A548998C8D3AE4646A786004BBCA5505C9F7CA28F97AE37AE6E9396395A16B
          A9A7BF782D829D45D7C4658D8B0C69D003B3F00B56E984948B78E597E22A2B8A
          124D3007A93E5D6B99D89FCE7BE74FCB2ECBCC6C6FE5FF00D48313B4B16EC29F
          3B93DBB4BB9375D5D0ACA523CBE7EBB28E9B636AC15816E94FEA655362EC6E7D
          E756D7F767F683DB5E5FB7DEFDEAE6857BE7F897C630C01A9FD9C4A9FAB311FC
          5C4F1D23875B6465D26E67285B8220AB7E66849F9D2807F3E90FBA3E7D7F3B2F
          8C70D3F617766C9C3EE5D854F322E5E3ADD99B7EBF0B0C41D4B8C8653654C2BF
          07E55F4ACCE5501F6B769F6A7EE95EE34926C7C9BBE787BD69AAF87732ACBC38
          AA4F8929C4800FCFAAAF84C405B9951CF0D6050FCB207F841EAFABF9787F329E
          BCF9C5B0AAF3584A397696FEDA8F4749BFB60D755A55CF89A8AB5BD2E530D57F
          B6F93DB99091596298A2BC6E34482F627127DE7F65F7DF6777BB7B4BD9C5D6C5
          75A8DB5CAAE9D5A78C722F0495450900D18657CC0714B87314AA0480571C187A
          8F3FB41E1F3EAD8B1B5D1D6428EA7F5206E3E9FD2FC93EE19E9CE9D3DFBAF75F
          FFD2DF6B70B15A27B71E93CFFB03EFDD7BAD167F9D199A0FE67BD7559B8D8D46
          DE6C0F4CCB4B0EA04A6221DC2532111034E92D52AE4826FCFBEA3FDD53449EC0
          EEB1D80D37DE3DF827D6429DA7F669FD9D34B5316E401EEFF26814FF002F5B79
          74D53D33AD3C912811B240F08B7D216890C6003736F1DBDF2E9C38790487F503
          B57EDD46BFCFABAD342538507472A9542C080016D23E9FEB0F75EADD45ADA582
          756128047F88BFD47FADEFDD7BA43E5F6FD0490B9558CB006C34FE749FF1F7EE
          BDD6AC5FF0A0AF8F33D66C2EB5F91BB7E934663AD73FFDD0DC75D4CA45426DFC
          F542D6602ADDD003A7199E8194371A7CE3DE737DCA79D56D77CE64F6FEF65FF1
          6BD8BEA6053C3C48C699947FA68CA9A7F44F544212E5750FD391749FB4647ED1
          51FB3AB57FE5FDF2313BC3E2F74F763BD5F9F2793DA34189DC404819E3DCDB71
          3F826652517244B2D4518979E48907BC68F79F93CF22FB9BCDDCBE23D368B72D
          343E9E0CFF00A894F90D457FDAF5580109E19F89095FD9C3F68A1EB5FEFE64D9
          EC87CE2FE69BD63F1B70952D5BB5FADE6C0ECAC978DCC94D4D233AEEAEC5AF7D
          04AAB53D25A9D8FF005880F79BFEC5C36FED0FDDCB7FF70F704D37D7B1CB762B
          C48A7856A99FE234207F48F5A0C562B99C7C6EC113F2C57FDE893F60EB629DC1
          B7E0C5F56EECA0C7D3A5263315D7DB871D8DA5880586971F8FDB3594B454F120
          B054869A15503FA0F7CEFD9EEAE2FB9AF67BEBB90BDDCDB9C323B1E2CEF3AB31
          3F6927AD4A812D6445E0108FD83AD50BF937F4FECAECFF00921BF337BB7074B9
          EADEB7DBABB8769C15F1AD450D0E72B770B500CABD23868AA2AA8A124C1AC111
          C8755AE07BE9AFDEFF009B37EE59F6DB6BB6D8EFDAD86E578209D930E61F0999
          9158657590031192B51515E9EB8EF36B0B7F6656A47AD00A03EA33C3CFADC530
          1D6D254635676566728096372D7B7049E0DFFC7EBEF95600028063ADF5A9D7F3
          B0EB7A5E8AF96DD4FDC5B3C26173FBC30B47BAB232D08FB695F756C6CEC10439
          A668C2FF0095D5D2AC22471CBB25CF24FBE9FF00DCFB7D9F9B3DACDFF9537826
          6B4B0B96B74D591E04F1EAF0F3F854960079020701D52250DF576E7E065AFD9A
          AA0FED22BF6F563DFCD37E4DE768BF97B527F0BC84B4391EF43D7F81AD7A6A92
          B21C46671716E6DC14F1491B0D51548A611BDB828C47D0FBC7CFBB27235ACDEF
          DEF104F16BB5D83EA992A31AD653044483C0A8248F9D0F4CC44CDF42ADE7DCDF
          ED47FD0543D13EFE581F21BE1A7C72F8FB4F1EF1ED8DA7B53B4F7AE6F2394DFD
          1E4E9320B97A7A6A4A834981C30A982866D78D828A3F30447D0D24A491702D23
          7DE6FDBCF79FDC8E7B8D762E579EE7942C6045B72B24611A461AA590AB383AAB
          44048A8031827A75CBADC4AEF1396E028A480BF223D4D49FCBAB3D9BF98FFC12
          6A5641F237643BBC6D1BC4F499A78E547055E2951F15E39227436656BAB0363E
          F1C07DDB3DF10559790EE4382082258410464107C4C107208C8EB464AD478121
          1FE90F5AF5E13B5BAC3A8BF9A6ED4EC8F8CDB8E8F25D5F9EECDDBF1A36112A68
          30CD43BF169F1FBBB034F0D4A40CD8D82B6BA4D00A84040D3C01EFA052F2DF33
          F357DDBAFF0097BDC9B068B9AA3DAE50E1CABBF896DA9A09895A8D6C1158D0D7
          26B9EB4CAE2C58B290636D4B5C1A03515AFCAAB9F2EAF13F9D665E5C87C14DC1
          0BBB32C7D93D7E46A37B68ADC8A5FF00D8DFDE187DCE493EF14048C9DAEE7FEB
          1757FF0089169E9ACFFC75BA047F92BF45ECCA6F8F949DB14184A5937F6F8DD1
          B8F1F94DC93C11CD92A5C560EAE2A2A2C462EA1D4BD0519F549288CAB4AEDEA2
          4003D88BEF95CDFBFDDFB810F2635FBAF2E5AD9C32885490AF2CBA8B3C807C64
          5005070B92054D7AD3524B8999F250E91E8300E3E66B93D5EEEE0D8F3D26D9CA
          CEECE7461B2ADEB666B5B1B524FD49B5EDEF102C1556FAC34A81FE3117FD5C5E
          B72FF652FF00A53FE03D69CDFCA83AA366768FCCADE93EF2C1D1EE11B1315BA7
          766DEA0C9429558E8B3E9BA56869B2153472AB4354F431D433C4AE0AACB66B12
          07BEABFDEAB9A77DE57F67ED0EC37EF6D3DEDC416F2488487F09A32CCA8C32A5
          B4D0919D2481C7AF4BDF159427FB365C8F5A28201F95788F3EB71BC375FCB538
          C15122967F1DEF637E00D3CF1F4B71FD3DF27C0031E5D5FAD45FF98AE05BAAFF
          009B06D3CDE162A6A2ABC96E7E92DE23ED55A1F264AA2B28292AEA6A74683F71
          54F4C4B91FAAF7FAFBEAAFDDFAE4F307DD9E4B0BD77748AD770B6EECF62F89A4
          0E38504015E14E99FF00889B8A7902C47E6037F87ADCE36BE6A5971DA5D8FA8F
          F53C6A373F9E3EBEF954828157D31D3A49D35F3A75AF57C6166DEFFCD3FE67EF
          3CAE9390DBCBB93198F8E6264923824CE63B08861660195528A900FA7D1ADEF1
          ABDBE5FDE1EF87B83B84FF00DAC2B22AFA81AD531F90EB3B3DE53FB93EEA7ECE
          ECD69FEE3DD3C0EE46012237973F6B1AFE5D30FF00381CCE43706E3F8BBD2E95
          5252E0B766E6ACCB6507934433D55465719B7695E6B117FB4A6AA9596FF42E6D
          ED3FDE1AE66BABEE48E5D0E56D6794BB67049658C57EC04FEDE957DCBEC6DB6F
          DA7DD3E7568835FDAC0B1A632156379980FF004CC057ECEAECBAD7ACB1583DB5
          82D9FB76829F1980DB18AA2C362B1F491AC54F4F49414E902E98D0052F332177
          63EA77624924FBC93DBEC2DB6AB1B3DB2CE254B4823545502800514FDA7893E6
          4F5839BCEEF7FCC1BBEE5BEEE970D2EE37733CB23B1A925989A7D805001C0014
          1D5527F3AEDB9FC0BA1FAC3528FDCEE0A054B8E508C3D516D371C6A1F5B7D7DC
          0BF78FFF00955362FF00A580FF008E1EB2F7EE458F70B9BA9E7B31AFFCE51C7A
          B5FEAEA5D5B1F6492357FBF5B6B9B706F7C2E3CDB9FEA0FB9E368FF926ED3FF3
          CF17FC717AC45E66FF0092F731FF00CF6DCFFD5D7EA9F7F973D2C789F9CFF3BB
          0B8FA88DB1B16E2A99238E93525234BFDF4AB65648EC2CD099997E9FD7DE3D7B
          44820F72FDD0B7898782246E1C3FB63E5F2A9EB343EF23235DFB17EC2DE5C464
          5D185055BE2A7D28E27E7407F6756BFF003071EE7E2DF7E4E49B47D3DBF64201
          E0DB6ED61E47D0FBCCDF6B403EE67B7E699FDEF6DFF5707582973FD8483ECFF0
          8EA82FF909D09ADD8FDF045FF6778ECD3C1B7EAC15581FD2FF004F7975F7EA00
          EEBEDDD47FA0DD7FC7A3E9D94FF8E37FCD25FF008F1E870FE7C7DAB94D9DF1E3
          ABBA8F155B2532F686F6ABC96E08A195A37ACC1ECCA2827A7A49954A9929A5CA
          D7A3303705A217FA7B21FB92F2C5BEE1CE3CD1CD3710EA6DBED12288918579D8
          EA23D0844A7D8DD6917C4BA894F0452DF9FC23FCBD23BE02FCA3F83DF1DFE367
          5F6CCABEE6D9DB777664A846E2EC75ACA3C94797A9DDD91777AAA6C84B0E3E46
          9E3C4C2120806B68D55495FD47DEFDFCF6B3DF2F71FDC9DEB77B6E51B89F6081
          843674963D021502AEAA5C51A46AB312031C03C074D82EB24AD24521918F9292
          29E401F4A7F327A3B597FE623F0172D8BADC4D7FC81D8593C7E4E92A283278CA
          DA2CC4F4590A1AA8DA1AAA3AB866C518A6A79E1765656B8B1FEB6F70F5B7DDD7
          DF9B2B9B7BDB1E49BA8AF6170F1BACB10647535565224A8208AF5E6757564682
          42A788D07AA23F809D99B53A73F9A653633A4F707F16E9EEC3DE3BAF61E22A29
          8CF4B4593DA99E827C8618082A7C723A62B23047E02EA1AC8081CFBCEBF7AB62
          DD39ABEEDD7573CDF63E0F365958437522E18C7730E9F132B51DE3503A7146F4
          EAD207FA6B69641FAA8C3F304E935FB41A9F9F5BE0F5CE55EAE8A0D6C49D0A39
          FADB8BFF00AFCFBE4E8C807A73A177C9FE1FEF3FF1AF7EEBDD7FFFD3DF833509
          9A8E451F5D2DF8BFE0FF0088B7D7DFBAF75A77FF00C2887A1F350E5BA77E48E1
          69257A3C425575D6EAABA789CFF0DA815C739B52BEA9D56D1C32CCF3C2ACD61A
          D547E47BE82FDC8F9C2DBC0E6DE40BA940B8671770A93F129511CCAA3CCA90AC
          69E4DD562216E591BE09529F9AD6A3F353FCBAB85FE59FF28301F223E3D75CEF
          BA3AE81F3B4188C7ED6DF58E5950D56237760A921A1AF8EAA2075451E49215A9
          849003C7271F43EF157DF0E40BCF6E7DC7E60D9A68586DB3CCF716AF4ED78656
          2C003E6636251879507AF548414530B7C71E3F2FC27EC23F9D7AB85C7662966A
          58DBC8BFA47E47E07B893A77A00BE4A77F6D8E82E9DECAED9DC790A5A3C56C2D
          A39ACFB3D548122A9AFA6A49461F1AA2E0C9364F2AF0C088BEA72FC7B1272772
          D5EF38F356C1CB1B7C4CF737B7291F6F1084D647F90440CC49C0A74DCCE52362
          BF1F01F36381FCFAA5EFE54BFCCCBE4CFCE9DDBDAEFDB1B53AF76F75FEC4C3E2
          53195DB430D96A1AEABDD19BAE93ED71D3D556E4AB60963A6C4C0F248AAA1F51
          0781EF23FEF1FECA7217B41B5F2DFF0057B70BE9B7BBE9DC159A44651146B566
          0AA8A72E54035A797552B3473C7134E1868248D207980323D4D7AB19F979D2D8
          FF00907D0DDA5D5992A68A68F7C6CCCC62E88CA8AC29B36299EAB01588CDFA5E
          9B2F042C1BEA39F78FDEDEF35CFC8FCEFCB1CD50B95167768CF4F3889D32A9F9
          18CB63D69D6E65631929FDA2E47DA323FCDF9F5ACE7F290F926DD17D7FF277A7
          7B1AB571755D2B3E7BB2A971F5F32C2F1AE222ABC5EEAC6D3A48CA5A4197A0A7
          6D0A2F7949B7BCDFFBD6FB71273AEFDED7F3472F44645DD658EC5DD454699489
          2191A9E410C99FB0755B890237D447F0CB1D47FA61803F3047ECE9FF00F92F75
          C657B7FBA3BFBE60EF2824ABC9E673D94C06DDACA94326ACC6EBAD6CDEE6ABA6
          91C13AA8B1C61A6B83C2CA47B4DF7C1E62B6E59E50E47F69F677090944925506
          9486D944712903C9E4EECF1D1D58A69782DC1C44953F36381FE53F9F5B317616
          1051F53EFE62A2E361EEE3F4B0E36EE47E9FEC47BC0FE5DFF958B97BFE7BEDFF
          00EAF275E9FF00B09BFD21FF00075AA17F218283E41F7B192DA46C0A1BDFFF00
          0EF22FFEF3EFA2FF007DDFF9503937FE96BFF581BA727FED6CBFE69B7F817ADD
          0F6B4F42B828C131FF009AB7E01FA7FC53DF343ADF5A77FF00C284B71E2B76FC
          98E8CEB8C0BAD6EE3C2EC4A9192A3A72B23C151BCB722AE1691D12EEB51530C3
          E40A45F43A9FCFBE977DC9AC2E36EE41E6FDF2ED7458DC6E3542715104403B7D
          81AA2BEA0F558582CB7529C2AA004FD953FC811D0A3FCD2FAAB2949F03FADA18
          296403A872DD731E5218B5B474D4AFB77FBB9572B93A9BC70573A2927E85BDC6
          3F75DE6BB6BAF7F39F9DE6046F0974F1934AB14B8F10014C650D7EC1D336DFA6
          D6048A55483F6B0D5FE4E82BFE5DFF00087E20FC91F8EFB67796EFD9957B977D
          536633381DF13C7BA73340F4994A5AB6968A2FB1A1A88E1A6867C54B13C640F5
          F27F07D89BEF0FEF7FBC1EDA7B8D75B2EC9B841072E4D6F1CB6DAADD1CB0234C
          9DE724AC80823C811EBD38C6579A6433BA95380294A1150720FCC7E5D59027F2
          77F838F4DE6FF45197BDAFFF001FCEE4B5FF00D8D4FF008FB837FE0B5F7BFF00
          E8FF0069FF0064B1FF009FAF6993FE52A4FDABFF0040F41963BE007F2D4DA5D9
          D87DAB18DAD8EECEC467B15362B6854F6CD79DCC99D82587218CA6FE04F91FBB
          96A5DD11C4256EEA7E96F6236F7D3EF39BA72FCFBB8B1924E5B96072D702C17C
          13150866F1380502B56E93492C4CB24725F49A4F69FCF14F87E74E943FCE8291
          E97E0C6E0D639FF489D7C0F16E4D6D79B7F8FD3D97FDCECD7DE484F91DB2EBF9
          98FA534A5C598F463FF1C6E973FC8E208E4F865B14BA836DEDBF8723FEAEF0FE
          7FD8FB63EF7FFF004FA6FF00FE95D6BFE07EBCBFDB5D7FCD4FF9F57ABCCDFF00
          4F126CBCE6941FF1EFE64FD071FEE2EABDE3458FFB9D61FF0035E3FF008FAF5E
          97FB297FD29FF075A5DFF24740FF0033BB8D5B907626EC363C8E37BD27E3DF4D
          7EF93FF4E87977FE9676DFF565FAF3FF00CB3FFD29FF008E0EB757DB74D10C0A
          9D2BC456FA5BF17FC73617F7CC1E9CEB4B9FE6FEA17F9A9F5F85000FB4E8BE07
          FDAE23F7D48FBB07FE23BEE5FE9F71FF000374CFFC47DCFF003FF8E2F5B70ED2
          89A4C7B05BFD3FDE7FA9FF006FEF96C3874E8E03ECEA83284B7427F389EC3C56
          697EC709F2076ED454E06AE7BC50D4D7676829327471C2E74A3BB66B172C1FF0
          736FAFBC69B16FEAB7DE137186E7B6DB7684E82700991430A7FB752BF6F59DBB
          BC479FFEE69B25CD8FEA5FF2F5C2F8AA3255617647A8E3FD9387FB074E9FCDFB
          AB771673ADFACFBA76C51CD5757D3FB967FE38D4F134B2D161B2F25254D16525
          F182E28E872F408B29FA209413ED77DE1F62BBB9D9F64E65B28CB3EDF3112505
          4AA390558FC83A804F957A28FB97F376DB63CC9CD5C8DBA4EAB1EF16CAD0D4D0
          3C918657415C6A68DAA0713A4F471BE2F7F32FF8CDBD7AF3019ADDDD9DB63AFB
          76C78BA2A6DD3B677656362AA28F33053A45593504AF1B4190C6D54C8648A48D
          890AD66008F632E56F77F92F7AD9ECEE6FF7B86D37108A258E53A4870284A9E0
          CA4E4107CE87A8CBDC0FBB67BA1CAFCCDB9D96CFCAD75B96C8D33B5BCF6EA1C1
          8D98955715051D41D2C08CD2A0907AAD7FE6F1F367A23BF769EC2EB0EA6DD49B
          DF23B6B7CAEECCEE77134D38DBB494B0514B451D0D3D754A44D5F592BC858F89
          4C6AA3F5127DC37EFA7B85CAFCC9B76D7B1EC37DF5334373E2BBA83E180015D2
          18D3513C7028079F5935F74CF66B9F791F7ADFF9AB9BB69FA1B6B9B1F02289D8
          19998B862CCAA4845005327513E43ABB5EBEEC3DBB8BE9EDB3BD2B3214F0EDBC
          5F5BE0F7255649E555A78F1341B628EBA5A86909D21445091FF06E3DE50EDBB8
          5A43CB7B7EEB24C058A58C7296F2D2B1024FF2EB01B7CD9771BBE79DE79761B6
          63BB4BBB4D02C6064C8F70CA053F3AFD99EAB1FF00945EDFC86F6DE1F277E48D
          652CD151F69764D5E376F4D2C657EE6860CAD7E72BE48EC02BAC4F574F19238D
          408F7077B0F6B2EE177CEFCE52A111DEDD948C9F31A99DA9F655475961F7BBDC
          2DF67DB7DAAF6CE09434DB66DE2498035D27C348901F3CE9723E5D5B8FCC6A6F
          17C4BF90B75B11D2FD81F8B5BFDFB9596FF1E7DE63FB59FF004F33903FE96F6D
          FF005707584171FD849F97F8475AFCFF00C27EB47F717E41EAB7FC7DFB23EBFF
          006A4AEF7975F7E9FF0092AFB77FF346EBFE3D1F4ECBFEE5B7FCD21FF1E3D28B
          FE1417B1EBEBBAF3E3E762D2C52CB41B77746E9DAB929106A869A4DC18FA1AFC
          73CB653A4CF2E3E44524D8DADEDAFB8DEF3047BB73EF2FC8C05C4D0C13A8F322
          32C8F4FB3529FCFAF4474DDA83C1A3207DA0D7FC07F974C1F0D3F97E7C2AEFCF
          8FDD57D875BB0EB77065F3DB7618F74D7C7BBF394D22EEBA191E9739492D3525
          4A4348D0D525C461459194FD0FB2FF0078FEF03EF5F207B93CCFCB16DB9DBC3B
          645306B60D6C8C4DBB80636D472DE609F50474D299A4326AB870CAC41029419C
          791E22878F473DBF93B7C1EF07913A9F2FAC81A47F7E3721049FE83EE7F27DC6
          3FF05B7BDC054EFF0069FF0064B1FF009FAB6993FE52A4FDABFF0040F497E9CF
          847FCBA367F77ED91D7951B51BB8B63EE3FE2184DBB47DAB5796DC18FCF60CBC
          932CFB7FF884B24D2E3C2B196274216C757D3D9B732FBC5F793DD3943726E61D
          BA55E51BCB52B2CC6C4471B4328A5449E41811A48E3514E92B4914A046D7B232
          B3014F2241E15D3EA3D7AD8A3AD69648A960D5F50AA4D87E4D89FA7F8FBC5502
          8001C3A5DD0D5EF7D7BAFFD4DFCE68C491B29FA1047FB022DEFDD7BA273F257A
          2367775EC0DD7D73BF7050EE0DA3BBB173E33338C997433C527AE1AAA5980D74
          B90A19D125A7997D51C8A0FD38F67BCB3CCBBCF27EFDB673272FDE18377B4903
          A3711E8C8C3F1238AABAF983EB4EAAE81D7492466A08E208E047CC7FB1D6A3BB
          8BE1EFCFEFE58FDAF9BEC6F8A13E5BB3FAB2AEADA5A9C762E93F8D3E430B1C8D
          2D3E277FEC60E9515353451128B5D4618F1A9594923DF4536FF757D8FF00BC3F
          2D5A72EFB9290EDDCC8AB812B78452522864B4B938A31CE8620D30CA7A69E456
          D3F540A4CBC245183F6E0D3E6AC29E87A30F88FE7CDF2B309411E373BF092AEA
          3354AA61AE9A9CF6063A9DAA505988A29711552D30BFD50C8D6FEBEC2F2FDCE7
          DBD9E53358FBB0EB66D9507E99CD3FD30650DF6D075ED7C297D011F3C7FCFDD1
          71ED0DC7FCD03F9B0D7E336BEE0D8F2F4F746264A0C83E3EB71D91DA5B2A278A
          43E0C9E5A6CB336E0DE792A38DC986355640DFA557EBEC61B427DDDBEECF05CE
          E516F6BB8F37B46575075B8BB604652358FF004E04623B89D3FD227AF2C90AB0
          75733CE38505117E75E03EDA93E9D6C45F047E21ED6F8ADD5580EAED9A25AD31
          4ED97DD5B9AAA110D76EBDCD571C6B5B96A945FF00334E8B188A9A1B9F0C0A07
          D4B5F03FDD6F73378F75F9BAEB99F754F0A00BE1DBC00D560841242D7CDD8F74
          8DE6DC30075745605DE435958D49F2C7003E43CBF69E3D59466B05A70FC8B3AC
          77BDB90C013CF36241FA7B8D8804107874E75A147F387E8EAFE8BF9A3BB729B5
          927C7603BDF1316F1A0A4C649253AD5643253A63774626486165F245539C83CA
          6320AB79471EFAE3F75BE708F9D3DA6DA60DC1849B86CD29B672E0120463542F
          53C0F8440AE08A1EBD6BA544D1381A636D43E4A73FC8D47E5D6CD7FCB4FE3CAF
          477C78EABD8535288B3116120DC5BA0E801A5DCFB942E57281C8FA9A6132402F
          F4117BE757BE9CEC79FBDD0E68DEE39356DF1CDF4F6FE9E0C154047FA66D4FF9
          F4CC24BAB4AC3B9CEAFC8FC3FCA9FCFAB25EDFC154CBD5BBE68E8A9E5A8ABA9D
          91BAA96969E242F2D454CFB7F210C1042839796695C2A81C96207B8E3619122D
          FB6296570B1ADEC04938000950924F900324F5BB8FEC26FF00487FC1D6889F16
          3A2BF98E749F616E6ECBE91EA5DF1B4F3BB6F1957559BC66EEC20C5E3F79609F
          24CD26DE8F199934F1EE19E575122D3C4C265D3AD0823DF5ABDCDE70F60B9AB6
          2B0E5CE79E67DBEE76EBA9D56368E50ED04DA4E9975C64986991E23516A749E3
          4EB725C5A4A9101236B02B5506AB815F2C8F519AFA75654FFCEEFE5FEDDC2D46
          D6ADF8575547BDE9A99A824ACA9A3DFF00F610E5C2F896A9F07FC34CAF0897D5
          E015166FA6AB7B82E1FBA07B63737097D6FEEB48FB2B36A0A1ED8931F1D22507
          D31AB4D47A57A6C382317F0E9F5F3FD9AA95FF00553A46FC29F837F21FE4B7C8
          F6F99DF32B1D93A177CDC5BB707B6B7153FD9E6B7467E029FC0649307CBE0B68
          EDC48D0C10C8A8D2F8D15574DDBDBDEF37BD5C8FEDE7211F693DA5B98A4BB301
          B779213AE3B6888A484C9C249E4A90684D0B16635A0EB7549105BC009B7AD59C
          FE3F503D6BE6780181D6C49D91F1CF6E76EF596EFEBBDEF8C392DB3BDB095B85
          CCC0A02CE22AB4052B292422D156D0D4AACD0BFF00664417F781BCA9CC9BAF26
          730ECFCCDB1CDE1EE76532C895F84D30C8C3CD5D4956F91F5E9C913C452B5A1A
          D411E44641FC8F5ABA7FA10F9DFF00CA83B677367BAAF67D7771F4AE72AC2D4C
          D418AACCEE0B71E2696477C7B6E3C4E299B2FB5F72E3E1729F708A149BE96743
          6F7D217E68F64BEF43CADB7EDDCCDB8A6DBCD708AAA348B14F6F2B0A3782EFD9
          344C7CBB8114A80C3147911CA99DBC2B9029AA9D8C3FC14F3A12083C3E63A673
          F9CEFCB9DCD866DB5D69F0DAAB03BBF229F6B8DCB5562B7B6E7FB4A8910A89E9
          30D262E869EA6A2376D49E6768C11EA523D846C7EE87ED6ED972B7DCC1EE73DC
          6D51E5E3D76F08600F069031201183A687D08EB4587E2BF8957FA3C7F2A939FC
          8F4B9FE5B5FCBD7BA6B7BE2ABE667CB6A5A8A7DF7535F91DC7B536C66DA2A8DC
          B55BAB341D65DDBB96188C91E2531D4F2B0A2A42C24562A4AAAA00487EF07EFC
          F2845C983DA3F6B674936EF0D619E68710476F1D3F4216FC6CF4A3B0AA815C92
          7AD92B2AC5142A45AA9AD4F163C4638D2B924F13C3A3D9FCE1BAAB7FF60FC2EC
          CEDBEBED9FB877B6E59B7FEC4AC4C1EDAC5D465329251D254D7B55D52D1D32B4
          AD0D30752EC0596FCFB87FEEA3BD6D1B07BB115FEF7B9C1696036EB85F125754
          4D44C741A9881534341E7D6DE448A5B5773450E739FE16F4E9F7F93275AEFBEB
          BF895B336DF606D2CFECCDC94DBC37BD4D460371E3A7C5E521A5AACA43252D44
          B495016648AA5012848F5017F6CFDEA779DA37EF772F771D937282EEC0D85B28
          92270E85943D46A524545723CBAD44EB24972C86AA64F9FF000AFAF5741BF68A
          59F67E6E18A37795F07968A3455259E4931B5291C6A2FCB3B9007F527DE3D599
          0B7B62CC68A278C93E803AD4FE5D5A5FECA5FF004A7FC1D6A19FC9DFE3DF78F5
          BFCB9ED7DC1D8BD4FBF76360721B2F74525066773EDDAEC4E3EB2B2A3785354D
          3D2D354D5468934D353A9750B7BA8BFD3DF447EF65CE3CA7CC1ED66C361B1F32
          595E5F26E16ECD1C33248E1444C0B15524800E09F5E9A13C529B158DAA421AE0
          E3B47A8EB704DBD4AE983552A6FE23C0049B816FEA3FA7BE72F4A7AD42FF009A
          C7C7AEF3DF3FCCBF646F5D95D49BFF0075ECDA4A5E985AADD182DB95F90C1D3B
          6332B1499359B230C6D4F19A04E6504FA0727DF48FEEE5CE7CA3B37B0D7FB56E
          FCCD636DBA33DFD229264490EB0DA28A483DDF8719E9234F1245B946CD490F01
          439EC1C283ADAA7AFB11235210549047D74F06C3EBFE3F5F7CDB1C3A54380EAA
          DBF9AAFC3EDD9DBBB5B6CF71750C53C3DDDD2354D9FDB9F63FB590CFE0A9E74C
          9556269245F5C993C75553FDCD225FF70978C72C07B873DE0E48BDE63DB6CB7F
          D82A39976C6D71E9F89D01D4547F4948D4A3CF2389EB27BEED1EEBED7C91BDEE
          9C9FCE041E46DF57C29756522958145761C047229D121F2ED6E00F498F88BF2F
          3AFBE596C79769EE98B1B88ED6C763A4C1F63F59EE28A14972D24717D9E4F218
          EC6572A8C962B20E18CD4FA4C94EEC5596D63ECC7DBEF70F68E7EDADB6EDC152
          3DFD13C3B8B6900FD4C519955BE256FC4B4AA9E2381E893DE4F65F993D9EE605
          DEF6579A5E50925F16CEF6124F8353A912474F81D3F03D42BAD0835A8E921B8B
          F9517C41DC39BADCC26CBDC985FBDA879DF1582DD95D4588A6791CBBA51D1CD0
          D4B52C2C49B207D2BF416F65D79EC37B7577712DC7D05C45A8D7424A420FB010
          683E55C7473B6FDEEFDEADBECA0B33BB59DC68503C496DC348C0702CCACA18FC
          E99F3E842C8FF2F4F8D9FE89B3BD4586EB7C6E0F0F9D884926E1A65FBDDE54D9
          6857FC8B3306E1AF1356354D23FF00BAAEB03A92A56C7D9D49ED27229E5EBBE5
          CB6D9D2282515F14774CAE3E171236707F0E148A8A741883EF1DEECAF39EDDCE
          B7DCCAF71716E69F4E7B2D5A33F1C6624A2D1BC9F2EA6841F2E8A3D17F2E3F94
          D59B5E9BA4721F2E6B26F8FD1D5474B36D9A6C0D6479F976E45502718812B318
          D636B5BC2D3B53AB73A4816F71DAFB3DCF4D60BCB13FB884F2886A78611B5F86
          0D74FA7FB5D5A6BE5D4D2FF796F691378979F2D3D98D3EE4321227322786262B
          A75D467FDB840E462BD5DFFC71E86DAFD45B236B75F6C9C3FF0008DAFB5A8128
          71D4DC3CD2B5CCB555F5B3594D4E42BEA59A59A4206A76FC00009E362D936EE5
          CDA6CB65DAA1F0EC604A28F327CD98F9B31C93D622F36F356F5CEFCC5BA734F3
          0DD78BBB5DC9A9CF0551C151079222D1547A67893D085F25FACEB3B07A3BB4BA
          F31D22D357EF5EBDDD7B5A82A251A628ABB3185ABA3A2699DAE122352E8198FE
          9537F639E4EDE61E5DE6DE58DFEE14B5BD95FC13381C74A382D4F53A6A40F3E1
          D05E756786454F8A98FCB3D6929F0EFBDFE467F2D5DFBDA5B3777FC6FDFDBB69
          338F45499FDB63199BC65451E636F495115066B1196A6C657D0D5D255D3CACA4
          80C92C65594823DF527DDAF6FF00913EF0BB1F2D6E3B7F3E5ADB3DBB33C33A34
          72064940D71BA175208201E20AB0C8EB6F3432949A2B98D64D3421CD3E791820
          83D6CE9BEFACB6CFCF3F88D8DC27616DCC96D5A0EDAD9585DD30E3A65F2E7761
          6E2A8A75AFC555C6668A02F90C2559B3AB2279632CA40D5C73B768DFF74F633D
          D7BBBAD82F56EDF6ABB92063F0A5D4380EA684D038CA904E9600E69D351969E1
          8A50C04A0D411915048FCC11C7E47AD79B62D1FF00301FE53FBCB71E0697ABAB
          3B7BA5F2D9796B01C763F279BDA398689BC54F9DC6D76144D94DA39AA8A5D227
          8A58C0278756B03EF3A37C4F62BEF45B46DB7B2730A6DFCD7047404BA45750D7
          2D13A49DB3461B81151E6AC2BD5DA58A460ED2783714A1D42AAD4F9F03F22082
          3CFA1B7797F37EF9B1DA3869762F4B7C4ACA6CADDD9B8A5A0A6DC3161377EEAC
          B500A94F0FDC6228EB71B438DA6AD8B55D669C3AC66C6C08BFB0B6D1F752F683
          966E9379E6DF7185E6D7010C6379208226D39A48CAC5994F9A822A306BD54942
          0F8B7F184FE87C47F99A7E42BD19EFE537FCB9F7F74D6F7CCFC94F90B046BDBD
          BA296BE9B6EEDE9A78F2595DB7167E5F36E0CFEE2AE89A48BFBC79757310895D
          8C31BB973A98011C7DE63DFDD8F9C36BB5F6E79066D7CB70C8A679D4698E5F0B
          FB386118AC4A4062D4A12005A8AF57D5E298B4A14B78FE107049A501A7900385
          7249AF5B47EC8A034F490EAE2C8BF5047D2C3FAFBC2DE9DE848B0FE83FDB0F7E
          EBDD7FFFD5DFAEAEAD29D09636E08FEB72781C0B9E7DFBAF744A7B63E6E7C4EE
          B7DC390D95D83F20FA936AEEBC780321B7F2FBC71B1652819C711D75342F31A4
          9F8E6372B22FE40F63BD9FDAFF0071F982C22DD364E48DCAE76E93E191216D0D
          F3526951F3183EBD326E22048D5523D013FCC023A2B3B87E72FC26C8C8F2D3FC
          9DE98D679063DE742A7EBF86B03CFB333EC9FBB6450FB71BA91FF347FD9EBC2E
          22FE23FEF2DFE6E8389FE63FC3B791997E4BF4D9B924B1DE78E37BFF0089173E
          F63D95F7740007B77BB01FF348FF00D05D53C6B7F4FF008C37FD03D39E3BE677
          C348E446AAF935D3765B01AF7A501000E2C2F7B01FE16F7A1EC9FBB60923DB8D
          D6A7FE13FECF571711529534FF004ADFE6E867DBDFCC0FE10625107FB33FD2AA
          428B9FEF951716B7E34DAFEF7FEB2BEEE7FE139DDBFE70FF00B3D6BEA22F56FF
          00796FF374E5BAFF0099B7C22A2C157D6C9F27BA826A7A1A49EAA68A83734590
          AC9238636768E928696292A6AEA64D3648D14B3B1007D7DBB07B1DEEFDCCF0DB
          C7EDDEE61DD8282D18451534AB316A2A8E249E03ADF8F19C0D44FA696FF375AC
          FEDEDCD90FE6C3FCCBB19D8F4B83C8537C7EE8F4A0ACA183230B156DBDB7EBCD
          561E1C8A9260872FBDB70DA778412520041BE927DE6C6EF6B6DF762FBBCDE6CC
          6F637E78DDB5AEA434D5713AE9764F331DB4580C7CC03E7D69D1D6331BE269CE
          47F0A0E3FCB1FE98F5B78F51ED8FB748A578B41621AD60B6E7F16FA007F1F8F7
          CD318F3AF4FF00460F2D864ABA4F195BD96C003F436FF11F8F7EEBDD0139DEAA
          5AF99A478DDCEB2C0B331B1E6C45EF63EEA110568A07E5D6EA7D7A4B9EA293C8
          1BC6E4F1CB00C47E3F5142DC7BB0A81A41217D01207ECEABA56B5D22BF674B7D
          B9D611D2CAB34B15DEEA599B93C0FA9D56E7DE8000500C75BE8614DB14C297C3
          E35FD16B102F7B7E78FADFDEFAF7416EE3EB48EA9E478A22A581175BADC1B9B1
          B7D47E39BFBD100D2A3AF741C8EA49566056365B5882A0291FD6C5541B5BDECD
          5810CC48F992475AD2A321457A10B6E758C14455DA1BB122EC7EA5BEB7E48B93
          FEB7BF7F83ADF4B1C86CA8A4842A46CAC01E57D2DF4B7F64A9B7BD150C28C011
          D7BA8586D911D1D4898C6C5EE3D46E49E7F3724FD07BF0014500A0EBDD2E7238
          55A8A53169FEC916BFF87F87E7DEFAF741C7F7095AA848E92300D7B3166B5AE4
          7D4902DEEA1154D42807AF74245161961A210E9006922DFD3D3C7D79F76EBDD2
          1331B2455D417024D05AE40660A7FAF00817E7DD4A21352A09FB3ADD4FAF4A4D
          BBB663C720558F48039078FEBFE3617F76EB5D44DD9B553214EDA12EE47057EA
          2DF907820DC7BF7CFAF100E0F0EA99BE557F2C7EA9EE8DCB27626DFA8CDF4FF6
          DC531AA8FB07AFCFD8CD5D5AA7525566F170BD3455955A80BD446F0CEDFDA2DE
          E2BE70F6939779A6E8EED6B249B773056BE3C18D47D5D452A7FA4083EB5EB20F
          DB4FBC773AFB7FB78E5DDC20877AE4F234FD25D77685FE18A421885A70460CA3
          CA9D163A2F8F1FCD4B6127F02DA3F2B3AEF77E0A9ACB4191DF3B7E4A8CE2C297
          58E39E5ACC5D6543B04FAEA964E7F3EC33172A7BE1B60FA5DBF9F2D2E2D47C2D
          3255E9E875293FCCF43D9BDC3FBA86FCDF5FBCFB49B8D96E0DF1A5B4948EBE64
          049157FE323A083BBE0FE61FD7588C01EEAF98BB2764E3F75660E0F6E627AAF6
          2E5770761EF2C9C317DF56E336D6DFDBB824C9D6B51E3E332D44C64829E9E2E6
          4916E2E28E5CF693EF1FEE0CD7B696BCF1B6DAEDD691096E6E59D2DE18222C14
          34D332A85D4DDA8A09676C2A9EB70FB85F74CD94B5DED5ED26E5797B4ED5B87D
          480E687BE465E3F23D5C57C4EEFDE9AEF6A99362EDDCDEE2A6ECAD9581C14FBA
          B667626D3CA6C7DF0F473D3C7471EE98F0D960C992C365ABA9E4FF0029A59A78
          D656D0E55AC0CFDBF720F30F28ED9B46E1B84D6979B3DC5638EF2D278EE6DA59
          6351E2278919EC947C4639155B49A80467AC47BD9A2B9BDBCB8820F0E0925765
          41C11598955FF6A085FCBAB2FC3EDE8282240230B6005AD622DFEF5EC27D26E8
          8FCBF3C3E39EE1DEBD99B1EB6A7766DCC2F59F60D0750653B4370EDD6A5EB6CF
          F6AE437050ED43B036AE529AA6B72F94CBD1E7F270534D33D143442463A66602
          FEE44BCF6CB98ED6D7647496D66DDAFB6F7BF5B38E506EA2B248DA6FAA990854
          48DA346651ACB903E0EBDD0CB93EACFBCAB13C70BCBAC1789847E42D18248284
          824A707E971EE39475D21A37EC3E871D68A826A5457A5261FAF0450047A76B30
          D23D1C16FC81C0E78FA7BB56BE79EB7D3566FAAFCC088E074F21B690A7D678B0
          D22E1CFF00B7F7A216B5F31E7E63AF71143C3A46C5D3B569320FB69D0B965016
          36467D24F91469452DA6DC8FC7BD16571467A81EA6A3AD050321457A13B6CF5A
          458C78CC94E5196C7D6855AE6E6E41B5BE9EF6083C0F5BE86AA0A24A5895145B
          4803FDB73F8E2D7F7BEBDD38FBF75EEBFFD6DE37B9F2D538DD8DBD2A2967929A
          A60D9FBA66A79E1631CB0544781AF9219E1910878E58A550CAC390C011ECC368
          459378D9E3750C8D770820E4106540411E608E3D3537F633538E83FE0EB44BFE
          5C1F08FA97E67CDDE9BBFBC32DBE3255FB637750D050AE1B38B45535F559C7CA
          D6D7E4B2F91ABA7ADABAC9EF02AA8B81C9249E3DF50FEF19EF57347B371F25D8
          72858D968BC8A42DE2A1608B1040AA8AACA00EECFD941D5CBC8A628217D08225
          3803CF14CF56C543FC8E7E1956461FC7DAE01E78DEB01FE9FF00569F78C5FF00
          0687BB9FF287B4FF00CE17FF00AD9D7AB73FF296DFEF2BFE6EA7FF00C3167C32
          FF0053DB1FFA1AC1FF00D68F7EFF008343DDDFF943DA3FE70BFF00D6CEBD5B9F
          F94B6FF795FF00375EFF00862CF865FEA7B63FF43583FF00AD1EFDFF000687BB
          BFF287B47FCE17FF00AD9D7AB73FF296DFEF2BFE6EBDFF000C59F0CBFD4F6C7F
          E86B07FF005A3DFBFE0D0F777FE50F68FF009C2FFF005B3AF56E7FE52DBFDE57
          FCDD75FF000C5BF0DB50089DAF7FC13BD60FF88C483EFDFF000687BB9E767B4F
          FCE17FFAD9D7AB73FF00296DFEF2BFE6EAD0BE2D7C3FEA4F8DBB561D91D4DB4E
          9F6D6164A98EB729552C8F5D9CCFE4557C6B5F9DCB4FFE515D52A8484074C712
          9211473780B9FBDC6E6EF733781BD7376E7E3CE8A563451A22854E4AC68302BF
          889AB3799E1D69134966662D21E2C78FD9E800F41D595ED5A0A3C65346A0A295
          5007D7F038B71C7D3D81BABF4B6FBCA422C6407F1F43EFDD7BAE06A284FD597F
          DB1F7EEBDD70326389B911923FC0FBF75EEB9AD4512FE9651FEB5FDFBAF7593E
          F697FE3A0FF6C7DFBAF75C1AA689BF5329FF006FEFDD7BAC5E4C75EFE8BFFAC7
          EBFD7FD7F7EEBDD6415344BF4651FEDFDFBAF75C8D5D19FABAFF00B63EFDD7BA
          EBEEA8C7D1D7FDE7DFBAF75CBEF293FD5AFF00B623FDEBDFBAF75C7EEA8BFD52
          FF00B63EFDD7BAE5F7949FF1D17FDB1F7EEBDD71FBAA3FF56BFED8FBF75EEBB1
          59483E8EBFED8FBF75EEB8C955492021A4537FC106C7FD7E3DFBAF7498C961B1
          95A1AFA093F93F5278FAF1EFDD7BA43556C5C6C8E59446393F8FF89FA7BF75EE
          AAE7F984F5E74A67F777C69D93BE37EEE8E98ED0CE6777D55F45F71D0262E936
          562772D2622862CF6C4DD798CDBCD845ABDEB89957ECE86B29DE3ADFB7755757
          0034BFEDAB7345BF2FF3FDEECFB1D8EF3CB690DB0DCB6E9848F24D0F8A5A2B98
          921D3328B69455A68DC18B502CA54E3D9E03A243BCBB47BA7E3676D764ED9CF4
          FD45DCDDE3B6BE21766EF5E9EEE3EB0DA782C2F60EC9ACC4EB4C26D6ECAC46D6
          ADC9EDECA63F3558ED91C6C6129649EA28C930329BFB91364E5AE4EE66D8B943
          74361B972FF2ADC737D95ADDD84F7524B6576655EEB9B469563955E241E0CCCD
          AC22483F501EBD8EA2EDBC9C7D31B53E137C85EA6F911BC37C76577FF6AED7D9
          7D95B4B27DB3B9F7D4DDCBD79BA76DE46ABB3F716F1DBD92CFE4A9282A76C542
          0A98EB2929A85B133B242850951ED55CDDEFFCC0BEF6ECFCE9CB36B63B0EC761
          7335A15B28ADBF775D5BCEAB651432AC68CE2E01D0C9234827525F3427AF7AE3
          A2579AF8F5D689F1FBE4A6EA886471F4FB7BF984EDFDA5430CDBA77518E87033
          F73D16DD8DAA6AAA73AFAE4A6C5E4E443553B3D497224697CAAAE247839E79B6
          7F717972D85F2317F6DDAE348B7B724CEDB74B316148B51AC91AB08C7E9D0680
          9A0B29DD73F3A7465BBA6A3B8F69FF00A41FE5FDD79D91BC286AFB3F7E6CFF00
          92BD61B8A83706625CC637ACE8B614DBA6AB0387CDC95B3D68C5D5F71ED5A4C7
          3422728C93329055C820BE50DD7668B68E58F7BB99B69B6B882C76C3B4DDC6D1
          A2AC97F25DAC22468C285120DBA7927A85143182294C6B87963A75AFF973B9FB
          5FACFE407C9ADC7BEFB5F6EE173DB4BE3A7C63C4D27586E94D9B9D7DCF04F8F9
          B7AD053EE1CAEAC46C0A6DCDD93B8AAB1D9ACDAA25553D0513A47207283DEEDF
          9325E5CE6BE47F6CEC6CB6D9AFEC7F7B6F170F7709B88FC13E22D9BB431D24B9
          68ECA15B882DAA55E4914B2915EBD4CE78F49BEA2ED1ED0E9AEFFEC5EB3A2ABA
          6DA1B43B03E19F7267F37B0B6E77FEF2EEBC5C7B8B138DC90DBDBCA7ABDD063A
          9D9FBB60A795D03523DAAE1225D5FA7DDF761B5F32F2272A7334B7173B86E10F
          3A6DF6F1DD5CED306D8ED04D9960448491716FA941FD4158DBB3D7AF52A3F3E9
          09357F7375CFC4BF87FBAB13D8FBFB73E2FE51EE1DA1B4BBA321BBFBA37B6D7A
          5CC516CFEB76ADEBCD8557BE68A5ACABEBFDB99AAEAB96268B1A94AD94FB3486
          79599DDD8DA2DC36DDE79FBDEEFDE1B75AC0FCB1F54DB6C567B6DBDCC90892F0
          4775762D58A0BC9A340AD59598401DA44401428DE33D59A7F2C6DD9DD1B3FBF7
          B73A733F9EDB753D61160B67E6E97AC313DC3B97BAAAFA83746469EACD618772
          6EAC74191C2E1F7851247509896A89FEDA5899D420939877DDBBAD9776E5AE49
          DF2DAC3706DEA696E51EFEE36E836D5BE850AE8FD182465925B77251A70881D5
          80258AF5AEAFC0303F437F703F5EEBBF7EEBDD7FFFD7DE1FB8F0F519BDABB931
          54C5167CAEDECD62E9DE5256259F238BAAA281A5600B08D659C16201E3DABDBE
          E12D371DBAEE404C70DC44E69C688EAC69F3A0C754954B452AAFC45481F691D6
          8BFD2BBC3E67FF002BCEC5ED1D8599F8D794DF541BC72B164E748F1D9EAEC456
          CB8EA8AD4C66776DEE7DBD4B574D5547514B5443C6C09E45C2B0F7D4CE78E5BF
          693EF2DB072D6F1073EADABD98608CAF12C8BAC2EB8A686520AB02A287045315
          07A6CC90BE86FA858A6540ACAE3D3F31E7E60907A39145FCE97E5550C6238BE0
          A56C8A05AEF1F615FF00D8E9C281EE2EFF008103DB3FFC2B8FFB6D7FE82EB5AC
          70FDE107ECFF00A1BA9DFF000F67F2BFFEF042A7FEA57627FF00597DFBFE040F
          6CFF00F0AE3FEDB5FF00A0BAF6B1FF0047083F67FD0DD7BFE1ECFE57FF00DE08
          54FF00D4AEC4FF00EB2FBF7FC081ED9FFE15C7FDB6BFF4175ED63FE8E107ECFF
          00A1BAF7FC3D9FCAFF00FBC10A9FFA95D89FFD65F7EFF8103DB3FF00C2B8FF00
          B6D7FE82EBDAC7FD1C20FD9FF4375C87F3B5F960A6E3E0854FFD4AEC4FAFFE79
          BDFBFE040F6CFF00F0AE3FEDB5FF00A0BAF6B1FF0047083F67FD0DD39537F3CA
          F975496317C119C11FF36BB1BFE230E3DFBFE040F6CFFF000AEBFEDB5FFA0BAF
          6B1FF47083F67FD0DD3BA7F3E5F99082CBF0525FFA95D8F7FF00DD47BF7FC081
          ED9FFE15D7FDB6BFF4175ED63FE53E0FD9FF00437593FE1FA7E65FFDE0A4BFF5
          2BB1FF00FAD1EF7FF020FB67FF008575FF006DAFFD05D7B58FF94F83FD5FEDBA
          F7FC3F4FCCBFFBC1497FEA5763FF00F5A3DFBFE041F6CFFF000AEBFEDB5FFA0B
          AF6B1FF29F07FABFDB75EFF87E9F997FF78292FF00D4AEC7FF00EB47BF7FC083
          ED9FFE15D7FDB6BFF4175ED63FE53E0FF57FB6EBDFF0FD3F32FF00EF0525FF00
          A95D8FFF00D68F7EFF008107DB3FFC2BAFFB6D7FE82EBDAC7FCA7C1FEAFF006D
          D7BFE1FA7E65FF00DE0A4BFF0052BB1FFF00AD1EFDFF00020FB67FF8575FF6DA
          FF00D05D7B58FF0094F83FD5FEDBAF7FC3F4FCCBFF00BC1497FEA5763FFF005A
          3DFBFE041F6CFF00F0AEBFEDB5FF00A0BAF6B1FF0029F07FABFDB75EFF0087E9
          F997FF0078292FFD4AEC7FFEB47BF7FC083ED9FF00E15D7FDB6BFF004175ED63
          FE53E0FF0057FB6EBDFF000FD3F32FFEF0525FFA95D8FF00FD68F7EFF8107DB3
          FF00C2BAFF00B6D7FE82EBDAC7FCA7C1FEAFF6DD7BFE1FA7E65FFDE0A4BFF52B
          B1FF00FAD1EFDFF020FB67FF008575FF006DAFFD05D7B58FF94F83FD5FEDBAF7
          FC3F4FCCBFFBC1497FEA5763FF00F5A3DFBFE041F6CFFF000AEBFEDB5FFA0BAF
          6B1FF29F07FABFDB75EFF87E9F997FF78292FF00D4AEC7FF00EB47BF7FC083ED
          9FFE15D7FDB6BFF4175ED63FE53E0FF57FB6EBDFF0FD3F32FF00EF0525FF00A9
          5D8FFF00D68F7EFF008107DB3FFC2BAFFB6D7FE82EBDAC7FCA7C1FEAFF006DD7
          BFE1FA7E65FF00DE0A4BFF0052BB1FFF00AD1EFDFF00020FB67FF8575FF6DAFF
          00D05D7B58FF0094F83FD5FEDBAF7FC3F4FCCBFF00BC1497FEA5763FFF005A3D
          FBFE041F6CFF00F0AEBFEDB5FF00A0BAF6B1FF0029F07FABFDB75EFF0087E9F9
          97FF0078292FFD4AEC7FFEB47BF7FC083ED9FF00E15D7FDB6BFF004175ED63FE
          53E0FF0057FB6EBDFF000FD3F32FFEF0525FFA95D8FF00FD68F7EFF8107DB3FF
          00C2BAFF00B6D7FE82EBDAC7FCA7C1FEAFF6DD7BFE1FA7E65FFDE0A4BFF52BB1
          FF00FAD1EFDFF020FB67FF008575FF006DAFFD05D7B58FF94F83FD5FEDBAEBFE
          1FA7E65FFDE0A4BFF52BB1FF00FAD1EF5FF0207B65FF008571FF006DAFFD05D7
          B58FF94F83FD5FEDBA0C3B6FF9C2FC84EF2DA751B27B3FF976637766DCA89127
          38FCCE2B7E64A9E3A98FFCD54C50D660E68D278AFE970030FC1F661B67DD5B92
          364BD8B72D93DF0BBB3DC52BA65865B78DC5788D48E0D0F98E07CC75ED63FE53
          E0FD9FF43740FF00527F315ECBE93A8967D85FCB7B1987599A47929E8709BDB1
          F04B2491B4466AA149818DEB26113150D297D2A6C2C3DAADF3EECDCB1CD13C57
          3CCDEFE5F5FCF1AE9433CD049A01E2175390B5F3D2057CFAF090529F5D07ECFF
          00A1BA67DB1F3D7796CCDF559D85B57F96760B6F6E0AFAC92BAAE6C36DEDE58E
          79E696A3EEA556A8A5DBD1D42413540D6F1A32A3B72C0FB51BAFDDC762DF36FB
          6D9F7AFBC36E579B4434D10CD710C91AE9145ED69083A46149A951C29D7BC415
          FF0073A0FD9FF4374B4ACFE667DA590DBBD95B42B3F96AE0AA36A76D646A733B
          DF6E4B84DF5261B2F9AADAB8EBEB3335541260DA09B29535F12CEF391E433286
          BDF9F6CC5F768E598373DBB7A83DFDBE4DDECE248A09966B712C3146A55238DC
          382A88A4A8518D248A50F5ED63FE53E0FD9FF43759713FCCFBB9F1197EBFDC34
          DFCBBE139FEB1DBF1ED2DA19A6A1EC0192C6ED58675A9A7DB90D5AE144A986A6
          A94574801D0AC0102FED2DC7DD6B932F2D6EEC6EFDF2BA92CAE2E8DCCB1B496C
          524B820A99DD4BD0CA55882E7342475ED6BFF29F07ECFF00A1BAE347FCCDBB4A
          87696FDD870FF2D4C17F72FB26AABEBB746D66C26F96C1565564EBA4C9D74FFC
          31B04697CB539395AA19B4EAF31D770DCFB7CFDD8F950EED6FBF9F7EAFBF7EC2
          104771E34026411208E30B207D4022008A01A051A7875ED6BFF29F07ECFF00A1
          BA0F3697CEBDE7B26AB1B57B7FF96B62685F1587CD6DDA64A5C1EF6A48DB6FEE
          20CB9AC2D4FDB60236AAA0C9A9FDE490B790000F007B5B7FF777DA376663BAFD
          E2772B9ADC473FEA5C40FF00AD0E2294069080F1D4E8614D3534EB5AC7FCA741
          FB3FE86E84F3FCD1BB7E5EA49BA36B7F96F62723D62EB1C706D4AEC3EFBA9C3D
          0C14F6FB2A7A2C7CD837A6A7868028100551E30069B7B2F8BEEC1CA506F2FCC7
          07BEF7B1F30B4AD21B959ADD66323925DCC8AE18B3D4EA35EEAE6BD6F58FF94F
          83F67FD0DD590FF26DEFDC8F616F4ECBDAF53F15A93E39E1B69E031B9DC54F8D
          C767317479DC965324282A69E487278DC7C153550D32EB325E49B480090BEE0D
          FBC6FB6767CA169B173137B9D77CC7BADD4ED031B8952568A3542E3495662A09
          C530BF9F5A494F8EB109E375284F6F114207A9C1AF5B2AE1EACD4C28C7F2A0F1
          FE3C7FBDFBC55E94F4F7EFDD7BAFFFD0DF67358C5AF85D0ADEEADF41FE1C5BFD
          6BFBF75EE8BF6E1EB1FBB959915C73F452401A8FE07201F7A2AA4D699EBDF68E
          9203AAEA50D97CF6FF00831FF7A03DEB4AFA75AA0F4EB97FA2EAAFF9BFFEDCFF
          00C53DFB4AFA75EA0F4EBDFE8BAABFE6FF00FB73FF0014F7ED2BE9D7A83D3AF7
          FA2EAAFF009BFF00EDCFFC53DFB4AFA75EA0F4EBDFE8BAABFE6FFF00B73FF14F
          7ED2BE9D7A83D3AF7FA2EAAFF9BFFEDCFF00C53DFB4AFA75EA0F4EBDFE8BAABF
          E6FF00FB73FF0014F7ED2BE9D7A83D3AF7FA2EAAFF009BFF00EDCFFC53DFB4AF
          A75EA0F4EBDFE8BAABFE6FFF00B73FF14F7ED2BE9D7A83D3AF7FA2EAAFF9BFFE
          DCFF00C53DFB4AFA75EA0F4EBDFE8BAABFE6FF00FB73FF0014F7ED2BE9D7A83D
          3AF7FA2EAAFF009BFF00EDCFFC53DFB4AFA75EA0F4EBDFE8BAABFE6FFF00B73F
          F14F7ED2BE9D7A83D3AF7FA2EAAFF9BFFEDCFF00C53DFB4AFA75EA0F4EBDFE8B
          AABFE6FF00FB73FF0014F7ED2BE9D7A83D3AF7FA2EAAFF009BFF00EDCFFC53DF
          B4AFA75EA0F4EBDFE8BAABFE6FFF00B73FF14F7ED2BE9D7A83D3AF7FA2EAAFF9
          BFFEDCFF00C53DFB4AFA75EA0F4EBDFE8BAABFE6FF00FB73FF0014F7ED2BE9D7
          A83D3AF7FA2EAAFF009BFF00EDCFFC53DFB4AFA75EA0F4EBDFE8BAABFE6FFF00
          B73FF14F7ED2BE9D7A83D3AF7FA2EAAFF9BFFEDCFF00C53DFB4AFA75EA0F4EBD
          FE8BAABFE6FF00FB73FF0014F7ED2BE9D7A83D3AF7FA2EAAFF009BFF00EDCFFC
          53DFB4AFA75EA0F4EBDFE8BAABFE6FFF00B73FF14F7ED2BE9D7A83D3AF7FA2EA
          AFF9BFFEDCFF00C53DFB4AFA75EA0F4EBDFE8BAABFE6FF00FB73FF0014F7ED2B
          E9D7A83D3AF7FA2EAAFF009BFF00EDCFFC53DFB4AFA75EA0F4EBDFE8BAABFE6F
          FF00B73FF14F7ED2BE9D7A83D3AF7FA2EAAFF9BFFEDCFF00C53DFB4AFA75EA0F
          4EBDFE8BAABFE6FF00FB73FF0014F7ED2BE9D7A83D3AF7FA2EAAFF009BFF00ED
          CFFC53DFB4AFA75EA0F4EBB1D5D577E3CFFF00251F7ED2BE9D7A83D3A59EDDD8
          53D1CD197F2108411A896E7FD88FF0F7B000E03AF500E03A1FF114869A144208
          B28FF0FA73EF7D6FA7AF7EEBDD7FFFD1DFDDBE9FD9FAFF006BE9FF0023F7EEBD
          D37CB6E7FE037FC85ABFA1FAFE7DFBAF7509F4DFFE503E9F9F27FC47BF75EEB8
          FA7FEADFFF00597DFBAF75EF4FFD5BFF00EB2FBF75EEBDE9FF00AB7FFD65F7EE
          BDD7BD3FF56FFF00ACBEFDD7BAF7A7FEADFF00F597DFBAF75EF4FF00D5BFFEB2
          FBF75EEBDE9FFAB7FF00D65F7EEBDD7BD3FF0056FF00FACBEFDD7BAF7A7FEADF
          FF00597DFBAF75EF4FFD5BFF00EB2FBF75EEBDE9FF00AB7FFD65F7EEBDD7BD3F
          F56FFF00ACBEFDD7BAF7A7FEADFF00F597DFBAF75EF4FF00D5BFFEB2FBF75EEB
          DE9FFAB7FF00D65F7EEBDD7BD3FF0056FF00FACBEFDD7BAF7A7FEADFFF00597D
          FBAF75EF4FFD5BFF00EB2FBF75EEBDE9FF00AB7FFD65F7EEBDD7BD3FF56FFF00
          ACBEFDD7BAF7A7FEADFF00F597DFBAF75EF4FF00D5BFFEB2FBF75EEBDE9FFAB7
          FF00D65F7EEBDD7BD3FF0056FF00FACBEFDD7BAF7A7FEADFFF00597DFBAF75EF
          4FFD5BFF00EB2FBF75EEBDE9FF00AB7FFD65F7EEBDD7BD3FF56FFF00ACBEFDD7
          BAF7A7FEADFF00F597DFBAF75EF4FF00D5BFFEB2FBF75EEBDE9FFAB7FF00D65F
          7EEBDD7634DC7FC5BFEA3FE3AFBF75EEA5456D5FF289F8FD1AAFF5FF001F7EEB
          DD38A7FC83F41FA6FF00EF17FC7BF75EEB27BF75EEBFFFD9}
        OnClick = Image1Click
      end
      object Image2: TImage
        Left = 376
        Top = 92
        Width = 137
        Height = 81
        Cursor = crHandPoint
        Hint = 'http://www.mysql.com/products/embedded/'
        Picture.Data = {
          07544269746D6170DA240000424DDA240000000000005A020000280000007E00
          000045000000010008000000000080220000120B0000120B0000890000008900
          0000634D0000635100006B510000635108006B5408006B5908006B5510006B59
          1000735B1000735D180073611800736421007B6421007B6921007B6A29007B6D
          31007B713100846D2900846D310084713100847539008C753900847939008C79
          39008C7B42008C7D4A008C824A0094824A00948652009C865200948A52009C8A
          52009C8C5A009C8E63009C926300A5936300A5966B00A59A6B00AD9A6B00A59B
          7300AD9D7300ADA27300ADA27B00B5A57B00007CEF00007DF7000882EF001387
          EF000882F7001487F700218EEF002992EF003196EF00399AEF00218EF7002992
          F7003193F700399AF700429CEF00429EF7004AA2F70052A5F70052AAF7005AAB
          F70063AEF70063B2F7006BB3F7006BB4FF0076B8FA00B5A68400B5AA8400B5AE
          8C00BDAE8C00BDB28C00BDB39400C6B49400C6B69C00C6BA9C00C6BEA500CEBE
          A500C6C3A500CEC3A500CEC5AD00D6C7AD00CEC7B500CECBB500D6C7B500D6CB
          B500D6CFBD00DECFBD00D6D3BD00DED3BD0084BEFB0084C3F7008CC3F70084C3
          FF008CC5FF0094C7F70094CBF7009CCDF70094C7FF0094CBFF009CCCFF00A5CF
          FC00ABD4FF00B5D7FF00B5DBFF00BDDCFF00DED5C600DEDBCE00E7DCCE00E7DF
          D600E7E3D600EFE3D600E7E5DE00EFE6DE00EFEBDE00C6DFFF00C6E3FF00CEE5
          FF00D6E7FF00D6EBFF00DEEEFF00EFECE700F7EDE700F7EFEF00F7F3EF00FFF3
          EF00E7EFFF00EAF4FF00F7F6F700FFF6F700FFFBF700F7F7FF00F7FBFF00FFFE
          FF00000000008787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          87878787880B8787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          87878787500E8787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878703118787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878788138787878787878787878787878787878787878787878787878787
          87878787878787520E0405080607050807070508060C18487C87878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878769168787878787878787878787878787878787878787878787878787
          878787878787874A0D00010104000101040001010400070B1855838787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          878787871C198787878787878787878787878787878787878787878787878787
          878787878787876E462A2A2B2A2B2A2B2A2B2A2B2818070103186F8787878787
          8787878787878787878787878787878787878787878787878787878787878787
          878064645D677887878787878787878787878787878787878787878787878787
          87878787881B8787878787878787878787878787878787878787878787878787
          87878787878787837B737C7B7C7B7C7B7C7B7C7B73511C000100578487878787
          8787878787878787878787878787878787878787878787878787878787878786
          7642373236447887878787878787878787878787878787878787878787878787
          87878787831E8787877E7C7D7E87878787878787847C70737E87878787878787
          837D7B7D83878787878787878783828383838283837F460200024E8387878181
          8181818181818181818186878787878787878787878681808080808081818175
          3F2C2C313C6A8787878787878681818181818181818181818187878781808086
          8787878736218787874A4946527B87878787877E6C2820234F7B878787878782
          6C4746496C87878787837E736C52575257525752574E200001004F7E86775F5E
          605E605E605E605E60616A7A81868787878787817A6A60444444444460636635
          2C2C2E407A878787878681786B63645E605E605E605E605C677A8781675C646A
          8787878788238787870404041B6C87878787835204040304041B87878787877C
          2408040427878787874F180604060404040604040404030100024F837A442E30
          2E302E302E302E302E302E313C768787878779432F312E302E302E302E302E2E
          2C2D2F6881878787866038312E302E302E302E302E302E303B6886444066443C
          6887878788268787870402001B6C87878787581401000100010B4B8487878774
          280401052B878784240B0600010A0C0A0A090A090C0A080001004F7E81602E2E
          2F2F312F2F2F312F2E2C2C2C2F34668787803F2F2E2C2C2C2F2F362F2C2C2C2C
          2C2C2E326681877A44322E2C2C2C312F2F2F312F2F2F312E3C6B81445C445C63
          448787874F298787870801021B6E878787874A0600010C09000714838787877C
          280700092B8787590E0100114E575757525752575752220200024E83867A6768
          686868686868686868653D302C3040818767322D2C3140676869613D2C2C2C3B
          3D372C2D3D758777322E2C3844686868686868686868686875817A5C5D434464
          428787871C008787870704001B6C8787876C2000040946180100085A87878774
          28040107458787480C00041B87878787878787878782480001004F7E87878787
          878787878787878787866B2F2C2C3D7786422F2C2C34778787775F342C2C3963
          693F2C2C386887682C2C2C427A878787878787878787878787878144403F3F40
          6687878718008787870903011B6E8787874A0C010424732B070204257387877C
          280700092B87874A0C02042087878787878787878783460100024E8387878787
          8787878787878787878781312C2D3C7786432F2C2C3B878787673D39393C6781
          81642C2C336986682C2D2C66878787878787878787878787878787674243425F
          7887878714008787870704001C6D87877E20010008588450170001055B878774
          28040107458787480C00041C8787878787878787877E460001004F7E87878787
          87878787878787878787812F2C2C3D768642312C2C3A878786766A6B767A8787
          815E2C2C376886672C2C2C5E87878787878787878787878787878787796B7680
          8787878711008787870901021B6E87874E1400040C7E878345010002487C877C
          280700092B87844B0C01042087878787878787878783450200014E8387878787
          878787878787878787866A312C2C3C7786442F2D2C3B87878787878787878787
          80602C2D376986682C2C2C608687878787878787878787878787878787878787
          878787870D008787870704001C6D87871F03010A468387874F0A010013558774
          28040107458787480C00041C8787878787878787877E460001004E7E87878787
          7963444242404140403A362C2C2C428686422F2C2C3587878787878787878787
          815D2C2C386886672C2C2C5E8687878787878787878787878787878787878787
          8787878709008787870903011B6F876D13020015738787876F260001044A837C
          280700092B87874A0C02042087878787878787878783450100024E8387877743
          3B3832312F312F312F312C2C3244778786442F2C2C3B87878787878787878787
          80602C2C336986682C2D2C608687878787878787878787878787878787878787
          8787878706008787870704001C6E842309000922878787878352040001206C74
          2A040107458787480C00041C8787878787878787877E460001004F7E87793F2E
          2C2E373C3D3D3D3D3D3D4244667987878641312C2C3A87878787878787878787
          815D2C2C376886672C2C2C5E8687878787878787878787878787878787878787
          8787878702008787870901021F737B0C010116568787878787701404000C487D
          450700092B8784480C01041C8787878787878787877E450200024D8386682C2D
          2C3B5E757576757675777A878787878786432F2D2C3B87878787878787878787
          80602C2D376986682C2C2C608687878787878787878787878787878787878787
          878787878800878787070400207057070101247E87878787877E4D0701002373
          4904010745878748150A0C208787878787878787877C480A0B09517E815E2C2C
          2C5E878787878787878787878787878786412F2C2C3A87878787878787878787
          815D2C2C386886672C2C2C5E8687878787878787878787878787878787878783
          878787877E0087878709030120591B04000E49878787878787877E0A0002184E
          4D0800092B87877C5756576F878787878787878787876E4D4D4D738781662C2C
          2C66878787878787878787878787878786432F2C2C3C87878787878787878787
          81642C2C336986682C2D2C608687878787878787878787878787878787877358
          7B8787876B008787870704001727060001226F8787878787878784240A000918
          4504010745878787878387848787878787878787878787828382878781602C2C
          2C6287878787878787878787878787878641312C2C3A87878787878787878787
          79422C2C376886672C2C2C5E8687878787878787878787878787878787552847
          878787875800878787080102080E0002044A8387878787878787876F13010109
          130400082B878787878787878787878787878787878787878787878786682C2D
          2C3E6779797A797A797A797A79797A878664322D2C31426B767777777777776A
          44362C2D3B7586682C2C2C608687878787878787878787878787877C2A1B2A73
          8787878745008787870401000101010018588787878787878787878723060100
          040001074587878787878787878787878787878787878787878787878776392C
          2C2F393D3F3F3F3F3F3F3F3F3F3E447587783C2E2C2C2F3B3D3D3D3D3D3D3D3B
          362C2C2E648085662C2C2C5E868787878787878787878787876E482213246D87
          8787878733008787871C08010002010C4A7D87878787878787878787521B0401
          000203185287878787878787878787878787878787878787878787878781663D
          32312C2C2C2D2C2C2C2D2C2C2C2D38678787674034312C2E2C2D2C2C2C2D2C2C
          2E363B447A8781662C2D2C6086878787878787878787877D57260A12256F8787
          8787878720008787876C2010110E14257B8787878787878787878787874D1B0E
          0E0F184D7E878787878787878787878787878787878787878787878787878777
          5F3D393439343934393439343934416B87878779663F3B353934393439343935
          3D446B878787866839343967878787878787878787877E4A180717497D878787
          878787870D0087878787877F6F6E7C8787878787878787878787878787878271
          6C7C878787878787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878746110A28828787878787
          878787874A008787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          878787878787878787878787878787878787878787581407287E878787878787
          8787878740008787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787877D7C878787878787878787878787844B0C040C1C202B4C6E7E87
          8787878737008787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          87878787876F6E7B878787878787878787878787876D4E292014171617205387
          878787872D008787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          87878787595870878787878787878787878787878787877D6C524A1B091C5787
          8787878724008787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787727C84
          878787522A58878787878787878787878787878787878787877B581C1B528787
          878787871A008787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          878787878787878787878787878787878787878787878787878787846C202B70
          87876D0A4D7F878787878787878787878787878787878787704804136E878787
          8787878710008787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          878787878787878787878787878787878787878787878787878787570703064A
          876C0A1A6F8787878787878787878787878787878787874D180B247B87878787
          8787878707008787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          87878787878787878787878787878787878787878787878787875A1D13480D48
          6E46076E878787878787878787878787878787876C451B0E1A51748787878787
          8787878780808787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          87878787878787878787878787878787878787878787878787844C1352701420
          2B182582878787878787878787878787877E52221B0B182A5B7E878787878787
          8787878780808787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          878787878787878787878787878787878787878787878787877C281884871811
          0E156E87878787878787878787878787875618091C28527D8787878787878787
          8787878780808787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          87878787878787878787878787878787878787878787878787571C1A87871B01
          092387878787878787878787878787877E20081C6F8787878787878787878787
          8787878780808787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          878787878787878787878787878787878787878787878787874C172487872409
          196C87878787878787878787878787874E151859878787878787878787878787
          8787878780808787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878747192B87876E1B
          248787878787878787878787878787871B012587878787878787878787878787
          8787878780808787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          878787878787878787878787878787878787878787878787874818468787846E
          6D87878787878787878787878787876C12185287878787878787878787878787
          8787878780808787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878747182A87878784
          8787878787878787878787878787831E082A7D87878787878787878787878787
          8787878780808787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          878787878787878787878787878787878787878787878787874F1A2487878787
          87878787878787878787878787876E0C11578787878787878787878787878787
          8787878780808787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          878787878787878787878787878787878787878787878787876C221A87878787
          878787878787878787878787877B46042B728787878787878787878787878787
          8787878700008787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          878787878787878787878787878787878787878787878787877F45187E878787
          878787878787878787878787876E0A086C878787878787878787878787878787
          8787878700008787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          87878787878787878787878787878787878787878787878787874D0B4D848787
          8787878787878787878787878754041971878787878787878787878787878787
          8787878700008787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          87878787878787878787878787878787878787878787878787874E0E20838787
          8787878787878787878787876D22075382878787878787878787878787878787
          8787878700008787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          87878787878787878787878787878787878787878787878787732A0D4A848787
          8787878787878787878787844D0A0D7387878787878787878787878787878787
          8787878700008787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          878787878787878787878787878787878787878787878787874F1B2373878787
          87878787878787878787876C1C0C468787878787878787878787878787878787
          8787878700008787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787877C20185787878787
          878787878787878787877C250C1C7D8787878787878787878787878787878787
          8787878700008787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787874A17298787878787
          878787878787878787874C151752878787878787878787878787878787878787
          8787878700008787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787871817528787878787
          8787878787878787876E13074587878787878787878787878787878787878787
          8787878788888787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          87878787878787878787878787878787878787878787877C14457C8787878787
          8787878787878787731304457D87878787878787878787878787878787878787
          8787878788888787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          87878787878787878787878787878787878787878787874A0C4A878787736E7B
          878787878787876C1801286F8787878787878787878787878787878787878787
          8787878788888787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          878787878787878787878787878787878787878787878218145787878328276E
          878787878783551F04246E878787878787878787878787878787878787878787
          8787878788888787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          878787878787878787878787878787878787878787846E072A7087877309246E
          87878787734D1B07237087878787878787878787878787878787878787878787
          8787878788888787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787877B280E5A8787876C216D87
          878783732B140B247E8787878787878787878787878787878787878787878787
          8787878788888787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787835207297B87878787878787
          877E580D07134D87878787878787878787878787878787878787878787878787
          8787878788888787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          87878787878787878787878787878787878787874E0B0F7C8787878787878773
          48150804286E8487878787878787878787878787878787878787878787878787
          8787878788888787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          878787878787878787878787878787878787876C200F6E87878770451B181B14
          0D0A1E527B848787878787878787878787878787878787878787878787878787
          878787872E008787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878748174587877E5622110A181B23
          2B4F6C8787878787878787878787878787878787878787878787878787878787
          8787878769008787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          87878787878787878787878787878787878787251346715248170E1A474F576D
          7E87878787878787878787878787878787878787878787878787878787878787
          8787878707008787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          878787878787878787878787878787878787874B1418270E0B13256C87878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878724008787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          878787878787878787878787878787878787877C46030107246D878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          878787872E008787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878748008787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878707008787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          878787872400}
        Transparent = True
        OnClick = lblMySQLEmbeddedClick
      end
      object Image3: TImage
        Left = 416
        Top = 188
        Width = 91
        Height = 22
        Cursor = crHandPoint
        AutoSize = True
        Picture.Data = {
          0954474946496D6167654749463839615B001600C40000FF1010FFEFEFFF2020
          FFCFCFFF3030FF9F9FFF5050FF7070FFDFDFFF8F8FFFAFAFFF6060FF8080FF40
          40FFBFBFFFFFFFFF000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000021F90400000000002C
          000000005B00160084FF1010FFEFEFFF2020FFCFCFFF3030FF9F9FFF5050FF70
          70FFDFDFFF8F8FFFAFAFFF6060FF8080FF4040FFBFBFFFFFFFFF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000005FF20248E64699E68AAAE6CEBBE702CCF746DDF78AEEF
          7CDF37C0609004101A0928C1419034068B0DC04AE00402A82A03E2C1ED76030C
          51C34B7E38A42441E011408F008E321701E0365280829C0D18040E280B5D0E84
          85715C05106366860E035C09250A5D916F8F0F088D0E7776287D739A06107E0B
          276A0F034C2679768B270C6C24065C8777227A01A2289D27095C80494827077C
          2A710AAE26C910005B0EA3A822750FC0BB0FB6266B6133939529C49B5C260293
          0823BE0F4C8B61CBD6D824047B5D892ADD2B82E10F045B65C0F10FDB20E0A222
          4E05AF12FFF6B459018B8EB107C8C47DEA82A0DA23300C32A23BC3A5548A8325
          7ED11080C84D0974069283C1E1121002AC6B93CA18981440D809902470BD83F1
          32802607FC12B5D3F380DE2905CC322A8D53730DAA9FB39EFE5CE07400541607
          9CEEA9D40E02BA012B178E5B5380C0A1320320682D4347C059320E590030A0B4
          EE0255100464344197C1828CBA4EF41541E040DD8C48081CAE8BADF0629B3E22
          4B9E4CB9B2E5CB98336BDE2C39040021F90400000000002C000000005B001600
          84FF1010FFEFEFFF2020FFCFCFFF3030FF9F9FFF5050FF7070FFDFDFFF8F8FFF
          AFAFFF6060FF8080FF4040FFBFBFFFFFFFFF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0005FF20248E64699E68AAAE6CEBBE702CCF746DDF78AEEF7CDF37C060900410
          1A0928C1419034068B0DC04AE00402A82A03E2C1ED76030C51C34B7E38A42441
          E011408F008E321701E0365280829C0D18040E280B5D0E8485715C0510636686
          0E035C09250A5D916F8F0F088D0E7776287D739A06107E0B276A0F034C267976
          8B270C6C24065C8777227A01A2289D27095C80494827077C2A710AAE26C91000
          5B0EA3A822750FC0BB0FB6266B6133939529C49B5C2602930823BE0F4C8B61CB
          D6D824047B5D892ADD2B82E10F045B65C0F10FDB20E0A2224E05AF12FFF6B459
          018B8EB107C8C47DEA82A0DA23300C32A23BC3A5548A83257ED11080C84D0974
          069283C1E1121002AC6B93CA18981440D809902470BD83F132802607FC12B5D3
          F380DE2905CC322A8D53730DAA9FB39EFE5CE074005416079CEEA9D40E02BA01
          2B178E5B5380C0A1320320682D4347C059320E590030A0B4EE02551004643441
          97C1828CBA4EF41541E040DD8C48081CAE8BADF0629B3E224B9E4CB9B2E5CB98
          336BDE2C39040021F90400000000002C000000005B00160084FF1010FFEFEFFF
          2020FFCFCFFF3030FF9F9FFF5050FF7070FFDFDFFF8F8FFFAFAFFF6060FF8080
          FF4040FFBFBFFFFFFFFF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000005FF20248E6469
          9E68AAAE6CEBBE702CCF746DDF78AEEF7CDF37C0609004101A0928C141903406
          8B0DC04AE00402A82A03E2C1ED76030C51C34B7E38A42441E011408F008E3217
          01E0365280829C0D18040E280B5D0E8485715C05106366860E035C09250A5D91
          6F8F0F088D0E7776287D739A06107E0B276A0F034C2679768B270C6C24065C87
          77227A01A2289D27095C80494827077C2A710AAE26C910005B0EA3A822750FC0
          BB0FB6266B6133939529C49B5C2602930823BE0F4C8B61CBD6D824047B5D892A
          DD2B82E10F045B65C0F10FDB20E0A2224E05AF12FFF6B459018B8EB107C8C47D
          EA82A0DA23300C32A23BC3A5548A83257ED11080C84D0974069283C1E1121002
          AC6B93CA18981440D809902470BD83F132802607FC12B5D3F380DE2905CC322A
          8D53730DAA9FB39EFE5CE074005416079CEEA9D40E02BA012B178E5B5380C0A1
          320320682D4347C059320E590030A0B4EE0255100464344197C1828CBA4EF415
          41E040DD8C48081CAE8BADF0629B3E224B9E4CB9B2E5CB98336BDE2C39040021
          F90400000000002C000000005B00160084FF1010FFEFEFFF2020FFCFCFFF3030
          FF9F9FFF5050FF7070FFDFDFFF8F8FFFAFAFFF6060FF8080FF4040FFBFBFFFFF
          FFFF000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000005FF20248E64699E68AAAE6CEBBE70
          2CCF746DDF78AEEF7CDF37C0609004101A0928C1419034068B0DC04AE00402A8
          2A03E2C1ED76030C51C34B7E38A42441E011408F008E321701E0365280829C0D
          18040E280B5D0E8485715C05106366860E035C09250A5D916F8F0F088D0E7776
          287D739A06107E0B276A0F034C2679768B270C6C24065C8777227A01A2289D27
          095C80494827077C2A710AAE26C910005B0EA3A822750FC0BB0FB6266B613393
          9529C49B5C2602930823BE0F4C8B61CBD6D824047B5D892ADD2B82E10F045B65
          C0F10FDB20E0A2224E05AF12FFF6B459018B8EB107C8C47DEA82A0DA23300C32
          A23BC3A5548A83257ED11080C84D0974069283C1E1121002AC6B93CA18981440
          D809902470BD83F132802607FC12B5D3F380DE2905CC322A8D53730DAA9FB39E
          FE5CE074005416079CEEA9D40E02BA012B178E5B5380C0A1320320682D4347C0
          59320E590030A0B4EE0255100464344197C1828CBA4EF41541E040DD8C48081C
          AE8BADF0629B3E224B9E4CB9B2E5CB98336BDE2C3904003B}
        Transparent = True
        OnClick = lblMySQLEmbeddedClick
      end
      object Image4: TImage
        Left = 360
        Top = 77
        Width = 93
        Height = 53
        OnClick = Panel3Click
      end
      object Image5: TImage
        Left = 488
        Top = 68
        Width = 25
        Height = 53
        OnClick = Panel3Click
      end
      object Image6: TImage
        Left = 442
        Top = 41
        Width = 55
        Height = 53
        OnClick = Panel3Click
      end
      object cxLabel4: TcxLabel
        Left = 8
        Top = 7
        Caption = #928#953#941#963#964#949' [ESC] '#947#953#945' '#964#951#957' '#945#960#972#954#961#965#968#951' '#964#969#957' credits.'
        Enabled = False
        Transparent = True
        OnClick = Panel3Click
      end
      object cxLabel5: TcxLabel
        Left = 32
        Top = 204
        Caption = #915#953#940#957#957#951#962' '#913#957#965#966#945#957#964#940#954#951#962
        Style.TextStyle = [fsBold]
        Transparent = True
        OnClick = Panel3Click
      end
      object cxLabel7: TcxLabel
        Left = 32
        Top = 220
        Caption = 'ioannisanif@gmail.com'
        Transparent = True
        OnClick = Panel3Click
      end
      object cxLabel6: TcxLabel
        Left = 256
        Top = 216
        Caption = #932#959' '#960#961#972#947#961#945#956#956#945' '#945#965#964#972' '#948#953#945#964#943#952#949#964#945#953' '#967#969#961#943#962' '#967#961#941#969#963#951'.'
        Transparent = True
        OnClick = Panel3Click
      end
      object cxLabel8: TcxLabel
        Left = 256
        Top = 228
        Caption = #928#949#961#953#956#941#957#969' '#949#961#969#964#942#963#949#953#962' '#954#945#953' '#960#961#959#964#940#963#949#953#962' '#963#964#959' email '#956#959#965'. '
        Transparent = True
        OnClick = Panel3Click
      end
      object cxLabel9: TcxLabel
        Left = 256
        Top = 241
        Caption = #931#945#962' '#949#965#967#945#961#953#963#964#974' '#947#953#945' '#964#951#957' '#960#961#959#964#943#956#951#963#942' '#963#945#962'.'
        Transparent = True
        OnClick = Panel3Click
      end
      object cxLabel10: TcxLabel
        Left = 8
        Top = 189
        Caption = #928#961#959#947#961#945#956#956#945#964#953#963#956#972#962' '#945#960#972':'
        Transparent = True
        OnClick = Panel3Click
      end
      object cxLabel11: TcxLabel
        Left = 207
        Top = 108
        Cursor = crHandPoint
        Caption = 'InvoiceX - v1.3.4'
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -13
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.TextStyle = [fsBold]
        Style.IsFontAssigned = True
        Transparent = True
        OnClick = Image1Click
      end
      object lblMySQLEmbedded: TcxLabel
        Left = 376
        Top = 153
        Cursor = crHandPoint
        Hint = 'http://www.mysql.com/products/embedded/'
        Caption = 'MySQL Embedded'
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -13
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.TextColor = clBlue
        Style.TextStyle = [fsUnderline]
        Style.IsFontAssigned = True
        Transparent = True
        OnClick = lblMySQLEmbeddedClick
      end
      object lblMarquee: TcxLabel
        Left = 8
        Top = 146
        AutoSize = False
        Caption = 
          #922#974#963#964#945#962' '#934#961#945#947#954#959#973#955#951#962'     '#923#949#965#964#941#961#951#962' '#928#945#960#945#948#945#957#964#969#957#940#954#951#962'     '#924#953#967#940#955#951#962' '#922#965#960#961#953#969 +
          #964#940#954#951#962'     '#917#955#941#957#951' '#934#961#945#947#954#953#959#965#948#940#954#951'     '#925#943#954#959#962' '#923#953#957#945#961#948#940#954#951#962'     '
        Style.TextStyle = [fsBold]
        Transparent = True
        OnClick = Panel3Click
        Height = 17
        Width = 346
      end
      object cxLabel13: TcxLabel
        Left = 8
        Top = 133
        Caption = #928#959#955#955#940' '#949#965#967#945#961#953#963#964#974' '#947#953#945' '#964#953#962' '#953#948#941#949#962' '#964#959#965#962' '#963#964#959#965#962
        Transparent = True
        OnClick = Panel3Click
      end
      object cxLabel12: TcxLabel
        Left = 32
        Top = 236
        Cursor = crHandPoint
        Caption = 'http://www.cybernate.gr/'
        Style.TextColor = clBlue
        Style.TextStyle = [fsUnderline]
        Transparent = True
        OnClick = Image1Click
      end
      object lblMySQLVersion: TcxLabel
        Left = 398
        Top = 168
        Caption = 'server version number'
        Properties.Alignment.Horz = taRightJustify
        Transparent = True
        OnClick = Panel3Click
        AnchorX = 510
      end
    end
  end
  object EmbeddedConnection: TMyEmbConnection
    Database = 'invoices'
    Options.UseUnicode = True
    Options.Charset = 'greek'
    Options.NullForZeroDelphiDate = True
    Params.Strings = (
      '--basedir=.'
      '--datadir=data')
    AfterConnect = EmbeddedConnectionAfterConnect
    LoginPrompt = False
    Left = 512
    Top = 24
  end
  object MyScript1: TMyScript
    SQL.Strings = (
      
        '/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */' +
        ';'
      
        '/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS ' +
        '*/;'
      
        '/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */' +
        ';'
      '/*!40101 SET NAMES utf8 */;'
      
        '/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREI' +
        'GN_KEY_CHECKS=0 */;'
      ''
      'DROP TABLE IF EXISTS `usr`;'
      'CREATE TABLE  `usr` ('
      '  `idusr` int(10) unsigned NOT NULL AUTO_INCREMENT,'
      '  `fullname` varchar(45) NOT NULL,'
      '  `anualgoal` double NOT NULL DEFAULT '#39'0'#39','
      '  PRIMARY KEY (`idusr`),'
      '  UNIQUE KEY `Index_2` (`fullname`)'
      ') ENGINE=InnoDB DEFAULT CHARSET=utf8;'
      ''
      'DROP TABLE IF EXISTS `expensemastertype`;'
      'CREATE TABLE  `expensemastertype` ('
      
        '  `idexpenseMasterType` int(10) unsigned NOT NULL AUTO_INCREMENT' +
        ','
      '  `descr` varchar(45) NOT NULL,'
      '  PRIMARY KEY (`idexpenseMasterType`),'
      '  UNIQUE KEY `Index_2` (`descr`)'
      ') ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;'
      ''
      'DROP TABLE IF EXISTS `expensetype`;'
      'CREATE TABLE `expensetype` ('
      '  `idexpenseType` int(10) unsigned NOT NULL AUTO_INCREMENT,'
      '  `descr` varchar(85) NOT NULL,'
      '  `idExpenseMasterType` int(10) unsigned NOT NULL,'
      '  `included` tinyint(1) NOT NULL DEFAULT '#39'1'#39','
      '  PRIMARY KEY (`idexpenseType`),'
      
        '  UNIQUE KEY `Index_2` (`idExpenseMasterType`,`descr`) USING BTR' +
        'EE,'
      '  KEY `Index_3` (`included`),'
      
        '  CONSTRAINT `FK_expensetype_1` FOREIGN KEY (`idExpenseMasterTyp' +
        'e`) REFERENCES `expensemastertype` (`idexpenseMasterType`)'
      ') ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;'
      ''
      'DROP TABLE IF EXISTS `store`;'
      'CREATE TABLE `store` ('
      '  `idstore` int(10) unsigned NOT NULL AUTO_INCREMENT,'
      '  `descr` varchar(45) DEFAULT NULL,'
      '  `afm` varchar(9) DEFAULT NULL,'
      '  `idExpenseType` int(10) unsigned zerofill DEFAULT NULL,'
      '  PRIMARY KEY (`idstore`),'
      '  UNIQUE KEY `Index_2` (`afm`),'
      '  KEY `FK_store_1` (`idExpenseType`),'
      
        '  CONSTRAINT `FK_store_1` FOREIGN KEY (`idExpenseType`) REFERENC' +
        'ES `expensetype` (`idexpenseType`)'
      ') ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;'
      ''
      'DROP TABLE IF EXISTS `usrinv`;'
      'CREATE TABLE `usrinv` ('
      '  `idusrinv` int(10) unsigned NOT NULL AUTO_INCREMENT,'
      '  `idusr` int(10) unsigned NOT NULL,'
      '  `invnum` varchar(25) NOT NULL,'
      '  `invDate` date NOT NULL,'
      '  `idstore` int(10) unsigned NOT NULL,'
      '  `amount` double NOT NULL,'
      '  `reason` longtext,'
      '  `idExpenseType` int(10) unsigned NOT NULL,'
      '  `included` tinyint(1) NOT NULL DEFAULT '#39'1'#39','
      '  PRIMARY KEY (`idusrinv`),'
      '  KEY `FK_usrinv_1` (`idusr`),'
      '  KEY `FK_usrinv_2` (`idstore`),'
      '  KEY `FK_usrinv_3` (`idExpenseType`),'
      '  KEY `Index_5` (`included`),'
      
        '  CONSTRAINT `FK_usrinv_1` FOREIGN KEY (`idusr`) REFERENCES `usr' +
        '` (`idusr`),'
      
        '  CONSTRAINT `FK_usrinv_2` FOREIGN KEY (`idstore`) REFERENCES `s' +
        'tore` (`idstore`),'
      
        '  CONSTRAINT `FK_usrinv_3` FOREIGN KEY (`idExpenseType`) REFEREN' +
        'CES `expensetype` (`idexpenseType`)'
      ') ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;'
      ''
      
        'INSERT INTO `expensemastertype` (`idexpenseMasterType`,`descr`) ' +
        'VALUES'
      ' (1,'#39#902#947#957#969#963#964#959#39'),'
      ' (2,'#39#916#953#945#963#954#941#948#945#963#951#39'),'
      ' (3,'#39#917#954#960#945#943#948#949#965#963#951#39'),'
      ' (4,'#39#904#957#948#965#963#951#39'),'
      ' (5,'#39#917#960#953#963#954#949#965#941#962#39'),'
      ' (6,'#39#921#945#964#961#953#954#940#39'),'
      ' (7,'#39#922#945#964#959#953#954#943#948#953#959#39'),'
      ' (8,'#39#923#959#947#945#961#953#945#963#956#959#943#39'),'
      ' (9,'#39#923#959#953#960#940' '#904#958#959#948#945#39'),'
      ' (10,'#39#924#949#964#945#954#953#957#942#963#949#953#962#39'),'
      ' (11,'#39#927#953#954#953#945#954#972#962' '#917#958#959#960#955#953#963#956#972#962#39'),'
      ' (12,'#39#927#960#964#953#954#940#39'),'
      ' (13,'#39#928#949#961#953#960#959#943#951#963#951' '#931#974#956#945#964#959#962#39'),'
      ' (15,'#39#928#961#972#963#964#953#956#945#39'),'
      ' (14,'#39#928#961#959#963#969#960#953#954#940' '#904#958#959#948#945#39'),'
      ' (16,'#39#932#945#958#943#948#953#945#39'),'
      ' (17,'#39#932#961#972#966#953#956#945#39'),'
      ' (18,'#39#936#953#955#953#954#940#39');'
      ''
      
        ' INSERT INTO `expensetype` (`idexpenseType`,`descr`,`idExpenseMa' +
        'sterType`) VALUES'
      ' (3,'#39#39',1),'
      ' (4,'#39'CD, DVD, Video Club'#39',2),'
      ' (5,'#39'Internet (Online '#931#965#957#948#961#959#956#941#962', '#928#945#953#967#957#943#948#953#945')'#39',2),'
      ' (6,'#39#917#955#949#973#952#949#961#959#962' '#935#961#972#957#959#962' ('#935#972#956#960#965', '#934#969#964#959#947#961#945#966#943#945', '#917#960#953#964#961#945#960#941#950#953#945')'#39',2),'
      ' (7,'#39#917#963#964#953#945#964#972#961#953#945', '#932#945#946#941#961#957#949#962', '#924#949#950#949#948#959#960#969#955#949#943#945#39',2),'
      ' (8,'#39#922#945#966#949#964#941#961#953#945', '#924#960#945#961', '#928#943#963#964#945', Club'#39',2),'
      ' (9,'#39#922#953#957#951#956#945#964#959#947#961#940#966#959#962', '#920#941#945#964#961#959', '#915#942#960#949#948#959', '#922#945#955#969#948#953#945#954#942#39',2),'
      ' (10,'#39#932#965#967#949#961#940' '#928#945#953#967#957#943#948#953#945', '#932#950#972#947#959#962#39',2),'
      ' (11,'#39#914#953#946#955#943#945', '#932#963#940#957#964#949#962', '#915#961#945#966#953#954#942' '#910#955#951#39',3),'
      ' (12,'#39#915#965#956#957#945#963#964#942#961#953#959', '#928#959#955#949#956#953#954#941#962' '#964#941#967#957#949#962#39',3),'
      ' (13,'#39#921#948#953#945#943#964#949#961#945' '#924#945#952#942#956#945#964#945#39',3),'
      ' (14,'#39#926#941#957#949#962' '#915#955#974#963#963#949#962#39',3),'
      ' (15,'#39#931#949#956#953#957#940#961#953#945', '#931#965#957#941#948#961#953#945#39',3),'
      ' (16,'#39#931#967#959#955#953#954#940' '#934#961#959#957#964#953#963#964#942#961#953#945#39',3),'
      ' (17,'#39#937#948#949#943#959', '#935#959#961#972#962', '#922#945#955#955#953#964#949#967#957#953#954#940#39',3),'
      ' (18,'#39#913#958#949#963#959#965#940#961' ('#932#963#940#957#964#949#962', '#918#974#957#949#962', '#922#945#960#941#955#945')'#39',4),'
      ' (19,'#39#922#945#955#955#965#957#964#953#954#940#39',4),'
      ' (20,'#39#928#945#960#959#973#964#963#953#945#39',4),'
      ' (21,'#39#929#959#973#967#945' ('#913#947#959#961#940', '#924#959#948#943#963#964#961#945', '#922#945#952#945#961#953#963#964#942#961#953#959')'#39',4),'
      ' (22,'#39#931#960#943#964#953' ('#933#948#961#945#965#955#953#954#940', '#919#955#949#954#964#961#953#954#940', '#935#961#974#956#945#964#945')'#39',5),'
      ' (23,'#39#917#958#949#964#940#963#949#953#962#39',6),'
      ' (24,'#39#917#960#949#956#946#940#963#949#953#962', '#935#949#953#961#959#965#961#947#949#943#945', '#915#941#957#957#949#962#39',6),'
      ' (25,'#39#917#960#953#963#954#941#968#949#953#962' '#921#945#964#961#974#957#39',6),'
      ' (26,'#39#920#949#961#945#960#949#943#949#962' (Spa, '#924#945#963#940#950', '#934#965#963#953#959#952#949#961#945#960#949#943#949#962')'#39',6),'
      ' (27,'#39#927#961#952#959#960#949#948#953#954#940', '#913#957#945#964#959#956#953#954#940', '#914#959#951#952#942#956#945#964#945#39',6),'
      ' (28,'#39#934#940#961#956#945#954#945#39',6),'
      ' (29,'#39#913#958#949#963#959#965#940#961#39',7),'
      ' (30,'#39#921#945#964#961#953#954#940' '#904#958#959#948#945#39',7),'
      ' (31,'#39#932#961#972#966#953#956#945', '#922#945#952#951#956#949#961#953#957#942' '#934#961#959#957#964#943#948#945#39',7),'
      ' (32,'#39#913#963#966#940#955#949#953#949#962' '#918#969#942#962', '#913#965#964#959#954#953#957#942#964#959#965', '#931#960#953#964#953#959#973#39',8),'
      ' (33,'#39#916#917#922#927' ('#917#933#916#913#928', '#927#932#917', '#916#917#919')'#39',8),'
      ' (34,'#39#917#957#945#955#955#945#954#964#953#954#942' '#932#951#955#949#966#969#957#943#945' (Forthnet, Tellas, On '#954'.'#955'.'#960'.)'#39',8),'
      ' (35,'#39#917#957#959#943#954#953#959#39',8),'
      ' (36,'#39#922#953#957#951#964#942' '#932#951#955#949#966#969#957#943#945#39',8),'
      ' (37,'#39#922#959#953#957#972#967#961#951#963#964#945', '#928#949#964#961#941#955#945#953#959', '#934#965#963#953#954#972' '#913#941#961#953#959#39',8),'
      ' (38,'#39#39',9),'
      ' (39,'#39#913#947#959#961#940' '#913#965#964#959#954#953#957#942#964#959#965', '#924#951#967#945#957#942#962', Service'#39',10),'
      ' (40,'#39#914#949#957#950#943#957#951', '#928#955#965#957#964#942#961#953#959#39',10),'
      ' (41,'#39#916#953#972#948#953#945#39',10),'
      ' (42,'#39#917#953#963#953#964#942#961#953#945', '#922#940#961#964#949#962#39',10),'
      ' (43,'#39#917#960#953#963#954#949#965#941#962' ('#913#965#964#959#954#943#957#951#964#959', '#924#951#967#945#957#942', '#931#954#940#966#959#962')'#39',10),'
      ' (44,'#39#928#940#961#954#953#957#947#954#39',10),'
      ' (45,'#39#932#945#958#943', '#917#957#959#953#954#953#940#963#949#953#962' '#924#941#963#959#965#39',10),'
      ' (46,'#39#917#943#948#951' '#933#947#953#949#953#957#942#962#39',11),'
      ' (47,'#39#917#960#943#960#955#969#963#951', '#931#954#949#973#951#39',11),'
      ' (48,'#39#917#961#947#945#955#949#943#945', '#949#943#948#951' '#954#942#960#959#965', '#940#957#952#951', '#955#953#960#940#963#956#945#964#945#39',11),'
      ' (49,'#39#919#955#949#954#964#961#959#957#953#954#940' ('#919'/'#933', TV, Stereo, DVD, '#913#957#945#955#974#963#953#956#945')'#39',11),'
      ' (50,'#39#922#945#952#951#956#949#961#953#957#940' '#904#958#959#948#945' ('#913#960#959#961#961#965#960#945#957#964#953#954#940', '#935#945#961#964#953#954#940')'#39',11),'
      ' (51,'#39#923#949#965#954#940' '#917#943#948#951' ('#928#949#964#963#941#964#949#962', '#931#949#957#964#972#957#953#945', '#935#945#955#953#940', '#922#959#965#961#964#943#957#949#962')'#39',11),'
      ' (52,'#39#915#965#945#955#953#940', '#934#945#954#959#943' '#917#960#945#966#942#962', '#913#957#945#955#974#963#953#956#945#39',12),'
      ' (53,'#39#913#953#963#952#951#964#953#954#941#962' '#917#960#949#956#946#940#963#949#953#962' ('#928#955#945#963#964#953#954#941#962', '#923#953#960#959#945#957#945#961#961#972#966#951#963#951')'#39',13),'
      ' (54,'#39#922#959#956#956#969#964#942#961#953#959#39',13),'
      ' (55,'#39#924#945#957#953#954#953#959#973#961', '#928#949#957#964#953#954#953#959#973#961', '#913#960#959#964#961#943#967#969#963#951#39',13),'
      ' (56,'#39#928#961#959#963#969#960#953#954#940', '#922#959#963#956#942#956#945#964#945', '#916#974#961#945' '#964#961#943#964#969#957#39',14),'
      ' (57,'#39#922#955#942#963#949#953#962', '#928#945#961#945#946#940#963#949#953#962', '#916#953#954#945#963#964#953#954#940#39',15),'
      ' (58,'#39#917#953#963#953#964#942#961#953#945' ('#913#949#961#959#960#959#961#953#954#940', '#928#955#959#943#959#965', '#932#961#941#957#959#965')'#39',16),'
      ' (59,'#39#926#949#957#959#948#959#967#949#943#945#39',16),'
      ' (60,'#39#931#965#957#940#955#955#945#947#956#945', '#904#958#959#948#945' '#963#964#959' '#949#958#969#964#949#961#953#954#972#39',16),'
      ' (61,'#39'Fast Food, '#922#965#955#953#954#949#943#945', '#932#965#961#972#960#953#964#949#962#39',17),'
      ' (62,'#39'Super Market'#39',17),'
      ' (63,'#39#913#961#964#959#960#959#953#949#943#945', '#918#945#967#945#961#959#960#955#945#963#964#949#943#959#39',17),'
      ' (64,'#39#922#940#946#945', '#928#959#964#940', '#926#951#961#959#943' '#922#945#961#960#959#943', '#922#945#966#941#962#39',17),'
      ' (65,'#39#922#961#949#959#960#969#955#949#943#959', '#924#945#957#940#946#951#962', '#921#967#952#965#959#960#969#955#949#943#959', '#923#945#970#954#942', '#954#955#960#39',17),'
      ' (66,'#39#917#966#951#956#949#961#943#948#949#962', '#928#949#961#953#959#948#953#954#940#39',18),'
      ' (67,'#39#923#959#953#960#940#39',18),'
      ' (68,'#39#932#963#953#947#940#961#945', '#928#959#973#961#945', '#913#957#945#960#964#942#961#949#962#39',18);'
      ''
      'DROP VIEW IF EXISTS `v_expensetype`;'
      'CREATE VIEW `invoices`.`v_expensetype` AS'
      
        'select et.idExpenseType idDetail, et.descr descrDetail, emt.idEx' +
        'penseMasterType idMaster, emt.descr descrMaster,'
      ''
      'case trim(et.descr) when '#39#39
      'then emt.descr'
      'else concat(emt.descr, '#39' - '#39', et.descr)'
      'end fullDescr'
      ''
      'from expensetype et, expenseMasterType emt'
      'where et.idExpenseMasterType=emt.idExpenseMasterType;'
      ''
      '/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;'
      '/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;'
      
        '/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */' +
        ';'
      '/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;')
    Connection = EmbeddedConnection
    Left = 552
    Top = 24
  end
  object tblUsr: TMyTable
    TableName = 'usr'
    Connection = EmbeddedConnection
    Left = 376
    Top = 40
  end
  object dsUsr: TMyDataSource
    DataSet = tblUsr
    Left = 416
    Top = 40
  end
  object tblUsr2: TMyTable
    TableName = 'usr'
    Connection = EmbeddedConnection
    Left = 232
    Top = 152
  end
  object dsUsr2: TMyDataSource
    DataSet = tblUsr2
    Left = 280
    Top = 152
  end
  object qrCusInv: TMyQuery
    Connection = EmbeddedConnection
    SQL.Strings = (
      'SELECT idusrinv, u.idusr, usr.fullname, invNum,'
      
        'invDate, year(invDate) yearDate, month(invDate) monthDate, day(i' +
        'nvDate) dayDate, week(invDate) weekOfDate,'
      ''
      'case weekday(invdate)'
      'when 0 then '#39#916#917#39
      'when 1 then '#39#932#929#39
      'when 2 then '#39#932#917#39
      'when 3 then '#39#928#917#39
      'when 4 then '#39#928#913#39
      'when 5 then '#39#931#913#39
      'when 6 then '#39#922#933#39
      'end weekDayDate,'
      ''
      'weekday(invdate) weekdaynum,'
      ''
      'amount, reason,'
      'u.idstore, s.descr storeName, s.afm storeAfm,'
      'u.idExpenseType,'
      'v.descrDetail expenseDescr, v.descrMaster expenseMasterDescr'
      ''
      ''
      'FROM usrinv u, store s, v_expensetype v, usr'
      'where'
      'u.idStore=s.idStore'
      'and v.idDetail=u.idExpenseType'
      'and u.idUsr=usr.idUsr'
      'and u.idusr=:p_idusr')
    Left = 432
    Top = 169
    ParamData = <
      item
        DataType = ftInteger
        Name = 'p_idusr'
        Value = 1
      end>
    object qrCusInvidusrinva: TLargeintField
      AutoGenerateValue = arAutoInc
      FieldName = 'idusrinv'
    end
    object qrCusInvidusr: TLargeintField
      FieldName = 'idusr'
      Origin = 'u.idusr'
    end
    object qrCusInvinvNum: TWideStringField
      FieldName = 'invNum'
      Origin = 'u.invNum'
      Size = 25
    end
    object qrCusInvinvDate: TDateField
      FieldName = 'invDate'
      Origin = 'u.invDate'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object qrCusInvyearDate: TLargeintField
      FieldName = 'yearDate'
      Origin = 'yearDate'
    end
    object qrCusInvmonthDate: TLargeintField
      FieldName = 'monthDate'
      Origin = 'monthDate'
    end
    object qrCusInvdayDate: TLargeintField
      FieldName = 'dayDate'
      Origin = 'dayDate'
    end
    object qrCusInvweekOfDate: TLargeintField
      FieldName = 'weekOfDate'
      Origin = 'weekOfDate'
    end
    object qrCusInvweekDayDate: TWideStringField
      FieldName = 'weekDayDate'
      Origin = 'weekDayDate'
      Size = 2
    end
    object qrCusInvamount: TFloatField
      FieldName = 'amount'
      Origin = 'u.amount'
    end
    object qrCusInvreason: TWideMemoField
      FieldName = 'reason'
      Origin = 'u.reason'
      BlobType = ftWideMemo
    end
    object qrCusInvidstore: TLargeintField
      FieldName = 'idstore'
      Origin = 'u.idstore'
    end
    object qrCusInvstoreName: TWideStringField
      FieldName = 'storeName'
      Origin = 's.storeName'
      Size = 45
    end
    object qrCusInvstoreAfm: TWideStringField
      FieldName = 'storeAfm'
      Origin = 's.storeAfm'
      Size = 9
    end
    object qrCusInvidExpenseType: TLargeintField
      FieldName = 'idExpenseType'
      Origin = 'u.idExpenseType'
    end
    object qrCusInvexpenseDescr: TWideStringField
      FieldName = 'expenseDescr'
      Origin = 'v.descrDetail'
      Size = 85
    end
    object qrCusInvexpenseMasterDescr: TWideStringField
      FieldName = 'expenseMasterDescr'
      Origin = 'v.descrMaster'
      Size = 45
    end
    object qrCusInvfullname: TWideStringField
      FieldName = 'fullname'
      Origin = 'usr.fullname'
      Size = 45
    end
    object qrCusInvweekdaynum: TLargeintField
      FieldName = 'weekdaynum'
    end
  end
  object dsCusInv: TMyDataSource
    DataSet = qrCusInv
    Left = 472
    Top = 169
  end
  object dxSkinController1: TdxSkinController
    SkinName = 'Black'
    UseSkins = False
    Left = 1104
    Top = 25
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 240
    Top = 392
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svFont, svTextColor]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clBlue
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svTextColor]
      TextColor = clGrayText
    end
  end
  object popupManagement: TPopupMenu
    Images = ImageList1
    MenuAnimation = [maTopToBottom]
    OnPopup = popupManagementPopup
    Left = 442
    Top = 329
    object transferMI: TMenuItem
      Caption = #924#949#964#945#966#959#961#940' '#945#960#972#948#949#953#958#951#962' '#963#949' '#940#955#955#959' '#967#949#953#961#953#963#964#942
      Hint = 
        #924#949#964#945#966#941#961#949#953' '#964#951#957' '#949#960#953#955#949#947#956#941#957#951' '#945#960#972#948#949#953#958#951' '#963#949' '#940#955#955#959' '#967#949#953#961#953#963#964#942' - '#928#961#959#971#960#959#952#941#964#949#953 +
        ' 2 '#942' '#960#949#961#953#963#963#972#964#949#961#959#965#962' '#967#949#953#961#953#963#964#941#962
      ImageIndex = 24
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object newMI: TMenuItem
      Caption = #925#917#913' '#945#960#972#948#949#953#958#951
      Hint = #922#945#964#945#967#974#961#953#963#951' '#957#941#945#962' '#945#960#972#948#949#953#958#951#962' '#947#953#945' '#964#959#957' '#949#960#953#955#949#947#956#941#957#959' '#967#949#953#961#953#963#964#942
      ImageIndex = 50
      OnClick = btnNewInvoiceClick
    end
    object editMI: TMenuItem
      Caption = #924#917#932#913#914#927#923#919' '#945#960#972#948#949#953#958#951#962
      Hint = #924#949#964#945#946#940#955#949#953' '#964#951#957' '#949#960#953#955#949#947#956#941#957#951' '#945#960#959#948#949#953#958#951
      ImageIndex = 64
      OnClick = editMIClick
    end
    object delMI: TMenuItem
      Caption = #916#921#913#915#929#913#934#919' '#945#960#972#948#949#953#958#951#962
      Hint = #916#953#945#947#961#940#966#949#953' '#964#951#957' '#949#960#953#955#949#947#956#941#957#951' '#945#960#972#948#949#953#958#951
      ImageIndex = 49
      OnClick = delMIClick
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object grid1: TMenuItem
      Caption = #927#956#945#948#959#960#959#943#951#963#951' '#963#964#959#953#967#949#943#969#957' '#960#943#957#945#954#945' '#946#940#963#949#953
      ImageIndex = 68
      object gridByItemMI: TMenuItem
        Caption = #949#943#948#959#965#962' '#945#947#959#961#940#962
        ImageIndex = 68
        OnClick = gridByItemMIClick
      end
      object gridByItemCategoryMI: TMenuItem
        Caption = #954#945#964#951#947#959#961#943#945#962' '#949#958#972#948#959#965
        ImageIndex = 68
        OnClick = gridByItemCategoryMIClick
      end
      object gridByStoreMI: TMenuItem
        Caption = #954#945#964#945#963#964#942#956#945#964#959#962
        ImageIndex = 68
        OnClick = gridByStoreMIClick
      end
      object N9: TMenuItem
        Caption = '-'
      end
      object gridByItemMonthMI: TMenuItem
        Caption = #956#942#957#945' '#941#964#959#965#962
        ImageIndex = 68
        OnClick = gridByItemMonthMIClick
      end
      object gridByItemWeekMI: TMenuItem
        Caption = #949#946#948#959#956#940#948#945#962' '#941#964#959#965#962
        ImageIndex = 68
        OnClick = gridByItemWeekMIClick
      end
      object gridByItemWeekDayMI: TMenuItem
        Caption = #951#956#941#961#945#962' '#949#946#948#959#956#940#948#945#962
        ImageIndex = 68
        OnClick = gridByItemWeekDayMIClick
      end
      object N11: TMenuItem
        Caption = '-'
      end
      object resetGridItems2MI: TMenuItem
        Caption = #917#960#945#957#945#966#959#961#940' '#928#943#957#945#954#945' '#963#964#951#957' '#945#961#967#953#954#942' '#964#959#965' '#956#959#961#966#942
        ImageIndex = 63
        OnClick = resetGridItemsMIClick
      end
    end
    object resetGridItemsMI: TMenuItem
      Caption = #917#960#945#957#945#966#959#961#940' '#928#943#957#945#954#945' '#963#964#951#957' '#945#961#967#953#954#942' '#964#959#965' '#956#959#961#966#942
      Hint = 
        #913#957' '#941#967#949#964#949' '#956#949#964#945#954#953#957#942#963#949#953' '#964#945' '#960#949#948#943#945' '#963#945#962' '#963#949' '#940#955#955#949#962' '#952#941#963#949#953#962' '#954#945#953' '#941#967#949#964#949' '#967#945#952#949 +
        #943', '#945#961#967#953#954#959#960#959#953#942#963#964#949' '#949#948#974' '#964#959' grid '#963#945#962
      ImageIndex = 63
      OnClick = resetGridItemsMIClick
    end
  end
  object qr: TMyQuery
    Connection = EmbeddedConnection
    Left = 666
    Top = 169
  end
  object dlgBackup: TSaveDialog
    FileName = 'my-backup.sql'
    Filter = 'SQL Files|*.sql'
    Title = 'Backup '#946#940#963#951#962' '#948#949#948#959#956#941#957#969#957
    Left = 146
    Top = 217
  end
  object dlgRestore: TOpenDialog
    FileName = 'my-backup.sql'
    Filter = 'SQL Files|*.sql'
    Title = 'Restore '#946#940#963#951#962' '#948#949#948#959#956#941#957#969#957
    Left = 202
    Top = 217
  end
  object MyDump1: TMyDump
    TableNames = 
      'expensemastertype, expensetype, store, usr, usrinv, v_expensetyp' +
      'e'
    SQL.Strings = (
      '-- MyDAC version: 5.90.0.58'
      '-- MySQL server version: 5.1.41-community-debug-embedded-log'
      '-- MySQL client version: 5.1.41'
      '-- Script date 07/01/2010 20:06:04'
      
        '-- -------------------------------------------------------------' +
        '--------- '
      '-- Server: '
      '-- Database: invoices'
      ''
      
        '/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */' +
        ';'
      
        '/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS ' +
        '*/;'
      
        '/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */' +
        ';'
      '/*!40101 SET NAMES utf8 */;'
      
        '/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREI' +
        'GN_KEY_CHECKS=0 */;'
      '-- '
      '-- Table structure for table  `expensemastertype`'
      '-- '
      ''
      'DROP TABLE IF EXISTS `expensemastertype`;'
      'CREATE TABLE `expensemastertype` ('
      
        '  `idexpenseMasterType` int(10) unsigned NOT NULL AUTO_INCREMENT' +
        ','
      '  `descr` varchar(45) NOT NULL,'
      '  PRIMARY KEY (`idexpenseMasterType`),'
      '  UNIQUE KEY `Index_2` (`descr`)'
      ') ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;'
      ''
      '-- '
      '-- Dumping data for table `expensemastertype`'
      '-- '
      ''
      '/*!40000 ALTER TABLE `expensemastertype` DISABLE KEYS */;'
      
        'INSERT DELAYED INTO `expensemastertype`(idexpenseMasterType, des' +
        'cr) VALUES'
      '  (1, '#39#902#947#957#969#963#964#959#39'),'
      '  (2, '#39#916#953#945#963#954#941#948#945#963#951#39'),'
      '  (3, '#39#917#954#960#945#943#948#949#965#963#951#39'),'
      '  (4, '#39#904#957#948#965#963#951#39'),'
      '  (5, '#39#917#960#953#963#954#949#965#941#962#39'),'
      '  (6, '#39#921#945#964#961#953#954#940#39'),'
      '  (7, '#39#922#945#964#959#953#954#943#948#953#959#39'),'
      '  (8, '#39#923#959#947#945#961#953#945#963#956#959#943#39'),'
      '  (9, '#39#923#959#953#960#945' '#904#958#959#948#945#39'),'
      '  (10, '#39#924#949#964#945#954#953#957#942#963#949#953#962#39'),'
      '  (11, '#39#927#953#954#953#945#954#972#962' '#917#958#959#960#955#953#963#956#972#962#39'),'
      '  (12, '#39#927#960#964#953#954#940#39'),'
      '  (13, '#39#928#949#961#953#960#959#943#951#963#951' '#931#974#956#945#964#959#962#39'),'
      '  (15, '#39#928#961#972#963#964#953#956#945#39'),'
      '  (14, '#39#928#961#959#963#969#960#953#954#940' '#904#958#959#948#945#39'),'
      '  (16, '#39#932#945#958#943#948#953#945#39'),'
      '  (17, '#39#932#961#972#966#953#956#945#39'),'
      '  (18, '#39#936#953#955#953#954#940#39');'
      '/*!40000 ALTER TABLE `expensemastertype` ENABLE KEYS */;'
      ''
      '-- '
      '-- Table structure for table  `expensetype`'
      '-- '
      ''
      'DROP TABLE IF EXISTS `expensetype`;'
      'CREATE TABLE `expensetype` ('
      '  `idexpenseType` int(10) unsigned NOT NULL AUTO_INCREMENT,'
      '  `descr` varchar(85) NOT NULL,'
      '  `idExpenseMasterType` int(10) unsigned NOT NULL,'
      '  PRIMARY KEY (`idexpenseType`),'
      
        '  UNIQUE KEY `Index_2` (`idExpenseMasterType`,`descr`) USING BTR' +
        'EE,'
      
        '  CONSTRAINT `FK_expensetype_1` FOREIGN KEY (`idExpenseMasterTyp' +
        'e`) REFERENCES `expensemastertype` (`idexpenseMasterType`)'
      ') ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;'
      ''
      '-- '
      '-- Dumping data for table `expensetype`'
      '-- '
      ''
      '/*!40000 ALTER TABLE `expensetype` DISABLE KEYS */;'
      
        'INSERT DELAYED INTO `expensetype`(idexpenseType, descr, idExpens' +
        'eMasterType) VALUES'
      '  (3, '#39#39', 1),'
      '  (4, '#39'CD, DVD, Video Club'#39', 2),'
      '  (5, '#39'Internet (Online '#931#965#957#948#961#959#956#941#962', '#928#945#953#967#957#943#948#953#945')'#39', 2),'
      '  (6, '#39#917#955#949#973#952#949#961#959#962' '#935#961#972#957#959#962' ('#935#972#956#960#965', '#934#969#964#959#947#961#945#966#943#945', '#917#960#953#964#961#945#960#941#950#953#945')'#39', 2),'
      '  (7, '#39#917#963#964#953#945#964#972#961#953#945', '#932#945#946#941#961#957#949#962', '#924#949#950#949#948#959#960#969#955#949#943#945#39', 2),'
      '  (8, '#39#922#945#966#949#964#941#961#953#945', '#924#960#945#961', '#928#943#963#964#945', Club'#39', 2),'
      '  (9, '#39#922#953#957#951#956#945#964#959#947#961#940#966#959#962', '#920#941#945#964#961#959', '#915#942#960#949#948#959', '#922#945#955#969#948#953#945#954#942#39', 2),'
      '  (10, '#39#932#965#967#949#961#940' '#928#945#953#967#957#943#948#953#945', '#932#950#972#947#959#962#39', 2),'
      '  (11, '#39#914#953#946#943#945', '#932#963#940#957#964#949#962', '#915#961#945#966#953#954#942' '#910#955#951#39', 3),'
      '  (12, '#39#915#965#956#957#945#963#964#942#961#953#959', '#928#959#955#949#956#953#954#941#962' '#964#941#967#957#949#962#39', 3),'
      '  (13, '#39#921#948#953#945#943#964#949#961#945' '#924#945#952#942#956#945#964#945#39', 3),'
      '  (14, '#39#926#941#957#949#962' '#915#955#974#963#963#949#962#39', 3),'
      '  (15, '#39#931#949#956#960#953#957#940#961#953#945', '#931#965#957#941#948#961#953#945#39', 3),'
      '  (16, '#39#931#967#959#955#953#954#940' '#934#961#959#957#964#953#963#964#942#961#953#945#39', 3),'
      '  (17, '#39#937#948#949#943#959', '#935#959#961#972#962', '#922#945#955#955#953#964#949#967#957#953#954#940#39', 3),'
      '  (18, '#39#913#958#949#963#959#965#940#961' ('#932#963#940#957#964#949#962', '#918#974#957#949#962', '#922#945#960#941#955#945')'#39', 4),'
      '  (19, '#39#922#945#955#955#965#957#964#953#954#940#39', 4),'
      '  (20, '#39#928#945#960#959#973#964#963#953#945#39', 4),'
      '  (21, '#39#929#959#973#967#945' ('#913#947#959#961#940', '#924#959#948#943#963#964#961#945', '#922#945#952#945#961#953#963#964#942#961#953#959')'#39', 4),'
      '  (22, '#39#931#960#943#964#953' ('#933#948#961#945#965#955#953#954#940', '#919#955#949#954#964#961#953#954#940', '#935#961#974#956#945#964#945')'#39', 5),'
      '  (23, '#39#917#958#949#964#940#963#949#953#962#39', 6),'
      '  (24, '#39#917#960#949#956#946#940#963#949#953#962', '#935#949#953#961#959#965#961#943#945', '#915#941#957#957#949#962#39', 6),'
      '  (25, '#39#917#960#953#963#954#941#968#949#953#962' '#921#945#964#961#974#957#39', 6),'
      '  (26, '#39#920#949#961#945#960#949#943#949#962' (Spa, '#924#945#963#945#950', '#934#965#963#953#959#952#949#961#945#960#949#943#949#962')'#39', 6),'
      '  (27, '#39#927#961#952#959#960#949#948#953#954#940', '#913#957#945#964#959#956#953#954#940', '#914#959#951#952#942#956#945#964#945#39', 6),'
      '  (28, '#39#934#940#961#956#945#954#945#39', 6),'
      '  (29, '#39#913#958#949#963#959#965#940#961#39', 7),'
      '  (30, '#39#921#945#964#961#953#954#940' '#904#958#959#948#945#39', 7),'
      '  (31, '#39#932#961#972#966#953#956#945', '#922#945#952#951#956#949#961#953#957#942' '#934#961#959#957#964#943#948#945#39', 7),'
      '  (32, '#39#913#963#966#940#955#949#953#949#962' '#918#969#942#962', '#913#965#964#959#954#953#957#942#964#959#965', '#931#960#953#964#953#959#973#39', 8),'
      '  (33, '#39#916#917#922#927' ('#917#933#916#913#928', '#927#932#917', '#916#917#919')'#39', 8),'
      '  (34, '#39#917#957#945#955#955#945#954#964#953#954#942' '#932#955#949#966#969#957#943#945' (Forthnet, Tellas, On '#954'.'#955'.'#960'.)'#39', 8),'
      '  (35, '#39#917#957#959#943#954#953#959#39', 8),'
      '  (36, '#39#922#953#957#951#964#942' '#932#951#955#949#966#969#957#943#945#39', 8),'
      '  (37, '#39#922#959#953#957#972#967#961#951#963#964#945', '#928#949#964#961#941#955#945#953#959', '#934#965#963#953#954#972' '#913#941#961#953#959#39', 8),'
      '  (38, '#39#39', 9),'
      '  (39, '#39#913#947#959#961#940' '#913#965#964#959#954#953#957#942#964#959#965', '#924#951#967#945#957#942#962', Service'#39', 10),'
      '  (40, '#39#914#949#957#950#943#957#951', '#928#955#965#957#964#942#961#953#959#39', 10),'
      '  (41, '#39#916#953#972#948#953#945#39', 10),'
      '  (42, '#39#917#953#963#953#964#942#961#953#945', '#922#940#961#964#949#962#39', 10),'
      '  (43, '#39#917#960#953#963#954#949#965#941#962' ('#913#965#964#959#954#943#957#951#964#959', '#924#951#967#945#957#942', '#931#954#940#966#959#962')'#39', 10),'
      '  (44, '#39#928#940#961#954#953#957#947#954#39', 10),'
      '  (45, '#39#932#945#958#943', '#917#957#959#953#954#953#940#963#949#953#962' '#924#941#963#959#965#39', 10),'
      '  (46, '#39#917#943#948#951' '#933#947#953#949#953#957#942#962#39', 11),'
      '  (47, '#39#917#960#943#960#955#969#963#951', '#931#954#949#973#951#39', 11),'
      '  (48, '#39#917#961#947#945#955#949#943#945', '#949#943#948#951' '#954#942#960#959#965', '#940#957#952#951', '#955#953#960#940#963#956#945#964#945#39', 11),'
      '  (49, '#39#919#955#949#954#964#961#959#957#953#954#940' ('#919'/'#933', TV, Stereo, DVD, '#913#957#945#955#974#963#953#956#945')'#39', 11),'
      '  (50, '#39#922#945#952#951#956#949#961#953#957#940' '#904#958#959#948#945' ('#913#960#959#961#961#965#960#945#957#964#953#954#940', '#935#945#961#964#953#954#940')'#39', 11),'
      '  (51, '#39#923#949#965#954#940' '#917#943#948#951' ('#928#949#964#963#941#964#949#962', '#931#949#957#964#972#957#953#945', '#935#945#955#953#940', '#922#959#965#961#964#943#957#949#962')'#39', 11),'
      '  (52, '#39#915#965#945#955#953#940', '#934#945#954#959#943' '#917#960#945#966#942#962', '#913#957#945#955#974#963#953#956#945#39', 12),'
      '  (53, '#39#913#953#963#952#951#964#953#954#941#962' '#917#960#949#956#946#940#963#949#953#962' ('#928#955#945#963#964#953#954#941#962', '#923#953#960#945#957#945#961#961#972#966#951#963#951')'#39', 13),'
      '  (54, '#39#922#959#956#956#969#964#942#961#953#959#39', 13),'
      '  (55, '#39#924#945#957#953#954#953#959#973#961', '#928#949#957#964#953#954#953#959#965#961', '#913#960#959#964#961#943#967#969#963#951#39', 13),'
      '  (56, '#39#928#961#959#963#969#945#960#953#954#940', '#922#959#963#956#942#956#945#964#945', '#916#974#961#945' '#964#961#943#964#969#957#39', 14),'
      '  (57, '#39#922#955#942#963#949#953#962', '#928#913#961#945#946#940#963#949#953#962', '#916#953#954#945#963#964#953#954#940#39', 15),'
      '  (58, '#39#917#953#963#953#964#942#961#953#945' ('#913#949#961#959#960#959#961#953#954#940', '#928#955#959#943#959#965', '#932#961#941#957#959#965')'#39', 16),'
      '  (59, '#39#926#949#957#959#948#959#967#949#943#945#39', 16),'
      '  (60, '#39#931#965#957#940#955#955#945#947#956#945', '#904#958#959#948#945' '#963#964#959' '#949#958#969#964#949#961#953#954#972#39', 16),'
      '  (61, '#39'Fast Food, '#922#965#955#953#954#949#943#945', '#932#965#961#972#960#953#964#949#962#39', 17),'
      '  (62, '#39'Super Market'#39', 17),'
      '  (63, '#39#913#961#964#959#960#959#953#949#943#945', '#918#945#967#945#961#959#960#955#945#963#964#949#943#959#39', 17),'
      '  (64, '#39#922#940#946#945', '#928#959#964#940', '#926#951#961#959#943' '#922#945#961#960#959#943', '#922#945#966#941#962#39', 17),'
      '  (65, '#39#922#961#949#959#960#969#955#949#943#959', '#924#945#957#940#946#951#962', '#921#967#952#965#959#960#969#955#949#943#959', '#923#945#970#954#942', '#954#955#960#39', 17),'
      '  (66, '#39#917#966#951#956#949#961#943#948#949#962', '#928#949#961#953#959#948#953#954#940#39', 18),'
      '  (67, '#39#923#959#953#960#940#39', 18),'
      '  (68, '#39#932#963#953#947#940#961#945', '#928#959#973#961#945', '#913#957#945#960#964#942#961#949#962#39', 18);'
      '/*!40000 ALTER TABLE `expensetype` ENABLE KEYS */;'
      ''
      '-- '
      '-- Table structure for table  `store`'
      '-- '
      ''
      'DROP TABLE IF EXISTS `store`;'
      'CREATE TABLE `store` ('
      '  `idstore` int(10) unsigned NOT NULL AUTO_INCREMENT,'
      '  `descr` varchar(45) DEFAULT NULL,'
      '  `afm` varchar(9) DEFAULT NULL,'
      '  `idExpenseType` int(10) unsigned zerofill DEFAULT NULL,'
      '  PRIMARY KEY (`idstore`),'
      '  UNIQUE KEY `Index_2` (`afm`),'
      '  KEY `FK_store_1` (`idExpenseType`),'
      
        '  CONSTRAINT `FK_store_1` FOREIGN KEY (`idExpenseType`) REFERENC' +
        'ES `expensetype` (`idexpenseType`)'
      ') ENGINE=InnoDB DEFAULT CHARSET=utf8;'
      ''
      '-- '
      '-- Dumping data for table `store`'
      '-- '
      ''
      ''
      '-- '
      '-- Table structure for table  `usr`'
      '-- '
      ''
      'DROP TABLE IF EXISTS `usr`;'
      'CREATE TABLE `usr` ('
      '  `idusr` int(10) unsigned NOT NULL AUTO_INCREMENT,'
      '  `fullname` varchar(45) NOT NULL,'
      '  `anualgoal` double NOT NULL DEFAULT '#39'0'#39','
      '  PRIMARY KEY (`idusr`),'
      '  UNIQUE KEY `Index_2` (`fullname`)'
      ') ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;'
      ''
      '-- '
      '-- Dumping data for table `usr`'
      '-- '
      ''
      '/*!40000 ALTER TABLE `usr` DISABLE KEYS */;'
      'INSERT DELAYED INTO `usr`(idusr, fullname, anualgoal) VALUES'
      '  (1, '#39'john'#39', 2342),'
      '  (2, '#39'sfadfasd'#39', 3234),'
      '  (3, '#39'george'#39', 77787);'
      '/*!40000 ALTER TABLE `usr` ENABLE KEYS */;'
      ''
      '-- '
      '-- Table structure for table  `usrinv`'
      '-- '
      ''
      'DROP TABLE IF EXISTS `usrinv`;'
      'CREATE TABLE `usrinv` ('
      '  `idusrinv` int(10) unsigned NOT NULL AUTO_INCREMENT,'
      '  `idusr` int(10) unsigned NOT NULL,'
      '  `invnum` varchar(25) NOT NULL,'
      '  `invDate` date NOT NULL,'
      '  `idstore` int(10) unsigned NOT NULL,'
      '  `amount` double NOT NULL,'
      '  `reason` longtext,'
      '  `idExpenseType` int(10) unsigned NOT NULL,'
      '  PRIMARY KEY (`idusrinv`),'
      '  KEY `FK_usrinv_1` (`idusr`),'
      '  KEY `FK_usrinv_2` (`idstore`),'
      '  KEY `FK_usrinv_3` (`idExpenseType`),'
      
        '  CONSTRAINT `FK_usrinv_1` FOREIGN KEY (`idusr`) REFERENCES `usr' +
        '` (`idusr`),'
      
        '  CONSTRAINT `FK_usrinv_2` FOREIGN KEY (`idstore`) REFERENCES `s' +
        'tore` (`idstore`),'
      
        '  CONSTRAINT `FK_usrinv_3` FOREIGN KEY (`idExpenseType`) REFEREN' +
        'CES `expensetype` (`idexpenseType`)'
      ') ENGINE=InnoDB DEFAULT CHARSET=utf8;'
      ''
      '-- '
      '-- Dumping data for table `usrinv`'
      '-- '
      ''
      ''
      '-- '
      '-- Table structure for table  `v_expensetype`'
      '-- '
      ''
      'DROP VIEW IF EXISTS `v_expensetype`;'
      
        'CREATE ALGORITHM=UNDEFINED DEFINER=`ODBC`@`localhost` SQL SECURI' +
        'TY DEFINER VIEW `v_expensetype` AS select `et`.`idexpenseType` A' +
        'S `idDetail`,`et`.`descr` AS `descrDetail`,`emt`.`idexpenseMaste' +
        'rType` AS `idMaster`,`emt`.`descr` AS `descrMaster`,(case trim(`' +
        'et`.`descr`) when '#39#39' then `emt`.`descr` else concat(`emt`.`descr' +
        '`,'#39' - '#39',`et`.`descr`) end) AS `fullDescr` from (`expensetype` `e' +
        't` join `expensemastertype` `emt`) where (`et`.`idExpenseMasterT' +
        'ype` = `emt`.`idexpenseMasterType`);'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;'
      '/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;'
      
        '/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */' +
        ';'
      '/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;')
    Connection = EmbeddedConnection
    Objects = [doTables, doData, doViews, doTriggers]
    Options.QuoteNames = True
    Options.CompleteInsert = True
    Options.DisableKeys = True
    Options.UseDelayedIns = True
    Left = 114
    Top = 217
  end
  object ImageList1: TImageList
    Left = 594
    Top = 170
    Bitmap = {
      494C010145004B00A80010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000002001000001002000000000000020
      0100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6A18800C4906700C18D
      6400BF896200BC865E00BA845D00B5805A00B37E5800B27C5500B07B5500AE77
      5400AB755100AA745000AA724E00C7A189000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C9936A00E7E5E500E5E5
      E500E5E5E700E5E5E500E5E5E500E6E5E500E5E5E500E7E5E500E5E7E500E6E6
      E600E7E6E600E6E6E600E7E6E700AA734E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CB966D00E7E7E700E9E8
      E800E8E8E800E7E7E700E7E7E700C3C3C300FAFAFA00FAFAFA00FAFAFA00FAFA
      FA00FAFAFA00FAFAFA00E8E8E800AB7450000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CE996E00EAEAEA00D386
      5500D3865500D3865500E9E9E900C4C4C400FAFAFA00FAFAFA00FAFAFA00FAFA
      FA00FAFAFA00FAFAFA00E9EAEA00AD7652000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D29D7100ECECEC00EDED
      EB00EDEDEB00ECECEC00EDEBEC00C4C4C400C4C4C400C3C3C300C4C4C400C3C3
      C300C4C4C400C4C4C400EDEDEB00B17C56000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D5A07400EFEEEE00EFEF
      EF00EFEFEF00EFEEEE00EFEFEF00EEEEEE00EEEFEF00EEEEEE00EEEEEE00EEEE
      EE00EFEFEF00EEEEEE00EEEEEF00B37D57000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D5A27500F1F1F000F1F1
      F100F0F1F100F2F1F200F1F1F100C4C4C400FAFAFA00FAFAFA00FAFAFA00FAFA
      FA00FAFAFA00FAFAFA00F1F1F100B67F59000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D9A47800F2F2F200D486
      5500D3865500D4865500F2F2F300C3C3C300FAFAFA00FAFAFA00FAFAFA00FAFA
      FA00FAFAFA00FAFAFA00F3F3F300B8825C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DAA57800F6F6F600F6F6
      F400F4F6F400F3F3F300F5F5F400C3C3C300C4C4C400C4C4C400C3C3C300C4C4
      C400C4C4C400C4C4C400F4F4F500BA865D000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DCA67900F6F6F600F6F6
      F600F6F6F600F7F7F700F6F6F600F6F6F600F6F6F600F6F6F600F6F6F600F7F7
      F700F6F6F600F6F6F600F6F6F600BE8861000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DDAA7A00DDAA7A00DDA9
      7A00DDA97900DDA97A00DDA97A00DDA97900DDA97A00DDA97A00DDA97A00DDA9
      7A00DDA97900DDA97900DDA97A00C18D65000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DEAD8500E9BB9200E8BA
      9100E9BB9200E9BB9200E8BA9100E8BA9100E9BB9200E9BB9200E8BA9100E9BB
      9200E9BB9200E9BB9100E9BB9200C1936F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DAC4B400DEB49100DDA9
      7A00DDA87800DBA67900D9A47800D5A27500D4A07400D39F7200D09C7100CF9B
      6F00CB986D00CA966A00C8A07F00DAC4B4000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000518030000000E001007
      03001F05150010020000090100000007000002150600000A0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CCB46D00BA9B4000AD8C2600AB892200B4943500C5AC60000000
      0000000000000000000000000000000000000000000000000000225462002254
      6200225462002254620022546200225462002254620022546200225462002254
      6200225462002254620000000000000000001500000000000000000000000000
      0000000000000000000000000000000000000000000000000000040012000000
      0000000000000000000000000000000000007AB98000579E5E00619C66004678
      4A0059755B007C7C7C0087878700898989008B8B8B008C8C8C00696969005C7D
      5F0048864E00609964003A7B3E00598A5B00000000000000000000000000D0B9
      7400B8973400B9931E00BA911300BB911100BB911100BA901200B48D1600A985
      1800C3AA5D00000000000000000000000000000000000000000022546200B6D9
      E200B6D9E200B6D9E200518C9B00B2EAFE00A1E6FD00518C9B00B6D9E200B6D9
      E200B6D9E200225462000000000000000000070D000000000000D8F13300F5F3
      2300FFF80000FFFF3200F0C80000F0EB4800291B0000000B0000130713000511
      11001900000000000000090015001C1600006AB9730066B4700072BF7D0062B4
      6D005E916200E5E5E50082828200A9A9A900ACACAC0089898900E2E2E20076BC
      7E0084CA8F0074C1800055A45E00337638000000000000000000C6AA5700BC98
      2D00BB911100927319009273190092731900BB911100BB911100927319009273
      190092741B00B0902D000000000000000000000000000000000022546200BCDC
      E400BCDCE400BCDCE400518C9B00C5EEFD00B2EAFE00518C9B00BCDCE400BCDC
      E400BCDCE4002254620000000000000000000000030000000000000000000000
      0000000000000000000000000000000B010025FFFF000000000000F9FF000000
      00002FFFE50000070000000D0000D1E63300E2F3E500C3E3C7007CBA8200528E
      5800A2C1A400F0F0F0007E7E7E00A4A4A400A6A6A60085858500F0F0F000A1D6
      A80059AF62006AAE7200A0C8A400C0D7C2000000000000000000BF9C3200BB91
      1100F6F3E900F5F2E700F4F1E50092731900BB911100F1EDDF00F1ECDD00F0EB
      DB0092731900AF891500C3AA5D0000000000000000000000000022546200C3DF
      E600C3DFE600C3DFE600518C9B00D8F3FD00C5EEFD00518C9B00C3DFE600C3DF
      E600C3DFE60022546200000000000000000012000C0000000000361F0000E7DF
      2000FFF51D00FFFF0300FEEE0000000001000203000009050000000000000FFF
      FF000000000012FFFC001C000A00DFFF00000000000000000000000000008B8B
      8B00F0F0F000EFEFEF007B7B7B009E9E9E00A1A1A10081818100EFEFEF00F4F4
      F40071717100E5E5E500000000000000000000000000C5A74D00BB911100BB91
      1100F7F4EB00F6F3EA00F5F2E70092731900BB911100F3EFE200F2EEE000F1ED
      DE0092731900BB911100A985180000000000000000000000000022546200CBE3
      EA00CBE3EA00CBE3EA00518C9B00E8F7FC00D8F3FD00518C9B00CBE3EA00CBE3
      EA00CBE3EA002254620000000000000000001900080000000000000000000007
      00001218070000000000001212000000000004FFEE00000000002BFFFF000000
      000000F8E6000000000005000A00FBF90B000000000000000000000000009090
      9000F1F1F100EFEFEF0077777700999999009C9C9C007C7C7C00EFEFEF00F4F4
      F40076767600E6E6E6000000000000000000C5AA6400C09C2C00BB911100BB91
      1100F8F6EE00F7F5EC00F6F3EA009273190092731900F4F0E400F3EFE200F2EE
      E00092731900BB911100B48D1600C2A75600000000000000000022546200D3E7
      EC00D3E7EC00D3E7EC00518C9B00F4FBFE00E8F7FC00518C9B00D3E7EC00D3E7
      EC00D3E7EC002254620000000000000000002300160000000000FFF234000008
      18000DEAE600240D00002C0700000A0E190000040000000E24000000000006FF
      FF000000000038FFFF0000040400FFFF110000000000C7DBEA00679CC3008D8F
      9100F6F6F600EFEFEF0074747400767676007777770078787800EFEFEF00F4F4
      F4008A8B8D004E618E00C1C9D90000000000C0A05100BC931600BB911100BB91
      1100F9F7F000F8F6EE00F7F5EC00C9B98C00C9B98C00F5F2E700F4F1E500F3EF
      E3009273190095751800BA901200B2933500000000000000000022546200DAEB
      EF00DAEBEF00DAEBEF00518C9B00518C9B00518C9B00518C9B00DAEBEF00DAEB
      EF00DAEBEF002254620000000000000000002A090D0000000000000000000000
      00000E002000000000000AFFFF000000000000FFEB000000000004FFFF000000
      000000FFF7000000000000000800D8EF0000C7DCEC003B85BB005796C2003F80
      B300DCDEE000EDEDED00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00DFE1
      E3002D4B81003A5F900027407A00C1C9D900BC9B4300BB911100BB911100F3EC
      D800FAF8F300F9F7F100F8F6EF00F7F5ED00F6F4EB00F6F3E900F5F2E700F4F1
      E500EDE8D600A17E1600BB911100AB892200000000000000000022546200E0EE
      F200E0EEF200E0EEF200E0EEF200E0EEF200E0EEF200E0EEF200E0EEF200E0EE
      F200E0EEF2002254620000000000000000000000070000000000EBEB2100FFFF
      1B00EAFF100000000B000002010007030E002300030022000E00000203000B00
      11000000000009FFF30042000100E4F23800629BCA005395C6007AAFD3005797
      C4004387BA00CDCFD000EEEEEE00EFEFEF00EFEFEF00EFEFEF00D9DADB003864
      970044709F005C8CB1003C649400566B9700C09E4700BB911100BB911100D3B9
      6800FBFAF500FAF9F300F9F7F100F8F6EF00F8F5ED00F7F4EB00F6F3EA00F5F2
      E700D5C69800BB911100BB911100AD8C260022546200518C9B00518C9B00E7F1
      F300E7F1F300E7F1F300E7F1F300E7F1F300E7F1F300E7F1F300E7F1F300E7F1
      F300E7F1F300518C9B00518C9B00225462001900060000000000000000000000
      0000000000000000000014001C0000FFDF0000050500000B190000FFD6000000
      00001AFFEA0000000400000E03001E160000000000003F85BE005293C60079AE
      D3005597C4004287BA00CACBCC00EDEDED00EFEFEF00D9DBDC003D76A6004D80
      AE006B9ABD004775A200395D910000000000C8AB5D00BC941700BB911100BB91
      1100D3B96800FBFAF600FAF9F400F9F8F200F9F7F000F8F6EE00F7F5EC00D6C7
      9A00BB911100BB911100BA911300B89B41000000000022546200518C9B00518C
      9B00EDF4F700EDF4F700EDF4F700EDF4F700EDF4F700EDF4F700EDF4F700EDF4
      F700518C9B00518C9B0022546200000000000000140000000000FAFE0000F9FF
      3500E6FB0400FFF20000EDF60C00001911002EF0E400030B0000020018000205
      00002B000F0000000000000000000000000000000000000000003E81BE005091
      C60076ADD3005495C6004189BC00D2D4D500D2D4D5004183B400558DBB0077A5
      C7005084B2003E70A3000000000000000000CFB46F00C5A13500BB911100BB91
      1100BB911100D3B96800FBFAF600FAF9F400FAF8F200F9F7F000D7C99D00BB91
      1100BB911100BB911100B9931E00C8AF6200000000000000000022546200518C
      9B00518C9B00F3F8F900F3F8F900F3F8F900F3F8F900F3F8F900F3F8F900518C
      9B00518C9B002254620000000000000000000006000000000000000000000000
      0000000000000000000000000000000000000003000000FAEC001E1508000000
      0000000000000000000000000000000000000000000000000000000000003C80
      BC004D90C40073ABD3005294C6003B83BA003C85B9005695C2007DACCF005591
      BC004380B10000000000000000000000000000000000D4B96900BB911100BB91
      1100BB911100BB911100D3B96800FBFAF700FBF9F500D8CAA000BB911100BB91
      1100BB911100BB911100B8973400000000000000000000000000000000002254
      6200518C9B00518C9B00F8FAFA00F8FAFA00F8FAFA00F8FAFA00518C9B00518C
      9B00225462000000000000000000000000000000100030001800000000000000
      190000000000000017000000000000030300000000002500000000FEE3001708
      0500000000000000000000000000000000000000000000000000000000000000
      00003C7EBB004B8DC30070AAD20071AAD20074ACD2007AAFD3005697C4004589
      BB000000000000000000000000000000000000000000E1CE9500CBAB4A00BB91
      1100BB911100BB911100BB911100D4B96800E4D3A000BB911100BB911100BB91
      1100BB911100BC982D00D0B97400000000000000000000000000000000000000
      000022546200518C9B00518C9B00FFFEFD00FFFEFD00518C9B00518C9B002254
      6200000000000000000000000000000000000000000002000000909090000000
      0000838586000000010085888C000E00000083798900001300001900290023F4
      FE00000700000000000000000000000000000000000000000000000000000000
      0000000000003A7BB900488BC2006AA6D0006EA9D1005193C6004389BF000000
      0000000000000000000000000000000000000000000000000000DAC37D00CBAB
      4A00BB911100BB911100BB911100BB911100BB911100BB911100BB911100BB91
      1100BF9C3200C6AA570000000000000000000000000000000000000000000000
      00000000000022546200518C9B00518C9B00518C9B00518C9B00225462000000
      000000000000000000000000000000000000000000000B060300787571000301
      000082838100111111007171710026002D005D9663000F070800B1C386000010
      1E000522AC00150B0B0000000000000000000000000000000000000000000000
      00000000000000000000427FBB003577B700367BB9004788C100000000000000
      000000000000000000000000000000000000000000000000000000000000E1CE
      9500D4B96900C5A13500BC941700BB911100BB911100BC931600C09C2C00C5A7
      4D00D5C07F000000000000000000000000000000000000000000000000000000
      0000000000000000000022546200518C9B00518C9B0022546200000000000000
      00000000000000000000000000000000000009020000000000000F0902000000
      00000501000000000000110C09000000000000010A0000000000000000000000
      0000001C00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D0B87700C4A35200BD994100B9973D00BB9A4900C7AC6B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000225462002254620000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6A18800C4906700C18D
      6400BF896200BC865E00BA845D00B5805A00B37E5800B27C5500B07B5500AE77
      5400AB755100AA745000AA724E00C7A18900000000006F6F6F006F6F6F006F6F
      6F006F6F6F006F6F6F006F6F6F006F6F6F006F6F6F009D9D9D00007600000076
      0000006600009D9D9D006666660000000000000000009B7C6B009D7E6D009C7E
      6D009C7E6D009C7E6D009C7D6D009C7D6C00BFABA1000075000000700000006D
      0000BFABA10067676700000000000000000000000000B6917C00C38E6800C08B
      6600BE886400BB856100B9835F00B47E5C00B27C5A00B17B5800AE795700AD76
      5600AB755400A9735300A9715100B6917C0000000000C9936A00E7E5E500E5E5
      E500E5E5E700E5E5E500E5E5E500E6E5E500E5E5E500E7E5E500E5E7E500E6E6
      E600E7E6E600E6E6E600E7E6E700AA734E00000000006F6F6F00FFDFDF00FFDF
      DF00FFDFDF00B39D9D00FFDDDD00FFEAEA00FFEAEA00FFEAEA000076000046DF
      790000760000FFE2E2009D9D9D0000000000000000009B77660000000000FAF4
      E900FAF4E900FAF4E900FAF3E800FAF3E700FCF8F100007D000044DD77000072
      0000BDA99D004F4F4F00000000000000000000000000C8926C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00A972510000000000CB966D00E7E7E700E9E8
      E800E8E8E800E7E7E700E7E7E700C3C3C300FAFAFA00FAFAFA00FAFAFA00FAFA
      FA00FAFAFA00FAFAFA00E8E8E800AB745000000000007C7C7C00FFDFDF00FFE2
      E200FFE2E200B39D9D00FFDFDF00FFEAEA0000840000008400000084000046DF
      79000076000000760000007600000000000000000000A27F6F0000000000DDC2
      B500DDC2B500DCC2B500E9D6CD0000870000008500000081000048E17B00007A
      00000075000000700000000000000000000000000000CA946E00FFFFFF00BEBE
      BE00BABABA00B2B2B200FEFAF700B2B2B200AEAEAE00A5A5A500FEFAF7009898
      98009595950091919100FFFFFF00AA73530000000000CE996E00EAEAEA00D386
      5500D3865500D3865500E9E9E900C4C4C400FAFAFA00FAFAFA00FAFAFA00FAFA
      FA00FAFAFA00FAFAFA00E9EAEA00AD765200000000007C7C7C00FFE5E500FFE5
      E500FFE5E500B3A1A100FFE2E200FFEEEE00008F00005EF7910058F18B0058F1
      8B0046DF790046DF7900007600000000000000000000A380700000000000DBC3
      BB00DBC3BA00DBC2B800EBDCD500008D00005EF791005AF38D0053EC860048E1
      7B0045DE780000780000000000000000000000000000CC976F00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00AC75540000000000D29D7100ECECEC00EDED
      EB00EDEDEB00ECECEC00EDEBEC00C4C4C400C4C4C400C3C3C300C4C4C400C3C3
      C300C4C4C400C4C4C400EDEDEB00B17C5600000000007C7C7C00B3A2A200B3A2
      A200B3A2A200B3A1A100B3A2A200D5CBCB00008F0000008F0000008F000058F1
      8B000084000000840000008400000000000000000000A987780000000000DBC7
      C200DBC6C100DBC4BC00EBDDD70000910000008D0000008B00005AF38D000083
      000000810000007D0000000000000000000000000000D19C7300FFFFFF00C2C2
      C200C0C0C000B9B9B900FEFAF700B5B5B500B3B3B300ABABAB00FEFAF700A2A2
      A2009E9E9E009B9B9B00FFFFFF00B07A580000000000D5A07400EFEEEE00EFEF
      EF00EFEFEF00EFEEEE00EFEFEF00EEEEEE00EEEFEF00EEEEEE00EEEEEE00EEEE
      EE00EFEFEF00EEEEEE00EEEEEF00B37D5700000000007C7C7C005CC2FF0056BC
      FF0056BCFF003A81B3004EB4FF0099CCFF0099CCFF0099CCFF00008F00005EF7
      9100008400008BC6FF00A5A5A5000000000000000000AB897A0000000000DBC7
      C300DBC7C200DBC5BE00EBDED900EBDCD600EBDBD300008F00005EF791000089
      0000C8B7AE006D6D6D00000000000000000000000000D49E7500FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B27C5A0000000000D5A27500F1F1F000F1F1
      F100F0F1F100F2F1F200F1F1F100C4C4C400FAFAFA00FAFAFA00FAFAFA00FAFA
      FA00FAFAFA00FAFAFA00F1F1F100B67F590000000000848484005CC2FF005CC2
      FF0056BCFF003A81B3004EB4FF004EB4FF0048AEFF0099CCFF0000990000008F
      0000008F00008BC6FF007C7C7C000000000000000000AF8E7F0000000000DCC5
      C000DCC5BF00DBC4BD00DBC2B900DBBFB400EBDBD3000093000000910000008D
      0000BFABA1006E6E6E00000000000000000000000000D5A07600FFFFFF00C8C8
      C800C6C6C600C0C0C000FEFAF700BABABA00B8B8B800B2B2B200FEFAF700A9A9
      A900A7A7A700A5A5A500FFFFFF00B57E5C0000000000D9A47800F2F2F200D486
      5500D3865500D4865500F2F2F300C3C3C300FAFAFA00FAFAFA00FAFAFA00FAFA
      FA00FAFAFA00FAFAFA00F3F3F300B8825C0000000000848484005CC2FF005CC2
      FF0056BCFF003A81B3004EB4FF004EB4FF0048AEFF0099CCFF008CB3D50094CC
      FF008BC6FF008BC6FF007C7C7C000000000000000000AF8F800000000000FEFE
      FE00FEFEFE00FEFDFD00FEFCFB00FDFBF800FEFCFA00FDFCF800FDFBF700FCF6
      ED009B7C6C006E6E6E00000000000000000000000000D8A27900FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B7815E0000000000DAA57800F6F6F600F6F6
      F400F4F6F400F3F3F300F5F5F400C3C3C300C4C4C400C4C4C400C3C3C300C4C4
      C400C4C4C400C4C4C400F4F4F500BA865D0000000000878787005CC2FF005CC2
      FF0056BCFF003A81B3004EB4FF004EB4FF0048AEFF0048AEFF002F77B3003DA3
      FF003DA3FF003399FF007C7C7C000000000000000000AF8F800000000000DFCE
      CC00DFCDCB00DECAC600DEC6C000DEC4BA00DEC1B400DEBEAD00DEBEAB00FCF6
      EE009C7C6D006F6F6F00000000000000000000000000D9A37900FFFFFF00CCCC
      CC00CACACA00C6C6C600FEFAF700BDBDBD00BBBBBB00B7B7B700FEFAF700B1B1
      B100B0B0B000ADADAD00FFFFFF00BA85600000000000DCA67900F6F6F600F6F6
      F600F6F6F600F7F7F700F6F6F600F6F6F600F6F6F600F6F6F600F6F6F600F7F7
      F700F6F6F600F6F6F600F6F6F600BE886100000000008D8D8D00B3ACAC00B3AC
      AC00B3ACAC00B3ACAC00B3A7A700B3A7A700B3A7A700B3A2A200B3A1A100B3A1
      A100B39D9D00B39D9D00848484000000000000000000B1908000000000000000
      00000000000000000000FEFEFE00FEFDFB00FDFBF800FCF9F400F9F4EE00F0E8
      E0009E8071007D7D7D00000000000000000000000000DBA47A00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00BD87630000000000DDAA7A00DDAA7A00DDA9
      7A00DDA97900DDA97A00DDA97A00DDA97900DDA97A00DDA97A00DDA97A00DDA9
      7A00DDA97900DDA97900DDA97A00C18D6500000000008D8D8D00FFF9F900FFF9
      F900FFF9F900B3ACAC00FFF3F300FFEEEE00FFEEEE00FFEAEA00B3A2A200FFE5
      E500FFE2E200FFDFDF00848484000000000000000000B7978700000000000000
      00000000000000000000FEFEFE00FEFDFB00FDFBF900A7827000A7827000A782
      7000A7827000B2B2B200000000000000000000000000DCA77B00DCA77B00DCA7
      7B00DCA77B00DCA77B00DCA77B00DCA77B00DCA77B00DCA77B00DCA77B00DCA7
      7B00DCA77B00DCA77B00DCA77B00C08B660000000000DEAD8500E9BB9200E8BA
      9100E9BB9200E9BB9200E8BA9100E8BA9100E9BB9200E9BB9200E8BA9100E9BB
      9200E9BB9200E9BB9100E9BB9200C1936F000000000092929200FFFCFC00FFFC
      FC00FFF9F900B3ACAC00FFF3F300FFEEEE00FFEEEE00FFEAEA00B3A2A200FFE5
      E500FFE2E200FFDFDF00878787000000000000000000B8988800000000000000
      000000000000FEFEFE00FEFEFD00FEFCFA00FDFBF900A7827000F5E2D900B18E
      7E00B3A9A40000000000000000000000000000000000DDAB8600E8B99200E8B9
      9200E8B99200E8B99200E8B99200E8B99200E8B99200E8B99200E8B99200E8B9
      9200E8B99200E8B99200E8B99200C18F700000000000DAC4B400DEB49100DDA9
      7A00DDA87800DBA67900D9A47800D5A27500D4A07400D39F7200D09C7100CF9B
      6F00CB986D00CA966A00C8A07F00DAC4B400000000009292920000000000FFFC
      FC00FFF9F900B3ACAC00FFF3F300FFEEEE00FFEEEE00FFEAEA00B3A2A200FFE5
      E500FFE2E200FFDFDF008D8D8D000000000000000000B8988800000000000000
      00000000000000000000000000000000000000000000A7827000B18E7E00B5AA
      A5000000000000000000000000000000000000000000C0A99B00DDA99200DCA7
      7B00DCA67A00DAA47A00D8A27900D5A07600D49E7500D29D7300CF9A7200CE99
      7000CB966F00C9946C00C5938000C0A99B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000095959500959595009595
      9500959595009292920092929200929292009292920092929200929292008D8D
      8D008D8D8D008D8D8D008D8D8D000000000000000000B8988800B8988800B493
      8300B4938300B08E7D00B08E7D00AC887700AC887700A7827000CDC2BC000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DFB69000D69F7100D2986600CF946100C7895300C3854E00C486
      4F00C4864F00C3854F00C4864F00D0A37A0000000000DBDBDB00CFCFCF00D2D2
      D200D8D8D800DBDBDB00E3E3E300F3F3F300FEFEFE0000000000000000000000
      00000000000000000000000000000000000000000000000000009B7C6B009D7E
      6D009C7E6D009C7E6D009F827100C7BAB000709F64002B811E001E7108003178
      1D006C966100C6BBAF00000000000000000000000000E0E0E0008C8C8C006F6F
      6F006D6D6D006D6D6D006D6D6D006D6D6D006D6D6D006D6D6D006D6D6D006D6D
      6D006F6F6F008C8C8C0000000000000000000000000000000000000000000000
      000000000000D8A37400F8F2ED00F8F1EB00F6EDE700F1E5DB00F1E3D900F1E3
      D800F1E3D800F1E3D900F1E3D800C68D5B00DEDEDE009A9A9A00737373007676
      7600828282008888880099999900C7C7C700F1F1F100FEFEFE00000000000000
      00000000000000000000000000000000000000000000000000009B776600FFFF
      FF00FAF4E900FAF4E900F6F5EE0041A43E00009A000000A20700009700000E89
      00002E7100003C7C2D00000000000000000000000000A5897A00A78C7C00A68C
      7C00A68C7C00A68C7C00A68B7C00A68A7A00A5897A00A5897A00A3887800A286
      7500A5897A006F6F6F000000000000000000000000000000000000000000E3BD
      9C00D9A98000E2B08600FAF5F100EBCDB200EACDB300E8C8AC00E8C8AC00E9C8
      AC00E9C9B000E9C9AE00F1E3D800C6895200924E25006D4037006E727D006792
      A6005A8FA800647580005B5A5A0085858500C7C7C700F2F2F200FEFEFE000000
      0000000000000000000000000000000000000000000000000000A27F6F00FFFF
      FF00DDC2B500E6D2C80082B779000DA71A002CB53F00FDFBFF006FD488000098
      0000009500003070000083B483000000000000000000A3827200FAFAFA00FAFA
      FA00FAFAFA00FAFAFA00FAFAFA00FAFAFA00FAFAFA00FAFAFA00FAFAFA00FAFA
      FA00A08574006D6D6D000000000000000000000000000000000000000000DCAB
      8100F8F2EF00E4B38B00FAF6F200EBCBAE00FAFAFA00E9C9AC00FAFAFA00FAFA
      FA00FAFAFA00FAFAFA00F2E6DC00C78851008A5F59005E292A008A9AAD0091D7
      FA0079CFF90069B4ED006385A7005656550086868600C8C8C800F2F2F200FEFE
      FE00000000000000000000000000000000000000000000000000A3807000FFFF
      FF00DBC3BB00ECDFDA0043AA47002EB33C00F5ECF400FFF6FF00FFFFFF006FD5
      8800009A000012880000368226000000000000000000AC8D7F00FAFAFA00DDC2
      B500DDC2B500DCC2B500DCBFB100DCBEAE00DCBBA900DCBAA500DCBAA300FAFA
      FA00A18575006D6D6D00000000000000000000000000E6C5A800DDB38E00E1AF
      8500F7ECE600E5B58E00FAF6F200E9C7AA00E9C8AC00E9C8AC00E8C8AC00EACB
      B000E9C9B000E8CDB500F2E7DE00C98C5600B1A3A900A398A100D0EEFD00B7E5
      FC009FDCFB006FB8EE004F99E2005D768E005656550086868600C8C8C800F2F2
      F200FEFEFE000000000000000000000000000000000000000000A9877800FFFF
      FF00DBC7C200EEE4E2003AAE44005DB96400C0D6BC0003AD21009DDBA700FFFF
      FF006DD48700009500001F760A000000000000000000AD8E8000FAFAFA00DBC3
      BB00DBC3BA00DBC2B800DBBFB300DBBDAF00DABBAA00DAB8A500DCB9A500FAFA
      FA00A28678006D6D6D00000000000000000000000000E0B58F00F9F5F000E6B8
      9400FAF6F000E8B99400FBF7F400E9C5A600FAFAFA00E8C8AC00FAFAFA00FAFA
      FA00FAFAFA00FAFAFA00F7F1EB00CC905C00DAEDFB00A0D3FA00F7FCFF00E0F4
      FE00C6EAFD0060A8E7004894E0004693E00058738D005656550088888800CDCD
      CD00F6F6F6000000000000000000000000000000000000000000AB897A00FFFF
      FF00DBC7C300ECE1DE004EB5570043CD6D0019BB420025C04F000BB22E009DD9
      A500FFFFFF0067CF7C00308828000000000000000000B5988B00FAFAFA00DBC7
      C200DBC6C100DBC4BC00DBC1B700DBBFB200DBBCAD00DABAA800DCBBA700FAFA
      FA00A3887A006D6D6D00000000000000000000000000E8C09D00FBF6F300E6B8
      9300F9F3EC00E9BC9800FBF7F500E9C4A600EAC5A700E9C5A600E9C5A600EAC5
      A700E9C5A600E9C5A600FBF7F500CF966200000000007ABFF30000000000F0F7
      FD005FA4E50099C5F100D2E5F8004191DF003D8EDE0055718C005A5A59009393
      9300D5D5D500F8F8F80000000000000000000000000000000000AF8E7F00FFFF
      FF00DCC5C000E5D4CF0085C07E0072DD960035CE6A002EC75E0025BE4B0009AF
      27008BD094004EBB590086BD86000000000000000000B79A8E00FAFAFA00DBC7
      C300DBC7C200DBC5BE00DBC2B900DBC0B400DBBDAF00DBBAA900DCBCA900FAFA
      FA00A4897A006D6D6D00000000000000000000000000E9C2A200FBF8F400E8BA
      9700FBF8F500EBBF9B00FBF7F400FAFAFA00FAFAFA00FAFAFA00FAFAFA00FAFA
      FA00FAFAFA00FAFAFA00FAF7F400D29968000000000086C5F500A8D1F600C2DC
      F600A4CCF600A2CDFA00CAE3FC00B1D3F4003288DC00378BDD004E667F005E5E
      5E0097979700D5D5D500F8F8F800000000000000000000000000AF8F8000FFFF
      FF00FEFEFE00FEFEFE00F9FBF9005AC0600071DD96004AD37A0029C252001CB7
      3B000DAA220036953000000000000000000000000000BCA09400FAFAFA00DCC5
      C000DCC5BF00DBC4BD00DBC2B900DBBFB400DBBDAF00DBBAAA00DDBBA900FAFA
      FA00A5897A006E6E6E00000000000000000000000000EAC4A500FBF8F500E9BC
      9900FBF4EF00EDC19E00FAF7F4009CD3A40098D1A0008BCA920082C487007EC1
      820079BF7E0076BD7A00FBF7F500D49C6D0000000000F2F9FE0086C5F500A3D3
      FA00F3F9FF00B7D8FB0097C7FA00BDDCFC009BC7F1003288DC003787D5005362
      6F005E5E5E0097979700D7D7D700F8F8F8000000000000000000AF8F8000FFFF
      FF00DFCECC00DFCDCB00DFCBC700E9DFDA0086C07E0052B85B003CB1480044AC
      480089C48600C7BAAF00000000000000000000000000BDA29600FAFAFA00FAFA
      FA00FAFAFA00FAFAFA00FAFAFA00FAFAFA00FAFAFA00FAFAFA00FAFAFA00FAFA
      FA00A5897A006E6E6E00000000000000000000000000EDC7A900FBF9F500EBBF
      9E00FBF8F500EFC7A800FBF7F500FBF7F500FBF7F500FBF7F400FBF7F400FBF7
      F500FAF7F400FBF7F400FBF7F400D9A576000000000000000000000000009ED0
      F60087C6F800F3F9FF00CDE4FC008CC1F900B1D5FB0093C2F0003288DC003686
      D5005F5B57005F5F5F00A1A1A100E6E6E6000000000000000000B1908000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FEFEFE00FEFEFC00FEFDFB00FEFCFA00FCF9
      F600F4EEE800A0817300000000000000000000000000BDA29600FAFAFA00DFCE
      CC00DFCDCB00DECAC600DEC6C000DEC4BA00DEC1B400DEBEAD00DEBEAB00FAFA
      FA00A68A7C006F6F6F00000000000000000000000000EDC9AC00FCF9F600ECC2
      A100FAF3EE00CACBA400E7C4A100EDC19E00EBC09D00E8B99300E4B48B00E3B1
      8700E0AD8300DEAB7F00DDA77C00E2B794000000000000000000000000000000
      0000C2E1F90074BEF600E9F4FE00C9E1FC0082BAF800A5CFFA008BBEF0004E7F
      AE00B16B1D00675648007A7A7A00D4D4D4000000000000000000B7978700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FEFEFE00FEFDFB00FDFBF900A7827000A782
      7000A7827000A7827000000000000000000000000000BEA39600FAFAFA00FAFA
      FA00FAFAFA00FAFAFA00FAFAFA00FAFAFA00FAFAFA00FAFAFA00FAFAFA00FAFA
      FA00A98D80007D7D7D00000000000000000000000000EFCBAE00FCF9F600F0CD
      B000FBF8F500FBF8F500FAF7F500FBF8F500FBF8F500FBF8F500FBF7F500F8EF
      E800FAF7F500DDAC830000000000000000000000000000000000000000000000
      000000000000DAEDFB0076BEF500D4EBFD00C5DFFB0077B4F7009DB8D900DFB5
      4C00D396200083592F0070707000CFCFCF000000000000000000B8988800FFFF
      FF00FFFFFF00FFFFFF00FEFEFE00FEFEFD00FEFCFA00FDFBF900A7827000F5E2
      D900B18E7E00E1D4CE00000000000000000000000000C5AC9F00FAFAFA00FAFA
      FA00FAFAFA00FAFAFA00FAFAFA00FAFAFA00FAFAFA00B1908000B1908000B190
      8000B1908000B2B2B200000000000000000000000000F0CDB100FCF9F600CDD0
      AC00E2C6A500E6C3A000E3C19D00E0BA9200DCB48B00DBB38800E4B48E00DEAC
      8100DFB08800E4BE9F0000000000000000000000000000000000000000000000
      00000000000000000000F2F9FE0079BFF500B7D5EE00D9E1ED00E2D1B700F7D6
      8600F1C342004E4F52007F7F7F00D7D7D7000000000000000000B8988800FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00A7827000B18E
      7E00E1D4CE0000000000000000000000000000000000C6ADA100FAFAFA00FAFA
      FA00FAFAFA00FAFAFA00FAFAFA00FAFAFA00FAFAFA00B1908000F5E2D900B18E
      7E00B3A9A40000000000000000000000000000000000F2D2BA00FCF9F600FCF9
      F600FCF9F600FCF9F600FCF9F500FCF9F500FBF9F500FBF9F500FCF9F600E0B6
      9200000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E7C67700F4DAA300FEFBF500EDDD
      BC0074737900213C7700B3B3B300EAEAEA000000000000000000B8988800B898
      8800B4938300B4938300B08E7D00B08E7D00AC887700AC887700A7827000E1D4
      CE000000000000000000000000000000000000000000C6ADA100FAFAFA00FAFA
      FA00FAFAFA00FAFAFA00FAFAFA00FAFAFA00FAFAFA00B1908000B18E7E00B5AA
      A5000000000000000000000000000000000000000000F8E7DB00F4D4BC00F1CD
      B200EFCCB000EDC7A900E9C3A200E8C09E00E5BD9C00E5BC9900E3B89500E7C5
      A900000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7D8AF0081879B003854
      AD003658BF0095A0BA00EBEBEB00FBFBFB000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6ADA100C6ADA100C1A6
      9900C1A69900BCA09200BCA09200B7988900B7988900B1908000CDC2BC000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000063493100634931006349
      3100634931006349310063493100634931006349310063493100634931006349
      3100634931006349310000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D0D7DF0069819C00647B
      9800667D9A00657D9900657D9900667F9A00667F9A00667F9A00667F9A006680
      9A00657E9800627E9600A8B1BD00000000000000000042424200424242004242
      42004242420042424200424242005A5A5A00A0A0A000A0A0A000A0A0A000A0A0
      A000A0A0A000A0A0A000A0A0A000000000009C8E8400C6B6A500F7F7EF000000
      00000000000000000000FFFFF700FFF7EF00F7EFE700F7E7DE00EFDFD600EFDF
      CE00DECFBD00CEB6AD006349310000000000D6BCA200AC794500B5814D00BF8C
      5900C9966300B1B1B100C9966200C08C5900B6824E00AD7A4600D6BCA2000000
      0000000000000000000000000000000000000000000095A8BE0000406D00003A
      6D00003B6D0000406D00003F6D00003C6D00003D6D00003D6D00003E6D00003F
      6D00003F6D0000476D00587694000000000071717100A0A0A000717171004242
      42008888880088888800545454005A5A5A00A0A0A0008E8E8E005A5A5A005A5A
      5A008E8E8E007171710042424200717171009C8E8400F7F7EF00843008004A28
      18004A281800CEC7BD0008617B0000304A0018303900B5A69C00082884000018
      5A0000185A0000185A00CEB6AD0063493100C18E5A00E2B17E00E7B48100F6C9
      9600D6A37000E9E9E900D6A37000F7CA9700E9B68300E3B27F00C18E5A000000
      00000000000000000000000000000000000000000000F1F3F6008299B300003B
      6D0017437400B6C2CF007892AF00003A6D0000366D0000366D0000366D000037
      6D00003C6D0043668B00E5E9EE000000000071717100A0A0A000717171004242
      4200A0A0A000A0A0A0005A5A5A005A5A5A00A0A0A00088888800424242004242
      4200888888007171710042424200717171009C8E84000000000084300800E771
      3900B55929004A28180010617B0000BEEF00009EC60029303100102884000838
      D6000028AD000028AD0000185A0063493100DAB48D00D9A67100EABA8600F6C9
      9600DEAB7800FCFCFC00DEAB7800F7CA9700ECBC8800DAA77200DAB48D000000
      0000000000000000000000000000000000000000000000000000BAC5D400003B
      6D002147750000000000ADBCCD00003B6D0000346D0000356D0000356D000035
      6D00003C6D00617A980000000000000000004E4E4E005A5A5A004E4E4E004242
      42007D7D7D0088888800545454005A5A5A00A0A0A0008E8E8E005A5A5A006060
      6000949494009494940088888800949494009C8E8400000000008C301000EF8E
      5200E7864A00BD61310018697B0000C7EF0000BEEF00009EC600183084001849
      DE001041D6000830AD0000185A0000000000FAF5EF00E2BD9800D9A77400EBBA
      8400BDA38A00AAAAAA00BDA38A00E4B97F00B8924C00A89A5200B4B0AB0073A6
      51006A9E48005F933E00578B3500ABC59A000000000000000000AFC0CD000046
      6D001F477600F7F7F700A3B4C80000396D0000336D0000336D0000336D000034
      6D00003A6D005B7695000000000000000000717171008E8E8E00666666004242
      4200666666005A5A5A00424242005A5A5A00A0A0A000A0A0A000888888007D7D
      7D00949494007D7D7D0054545400717171009C8E84000000000094411800F79E
      6B00EF966300C6714A002169840018C7EF0008C7EF00009EC600213884003151
      DE002149DE001038B50000185A00000000000000000000000000F2DFCC00E0B3
      85003366990080B3E30028608B00BFA25A00B7C3760081B45F00E9E9E90081B4
      5F00A8DB860094C7720090C36E0071A64F000000000000000000AFC0CD000C50
      6D001F547600F8F8F8009EB0C500003B6D0000306D0000316D0000316D000031
      6D0000376D005B759500000000000000000071717100A0A0A000717171004242
      4200A0A0A000A0A0A0005A5A5A005A5A5A00A0A0A00088888800424242004242
      4200888888007171710042424200717171009C8E8400FFFFF700A5492100FFAE
      8400F7AE7B00C68663002971840031CFF70021CFEF0010A6C600294184004269
      E7003961E7002949BD0000185A0000000000000000000000000000000000E1DF
      DC004276AA004B7FB10038709A007DB28000A7DA850089BC6700FCFCFC0089BC
      6700A8DB86009BCE79008ABF6700A4CA8A000000000000000000AFBFCD00054C
      6D001F537700F3F3F300CFD9E2000A437000003F6D00003C6D00003B6D00003D
      6D0000456D005C7B960000000000000000004E4E4E00666666005A5A5A004242
      4200A0A0A000A0A0A0005A5A5A005A5A5A00A0A0A00088888800424242004242
      420088888800888888007D7D7D00949494009C8E8400FFF7EF00A5512900B561
      4200C6865A00CE9673003171840052D7F70042D7F70029AEC600314984005A79
      EF005271EF003951BD0000185A0000000000000000000000000000000000769D
      C3007BAFDF0083B6E60079AEDD00518A8E009CD278007DB07D00AAAAAA007DB0
      7D009ED3790092C57100AFD49700F3F8EF000000000000000000AFBECD00004A
      6D001F507700E8E8E80000000000A7B9CA0000416D001F4E740007446E002052
      750007476C00547892000000000000000000C4C4C40095959500666666004242
      4200424242005A5A5A00484848005A5A5A00A0A0A0009A9A9A00888888009A9A
      9A00A0A0A000B2B2B200CACACA00E1E1E1009C8E8400F7EFE700B5613900FFF7
      EF00FFEFDE00FFD7C6003979840073DFF70063DFF70042AEC60039518C00738E
      F7006B86F7004A61C60000185A00000000000000000000000000A7C0DA00406A
      970093C6F2008EC1EE0093C6F20037638D00588986003366990080B3E3003366
      990079A27700B2C6B80000000000000000000000000000000000AFBECD000048
      6D001F4F7600E8E8E80000000000000000007E99B2006A819C008CA3B8006981
      9C008FA5BA00567591000000000000000000000000000000000000000000E7E7
      E7006666660048484800484848005A5A5A00A0A0A0009A9A9A009A9A9A00ACAC
      AC00F3F3F3000000000000000000000000009C8E8400F7E7DE00E7C7B500E796
      6B00E7966B00DE8E63004286840094E7F70084E7F70063B6CE0042518C00849E
      FF007B96F7005A71CE0000185A00000000000000000000000000A9C2DC002121
      3200A0D3FA0098CBF500A0D3FA0013142A0033549200406BA300598DBF00406B
      A300334081003945840000000000000000000000000000000000AFBECD000046
      6D001F4E7600E8E8E8000000000000000000FDFDFD00D4D9E100FEFEFE00D5D9
      E10000000000D1D6DE00FEFEFE00000000000000000000000000000000000000
      0000898989005A5A5A00ACACAC00E1E1E100BEBEBE004242420088888800B8B8
      B800000000000000000000000000000000009C8E8400EFDFD600EFE7DE006349
      310000000000000000004A868400B5EFFF00A5EFFF007BBECE004A598C006371
      AD007386D6006B79CE0000185A00000000000000000000000000000000003939
      4A0080A6CA0099CCF60080A6CA0011132F00333C77006B9ECF008FC2EF006B9E
      CF00333C77004D55880000000000000000000000000000000000AFBDCD000044
      6D001E4C7400E8E8E80000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A0A0A0005A5A5A00B8B8B80000000000D0D0D0004242420088888800BEBE
      BE00000000000000000000000000000000009C8E8400EFDFCE00EFDFD6006349
      31000000000000000000528E8400CEF7FF00C6F7FF0094C7CE004A7984006B79
      AD00DEE7FF00B5C7FF0008185A00000000000000000000000000000000007272
      7F004B4B5C006A6A7A00474759002628490033386E0093C6F2008EC1EE0093C6
      F20033386E006A6D950000000000000000000000000000000000ABB9CA000047
      6D0014416D00EAEAEB0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000ACACAC005A5A5A00BEBEBE0000000000F3F3F3004242420088888800D0D0
      D00000000000000000000000000000000000ADA69400D6C7BD00E7D7CE006349
      31000000000000000000528E8C006B9E9C0084B6B500ADCFCE004A797B00C6C7
      D6006371A5006371A50052598C0000000000000000000000000000000000E8E8
      EA007B7B88005B5B6B007B7B8800898AA000333F76008DC0EB0098CBF50096C9
      F200333F76009C9DB60000000000000000000000000000000000DEE3EA00809B
      B30094A6BA00FCFCFC0000000000000000000000000000000000CCCFD600C7C9
      D000FDFDFD000000000000000000000000000000000000000000000000000000
      000000000000A0A0A000A0A0A000A0A0A000DBDBDB004242420088888800D0D0
      D00000000000000000000000000000000000D6C7BD00AD9E8C00D6C7BD006349
      31000000000000000000528E8C00F7FFFF00EFFFFF00CEFFFF004A797B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DCDCE40038518C001D5094004B7EB8006A9D
      D10038518C00DCDCE50000000000000000000000000000000000000000000000
      0000EEEEF2006F8199008493A600B6BCC700B3B9C4004D6F8D00003D66000040
      6800677C9200FBFBFB0000000000000000000000000000000000000000000000
      000000000000F3F3F300D0D0D000B8B8B8008989890071717100A6A6A600EDED
      ED000000000000000000000000000000000000000000D6C7BD00B59E94006349
      31000000000000000000B5CFCE00528E8C00528E8C00528E8C00528E8C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000006482B2003F6DB1003B6EB1002556
      9A006482B2000000000000000000000000000000000000000000000000000000
      000000000000B5C3D1002C58810000366A00003A6C0000426D00466F8F005D84
      A00029557900B9BDC70000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7E7E70071717100A0A0
      A000A0A0A000A0A0A000E7E7E700000000000000000000000000D6C7BD009479
      6B00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E4EAF300648ABD003D6CAC00648A
      BD00E4EAF3000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000B6C3D2006685A200A1AFC200000000000000
      0000FEFEFE00DFE2E80000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C4C4C4007171
      710071717100666666004E4E4E00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000005000500000204000004
      03000B0003000E000100170308000400000015000000000400001E000000160E
      0E00001008001800000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000002640000099FF000099FF000099FF000099
      FF000099FF000099FF000099FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000B010000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000050000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000003380000066FF00003380000000
      0000004DBF00004DBF0000133000002640000099FF00007CCF00002640000026
      4000007CCF00004D800000000000004D80000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000D030000FFFFFF00FC001400FC00
      1400FC001400FFFFFF00FC001400FC001400FC001400FFFFFF00FC001400FC00
      1400FFFFFF0009010000000000000000000000000000FFFFFF00FF000000FF00
      0000FF000000FFFFFF00FF000000FF000000FF000000FFFFFF00FF000000FF00
      0000FFFFFF00000000000000000000000000003380000066FF00003380000000
      00000066FF000066FF00001A4000002640000099FF000073BF00000000000000
      00000073BF00004D800000000000004D80000000000000000000000000000084
      8400008484000084840000848400008484000084840000848400008484000000
      0000000000000000000000000000000000000B010000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0021050000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000D2000001A4000000D20000000
      000000409F00004DBF0000133000002640000099FF00007CCF00002640000030
      50000086DF000086DF000073BF000086DF000000000000000000FFFFFF008484
      8400000000000084840000848400008484000084840000848400008484000084
      8400008484000000000000000000000000000B010000FFFFFF00FC001400FC00
      1400FC001400FFFFFF00FC001400FC001400FC001400FFFFFF00FC001400FC00
      1400FFFFFF0008040000000000000000000000000000FFFFFF00FF000000FF00
      0000FF000000FFFFFF00FF000000FF000000FF000000FFFFFF00FF000000FF00
      0000FFFFFF00000000000000000000000000003380000053CF00002660000000
      000000266000001A400000000000002640000099FF000099FF000073BF000060
      9F000086DF0000609F00001D3000004D8000000000000000000000FFFF008484
      8400000000000084840000848400008484000084840000848400008484000084
      84000084840000848400000000000000000003020000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0017040100000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      000000000000A3A3A3000000000000000000003380000066FF00003380000000
      00000066FF000066FF00001A4000002640000099FF000073BF00000000000000
      00000073BF00004D800000000000004D80000000000000000000FFFFFF008484
      8400FFFFFF000000000000000000000000000084840000848400008484000084
      84000084840000848400000000000000000003020000FFFFFF00FC001400FC00
      1400FC001400FFFFFF00FC001400FC001400FC001400FFFFFF00FC001400FC00
      1400FFFFFF00000C0000000000000000000000000000FFFFFF00FF000000FF00
      0000FF000000FFFFFF00FF000000FF0000000000000029FF29000DFF0D0000FF
      000000000000A3A3A3000000000000000000000D200000266000001A40000000
      00000066FF000066FF00001A4000002640000099FF000073BF00000000000000
      00000073BF000073BF0000609F000086DF00000000000000000000FFFF008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      00000000000000000000000000000000000003020000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0025001900000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000FF000000FF000000FF
      000000000000A3A3A300E0E0E00000000000AFAFAF0070707000303030000000
      000000000000001A400000061000002640000099FF00008FEF000073BF00008F
      EF000099FF0030ACFF0070C6FF00AFDFFF000000000000000000FFFFFF008484
      8400FFFFFF00C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C6000000
      00000000000000000000000000000000000003020000FFFFFF00FC001400FC00
      1400FC001400001900002700060000100500300000000B000000010600000010
      0000000900000C060000060A00000004050000000000FFFFFF00FF000000FF00
      0000FF0000000000000000000000000000000000000000FF000000FF000000FF
      000000000000000000000000000000000000000000000000000000000000DFDF
      DF0030303000000A1000000A1000002640000099FF00008FEF00008FEF0020A6
      FF00DFF2FF00000000000000000000000000000000000000000000FFFF008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C6C6C6000000
      00000000000000000000000000000000000003020000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000B000000F8000000F8000000F8000000F8000000F8000000
      F8000000F8000000F8000000F8000000160000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF0000000000000000000000000000000000000000
      0000606060000026400020A6FF00CFD9DF008FACBF00000000000073BF0040B3
      FF00000000000000000000000000000000000000000000000000000000008484
      8400FFFFFF00C6C6C600C6C6C600C6C6C600FFFFFF00FFFFFF00C6C6C6000000
      0000000000000000000000000000000000000302000002000B00000842000900
      190000050400150621000000F8000000F8000000F8000000F8000000F8000000
      F8000000F8000000F8000000F800180A16000000000000000000000000000000
      0000000000000000000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF0000000000000000000000000000000000000000
      00007F7F7F000026400040B3FF0000000000BFBFBF00000000000073BF0050B9
      FF00000000000000000000000000000000000000000000000000000000008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF008484840084848400848484000000
      00000000000000000000000000000000000003020000FFFF1000FFFF1000FFFF
      1000FFFF1000110602000000F8000000F8000000F8000000F8000000F8000000
      F8000000F8000000F8000000F8001304240000000000FFFF0000FFFF0000FFFF
      0000FFFF00000000000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF0000000000000000000000000000000000000000
      00008F8F8F000026400050B9FF0000000000EFEFEF00000000000073BF007FCC
      FF00000000000000000000000000000000000000000000000000000000008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF008484840000008400000084000000
      84000000840000008400000000000000000003020000000A00000E0900000E00
      260001110000000900001600010016010A000001000017000000001500000001
      0400200000000000030005082700150400000000000000000000000000000000
      0000000000000000000000000000000000000000000000FF000000FF000000FF
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007F7F7F007F7F7F007F7F7F00CFCFCF00000000000073BF007FCC
      FF00000000000000000000000000000000000000000000000000000000008484
      840084848400848484008484840084848400848484000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FF000000FF000000FF
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000EFEFEF00BFBFBF009F9F9F0060606000404040004096CF00CFEC
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400848484008484
      8400848484008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FF000000FF000000FF
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DFDFDF00404040007F7F
      7F007F7F7F007F7F7F00DFDFDF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000AFAFAF004040
      4000404040003030300010101000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005F5F
      5F00000000005F5F5F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000084
      8400008484000084840000848400008484000084840000848400008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF0000000000000000000000000000000000000000000000FF0000000000
      000000000000000000000000000000000000000000005F3F3F005F3F3F000000
      00003F9FDF00000000005F3F3F005F3F3F000000000000000000BFBFBF000000
      0000BFBFBF00000000000000000000000000000000000000000000FFFF000000
      0000008484000084840000848400008484000084840000848400008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF00
      0000FF000000FF000000000000000000000000000000FF000000FF000000FF00
      000000000000000000000000000000000000000000005F3F3F003F9FBF003FBF
      FF003FBFFF003F9FDF003F7FBF005F3F3F00000000000000000000000000BFBF
      BF0000000000000000000000000000000000000000000000000000FFFF008484
      8400000000000084840000848400008484000084840000848400008484000084
      8400008484000084840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000038D01000000
      0000000000000000000000000000000000000000000000000000FF000000FF00
      0000FF000000FF000000FF00000000000000FF000000FF000000FF000000FF00
      0000FF0000000000000000000000000000005F5F5F00000000005FDFFF00A4A0
      A000000000003F9FDF003F9FDF00000000005F5F5F0000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF008484
      8400FFFFFF000000000000000000000000000084840000848400008484000084
      8400008484000084840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000038D0100038D
      0100000000000000000000000000000000000000000000000000000000000000
      0000FF0000000000000000000000000000000000000000000000FF0000000000
      000000000000000000000000000000000000000000005FDFFF005FDFFF000000
      0000F0FBFF00000000003FBFFF003F9FDF000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000038D
      0100038D0100038D0100038D0100038D0100038D0100038D0100038D0100038D
      0100038D01000000000000000000000000000000000000000000000000000000
      0000FF0000000000000000000000000000000000000000000000FF0000000000
      0000000000000000000000000000000000005F5F5F00000000009FFFFF00A4A0
      A00000000000A4A0A0003FBFFF00000000005F5F5F00000000005F5F5F000000
      00005F5F5F000000000000000000000000000000000000000000FFFFFF008484
      8400FFFFFF00C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000038D0100038D
      0100000000000000000000000000000000000000000000000000000000000000
      0000FF0000000000000000000000000000000000000000000000FF0000000000
      000000000000000000000000000000000000000000005F3F3F003F9FBF009FFF
      FF005FDFFF005FDFFF003F9FBF005F3F3F007FBFBF005F3F3F00000000003F9F
      DF00000000005F3F3F005F3F3F0000000000000000000000000000FFFF008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000038D01000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF0000000000000000000000000000000000000000000000FF0000000000
      000000000000000000000000000000000000000000005F3F3F005F3F3F000000
      00005FDFFF00000000005F3F3F007FBFBF005F3F3F003F9FBF003FBFFF003FBF
      FF003F9FDF003F7FBF005F3F3F00000000000000000000000000000000008484
      8400FFFFFF00C6C6C600C6C6C600FFFFFF00FFFFFF00FFFFFF00FFFFFF0000FF
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF0000000000000000000000000000000000000000000000FF0000000000
      0000000000000000000000000000000000000000000000000000000000005F5F
      5F00000000005F5F5F00000000005F5F5F00000000005FDFFF00A4A0A0000000
      00003F9FDF003F9FDF00000000005F5F5F000000000000000000000000008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008484840000FF000000FF
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF0000000000000000000000000000000000000000000000FF0000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005FDFFF005FDFFF0000000000F0FB
      FF00000000003FBFFF003F9FDF00000000000000000000000000000000008484
      8400FFFFFF00C6C6C600C6C6C600C6C6C6008484840000FF000000FF000000FF
      000000FF000000FF000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF0000000000000000000000000000000000000000000000FF0000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000005F5F5F00000000009FFFFF00A4A0A0000000
      0000A4A0A0003FBFFF00000000005F5F5F000000000000000000000000008484
      840084848400848484008484840084848400848484008484840000FF000000FF
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000BFBFBF000000000000000000000000005F3F3F003F9FBF009FFFFF005FDF
      FF005FDFFF003F9FBF005F3F3F00000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FF000000FF
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000BFBF
      BF0000000000BFBFBF0000000000000000005F3F3F005F3F3F00000000005FDF
      FF00000000005F3F3F005F3F3F00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848484008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005F5F5F000000
      00005F5F5F000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005F3F3F005F3F3F005F3F3F005F3F
      3F005F3F3F005F3F3F005F3F3F005F3F3F005F3F3F005F3F3F005F3F3F005F3F
      3F005F3F3F005F3F3F005F3F3F005F3F3F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FCFCFC0000000000FCFCFC0000000000FCFCFC0000000000FCFCFC000000
      0000000000000000000000000000000000005F3F3F005F3F3F005F3F3F005F5F
      5F00000000005F5F5F005F3F3F005F3F3F005F3F3F0000000000000000005F3F
      3F005F3F3F005F3F3F005F3F3F005F3F3F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000000000000FCFC
      FC0085A29E00969696009394940096969600939494009696960085A29E00FCFC
      FC00000000000000000000000000000000005F3F3F005F3F3F005F3F3F000000
      00003F9FDF00000000005F3F3F005F3F3F005F3F3F005F3F3F00BFBFBF000000
      0000BFBFBF00000000005F3F3F005F3F3F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000800000000000000000000000000000000000000000000000000000003333
      3300333333003333330033333300333333003333330033333300333333003333
      3300FFFFFF000000000000000000000000000000000000000000FCFCFC0085A2
      9E00B5B5B500BABABA00B5B5B500BABABA00B5B5B500BABABA00B5B5B50085A2
      9E00FCFCFC000000000000000000000000005F3F3F005F3F3F003F9FBF003FBF
      FF003FBFFF003F9FDF003F7FBF005F3F3F005F3F3F005F3F3F005F3F3F00BFBF
      BF0000000000000000005F3F3F005F3F3F000000000000000000000000000000
      0000000000000000000000000000800000008000000000000000000000000000
      0000800000000000000000000000000000000000000000000000000000003333
      3300FFFFFF00CCCCCC00CCCCCC00999999009999990099999900666666003333
      3300FFFFFF0000000000000000000000000000000000FCFCFC0085A29E00B5B5
      B500BABABA00CBCBCB00D0D0D000CBCBCB00D0D0D000CBCBCB00BABABA00B5B5
      B50085A29E00FCFCFC0000000000000000005F5F5F00000000005FDFFF00A4A0
      A000000000003F9FDF003F9FDF00000000005F5F5F005F3F3F005F3F3F000000
      000000000000000000005F3F3F005F3F3F000000000000000000000000000000
      0000000000000000000000000000000000008000000080000000800000000000
      0000800000000000000000000000000000000000000000000000000000003333
      3300FFFFFF0099999900CCCCCC00666666009999990033333300666666003333
      3300FFFFFF000000000000000000000000000000000085A29E00B5B5B500BABA
      BA00C8C8C800CDCDCD00B5B5B500BABABA00B5B5B500CDCDCD00C8C8C800BABA
      BA00B5B5B50085A29E00FCFCFC0000000000000000005FDFFF005FDFFF000000
      0000F0FBFF00000000003FBFFF003F9FDF00000000005F3F3F005F3F3F005F3F
      3F005F3F3F005F3F3F005F3F3F005F3F3F000000000080000000800000008000
      0000800000008000000080000000800000008000000080000000800000008000
      0000800000000000000000000000000000000000000000000000000000003333
      3300FFFFFF0099999900CCCCCC00666666009999990033333300666666003333
      3300FFFFFF00000000000000000000000000000000008A989700DFDFDF00CBCB
      CB00CDCDCD00B5B5B500BABABA00B5B5B500BABABA00B5B5B500CDCDCD00CBCB
      CB00BABABA009696960000000000000000005F5F5F00000000009FFFFF00A4A0
      A00000000000A4A0A0003FBFFF00000000005F5F5F005F3F3F005F5F5F000000
      00005F5F5F005F3F3F005F3F3F005F3F3F000000000000000000000000000000
      0000000000000000000000000000000000008000000080000000800000000000
      0000800000000000000000000000000000000000000000000000000000003333
      3300FFFFFF0099999900CCCCCC00666666009999990033333300666666003333
      3300FFFFFF000000000000000000000000000000000085A29E00DBDBDB00D0D0
      D000B5B5B500BABABA00B5B5B500BABABA00B5B5B500BABABA00B5B5B500D0D0
      D000B5B5B50093949400FCFCFC00000000005F3F3F005F3F3F003F9FBF009FFF
      FF005FDFFF005FDFFF003F9FBF005F3F3F007FBFBF005F3F3F00000000003F9F
      DF00000000005F3F3F005F3F3F005F3F3F000000000000000000000000000000
      0000000000000000000000000000800000008000000000000000000000000000
      0000800000000000000000000000000000000000000000000000000000003333
      3300FFFFFF0099999900CCCCCC00666666009999990033333300666666003333
      3300FFFFFF00000000000000000000000000000000008A989700DFDFDF00CBCB
      CB00BABABA00B5B5B500BABABA00B5B5B500BABABA00B5B5B500BABABA00CBCB
      CB00BABABA009696960000000000000000005F3F3F005F3F3F005F3F3F000000
      00005FDFFF00000000005F3F3F007FBFBF005F3F3F003F9FBF003FBFFF003FBF
      FF003F9FDF003F7FBF005F3F3F005F3F3F000000000080000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000800000000000000000000000000000000000000000000000000000003333
      3300FFFFFF0099999900CCCCCC00666666009999990033333300666666003333
      3300FFFFFF000000000000000000000000000000000085A29E00DBDBDB00D0D0
      D000B5B5B500BABABA00B5B5B500BABABA00B5B5B500BABABA00B5B5B500D0D0
      D000B5B5B50093949400FCFCFC00000000005F3F3F005F3F3F005F3F3F005F5F
      5F00000000005F5F5F005F3F3F005F5F5F00000000005FDFFF00A4A0A0000000
      00003F9FDF003F9FDF00000000005F5F5F000000000080000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000003333
      3300FFFFFF0099999900CCCCCC00666666009999990033333300666666003333
      3300FFFFFF00000000000000000000000000000000008A989700DFDFDF00CBCB
      CB00CDCDCD00B5B5B500BABABA00B5B5B500BABABA00B5B5B500CDCDCD00CBCB
      CB00BABABA009696960000000000000000005F3F3F005F3F3F005F3F3F005F3F
      3F005F3F3F005F3F3F005F3F3F00000000005FDFFF005FDFFF0000000000F0FB
      FF00000000003FBFFF003F9FDF00000000000000000080000000000000000000
      0000000000008000000080000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000003333
      3300FFFFFF00CCCCCC00CCCCCC00999999009999990099999900666666003333
      3300FFFFFF00FFFFFF0000000000000000000000000085A29E00DEE7F000DFDF
      DF00C8C8C800CDCDCD00B5B5B500BABABA00B5B5B500CDCDCD00C8C8C800BABA
      BA00B5B5B50085A29E00FCFCFC00000000005F3F3F005F3F3F00000000000000
      0000000000005F3F3F005F3F3F005F5F5F00000000009FFFFF00A4A0A0000000
      0000A4A0A0003FBFFF00000000005F5F5F000000000080000000000000008000
      0000800000008000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000333333003333
      3300333333003333330033333300333333003333330033333300333333003333
      330033333300FFFFFF000000000000000000000000000000000085A29E00DEE7
      F000DFDFDF00CBCBCB00D0D0D000CBCBCB00D0D0D000CBCBCB00BABABA00B5B5
      B50085A29E00FCFCFC0000000000000000005F3F3F005F3F3F00000000000000
      0000BFBFBF005F3F3F005F3F3F005F3F3F005F3F3F003F9FBF009FFFFF005FDF
      FF005FDFFF003F9FBF005F3F3F005F3F3F000000000080000000800000008000
      0000800000008000000080000000800000008000000080000000800000008000
      000080000000000000000000000000000000000000000000000033333300CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00999999009999990099999900999999006666
      660033333300FFFFFF00000000000000000000000000000000000000000085A2
      9E00DEE7F000DFDFDF00DBDBDB00DFDFDF00DBDBDB00DFDFDF00B5B5B50085A2
      9E00FCFCFC000000000000000000000000005F3F3F005F3F3F0000000000BFBF
      BF0000000000BFBFBF005F3F3F005F3F3F005F3F3F005F3F3F00000000005FDF
      FF00000000005F3F3F005F3F3F005F3F3F000000000080000000000000008000
      0000800000008000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000333333003333
      3300333333003333330033333300333333003333330033333300333333003333
      3300333333000000000000000000000000000000000000000000000000000000
      000085A29E008A98970085A29E008A98970085A29E008A98970085A29E00FCFC
      FC00000000000000000000000000000000005F3F3F005F3F3F005F3F3F005F3F
      3F005F3F3F0000000000000000005F3F3F005F3F3F005F3F3F005F5F5F000000
      00005F5F5F005F3F3F005F3F3F005F3F3F000000000080000000000000000000
      0000000000008000000080000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000033333300333333003333330000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005F3F3F005F3F3F005F3F3F005F3F
      3F005F3F3F005F3F3F005F3F3F005F3F3F005F3F3F005F3F3F005F3F3F005F3F
      3F005F3F3F005F3F3F005F3F3F005F3F3F000000000080000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F8F8F800575657000F0F0F000F0F0F0053535300F7F7F7000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F8F8F800575757000F0F0F000F0F0F0053535300F7F7F7000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F8F8F800575757000F0F0F000F0F0F0053535300F7F7F7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F1F1F100676767003F363B00778475006E7B79002B2324004E4E4F00F0F0
      F000000000000000000000000000000000000000000000000000000000000000
      0000F1F1F1006C6D6C00312D3100383E38003C433C002B262B0050505000F0F0
      F000000000000000000000000000000000000000000000000000000000000000
      0000F1F1F1006C6D6C00312D3100383E38003C433C002B262B0050505000F0F0
      F000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F3F3F3007E7E
      7E0013131300453C47006FBD870017FF520017FF5D0057A37A0031292C001212
      12007F7F7F00F3F3F30000000000000000000000000000000000F3F3F3007E7E
      7E00161616003733370022472200047A04000B820B003F663F00312C31001212
      12007F7F7F00F3F3F30000000000000000000000000000000000F3F3F3007E7E
      7E00161616003733370022472200047A04000B820B003F663F00312C31001212
      12007F7F7F00F3F3F30000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000ECECEC004D4D4D000000
      000000000000758386001DFF6C0000FF3B0000FF3C001CFF4B006D7966000000
      0000000000004D4D4D00ECECEC000000000000000000ECECEC004D4D4D000000
      000005030500383E3800057A050000840000008200000B820B003B423B000503
      0500000000004D4D4D00ECECEC000000000000000000ECECEC004D4D4D000000
      000005030500383E3800057A050000840000008200000B820B003B423B000503
      0500000000004D4D4D00ECECEC00000000000000000000000000000000000000
      00009090E00000000000000000000000000000000000000000009090E0008088
      E00000000000000000000000000000000000000000007E7E7E000A0A0A000303
      0300000000006B7876001CFF550000FF3C0000FF3F001DFF4F0075826E000000
      0000030303000A0A0A007E7E7E0000000000000000007E7E7E000A0A0A000303
      0300060406003A413A000C820C000080000000820000057A0500383E38000604
      0600030303000A0A0A007E7E7E0000000000000000007E7E7E000A0A0A000303
      0300060406003A413A000C820C000082000000840000057A0500383E38000604
      0600030303000A0A0A007E7E7E00000000000000000000000000000000000000
      C8000000C0006060C8000000000000000000000000006060C8000000C8000000
      C8000000000000000000000000000000000000000000808080002B2B2B003434
      34001A191A003C343800549F5D001AFF5C001BFF3A0070BC7600494048001818
      1800343434002B2B2B00808080000000000000000000808080002B2B2B003434
      34001A1A1A003B373C00395D370019810700147C03001E411B003A373B001A1A
      1A00343434002B2B2B00808080000000000000000000808080002B2B2B003434
      34001A1A1A003C373C003C613A000B7C060004750000224721003B373B001A1A
      1A00343434002B2B2B0080808000000000000000000000000000000000006060
      C8000000C8000000C0006060C800000000006060C8000000C0000000C8006060
      C0000000000000000000000000000000000000000000FEFEFE00F9F9F900DFDF
      DF0041424100312E2D004944510055A19F0055A18C0049434B00302D2D004142
      4200DFDFDF00F9F9F900FEFEFE000000000000000000FEFEFE00F9F9F900DFDF
      DF00414242002E29290057595D0056DAD3005BDFD800727478002E2828004142
      4200DFDFDF00F9F9F900FEFEFE000000000000000000FEFEFE00F9F9F900DFDF
      DF00414141003230300048484C0024696300175C55003B3A3E00353232004141
      4100DFDFDF00F9F9F900FEFEFE00000000000000000000000000000000000000
      00006060C8000000C8000000C8002020B8000000C0000000C8006060C8000000
      0000000000000000000000000000000000000000000000000000B6B6B6001010
      1000030202003E3E3E0021797900007D8000007C81000C5F60003A3A3A000403
      030010101000B6B6B60000000000000000000000000000000000B6B6B6001010
      1000000000005655550038E5E50000FFFF0000FFFF0043F3F300686868000000
      000010101000B6B6B60000000000000000000000000000000000B6B6B6001010
      1000030202003E3D3D00227A7A0000868800008688000C6262003A3A3A000403
      030010101000B6B6B60000000000000000000000000000000000000000000000
      0000000000006060C0000000C8000000C0000000C8006060C800000000000000
      00000000000000000000000000000000000000000000C8C8C800151515000000
      0000050303003841410005828200008181000081810005828200384141000503
      03000000000015151500C8C8C8000000000000000000C8C8C800151515000000
      0000000000007888880018FFFF0000FFFF0000FFFF0018FFFF00788888000000
      00000000000015151500C8C8C8000000000000000000C8C8C800151515000000
      0000050303003841410005828200008181000081810005828200384141000503
      03000000000015151500C8C8C800000000000000000000000000000000000000
      0000000000006060C8000000C0000000C8000000C8006060C000000000000000
      0000000000000000000000000000000000000000000051515100111111001414
      1400090808003A3A3A000C6262000088860000888600227A7A003D3D3D000807
      0700141414001111110051515100000000000000000051515100111111001414
      1400030101006867670043F3F30000FFFF0000FFFF0038E5E500555555000503
      0300141414001111110051515100000000000000000051515100111111001414
      1400090808003A3A3A000C605F0000807C0000827D00217A79003D3D3D000807
      0700141414001111110051515100000000000000000000000000000000000000
      00006060C8000000C8000000C8002020B8000000C0000000C8006060C8000000
      00000000000000000000000000000000000000000000DBDBDB00909090009D9D
      9D0040404000363333003B3E3A0017555C0024636900484C4800333131004040
      40009D9D9D0090909000DBDBDB000000000000000000DBDBDB00909090009D9D
      9D00404040002F292900727874005BD8DF0056D3DA00575D59002F2A2A004041
      41009D9D9D0090909000DBDBDB000000000000000000DBDBDB00909090009D9D
      9D0040404000312E2E00494C43005594A1005594A100494D4400322F2F004040
      40009D9D9D0090909000DBDBDB00000000000000000000000000000000006060
      C8000000C0000000C8006060C000000000006060C8000000C0000000C8006060
      C000000000000000000000000000000000000000000000000000FAFAFA009191
      9100191919003A3A350022214700040075000B067C003C3A61003A3B36001919
      190091919100FAFAFA0000000000000000000000000000000000FAFAFA009191
      910019191900393A35001E1B410014037C001907810039375D003A3B36001919
      190091919100FAFAFA0000000000000000000000000000000000FAFAFA009191
      91001717170048483F00706FBC001B12FF001A11FF0054529F003B3B32001919
      180091919100FAFAFA0000000000000000000000000000000000000000000000
      C8000000C8006060C8000000000000000000000000006060C8000000C8000000
      C8000000000000000000000000000000000000000000ECECEC004D4D4D000000
      00000505030038383E0005057A0000008400000082000C0C82003A3A41000505
      0200000000004D4D4D00ECECEC000000000000000000ECECEC004D4D4D000000
      00000505030038383E0005057A0000008200000080000C0C82003A3A41000505
      0200000000004D4D4D00ECECEC000000000000000000ECECEC004D4D4D000000
      000000000000757583001D1DFF000000FF000000FF001C1CFF006C6C78000000
      0000000000004D4D4D00ECECEC00000000000000000000000000000000000000
      00008890D80000000000000000000000000000000000000000008890D8008088
      E00000000000000000000000000000000000000000007E7E7E000A0A0A000303
      0300060604003B3B42000B0B8200000082000000840005057A0038383E000606
      0400030303000A0A0A007E7E7E0000000000000000007E7E7E000A0A0A000303
      0300060604003B3B42000B0B8200000082000000840005057A0038383E000606
      0400030303000A0A0A007E7E7E0000000000000000007E7E7E000A0A0A000303
      0300000000006C6C79001C1CFF000000FF000000FF001D1DFF00757582000000
      0000030303000A0A0A007E7E7E00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000808080002B2B2B002E2E
      2E001414140033332F00424269000C0C850004047D0025254C003B3B36001717
      17002D2D2D002B2B2B00808080000000000000000000808080002B2B2B002E2E
      2E001414140033332F00424269000C0C850004047D0025254C003B3B36001717
      17002D2D2D002B2B2B00808080000000000000000000808080002B2B2B002E2E
      2E001313130034342C005A5AA7001B1BFF001B1BFF007474C100494940001515
      14002D2D2D002B2B2B0080808000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FCFCFC00EEEEEE00EFEF
      EF00F0F0F00059595900282824002B2B320028282F00262622006B6B6B00F1F1
      F100EEEEEE00EEEEEE00FCFCFC000000000000000000FCFCFC00EEEEEE00EFEF
      EF00F0F0F00059595900282824002B2B320028282F00262622006B6B6B00F1F1
      F100EEEEEE00EEEEEE00FCFCFC000000000000000000FCFCFC00EEEEEE00EFEF
      EF00F0F0F00057575700282821005A5A67006363700033332A0066666600F2F2
      F200EEEEEE00EEEEEE00FCFCFC00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F2F2F200FFFFFF00F2F2F200FFFFFF00F2F2F200FFFFFF00F2F2F2000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FCFCFC0000000000FCFCFC0000000000FCFCFC0000000000FCFCFC000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FCFCFC0000000000FCFCFC0000000000FCFCFC0000000000FCFCFC000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FCFCFC0000000000FCFCFC0000000000FCFCFC0000000000FCFCFC000000
      000000000000000000000000000000000000000000000000000000000000F2F2
      F2000D0D0D00000000000D0D0D00000000000D0D0D00000000000D0D0D00F2F2
      F20000000000000000000000000000000000000000000000000000000000FCFC
      FC0022702D00000000000416070000000000041607000000000022702D00FCFC
      FC0000000000000000000000000000000000000000000000000000000000FCFC
      FC0085A29E00969696009394940096969600939494009696960085A29E00FCFC
      FC0000000000000000000000000000000000000000000000000000000000FCFC
      FC0015796900000000000316160000000000031616000000000015796900FCFC
      FC00000000000000000000000000000000000000000000000000F2F2F2000D0D
      0D000000A1000D0DAE000000A1000D0DAE000000A1000D0DAE000000A1000D0D
      0D00F2F2F2000000000000000000000000000000000000000000FCFCFC002270
      2D0013892300169E280013892300169E280013892300169E2800138923002270
      2D00FCFCFC000000000000000000000000000000000000000000FCFCFC0085A2
      9E00B5B5B500BABABA00B5B5B500BABABA00B5B5B500BABABA00B5B5B50085A2
      9E00FCFCFC000000000000000000000000000000000000000000FCFCFC001579
      69001083880013989E001083880013989E001083880013989E00108388001579
      6900FCFCFC0000000000000000000000000000000000F2F2F2000D0D0D000000
      A1000D0DAE005F5FB1006C6CBE005F5FB1006C6CBE005F5FB1000D0DAE000000
      A1000D0D0D00F2F2F200000000000000000000000000FCFCFC0022702D001389
      2300169E280027E140003EE5540027E140003EE5540027E14000169E28001389
      230022702D00FCFCFC00000000000000000000000000FCFCFC0085A29E00B5B5
      B500BABABA00CBCBCB00D0D0D000CBCBCB00D0D0D000CBCBCB00BABABA00B5B5
      B50085A29E00FCFCFC00000000000000000000000000FCFCFC00157969001083
      880013989E001EDCE50035DFE7001EDCE50035DFE7001EDCE50013989E001083
      880015796900FCFCFC000000000000000000000000000D0D0D000000A1000D0D
      AE004646B9005353C6000000A1000D0DAE000000A1005353C6004646B9000D0D
      AE000000A1000D0D0D00F2F2F200000000000000000022702D0013892300169E
      28001EDA37002EE2460013892300169E2800138923002EE246001EDA3700169E
      28001389230022702D00FCFCFC00000000000000000085A29E00B5B5B500BABA
      BA00C8C8C800CDCDCD00B5B5B500BABABA00B5B5B500CDCDCD00C8C8C800BABA
      BA00B5B5B50085A29E00FCFCFC00000000000000000015796900108388001398
      9E001AD3DB0025DEE6001083880013989E001083880025DEE6001AD3DB001398
      9E001083880015796900FCFCFC000000000000000000000000009B9BDE005F5F
      B1005353C6000000A1000D0DAE000000A1000D0DAE000000A1005353C6005F5F
      B1000D0DAE0000000000FFFFFF00000000000000000015501D0081EE900027E1
      40002EE2460013892300169E280013892300169E2800138923002EE2460027E1
      4000169E2800000000000000000000000000000000008A989700DFDFDF00CBCB
      CB00CDCDCD00B5B5B500BABABA00B5B5B500BABABA00B5B5B500CDCDCD00CBCB
      CB00BABABA00969696000000000000000000000000000D544C0077EAEF001EDC
      E50025DEE6001083880013989E001083880013989E001083880025DEE6001EDC
      E50013989E00000000000000000000000000000000000D0D0D008E8ED1006C6C
      BE000000A1000D0DAE000000A1000D0DAE000000A1000D0DAE000000A1006C6C
      BE000000A1000D0D0D00F2F2F200000000000000000022702D006AEB7B003EE5
      540013892300169E280013892300169E280013892300169E2800138923003EE5
      54001389230004160700FCFCFC00000000000000000085A29E00DBDBDB00D0D0
      D000B5B5B500BABABA00B5B5B500BABABA00B5B5B500BABABA00B5B5B500D0D0
      D000B5B5B50093949400FCFCFC0000000000000000001579690062E7ED0035DF
      E7001083880013989E001083880013989E001083880013989E001083880035DF
      E7001083880003161600FCFCFC000000000000000000000000009B9BDE005F5F
      B1000D0DAE000000A1000D0DAE000000A1000D0DAE000000A1000D0DAE005F5F
      B1000D0DAE0000000000FFFFFF00000000000000000015501D0081EE900027E1
      4000169E280013892300169E280013892300169E280013892300169E280027E1
      4000169E2800000000000000000000000000000000008A989700DFDFDF00CBCB
      CB00BABABA00B5B5B500BABABA00B5B5B500BABABA00B5B5B500BABABA00CBCB
      CB00BABABA00969696000000000000000000000000000D544C0077EAEF001EDC
      E50013989E001083880013989E001083880013989E001083880013989E001EDC
      E50013989E00000000000000000000000000000000000D0D0D008E8ED1006C6C
      BE000000A1000D0DAE000000A1000D0DAE000000A1000D0DAE000000A1006C6C
      BE000000A1000D0D0D00F2F2F200000000000000000022702D006AEB7B003EE5
      540013892300169E280013892300169E280013892300169E2800138923003EE5
      54001389230004160700FCFCFC00000000000000000085A29E00DBDBDB00D0D0
      D000B5B5B500BABABA00B5B5B500BABABA00B5B5B500BABABA00B5B5B500D0D0
      D000B5B5B50093949400FCFCFC0000000000000000001579690062E7ED0035DF
      E7001083880013989E001083880013989E001083880013989E001083880035DF
      E7001083880003161600FCFCFC000000000000000000000000009B9BDE005F5F
      B1005353C6000000A1000D0DAE000000A1000D0DAE000000A1005353C6005F5F
      B1000D0DAE0000000000FFFFFF00000000000000000015501D0081EE900027E1
      40002EE2460013892300169E280013892300169E2800138923002EE2460027E1
      4000169E2800000000000000000000000000000000008A989700DFDFDF00CBCB
      CB00CDCDCD00B5B5B500BABABA00B5B5B500BABABA00B5B5B500CDCDCD00CBCB
      CB00BABABA00969696000000000000000000000000000D544C0077EAEF001EDC
      E50025DEE6001083880013989E001083880013989E001083880025DEE6001EDC
      E50013989E00000000000000000000000000000000000D0D0D008E8ED1009B9B
      DE004646B9005353C6000000A1000D0DAE000000A1005353C6004646B9000D0D
      AE000000A1000D0D0D00F2F2F200000000000000000022702D00DDE2EE0081EE
      90001EDA37002EE2460013892300169E2800138923002EE246001EDA3700169E
      28001389230022702D00FCFCFC00000000000000000085A29E00DEE7F000DFDF
      DF00C8C8C800CDCDCD00B5B5B500BABABA00B5B5B500CDCDCD00C8C8C800BABA
      BA00B5B5B50085A29E00FCFCFC00000000000000000015796900DBE5EF0077EA
      EF001AD3DB0025DEE6001083880013989E001083880025DEE6001AD3DB001398
      9E001083880015796900FCFCFC000000000000000000000000000D0D0D008E8E
      D1009B9BDE005F5FB1006C6CBE005F5FB1006C6CBE005F5FB1000D0DAE000000
      A1000D0D0D00F2F2F2000000000000000000000000000000000022702D00DDE2
      EE0081EE900027E140003EE5540027E140003EE5540027E14000169E28001389
      230022702D00FCFCFC000000000000000000000000000000000085A29E00DEE7
      F000DFDFDF00CBCBCB00D0D0D000CBCBCB00D0D0D000CBCBCB00BABABA00B5B5
      B50085A29E00FCFCFC000000000000000000000000000000000015796900DBE5
      EF0077EAEF001EDCE50035DFE7001EDCE50035DFE7001EDCE50013989E001083
      880015796900FCFCFC0000000000000000000000000000000000000000000D0D
      0D008E8ED1009B9BDE008E8ED1009B9BDE008E8ED1009B9BDE000000A1000D0D
      0D00F2F2F2000000000000000000000000000000000000000000000000002270
      2D00DDE2EE0081EE90006AEB7B0081EE90006AEB7B0081EE9000138923002270
      2D00FCFCFC0000000000000000000000000000000000000000000000000085A2
      9E00DEE7F000DFDFDF00DBDBDB00DFDFDF00DBDBDB00DFDFDF00B5B5B50085A2
      9E00FCFCFC000000000000000000000000000000000000000000000000001579
      6900DBE5EF0077EAEF0062E7ED0077EAEF0062E7ED0077EAEF00108388001579
      6900FCFCFC000000000000000000000000000000000000000000000000000000
      00000D0D0D00000000000D0D0D00000000000D0D0D00000000000D0D0D00F2F2
      F200000000000000000000000000000000000000000000000000000000000000
      000022702D0015501D0022702D0015501D0022702D0015501D0022702D00FCFC
      FC00000000000000000000000000000000000000000000000000000000000000
      000085A29E008A98970085A29E008A98970085A29E008A98970085A29E00FCFC
      FC00000000000000000000000000000000000000000000000000000000000000
      0000157969000D544C00157969000D544C00157969000D544C0015796900FCFC
      FC00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000848400008484000000
      00000000000000000000000000000000000000000000C6C6C600C6C6C6000000
      0000008484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C6000000
      0000C6C6C6000000000000000000000000000000000000848400008484000000
      00000000000000000000000000000000000000000000C6C6C600C6C6C6000000
      0000008484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C6C60000000000000000000000000000848400008484000000
      00000000000000000000000000000000000000000000C6C6C600C6C6C6000000
      0000008484000000000000000000000000000000000000000000000000000084
      8400008484000084840000848400008484000084840000848400008484000084
      84000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000080808000C0C0C000C0C0C0008080
      80000000000000000000000000000000000000000000C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C60000FFFF0000FFFF0000FFFF00C6C6C600C6C6
      C600000000000000000000000000000000000000000000848400008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000848400000000000000000000000000000000000000000000FFFF000000
      0000008484000084840000848400008484000084840000848400008484000084
      84000084840000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000080808000C0C0C000C0C0C000FFFF00008080
      8000FFFFFF0000000000000000000000000000000000C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600848484008484840084848400C6C6C600C6C6
      C60000000000C6C6C60000000000000000000000000000848400008484000084
      8400008484000084840000848400008484000084840000848400008484000084
      8400008484000000000000000000000000000000000000000000FFFFFF0000FF
      FF00000000000084840000848400008484000084840000848400008484000084
      84000084840000848400000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000C0C0C000C0C0C000C0C0C000C0C0C0008080
      8000C0C0C0000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C6C600C6C6C600000000000000000000848400008484000000
      0000000000000000000000000000000000000000000000000000000000000084
      840000848400000000000000000000000000000000000000000000FFFF00FFFF
      FF0000FFFF000000000000848400008484000084840000848400008484000084
      84000084840000848400008484000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000C0C0C000FFFF0000C0C0C000C0C0C0008080
      8000C0C0C00000000000000000000000000000000000C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C6000000
      0000C6C6C60000000000C6C6C60000000000000000000084840000000000C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C6000000
      0000008484000000000000000000000000000000000000000000FFFFFF0000FF
      FF00FFFFFF0000FFFF0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000080808000FFFF0000FFFF0000C0C0C0008080
      8000FFFFFF000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C6
      C60000000000C6C6C6000000000000000000000000000084840000000000C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C6000000
      000000848400000000000000000000000000000000000000000000FFFF00FFFF
      FF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000080808000C0C0C000C0C0C0008080
      800000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000C6C6C60000000000C6C6C60000000000000000000084840000000000C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C6000000
      0000008484000000000000000000000000000000000000000000FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000FFFFFF000000
      000000000000000000000000000000000000000000000084840000000000C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C6000000
      000000848400000000000000000000000000000000000000000000FFFF00FFFF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000000000000000000000000000000000000084840000000000C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF000000000000000000000000000000000000000000FFFF
      FF0000000000000000000000000000000000000000000084840000000000C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C6000000
      0000C6C6C6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C0C0C000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000004010000040100000401000004010000040100000401
      0000040100000401000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000040100006699CC006699CC006699CC006699CC006699CC006699
      CC006699CC006699CC0004010000000000000000000000000000000000000000
      000000000000000000000000000064686A00002A7F0040404000565656000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000004010000040100000401
      000004010000040100000401000004010000040100006699CC0099CCFF0099CC
      FF0099CCFF006699CC0004010000000000000000000000000000000000000000
      00000000000064686A00002A3F000073DC000055FF00002A7F00393055007274
      7500ABB1B4000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000040100006699CC006699CC006699
      CC006699CC006699CC006699CC006699CC006699CC00040100006699CC0099CC
      FF00040100000401000004010000000000000000000000000000000000006468
      6A006E2C4A00FF6B6B0000557F000073DC000055FF00002A7F006035CA005656
      5600565656008F9395000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000401000099CCFF006699CC006699
      CC006699CC006699CC006699CC006699CC006699CC00040100006699CC0099CC
      FF0099CCFF006699CC0004010000000000000000000000000000000000007F58
      5800DC589500FF6B6B0000557F000073DC000049DC0000246D006035CA000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000086BF8A00DCDCDC00DCDCDC00DCDCDC00DCDCDC00DCDCDC00DCDCDC00DCDC
      DC00DCDCDC000000000000000000000000000401000099CCFF006699CC006699
      CC006699CC006699CC006699CC000401000004010000040100006699CC0099CC
      FF0099CCFF006699CC0004010000000000000000000000000000000000007F58
      5800DC589500BF505000007FBF00008EED000079ED00001E5C006035CA000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000003DAA
      410044AD48000000000086BF8A00DCDCDC00DCDCDC00DCDCDC00DCDCDC00DCDC
      DC00DCDCDC000000000000000000000000000401000099CCFF006699CC006699
      CC006699CC006699CC00040100006699CC006699CC00040100006699CC0099CC
      FF0099CCFF000401000004010000040100000000000000000000000000007F58
      5800DC5895007F46460000AAFF0000AAFF00007FFF00004FED002F1A65000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000003DAA
      41003DAA41003DAA41003DAC42000000000086BF8A00DCDCDC00DCDCDC00DCDC
      DC00DCDCDC000000000000000000000000000401000099CCFF006699CC006699
      CC006699CC006699CC006699CC006699CC006699CC00040100006699CC0099CC
      FF0099CCFF0099CCFF006699CC0004010000000000000000000064686A00FFB1
      B100FFB1B1007F58580000AAFF0000AAFF00007FFF000055FF0047357F00969C
      9F0000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000003DAA
      41003DAA41003DAA41003DAA41003DAA41003DAA41000000000086BF8A00DCDC
      DC00DCDCDC000000000000000000000000000401000099CCFF006699CC006699
      CC006699CC006699CC006699CC006699CC006699CC006699CC00040100006699
      CC0099CCFF0099CCFF006699CC0004010000000000000000000064686A00FFB1
      B100FFB1B1007F58580000AAFF0000AAFF00007FFF000055FF0047357F006468
      6A0000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000003DAA
      41003DAA41003DAA41003DAA41003DAA41003DAA41003DAA410045AE490076BB
      7C0086BF8A000000000000000000000000000401000099CCFF006699CC006699
      CC006699CC006699CC006699CC006699CC006699CC006699CC00040100006699
      CC000401000099CCFF006699CC0004010000000000000000000064686A00FFB1
      B100FFB1B1007F58580000AAFF003FBFFF0075B8FF001A71FF0047357F006468
      6A0000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000003DAA
      41003DAA41003DAA41003DAA41003DAA41003DAA41000000000086BF8A00DCDC
      DC00DCDCDC000000000000000000000000000401000099CCFF006699CC006699
      CC006699CC006699CC006699CC006699CC006699CC006699CC00040100006699
      CC000401000099CCFF006699CC0004010000000000000000000064686A00FFB1
      B100FFB1B1007F6B6B005A9BBF00556A9500003FBF001A467F008775BF006468
      6A0000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000003DAA
      41003DAA41003DAA41003DAC42000000000086BF8A00DCDCDC00DCDCDC00DCDC
      DC00DCDCDC000000000000000000000000000401000099CCFF006699CC006699
      CC006699CC006699CC006699CC00040100006699CC006699CC00040100006699
      CC0099CCFF0099CCFF006699CC0004010000000000000000000064686A00FFC4
      C400FFC6C600FF9F9F005D4F5D00525252003D3D3D008063A5008247CA006468
      6A0000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000003DAA
      410044AD48000000000086BF8A00DCDCDC00DCDCDC00DCDCDC00DCDCDC00DCDC
      DC00DCDCDC000000000000000000000000000401000099CCFF006699CC006699
      CC006699CC006699CC006699CC00040100006699CC006699CC00040100006699
      CC00CCFFFF00CCFFFF0099CCFF00040100000000000000000000000000000000
      0000989898005C5C5C003B3D3E00B6B6B60053535300919191005C5C5C000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000086BF8A00DCDCDC00DCDCDC00DCDCDC00DCDCDC00DCDCDC00DCDCDC00DCDC
      DC00DCDCDC000000000000000000000000000401000099CCFF006699CC006699
      CC006699CC006699CC006699CC006699CC006699CC006699CC00040100000401
      0000040100000401000004010000000000000000000000000000000000000000
      0000E9E9E9007D7D7D0064686A00999FA200BFBFBF00EFEFEF007D7D7D000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000401000099CCFF0099CCFF0099CC
      FF0099CCFF0099CCFF0099CCFF0099CCFF0099CCFF006699CC00040100000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000058585800515354000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000004010000040100000401
      0000040100000401000004010000040100000401000004010000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003F3F7F00000080000000
      8000000080000000800000008000000080000000000000000000000000000000
      00000000000000000000000000000000000000000000308F5D0020603E002060
      3E0020603E0020603E0020603E0020603E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005F5F
      5F00000000005F5F5F000000000000000000000000003F3F7F003F3FBF003F3F
      9F003F3F9F003F3F9F003F3F9F00000080000000000000000000000000005F5F
      5F00000000005F5F5F00000000000000000000000000308F5D0040BF7C0038A7
      6D0038A76D0038A76D0038A76D0020603E000000000086D79E002F8D4B003295
      500033985100339851003398510033985100329650003295500031934E00308F
      4C002F8D4B002C8346007AD3950000000000000000008686D7002F2F8D003232
      9500333398003333980033339800333398003232960032329500313193003030
      8F002F2F8D002C2C83007A7AD30000000000000000005F3F3F005F3F3F000000
      00003F9FDF00000000005F3F3F005F3F3F00000000003F3F7F003F3FBF003F3F
      DF003F3FDF003F3FDF003F3F9F0000008000000000005F3F3F005F3F3F000000
      00003F9FDF00000000005F3F3F005F3F3F0000000000308F5D0040BF7C0057C7
      8C0057C78C0057C78C0038A76D0020603E000000000037A55800D8F2E0000000
      00000000000090DAA6005AC87B004DC470003FBC64003EB963003CB35F003AAD
      5C0038A75900349C53002C83460000000000000000003737A500D8D8F2000000
      0000000000009090DA005A5AC8004D4DC4003F3FBC003E3EB9003C3CB3003A3A
      AD003838A70034349C002C2C830000000000000000005F3F3F003F9FBF003FBF
      FF003FBFFF003F9FDF003F7FBF005F3F3F00000000003F3F7F003F3FBF003F3F
      DF003F3FDF003F3FDF003F3F9F0000008000000000005F3F3F003F9FBF003FBF
      FF003FBFFF003F9FDF003F7FBF005F3F3F0000000000308F5D0040BF7C0057C7
      8C0057C78C0057C78C0038A76D0020603E00000000003CB35F00000000007BD3
      95009EDFB1000000000000000000E3F6E90065CC840045C16A0040BF66003EB9
      63003BB15E0037A558002F8C4B0000000000000000003C3CB300000000007B7B
      D3009E9EDF000000000000000000E3E3F6006565CC004545C1004040BF003E3E
      B9003B3BB1003737A5002F2F8C00000000005F5F5F00000000005FDFFF00A4A0
      A000000000003F9FDF003F9FDF00000000005F5F5F003F3F7F003F3FBF003F3F
      DF003F3FDF003F3FDF003F3F9F00000080005F5F5F00000000005FDFFF00A4A0
      A000000000003F9FDF003F9FDF00000000005F5F5F00308F5D0040BF7C0057C7
      8C0057C78C0057C78C0038A76D0020603E00000000003EBA630000000000ADE4
      BD005DC97D009BDEAF00C0EACD0000000000CEEFD8004BC36F0044C169003FBD
      65003CB4600039AA5B00308F4C0000000000000000003E3EBA0000000000ADAD
      E4005D5DC9009B9BDE00C0C0EA0000000000CECEEF004B4BC3004444C1003F3F
      BD003C3CB4003939AA0030308F0000000000000000005FDFFF005FDFFF000000
      0000F0FBFF00000000003FBFFF003F9FDF00000000003F3F7F003F3FBF003F3F
      BF003F3FBF003F3FBF003F3F9F0000008000000000005FDFFF005FDFFF000000
      0000F0FBFF00000000003FBFFF003F9FDF0000000000308F5D0040BF7C0040BF
      7C0040BF7C0040BF7C0038A76D0020603E000000000044C1690084D69C000000
      0000BCE9C90060CA80005CC97C0000000000D5F1DD007BD395006ECF8B0040BF
      66003EB963003AAE5D003295500000000000000000004444C1008484D6000000
      0000BCBCE9006060CA005C5CC90000000000D5D5F1007B7BD3006E6ECF004040
      BF003E3EB9003A3AAE0032329500000000005F5F5F00000000009FFFFF00A4A0
      A00000000000A4A0A0003FBFFF00000000005F5F5F003F3F7F005F5F5F000000
      00005F5F5F003F3F7F003F3F7F00000080005F5F5F00000000009FFFFF00A4A0
      A00000000000A4A0A0003FBFFF00000000005F5F5F00308F5D00308F5D000000
      0000308F5D00308F5D00308F5D0020603E00000000004BC36F005FCA7F0092DB
      A80000000000B9E8C7005FCA7F0099DDAD00F9FDFA000000000000000000D2F0
      DB0060CA80003CB460003399510000000000000000004B4BC3005F5FCA009292
      DB0000000000B9B9E8005F5FCA009999DD00F9F9FD000000000000000000D2D2
      F0006060CA003C3CB4003333990000000000000000005F3F3F003F9FBF009FFF
      FF005FDFFF005FDFFF003F9FBF005F5F5F00A4A0A0005F3F3F00000000003F9F
      DF00000000005F3F3F005F3F3F0000000000000000005F3F3F003F9FBF009FFF
      FF005FDFFF005FDFFF003F9FBF005F5F5F00A4A0A0005F3F3F00000000003F9F
      DF00000000005F3F3F005F3F3F00000000000000000054C6760066CC840069CD
      870093DBA80000000000B4E6C30057C7780071D08D005AC87B006CCE89000000
      0000E7F7EC0051C57400349C530000000000000000005454C6006666CC006969
      CD009393DB0000000000B4B4E6005757C7007171D0005A5AC8006C6CCE000000
      0000E7E7F7005151C50034349C0000000000000000005F3F3F005F3F3F000000
      00005FDFFF00000000005F3F3F00A4A0A0005F3F3F003F9FBF003FBFFF003FBF
      FF003F9FDF003F7FBF005F3F3F0000000000000000005F3F3F005F3F3F000000
      00005FDFFF00000000005F3F3F00A4A0A0005F3F3F003F9FBF003FBFFF003FBF
      FF003F9FDF003F7FBF005F3F3F0000000000000000005AC87B006CCE89006ECF
      8B006BCE89009FDFB2000000000071D08D004EC4710047C26C0041C067004AC3
      6E000000000098DDAD00359F550000000000000000005A5AC8006C6CCE006E6E
      CF006B6BCE009F9FDF00000000007171D0004E4EC4004747C2004141C0004A4A
      C300000000009898DD0035359F00000000000000000000000000000000005F5F
      5F00000000005F5F5F00000000005F5F5F00000000005FDFFF00A4A0A0000000
      00003F9FDF003F9FDF00000000005F5F5F000000000000000000000000005F5F
      5F00000000005F5F5F00000000005F5F5F00000000005FDFFF00A4A0A0000000
      00003F9FDF003F9FDF00000000005F5F5F000000000063CB820075D1900072D0
      8E006CCE890089D8A1000000000062CB81004DC470005DC97D0075D1900040BF
      6600ADE4BD00C5ECD10036A2560000000000000000006363CB007575D1007272
      D0006C6CCE008989D800000000006262CB004D4DC4005D5DC9007575D1004040
      BF00ADADE400C5C5EC003636A200000000000000000000000000000000000000
      0000000000000000000000000000000000005FDFFF005FDFFF0000000000F0FB
      FF00000000003FBFFF003F9FDF00000000000000000000000000000000000000
      0000000000000000000000000000000000005FDFFF005FDFFF0000000000F0FB
      FF00000000003FBFFF003F9FDF00000000000000000066CC84007DD497007AD3
      95006FCF8C008FDAA500000000006CCE890050C57300AAE3BB00F9FDFA0051C5
      7400BCE9C900BDE9CA0037A4570000000000000000006666CC007D7DD4007A7A
      D3006F6FCF008F8FDA00000000006C6CCE005050C500AAAAE300F9F9FD005151
      C500BCBCE900BDBDE9003737A400000000000000000000000000000000000000
      00000000000000000000000000005F5F5F00000000009FFFFF00A4A0A0000000
      0000A4A0A0003FBFFF00000000005F5F5F000000000000000000000000000000
      00000000000000000000000000005F5F5F00000000009FFFFF00A4A0A0000000
      0000A4A0A0003FBFFF00000000005F5F5F000000000071D08D0089D8A10080D5
      990075D1900077D29200E7F7EC00C8EDD3005AC87B0054C6760065CC840078D2
      9300FEFFFE0083D69C0036A2560000000000000000007171D0008989D8008080
      D5007575D1007777D200E7E7F700C8C8ED005A5AC8005454C6006565CC007878
      D200FEFEFF008383D6003636A200000000000000000000000000000000000000
      0000000000000000000000000000000000005F3F3F003F9FBF009FFFFF005FDF
      FF005FDFFF003F9FBF005F3F3F00000000000000000000000000000000000000
      0000000000000000000000000000000000005F3F3F003F9FBF009FFFFF005FDF
      FF005FDFFF003F9FBF005F3F3F0000000000000000007DD4970099DDAD008FDA
      A50080D5990078D293009EDFB100F9FDFA00E1F5E700A2E0B400B7E7C5000000
      0000BAE8C80048C26C00359E540000000000000000007D7DD4009999DD008F8F
      DA008080D5007878D2009E9EDF00F9F9FD00E1E1F500A2A2E000B7B7E7000000
      0000BABAE8004848C20035359E00000000000000000000000000000000000000
      0000000000000000000000000000000000005F3F3F005F3F3F00000000005FDF
      FF00000000005F3F3F005F3F3F00000000000000000000000000000000000000
      0000000000000000000000000000000000005F3F3F005F3F3F00000000005FDF
      FF00000000005F3F3F005F3F3F00000000000000000087D79F00A8E2B90099DD
      AD0089D8A10080D599007BD3950095DCAA00C9EDD400EDF9F100D8F2E0009FDF
      B2005AC87B0042C068003399510000000000000000008787D700A8A8E2009999
      DD008989D8008080D5007B7BD3009595DC00C9C9ED00EDEDF900D8D8F2009F9F
      DF005A5AC8004242C00033339900000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005F5F5F000000
      00005F5F5F000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005F5F5F000000
      00005F5F5F0000000000000000000000000000000000BAE8C80086D79E0078D2
      93006ECF8B0068CD860063CB82005FCA7F005FCA7F005CC97C0054C6760051C5
      740048C26C003DB661008CD9A3000000000000000000BABAE8008686D7007878
      D2006E6ECF006868CD006363CB005F5FCA005F5FCA005C5CC9005454C6005151
      C5004848C2003D3DB6008C8CD900000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003F3F7F00000080000000
      8000000080000000800000008000000080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005F5F
      5F00000000005F5F5F000000000000000000000000003F3F7F003F3FBF003F3F
      9F003F3F9F003F3F9F003F3F9F000000800000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005F3F3F005F3F3F000000
      00003F9FDF00000000005F3F3F005F3F3F00000000003F3F7F003F3FBF003F3F
      DF003F3FDF003F3FDF003F3F9F000000800000000000FFFFFF00FF000000FF00
      0000FF000000FFFFFF00FF000000FF000000FF000000FFFFFF00FF000000FF00
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      00004D4D4D004D4D4D004D4D4D004D4D4D004D4D4D0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000086BF8A000000000000000000000000000000
      000000000000000000000000000000000000000000005F3F3F003F9FBF003FBF
      FF003FBFFF003F9FDF003F7FBF005F3F3F00000000003F3F7F003F3FBF003F3F
      DF003F3FDF003F3FDF003F3F9F000000800000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000004D4D4D004D4D4D00A6A6A6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000076BB7C000000000000000000000000000000
      0000000000000000000000000000000000005F5F5F00000000005FDFFF00A4A0
      A000000000003F9FDF003F9FDF00000000005F5F5F003F3F7F003F3FBF003F3F
      DF003F3FDF003F3FDF003F3F9F000000800000000000FFFFFF00FF000000FF00
      0000FF000000FFFFFF00FF000000FF000000FF000000FFFFFF00FF000000FF00
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      000000000000A6A6A6004D4D4D004D4D4D000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000086BF8A0045AE490086BF8A0000000000000000000000
      000000000000000000000000000000000000000000005FDFFF005FDFFF000000
      0000F0FBFF00000000003FBFFF003F9FDF00000000003F3F7F003F3FBF003F3F
      BF003F3FBF003F3FBF003F3F9F000000800000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000004D4D4D004D4D4D00A6A6A60000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000060B566003DAA410060B5660000000000000000000000
      0000000000000000000000000000000000005F5F5F00000000009FFFFF00A4A0
      A00000000000A4A0A0003FBFFF00000000005F5F5F003F3F7F005F5F5F000000
      00005F5F5F003F3F7F003F3F7F000000800000000000FFFFFF00FF000000FF00
      0000FF000000FFFFFF00FF000000FF000000FF0000000000000000FF000000FF
      000000FF00000000000000000000000000000000000000000000000000000000
      00000000000000000000A6A6A6004D4D4D004D4D4D0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000086BF8A003DAA41003DAA41003DAA410086BF8A00000000000000
      000000000000000000000000000000000000000000005F3F3F003F9FBF009FFF
      FF005FDFFF005FDFFF003F9FBF005F5F5F00A4A0A0005F3F3F00000000003F9F
      DF00000000005F3F3F005F3F3F000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000FF000000FF
      000000FF00000000000000000000000000000000000000000000000000000000
      00000000000000000000A6A6A6004D4D4D004D4D4D0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000060B566003DAA41003DAA41003DAA410060B56600000000000000
      000000000000000000000000000000000000000000005F3F3F005F3F3F000000
      00005FDFFF00000000005F3F3F00A4A0A0005F3F3F003F9FBF003FBFFF003FBF
      FF003F9FDF003F7FBF005F3F3F000000000000000000FFFFFF00FF000000FF00
      0000FF000000FFFFFF000000000000000000000000000000000000FF000000FF
      000000FF00000000000000000000000000000000000000000000000000000000
      00000000000000000000000000004D4D4D004D4D4D00A6A6A600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000086BF8A003DAC42003DAA41003DAA41003DAA41003DAC420086BF8A000000
      0000000000000000000000000000000000000000000000000000000000005F5F
      5F00000000005F5F5F00000000005F5F5F00000000005FDFFF00A4A0A0000000
      00003F9FDF003F9FDF00000000005F5F5F0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF00000000000000000000000000000000
      0000000000000000000000000000A6A6A6004D4D4D004D4D4D00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000060B566003DAA41003DAA41003DAA41003DAA41003DAA410060B566000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005FDFFF005FDFFF0000000000F0FB
      FF00000000003FBFFF003F9FDF00000000000000000000000000000000000000
      000000000000000000000000000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF00000000000000000000000000000000
      0000000000000000000000000000000000004D4D4D004D4D4D00A6A6A6000000
      00000000000000000000000000000000000000000000000000000000000086BF
      8A0044AD48003DAA41003DAA41003DAA41003DAA41003DAA410044AD480086BF
      8A00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000005F5F5F00000000009FFFFF00A4A0A0000000
      0000A4A0A0003FBFFF00000000005F5F5F0000000000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF00000000000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF00000000000000000000000000000000
      00000000000000000000000000004D4D4D004D4D4D004D4D4D004D4D4D004D4D
      4D000000000000000000000000000000000000000000000000000000000060B5
      66003DAA41003DAA41003DAA41003DAA41003DAA41003DAA41003DAA410060B5
      6600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005F3F3F003F9FBF009FFFFF005FDF
      FF005FDFFF003F9FBF005F3F3F00000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FF000000FF
      000000FF00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005F3F3F005F3F3F00000000005FDF
      FF00000000005F3F3F005F3F3F00000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FF000000FF
      000000FF00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005F5F5F000000
      00005F5F5F000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FF000000FF
      000000FF00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00635F63000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000F2F2
      F200FFFFFF00F2F2F200FFFFFF00F2F2F200FFFFFF00F2F2F200FFFFFF00F2F2
      F200FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF006D6A6D00ECE5
      EC00CDBABF00CDBABF00CDBABF00CDBABF00CDBABF00CDBABF00CDBABF00C1BC
      C10000000000FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000840084008400840084848400000000000000
      0000000000000000000000000000000000000000000000000000F2F2F2000D0D
      0D00000000000D0D0D00000000000D0D0D00000000000D0D0D00000000000D0D
      0D008E8E8E00FFFFFF00000000000000000000000000F2F2F200EDB3CB00EDB3
      CB00D39DC000EDB3CB00D7A4A900AA818D00B48F9B00C997AC00EDB3CB00C997
      AC00EDB3CB00EDB3CB0000000000FFFFFF00FFFFFF00FFFFFF0062656200FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0012121D00144F8F00373B3D00CFCFD300C1BC
      C10000000000FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000008400840084008400FFFFFF00FFFFFF00C6C6C600848484000000
      00000000000000000000000000000000000000000000F2F2F2000D0D0D00A57B
      7B00B2888800A77E7F00B78E9000AC858600BD969600B18C8C00C09A9B00B690
      91000D0D0D008E8E8E00FFFFFF000000000000000000FFFFFF00EDB3CB00D39D
      C000AA818D00D39DC000EDB3CB00D39DC000C997AC00EDB3CB00C997AC00B48F
      9B00C997AC00EDB3CB0000000000FFFFFF00FFFFFF00FFFFFF006E636E00FFFA
      FF00748674007486740074867400144F8F0092B7FF00007AFF00071D2500B9AE
      B30000000000FFFFFF00FFFFFF00FFFFFF000000000000000000000000008400
      840084008400FFFFFF00FFFFFF000000000000000000C6C6C600C6C6C6008484
      840000000000000000000000000000000000000000000D0D0D00E5B6B7000D0D
      0D00BF959600CEA5A600C39B9C00D3ABAC00C9A1A200D8B1B200CEA8A900DEB8
      B800D2ADAE000D0D0D00F2F2F2000000000000000000F2F2F200FFFFFF00F2F2
      F200FFFFFF00F2F2F200FFFFFF00F2F2F200FFFFFF00F2F2F200FFFFFF00F2F2
      F200FFFFFF00F2F2F20000000000FFFFFF00FFFFFF00FFFFFF0062656200FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000B3D0081FAFF0000B7FF000083F500080D
      180000000000FFFFFF00FFFFFF00FFFFFF00848484008400840084008400FFFF
      FF00FFFFFF000000000000000000840084008400840000000000C6C6C600C6C6
      C600848484000000000000000000000000000000000000000000B28888000000
      0000DBB1B200D1A7A800C0989900B58C8D00C59D9E00BB939500CBA3A400C09A
      9B00F0C9CA0000000000FFFFFF00000000000000000000000000000000000D0D
      0D00000000000D0D0D00000000000D0D0D00000000000D0D0D00000000000D0D
      0D0000000000000000000000000000000000FFFFFF00FFFFFF006D6A6D00FFFA
      FF007486740074867400748674009E968D000000450077F4FF0000C4F900007F
      FF0019090C00FFFFFF00FFFFFF00FFFFFF008484840084008400FFFFFF000000
      000000000000840084008400840084008400840084008400840000000000C6C6
      C600C6C6C600848484000000000000000000000000000D0D0D00E5B6B7000D0D
      0D00CBA0A000DBB0B100D0A6A600DFB6B700D5ACAD00E4BDBD00DAB2B200E9C3
      C300DFB8B9000D0D0D00F2F2F200000000000000000000000000B48F9B00EDB3
      CB00EDB3CB00EDB3CB00EDB3CB00EDB3CB00EDB3CB00EDB3CB00EDB3CB00EDB3
      CB00B48F9B0000000000FFFFFF0000000000FFFFFF00FFFFFF00635F6300FFFF
      FF00ECE5EC00ECE5EC00D2E9E200EAE3F700F3DBDB0000013E0081FFFF0000BC
      FF00007AF500000E2A00FFFFFF00FFFFFF008484840000000000000000008400
      840084008400840084000084840000FFFF008400840084008400840084000000
      0000C6C6C600C6C6C600848484000000000000000000000000000D0D0D000000
      0000D4AAAA00CA9FA000BA909000B0878700BF979700B58C8C00C49B9C00BA92
      9300E9C2C20000000000FFFFFF00000000000000000000000000F2F2F2000D0D
      0D00000000000D0D0D00000000000D0D0D00000000000D0D0D00000000000D0D
      0D00EDB3CB0000000000FFFFFF0000000000FFFFFF00FFFFFF006D6A6D00FFFA
      FF00827975005F2A4200443B3E001E4C2B00462E46009E968D0000013E0077FA
      FF000F0E0F00C9D1D60016131600FFFFFF008484840084008400840084008400
      8400840084008400840084008400008484008400840084008400840084008400
      840000000000C6C6C60000000000000000000000000000000000F2F2F2000D0D
      0D00C4999900D3A9AA00C99E9F00D9AFAF00CFA5A600DEB4B500D4ABAB00E3BB
      BC00D8B1B1000D0D0D00F2F2F200000000000000000000000000FFFFFF000000
      0000C1CEB000B7C4A500C1CEB000AFBE9B00A6B88F0086A1690086A169000000
      0000EDB3CB0000000000FFFFFF0000000000FFFFFF00FFFFFF0062656200FFFA
      FF00ECE5EC00ECE5EC00EBEBEB00E1E1E100F6EFF600E1E1E100F6D6D3001A12
      0D00FFF5FF00001D19003931FF0000008E000000000084008400FFFFFF008400
      84008400840084008400840084008400840000FFFF0000FFFF00840084008400
      84008400840000000000000000000000000000000000F2F2F2000D0D0D000000
      0000CEA1A300C3989800B4898800AA7F8000BA8F9000AF858600BE959500B38B
      8C00E3BABB0000000000FFFFFF00000000000000000000000000F2F2F2000D0D
      0D00CBD4BE00DCE3D100CBD4BE00CBD4BE00A6B88F00A6B88F0086A169000D0D
      0D00EDB3CB0000000000FFFFFF0000000000FFFFFF00FFFFFF006D6A6D00FFFF
      FF00767A7600443B3E00373B3D0080848000E1E1E100F6EFF600EBEBEB00D4B9
      B9001A121A0092B7FF00777FFF0012008300000000000000000084008400FFFF
      FF0084008400840084008400840084008400840084000084840000FFFF0000FF
      FF0084008400840084000000000000000000000000000D0D0D00E5B6B7000D0D
      0D00BF919100CEA1A200C3989800D2A8A800C89E9E00D8ADAE00CDA4A400DDB3
      B300D2AAA9000D0D0D00F2F2F200000000000000000000000000FFFFFF000000
      0000E8E9E700E3E5E200E8E9E700CBD4BE00C1CEB000A6B88F0096B281000000
      0000EDB3CB0000000000FFFFFF0000000000FFFFFF00FFFFFF00635F6300FFFF
      FF00ECE5EC00EBEBEB00EBEBEB00E1E1E100EBEBEB00ECE5EC00ECE5EC00C1BC
      C100554A4E001200830012008300FFFFFF000000000000000000000000008400
      8400FFFFFF00840084008400840084008400008484008400840000FFFF0000FF
      FF00840084008400840084008400000000000000000000000000B28888000000
      0000D4A7A700CA9D9E00BA8E8D00B0848400BF949500B58B8B00C49A9A00BA90
      9000DDB3B30000000000FFFFFF00000000000000000000000000F2F2F2000D0D
      0D00E3E5E200FFFFFF00E3E5E200DCE3D100B7C4A500AFBE9B0086A169000D0D
      0D00EDB3CB0000000000FFFFFF0000000000FFFFFF00FFFFFF006E636E00FFFF
      FF00FFFFFF00FFFAFF00FFFAFF00FFFAFF00FFFFFF00FFFAFF00FFFFFF00ECE5
      EC0000000000FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      000084008400FFFFFF00840084008400840000FFFF0000FFFF0000FFFF008400
      840084008400840084000000000000000000000000000D0D0D00E5B6B7000D0D
      0D00E5B6B700F4C6C600E9BCBC00F8CCCC00EDC2C200FDD2D200F2C8C800FFD8
      D800CDA2A2000D0D0D00F2F2F200000000000000000000000000FFFFFF000000
      0000E8E9E700E3E5E200E8E9E700CBD4BE00C1CEB00096B2810096B281000000
      0000EDB3CB0000000000FFFFFF0000000000FFFFFF00FFFFFF006D6A6D00635F
      63006E636E006E636E006D6A6D006E636E006E636E00635F63006D6A6D006265
      62006D6A6D00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      00000000000084008400FFFFFF00840084008400840084008400840084008400
      84000000000000000000000000000000000000000000000000000D0D0D00B587
      8700C4959600B88B8B00C7999A00BD8F8F00CC9F9F00C2959500D0A5A600C79C
      9C000D0D0D008E8E8E00FFFFFF00000000000000000000000000F2F2F2000D0D
      0D00000000000D0D0D00000000000D0D0D00000000000D0D0D00000000000D0D
      0D00EDB3CB0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000084008400FFFFFF008400840084008400000000000000
      0000000000000000000000000000000000000000000000000000000000000D0D
      0D00000000000D0D0D00000000000D0D0D00000000000D0D0D00000000000D0D
      0D008E8E8E00FFFFFF0000000000000000000000000000000000B4B5B700F2F2
      F200FFFFFF00F2F2F200FFFFFF00F2F2F200FFFFFF00F2F2F200FFFFFF00F2F2
      F200B4B5B70000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000840084008400840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000020202000606060006060600060606000606060006060
      6000606060006060600060606000303030000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00003F0000007F00000080404000BFFFFF00BFFFFF00BFFFFF00BFFFFF00BFFF
      FF00BFFFFF00BFFFFF00BFFFFF00606060000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000635F63000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000202020009F20
      2000DF000000DF0000009F404000BFFFFF00FF7F7F00BF7F7F00FF7F7F00BF7F
      7F00FF7F7F00BF7F7F00BFFFFF00606060000000000000000000000000000000
      0000000000000000000000FFFF00FFFFFF000080800000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000808080008080800080808000000000000000
      00000000000000000000000000000000000000000000000000006D6A6D00ECE5
      EC00CDBABF00CDBABF00CDBABF00CDBABF00CDBABF00CDBABF00CDBABF00C1BC
      C100000000000000000000000000000000000000000020202000BF402000FF00
      0000FF000000FF00000080804000BFFFFF00FF7F7F00BF7F7F00FF7F7F00BF7F
      7F00FF7F7F00BF7F7F00BFFFFF0060606000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C00000808000008080000080800000000000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000808080008080
      8000000000000000000000000000000000000000000000000000626562000000
      000000000000000000000000000012121D00144F8F00373B3D00CFCFD300C1BC
      C100000000000000000000000000000000000000000060804000FF000000FF00
      0000FF000000FF0000007F604000BFFFFF00FF7F7F00BF7F7F00FF7F7F00BF7F
      7F00FF7F7F00BF7F7F00BFFFFF00606060000000000000000000000000000000
      0000000000000000000000000000C0C0C0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      00000000000000000000000000000000000000000000000000006E636E00FFFA
      FF00748674007486740074867400144F8F0092B7FF00007AFF00071D2500B9AE
      B30000000000000000000000000000000000404040003F600000BF400000FF00
      0000FF000000FF00000040804000BFFFFF00FF7F7F00BF7F7F00FF7F7F00BF7F
      7F00FF7F7F00BF7F7F00BFFFFF00606060000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000008080800000000000000000000000000000000000626562000000
      0000000000000000000000000000000B3D0081FAFF0000B7FF000083F500080D
      18000000000000000000000000000000000090907000A0A06000EF303000EF30
      30009F8060004080000040804000BFFFFF00FF7F7F00BF7F7F00FF7F7F00BF7F
      7F00FF7F7F00BF7F7F00BFFFFF00606060000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF000000000000000000000000000000000000000000FFFFFF00FFFF
      FF000000000080808000000000000000000000000000000000006D6A6D00FFFA
      FF007486740074867400748674009E968D000000450077F4FF0000C4F900007F
      FF0019090C0000000000000000000000000090907000A0A06000FF3F3F009FBF
      9F003F9F3F000080000040804000BFFFFF00FF7F7F00BF7F7F00FF7F7F00BF7F
      7F00FF7F7F00BF7F7F00BFFFFF006060600080808000C0C0C000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00C0C0C0008080800000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000008080800000000000000000000000000000000000635F63000000
      0000ECE5EC00ECE5EC00D2E9E200EAE3F700F3DBDB0000013E0081FFFF0000BC
      FF00007AF500000E2A00000000000000000090907000DFDFBF0000000000AF8F
      6F003F9F3F000080000040804000BFFFFF00BFFFFF00BFFFFF00BFFFFF00BFFF
      FF00DFDFDF003F7F7F007F7F7F003030300080808000C0C0C000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000C0C0C000808080008080800000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000080808000000000000000000000000000000000006D6A6D00FFFA
      FF00827975005F2A4200443B3E001E4C2B00462E46009E968D0000013E0077FA
      FF000F0E0F00C9D1D600161316000000000080804000EFEFEF0000000000EFAF
      AF007F7F3F000080000040804000BFFFFF00BFFFFF00FFBFBF00BF7F7F00BFBF
      BF00BFBFBF00EFEFEF00707070000000000080808000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000808080008080800000000000000000000000000000000000FFFF
      FF00FFFFFF008080800080808000808080008080800080808000FFFFFF00FFFF
      FF0000000000808080000000000000000000000000000000000062656200FFFA
      FF00ECE5EC00ECE5EC00EBEBEB00E1E1E100F6EFF600E1E1E100F6D6D3001A12
      0D00FFF5FF00001D19003931FF0000008E007F7F7F00CFCFAF00000000003090
      30009F400000CF303000BF404000BFFFFF00BFFFFF00BFFFFF00BFFFFF00BFFF
      FF00BFBFBF0070707000400000000000000080808000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C0000080
      000000FF00008080800080808000000000000000000000000000000000008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00808080008080
      80000000000080808000000000000000000000000000000000006D6A6D000000
      0000767A7600443B3E00373B3D0080848000E1E1E100F6EFF600EBEBEB00D4B9
      B9001A121A0092B7FF00777FFF001200830020202000BFBFBF0000000000EFEF
      EF003F9F3F003F6000007F6040007FBFBF00BFBFBF007FBFBF00BFBFBF007FBF
      BF009F9F9F0040200000002000000000000080808000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00808080008080800000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000635F63000000
      0000ECE5EC00EBEBEB00EBEBEB00E1E1E100EBEBEB00ECE5EC00ECE5EC00C1BC
      C100554A4E00120083001200830000000000000000007F7F7F00AFAF6F00AF8F
      6F007F7F3F000080000000800000008000000080000000800000008000000080
      0000206000004020000000000000000000000000000080808000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C00080808000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      00000000000000000000000000000000000000000000000000006E636E000000
      000000000000FFFAFF00FFFAFF00FFFAFF0000000000FFFAFF0000000000ECE5
      EC000000000000000000000000000000000000000000000000007F7F7F00EFEF
      EF00BFDFBF009F7F3F009F200000008000000080000000800000008000000080
      0000204000000000000000000000000000000000000000000000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000006D6A6D00635F
      63006E636E006E636E006D6A6D006E636E006E636E00635F63006D6A6D006265
      62006D6A6D000000000000000000000000000000000000000000000000007F7F
      7F00BFBF9F008FAF6F00FF3F3F0000800000208000003F600000008000004080
      4000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000202020007F7F7F00808040009F9F7F009F60400080804000404040000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000B3B6B500887E7400000000000000
      000000000000000000000000000000000000000000000000000000000000D6DD
      DA00A7AEC6007175B100262786000F0F7F000F0F7B00262780007175A500A7AE
      BA00D6DDDB000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000091696400846C7700D5E0
      E3000000000000000000000000000000000000000000AD8879008C4736008C4C
      3D008C4D3D008C4D3D008A4D3D00874D3D00884D3D008C4D3D008C4D3D008C4D
      3D008C4D3D008A4E3E0090372100000000000000000000000000000000000000
      0000000000000000000000000000ACB1B200B5936F00A98A6E00BDC6CD000000
      0000000000000000000000000000000000000000000000000000D5DDD9005C61
      AB0003048900000081000000890000008C0000008B0000008800000077000404
      72005C618E00D5DDDA0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D6B99B00A1634600816C7700D4E2
      E500000000000000000000000000000000006CB5C30008A0C500029DC700049E
      C700049EC700049DC80003AAD40004AFDB0000A6D300049FCB00049EC700049E
      C700049DC70001A9D500198CAF006F322F000000000000000000000000000000
      00000000000000000000A8AFB100A78F7500EABA8C00A68F7600B1BDC4000000
      00000000000000000000000000000000000000000000D4DDD8005A5EB5000000
      8B0000008A00000096000000A0000000A00000009D0000009500000088000000
      8200000076005A5E9400D5DEDA00000000000000000000000000000000000000
      000000000000B4AFB700A799A100B9A19200E7BB9000AE7C64005F3C4D00BCC0
      C800D0DCDF000000000000000000000000000FB6DA0000C5F30000D1FD0000CF
      FC0000CFFC0000DAFE0002A7C400465066005BAFCD0000D5FE0000D0FC0000CF
      FC0000D1FC0000D4FF0000D1FC00623D4F000000000000000000000000000000
      00009D9F9B0090837400A1897000C9A78400F0C49F00BA97770082705F009293
      910000000000000000000000000000000000D6DED9006064B900000095000504
      A0007575CB002727B2000000A7000000AE000000AC000000A1006868C5003939
      AB000000860000007A005B5E8100D6DDDB00000000000000000000000000B2A3
      9E00967266009D766400AA806700C69F8000EEC69F00D0A98700845647006536
      3A008C78820000000000000000000000000036C4E40000C7F20006E1FF0000D6
      FF0000D5FF0000EDFF00035C6500360000006439460007DDF50000DAFF0000D5
      FF0009E3FF0005E8FF0010A9C600914452000000000000000000BFBCB800B599
      8200BD9E8400D6B29100E5C09E00F0C8A100FFDBB600E9C39F00CEA88300A885
      66007E706500A4ACAF000000000000000000AEB4D3000506A6000000A4006A6A
      A700FFFFFB00C9CAF4001D1DB8000000AF000000AE006F6EC200FFFFFF00E2E2
      EB001A1B9B000000940002026A00A7ADAF000000000000000000B7A39800B490
      7300CDAD8F00E3BE9B00ECC6A000F5D3AC00FFDDB500F4CDA700E3BD9900C59F
      7E008857470075525A0000000000000000009ACDD90002C1EB0007DFFC0006E1
      FF0000D7FF0000E3FF0003B9CF0003636600088E9E0000E3FE0000D9FF0003DA
      FF0016F2FF0000E2FF00495E6300C7A1970000000000C0BFBB00BEA48D00E1C1
      A400F9D9B900FFE5C500E1B49100A94F2700CB7E5300FAD5B100FAD8B300EEC5
      A300C9A3830088735E00A2AAAD00000000007277C1000000A6000000BC002E2E
      A200BEBEB100FFFFFF00C6C6F1001E1DBF006B6BD100FEFEF900FFFEEF008483
      B5000C0DAE000000A100000085006F73940000000000BAA99E00C2A48C00E6CB
      AE00FEE1C200FBDAB900C07A5100C56E3B00C8734100D6946900FEDAB800F7D3
      B100DFBD9A009D6E58007451580000000000D3D4D7003BC8E40000D1F4000BEA
      FF0001DDFF0000DBFF0000EEFF000AC9DD0001E4FA0000DEFF0000DAFF0011ED
      FF000EF6FF0010ADC40099574A00D3D9DB00C6C7C500BEA89300E7CBB400FFE5
      CB00FFE5CB00FFEDD000D2A98C0064130000853C1700FAD9B500FFE4C200FFE0
      BC00F2CEAE00CCA787007C6F6200BFC7CC004546C4000000B5000002C9000000
      D4001E1EA400C3C2B700FFFFFD00DFDEF900F9F9FF00FFFFF2007E7EB3000000
      B8000000BF000000AF000000A100292B7200C6BDB700C4A89200EDD2BC00FFE7
      CC00FFE6CB00FFECD000D8B394008E2400009A390E00F1CFAF00FFE6C600FFE2
      C000FBD8B800E0C1A100885949008B768100D6D3D6009CCED70006CAE90005E3
      FB0006E8FF0000E0FF0000EDFE002956670009C2D70000EAFF0003E2FF0014F6
      FF0000E7FE00515C5C00C9A1970000000000C2B5A700DFC7B500FFE9D200FFE9
      D000FFE7CF00FFE6CF00FFE7CE00DBB49900EBCBAF00FFE6CD00FFE2C600FFE2
      C500FFE2C300F2CFB000AB9079009B9F9F001C1DC6000505C8000607D4000204
      D8000000D8002627B500E7E7E600FFFFFF00FFFFFF008F8FD7000000C0000000
      CB000003C7000001BA000000B50012156A00C7B0A500E5CBB600FFEAD400FFEA
      D400FFE8D100FFEFD700DEBDA2008C260000A4471D00FCDFC300FFE6CA00FFE3
      C600FFE4C800F9D9BD00CDAE930072474F0000000000D3D0D30043CADE0000D3
      F10008EEFF0000ECFF0000E3EC004D303A001BB1BE0000F2FF000AF0FF0007F7
      FF0011ABBC009B574A000000000000000000D1BFB200F4E0CF00FFEFDC00FFEB
      D800FFEBD500FFEBD600FFF2DC00D2936D00E7BD9E00FFF4DF00FFE5CC00FFE5
      CD00FFE6CC00FDE1C600D1B8A1008B857A001D1FCE00090BD8000A0DE7000404
      E3000000D7006565D400F3F4F700FFFFFF00FFFFFF00BBBBF2001A1AD1000000
      CD000406D2000205C5000003BE00161A7300D4C3B700F5E1D200FFEFDB00FFEC
      D900FFEBD600FFF3DF00DDBFA7008D270000A3471F00FBDFC600FFE9D100FFE6
      CD00FFE6CC00FFE5CC00E8CCB100936F6A0000000000D3D0D300A2CFD60006CF
      E70004EDFB0003FCFF0000C2CD00651A250036929E0000FCFF0012FAFF0000F1
      FD00535B5900C9A49E00C8D1D50000000000D7CBBE00F7E9DC00FFF1E200FFEF
      DE00FFEFDE00FFEEDF00FFFCEC00D1A58700B35C3000FFDEC200FFF0DB00FFE9
      D100FFE9D100FFE7D000E4CCB7009A9285004C4DDB000D0FE6001516F8000000
      F6006B6BDB00FEFEF400FFFFEF009899C600C8C9CC00FFFFFE00CBCCF9002020
      DE000001DD000809CE000101C0003A3D8C00D7CCC000F6E8DB00FFF2E400FFEF
      DF00FFEFDD00FFFAEB00E3C8B40090260000A4471E00FBE2CC00FFEDD900FFE9
      D100FFE9D200FFEAD300ECD6C000937273000000000000000000D5D0D30048D2
      DF0000E1F70006FFFF00068A91003E0002002E4C620008FAFC000AFFFF0015B2
      B800A0574F00000000000000000000000000DCD0C500FAEFE700FFF5EB00FFF3
      E600FFF4E700FFF1E100FEE9D300F8EBDB0097472200AF4D2400E8CCB600FFF4
      E200FFECDA00FFEED900E5CEBB009B9286007A80DB001112F400191BFE006D6E
      D500FFFFF200FFFEE9007978BB000000E0002121BA00BEBCAD00FFFFFD00D0D0
      EF001918E4000609DC000000BA007C83AE00DCD1C700F7EEE300FFF6EB00FFF3
      E700FFF6EC00F1E4D600A766460082190000903C1900FAE6D500FFF2E000FFED
      D900FFECD800FFF2DE00EFD8C500A28384000000000000000000C9D0D400A6D0
      D60006D9ED0004FFFF00036260002A0000003618230011EBF10000FDFF00515D
      5B00CAA6A200C8D1D5000000000000000000D9D3D100F8EDE800FFFCF800FFF7
      EF00FFFFFB00CBA38C009A3A1B00E1C4AF00AE795F00750E0000C3907400FFFC
      F000FFF5E600FDF1E200D6BFAD00B2AAA500AFB6DA002A2AF8002F2FFE00696A
      CB00D6D6BB007776BD000000EB000000F1000000F6001819BB00B3B49D009899
      BD001F20F0000D10E8001B1DB600B7BFC100DCD2CB00F4EDE800FFFCF700FFF6
      EF00FFF8F300F3E5D900CDB2A200CEB7AC00CCB5A600FDEFE300FFF3E600FFF0
      E100FFF3E600FFF9EB00E5C9B900B7A8A800000000000000000000000000D5CF
      D3004CD6E40000F4FE0008A7A6000A1E1F001E686B0009FDFF000FBABC009A5E
      5700C9D1D50000000000000000000000000000000000EDE3E000FEFCFA00FFFD
      FA00FFFFFF00C5A4900075170000DCB7A5008E4B31006E100000D5B39F00FFFF
      FB00FFFDF700F5E5DC00C7B6A800CBCCCB00D5DCD5007175EA003232FF005355
      FE006465CC003F40F1002122FF001012FC001315FE001D1FFF00383ACE002E30
      E8002325FF001415E3008185BA00D5DDD700D9D9D900EDE6E200FCFAF900FFFE
      FA00FFFBF500FFFEFA00F6ECE400E3AE8E00F9D5BD00FFF8F100FFF6EB00FFF7
      EF00FFFDFA00FAEEE100D1B9AF0000000000000000000000000000000000C9D0
      D400A9D2D70006E2EE0004FDFF0002EDEF0010FDFD0000FCFE0050615A00C9A9
      A700C8D1D50000000000000000000000000000000000D6D1CE00F0ECEA00FFFE
      FF00FFFFFF00FCFAF800CBA38D00C17C5E009B634B00C3A89900FFFFFC00FFFF
      FF00F9EFE700DBCFC500000000000000000000000000D1D9D1007175EC00484A
      FF007C7CFF009392FF008686FF006F70FF006867FF006767FF005657FF003C3D
      FF001F20FB006E72D400D2DBD2000000000000000000E4E0DE00F3ECEA00FDFD
      FD00FFFFFD00FFFFFF00BB9B8D00790D00009F4C2400F8F2EC00FFFEFC00FFFF
      FF00FAF4F100E4D5CA00C6C5C500000000000000000000000000000000000000
      0000D5CFD3004FDBE00000EFF60024FFFF0010FFFF0010B8B9009C625B00C9D1
      D500000000000000000000000000000000000000000000000000D6D2D000F1ED
      EC00FDFBFC00FFFFFF00FFFFFF00FEFCFB00FFFFFF00FFFFFF00FFFDFC00F7EE
      E900DBD6CE00C6C8C80000000000000000000000000000000000D0D8D100767A
      EB005657FE007B7AFF00A2A2FF00AAA8FF009292FF006C6DFF004645FF003D3D
      F6008489DC000000000000000000000000000000000000000000E0E0E000F3F0
      F100FCF9FC00FEFEFF00E9E2DE00966F6700BEA69B00FEFFFF00FEFBFB00FCF4
      F100E3DBD7000000000000000000000000000000000000000000000000000000
      000000000000ADD3D70002EDF10022FFFF0000FCFE0055675F00CAADAC000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DDDEDE00EEE8E700F8F7F900F8F7F900F8F7F900F7F1F000E9E6E500D2D1
      CF00000000000000000000000000000000000000000000000000000000000000
      0000AEB6DE009095EA008282FA007271FF006A69FF007476FA00888EE900B9C0
      DB00D6DDD400000000000000000000000000000000000000000000000000D6D9
      DC00EDEAEB00F6F3F200F9F9FA00FEFFFF00FCFFFF00FAF4F200F9F1F000DEDE
      DF00000000000000000000000000000000000000000000000000000000000000
      000000000000D4D1D50087E0E3000FFFFF004BC3B400C3A19700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F2F2F200FFFFFF00F2F2F200FFFFFF00F2F2F200FFFFFF00F2F2F2000000
      0000000000000000000000000000000000000000000000000000000000001B1B
      1B00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000840000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000F2F2
      F2000D0D0D00000000000D0D0D00000000000D0D0D00000000000D0D0D00F2F2
      F200000000000000000000000000000000000000000000000000000000002828
      28001B1B1B000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084000000000000000000000084000000840000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D0E0D00070A87000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F2F2F2000D0D
      0D000000A1000D0DAE000000A1000D0DAE000000A1000D0DAE000000A1000D0D
      0D00F2F2F20000000000000000000000000000000000F2F2F200FFFFFF001B1B
      1B0053D3D2001B1B1B0028282800F2F2F200FFFFFF00F2F2F200FFFFFF00F2F2
      F200FFFFFF00F2F2F200FFFFFF00000000000000000000000000000000000000
      0000000000000000000084000000000000008400000000000000000000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000D0E0D000187810000870000060A060000000000000000000000000000000
      00000000000000000000000000000000000000000000F2F2F2000D0D0D000000
      A1000D0DAE005F5FB1006C6CBE005F5FB1006C6CBE005F5FB1000D0DAE000000
      A1000D0D0D00F2F2F2000000000000000000000000000D0D0D00000000002828
      280046C6C500614EBE001B1B1B000D0D0D00000000000D0D0D00000000000D0D
      0D00000000000D0D0D00F2F2F200000000000000000000000000000000000000
      0000000000000000000084000000000000008400000000000000000000008400
      000000000000000000000000000000000000000000000000000000000000D0E0
      D0001878100008700000087800000870000068A8600000000000000000000000
      000000000000000000000000000000000000000000000D0D0D000000A1000D0D
      AE004646B9005353C6000000A1000D0DAE000000A1005353C6004646B9000D0D
      AE000000A1000D0D0D00F2F2F200000000000000000000000000FFFFFF00BADC
      DC002828280046C6C500614EBE001B1B1B00C7E9E900BADCDC00C7E9E900BADC
      DC00C7E9E90000000000FFFFFF00000000000000000000000000000000000000
      0000840000008400000084000000000000008400000000000000000000008400
      00000000000000000000000000000000000000000000000000000000000048A0
      4000087000000878000008700000087800000870000060A06000000000000000
      00000000000000000000000000000000000000000000000000009B9BDE005F5F
      B1005353C6000000A1000D0DAE000000A1000D0DAE000000A1005353C6005F5F
      B1000D0DAE0000000000FFFFFF0000000000000000000D0D0D00F2F2F200C7E9
      E9001B1B1B0053D3D2005441B10028282800BADCDC006AC1C7005DB4BA006AC1
      C700BADCDC000D0D0D00F2F2F200000000000000000000000000000000000000
      0000000000000000000084000000000000008400000084000000840000000000
      0000000000000000000000000000000000000000000000000000000000004098
      400008700000187810005098500008780000087000000870000068A860000000
      000000000000000000000000000000000000000000000D0D0D008E8ED1006C6C
      BE000000A1000D0DAE000000A1000D0DAE000000A1000D0DAE000000A1006C6C
      BE000000A1000D0D0D00F2F2F200000000000000000000000000FFFFFF00BADC
      DC00C7E9E9001B1B1B0053D3D2005441B10028282800BADCDC00C7E9E900BADC
      DC00C7E9E90000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000084000000000000008400000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000048A0
      400018781000D0E0D0000000000068A8600008780000087800000870000070A8
      70000000000000000000000000000000000000000000000000009B9BDE005F5F
      B1000D0DAE000000A1000D0DAE000000A1000D0DAE000000A1000D0DAE005F5F
      B1000D0DAE0000000000FFFFFF0000000000000000000D0D0D00F2F2F200C7E9
      E9005DB4BA002828280046C6C500614EBE001B1B1B00C7E9E900BADCDC00C7E9
      E900BADCDC000D0D0D00F2F2F200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005098
      5000D8E0D00000000000000000000000000060A0600008700000087800004890
      400000000000000000000000000000000000000000000D0D0D008E8ED1006C6C
      BE000000A1000D0DAE000000A1000D0DAE000000A1000D0DAE000000A1006C6C
      BE000000A1000D0D0D00F2F2F200000000000000000000000000FFFFFF00BADC
      DC00C7E9E900BADCDC002828280046C6C500614EBE001B1B1B00C7E9E9002828
      9E003535AB0000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000068A86000087800004890
      40000000000000000000000000000000000000000000000000009B9BDE005F5F
      B1005353C6000000A1000D0DAE000000A1000D0DAE000000A1005353C6005F5F
      B1000D0DAE0000000000FFFFFF0000000000000000000D0D0D00F2F2F200FFFF
      FF00F2F2F200FFFFFF001B1B1B0053D3D2001B1B1B0028282800F2F2F2007878
      FF006B6BF2000D0D0D00F2F2F200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000060A060004890
      400000000000000000000000000000000000000000000D0D0D008E8ED1009B9B
      DE004646B9005353C6000000A1000D0DAE000000A1005353C6004646B9000D0D
      AE000000A1000D0D0D00F2F2F2000000000000000000000000000D0D0D000000
      00000D0D0D00000000000D0D0D001B1B1B0053D3D2005441B100282828000000
      00000D0D0D0000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000A0D0
      A0000000000000000000000000000000000000000000000000000D0D0D008E8E
      D1009B9BDE005F5FB1006C6CBE005F5FB1006C6CBE005F5FB1000D0DAE000000
      A1000D0D0D00F2F2F20000000000000000000000000000000000000000000000
      000000000000000000000000000028282800ADAFD900BABCE6001B1B1B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000D0D
      0D008E8ED1009B9BDE008E8ED1009B9BDE008E8ED1009B9BDE000000A1000D0D
      0D00F2F2F2000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000028282800ADAFD900282828000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000D0D0D00000000000D0D0D00000000000D0D0D00000000000D0D0D00F2F2
      F200000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001B1B1B00282828001B1B1B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000200100000100010000000000000900000000000000000000
      000000000000000000000000FFFFFF00FFFF0000000000008000000000000000
      8000000000000000800000000000000080000000000000008000000000000000
      8000000000000000800000000000000080000000000000008000000000000000
      8000000000000000800000000000000080000000000000008000000000000000
      FFFF000000000000FFFF000000000000803FFFFFF81FC0037FDF0000E007C003
      40040000C003C0037E500000C001C0034028E0038001C0036554E0030000C003
      402880010000C003755400000000C00340080000000000007C10800100008001
      4007C0030000C0037F1FE0078001E0072A8FF00F8001F00F8007F81FC003F81F
      8003FC3FE007FC3F5577FFFFF81FFE7FFFFFFFFFFFFFFFFF8000800180038000
      80008001A003800080008001A003800080008001A003800080008001A0038000
      80008001A003800080008001A003800080008001A003800080008001A0038000
      80008001BC03800080008001BC03800080008001B80780008000A001BF8F8000
      FFFF8001801FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800807FC0038003F800
      003FC0038003E000001FC0018003E000000FC001800380000007C00180038000
      0007C00180038000A003C001800380008001C003800380008000C00380038000
      E000C00380038000F000C00380038003F800C00380038003FC00C0078007800F
      FF00C00F800F800FFF80FFFF801FFFFF8003FFFF800180011C01001F80010000
      0000001F800100004000001FC403000040010000C00300004001C000C0030000
      0001E000C00300000001E000C20300000001C003C303E0070001C003C309F00F
      0C01E003C3FFF10F0C01E003C3FFF10F0C01E003C3C7F80F0C1FFE03F003F80F
      8C1FFF07F803FF81CFFFFF07FE33FFC1FFFF800380038001FFFF000300030000
      C01F000300030000C00F000300030000C003000300030000C003000300030000
      C003000300030000C003000300010000C00F00000000E007C00F00000000F00F
      C00F00000000F10FE00F00000000F10FE00300000000F80FE003FFFFFF07F80F
      FF83FFFFFF07FF81FFFFFFFFFF07FFC1FFFFFFFFFFFFFFFFFFFFFFFFFFFFC01F
      FFFFFFFFE39FC00FFFFFF7DF80C3C007FFFFE38F80E3C003FFDFC1070063C003
      FFCFF7DF007FC003E007F7DF0047C00FFFCFF7DF8001C007FFDFF7DF8001C007
      FFFFF7DFE200E003FFFFF7DFFE00E003FFFFF7DFC600E003FFFFFFFFC701FFC7
      FFFFFFFFC301FFCFFFFFFFFFF9C7FFFFFFFF0000FFFFFFFFF55F0000FFFFF007
      E00F0000FFF7E007C0070000FE77E00780030000FF17E007800100008007E007
      80030000FF17E00780010000FE77E00780030000BFF7E00780010000BFFFE007
      80030000B9FFE00380010000A3FFC003C00300008007C003E0070000A3FFC007
      F00F0000B9FFFC7FFFFF0000BFFFFFFFF81FF81FF81FFFFFF00FF00FF00FFFFF
      C003C003C003FFFF800180018001F7CF800180018001E38F800180018001E10F
      800180018001F01FC003C003C003F83F800180018001F83F800180018001F01F
      800180018001E10FC003C003C003E38F800180018001F7CF800180018001FFFF
      800180018001FFFF800180018001FFFFFFFFFFFFFFFFFFFFF01FF55FF55FF55F
      E00FE00FE00FE00FC007C007C007C00780038003800380038001800180018001
      8001800380038003800180018001800180018003800380038001800180018001
      80018003800380038001800180018001C003C003C003C003E007E007E007E007
      F00FF00FF00FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFF8003FFFF000DC0070003
      FFFF000980030003800F00010001000380070003000100038003000300010003
      800100030000000380000003000000038000000380000003800F0007C0000003
      800F000FE0010003800F000FE0070003C7F8000FF0070003FFFC001FF0030003
      FFBA003FF803FFFFFFC7007FFFFFFFFFFC03FFFFFFFFFFFFF801FE1FFFFFFFFF
      8001F807E007FFFF0001E003C007FFFF0001E01FC007F0070001E01FC007E407
      0000E01FC007E1070000C00FC007E0470000C00FC007E0070000C00FC007E047
      0000C00FC007E1070000C00FC007E4070000E01FC00FF0070001E01FC01FFFFF
      001FF31FC03FFFFF803FFFFFFFFFFFFFFF80FF80FFFFFFFFE380E38080018001
      808080809801980180808080A601A60100000000A101A1010000000091019101
      000000008861886180018001841184118001800182098209E200E20082018201
      FE00FE0082018201FE00FE0080018001FF01FF0180118011FF01FF0180018001
      FFC7FFC780018001FFFFFFFFFFFFFFFFFFFFFFFF00008003FFFFFFFF00000003
      FFFFFFFF00000003F07FFEFF00000003F8FFFEFF00000003F8FFFC7F00000003
      FC7FFC7F00000003FC7FF83F00000003FC7FF83F00000000FE3FF01F00000000
      FE3FF01F00000000FF1FE00F00000000FE0FE00F00000000FFFFFFFF0000FF83
      FFFFFFFF0000FF83FFFFFFFF0000FF830000FFFFFFFF80000000FE3FE0070000
      0000F81FC00300000000E00F8001000000008007800100000000000380010001
      000000018001800100000000C001800100000001C00180010000800180018001
      0000C001800180010000E000800180010000F000800180010000F803C0018001
      0000FC0FE00380010000FE3FFFFFC007F800FFFFFFFFFFFFE000FC3FFFFFC007
      C0000000FC3FC00780000000F80FDE078000FE7FE00FC0070000FE7FC003DE07
      00008003C7C3C00700000001D833D00320000000C003C00120000000C003C000
      20000000C003D00020010000C007D00180018000E00FD8A7C003C001F83FC007
      E007FFFFFFFFFFFFF01FFFFFFFFFFFFFFF3FE007FF8F8001FE1FC003FF0F0000
      FC1F8001F8070000F00F0000E0070000C0030000C00300008001000080010000
      000000000000000100000000000080030000000000008001000000000000C007
      000000000000C003000000000000E007800000000001E007800380018001F00F
      C003C007C007F81FF00FF007E00FF83FFFFFFFFFFFFFFFFFFFFFF01FEFFFF3FF
      FFFFE00FE7FFFD9FF9FFC0078001FD6FF0FF80038001FD6FE07F80018001F16F
      E03F80018001FD1FE01F80018001FC7FE20F80018001FC7FE70F80018001FD7F
      FF8F80018001F93FFFCF80018001FBBFFFEFC003FE1FFBBFFFFFE007FF1FFBBF
      FFFFF00FFF1FFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object popupExport: TPopupMenu
    Images = ilExport
    Left = 226
    Top = 82
    object excelMI: TMenuItem
      Caption = 'Excel'
      ImageIndex = 0
      OnClick = excelMIClick
    end
    object csvMI: TMenuItem
      Caption = 'CSV'
      ImageIndex = 1
      OnClick = csvMIClick
    end
    object xmlMI: TMenuItem
      Caption = 'XML'
      ImageIndex = 2
      OnClick = xmlMIClick
    end
    object htmlMI: TMenuItem
      Caption = 'HTML'
      ImageIndex = 3
      OnClick = htmlMIClick
    end
    object pdfMI: TMenuItem
      Caption = 'PDF (Acrobat)'
      ImageIndex = 4
      OnClick = pdfMIClick
    end
  end
  object dlgSaveExportedFile: TSaveDialog
    Left = 146
    Top = 274
  end
  object printer: TdxComponentPrinter
    CurrentLink = printerLink1
    Version = 0
    Left = 770
    Top = 218
    object printerLink1: TdxGridReportLink
      Component = gridInput
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.LeftTitle.Strings = (
        #931#964#945#952#956#972#962' '#949#961#947#945#963#943#945#962': [Machine Name] - [User Name]')
      PrinterPage.PageFooter.RightTitle.Strings = (
        '[Page #] '#945#960#972' [Total Pages]')
      PrinterPage.PageHeader.LeftTitle.Strings = (
        #949#954#964#973#960#969#963#951' '#964#951#957' [Date & Time Printed]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40364.762991412030000000
      OptionsSize.AutoWidth = True
      BuiltInReportLink = True
    end
  end
  object scrolltimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = scrolltimerTimer
    Left = 58
    Top = 210
  end
  object tblStore: TMyTable
    TableName = 'store'
    Connection = EmbeddedConnection
    Filter = 'idExpenseType=null'
    BeforeEdit = tblStoreBeforeEdit
    BeforePost = tblStoreBeforePost
    AfterPost = tblStoreAfterPost
    AfterCancel = tblStoreAfterCancel
    OnCalcFields = tblStoreCalcFields
    OnDeleteError = tblStoreDeleteError
    OnPostError = tblStorePostError
    Left = 322
    Top = 418
    object tblStoreidstore: TLargeintField
      AutoGenerateValue = arAutoInc
      FieldName = 'idstore'
      Origin = 'store.idstore'
    end
    object tblStoredescr: TWideStringField
      FieldName = 'descr'
      Origin = 'store.descr'
      Size = 45
    end
    object tblStoreafm: TWideStringField
      FieldName = 'afm'
      Origin = 'store.afm'
      Size = 9
    end
    object tblStoreidExpenseType: TLargeintField
      FieldName = 'idExpenseType'
      Origin = 'store.idExpenseType'
    end
    object tblStorecorrectAFM: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'correctAFM'
      Calculated = True
    end
  end
  object dsStore: TMyDataSource
    DataSet = tblStore
    Left = 362
    Top = 418
  end
  object tblExpenseType: TMyTable
    TableName = 'v_expensetype'
    Connection = EmbeddedConnection
    KeyFields = 'idDetail'
    Left = 322
    Top = 482
  end
  object dsExpenseType: TMyDataSource
    DataSet = tblExpenseType
    Left = 352
    Top = 488
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default'
      #913#961#967#949#943#959
      #917#956#966#940#957#953#963#951' (Skins)'
      #913#957#964#943#947#961#945#966#945' '#945#963#966#945#955#949#943#945#962
      'Menus')
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True)
    MenuAnimations = maRandom
    PopupMenuLinks = <>
    ShowShortCutInHint = True
    UseSystemFont = True
    Left = 130
    Top = 450
    DockControlHeights = (
      0
      0
      24
      0)
    object dxBarManager1Bar1: TdxBar
      AllowClose = False
      AllowCustomizing = False
      AllowQuickCustomizing = False
      AllowReset = False
      BorderStyle = bbsNone
      Caption = #914#945#963#953#954#972' '#924#949#957#959#973
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 444
      FloatTop = 257
      FloatClientWidth = 147
      FloatClientHeight = 114
      IsMainMenu = True
      ItemLinks = <
        item
          Visible = True
          ItemName = 'N2'
        end
        item
          Visible = True
          ItemName = 'N8'
        end
        item
          Visible = True
          ItemName = 'N4'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem2'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      MultiLine = True
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      UseRecentItems = False
      Visible = True
      WholeRow = True
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #914#959#942#952#949#953#945'     '
      Category = 0
      Visible = ivAlways
      Glyph.Data = {
        AA030000424DAA03000000000000360000002800000011000000110000000100
        18000000000074030000C40E0000C40E00000000000000000000C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4000000000000000000
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4000000000000840084840084848484000000C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4000000000000840084840084FF
        FFFFFFFFFFC6C6C6848484000000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0
        D4000000000000840084840084FFFFFFFFFFFF000000000000C6C6C6C6C6C684
        8484000000C8D0D4C8D0D4C8D0D4C8D0D400848484840084840084FFFFFFFFFF
        FF000000000000840084840084000000C6C6C6C6C6C6848484000000C8D0D4C8
        D0D4C8D0D400848484840084FFFFFF0000000000008400848400848400848400
        84840084000000C6C6C6C6C6C6848484000000C8D0D4C8D0D400848484000000
        00000084008484008484008400848400FFFF840084840084840084000000C6C6
        C6C6C6C6848484000000C8D0D400848484840084840084840084840084840084
        840084008484840084840084840084840084000000C6C6C6000000C8D0D4C8D0
        D400C8D0D4840084FFFFFF84008484008484008484008484008400FFFF00FFFF
        840084840084840084000000000000C8D0D4C8D0D400C8D0D4C8D0D4840084FF
        FFFF84008484008484008484008484008400848400FFFF00FFFF840084840084
        000000C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4840084FFFFFF84008484008484
        008400848484008400FFFF00FFFF840084840084840084000000C8D0D400C8D0
        D4C8D0D4C8D0D4C8D0D4840084FFFFFF84008484008400FFFF00FFFF00FFFF84
        0084840084840084000000000000C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4840084FFFFFF840084840084840084840084840084000000000000C8D0D4C8
        D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4840084FFFFFF8400
        84840084000000000000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4840084840084000000C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D400}
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnChanges'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem3'
        end
        item
          Visible = True
          ItemName = 'About1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'btnDonate'
        end>
    end
    object About1: TdxBarButton
      Caption = #931#967#949#964#953#954#940' '#956#949' '#964#951#957' '#949#966#945#961#956#959#947#942
      Category = 0
      Hint = #931#967#949#964#953#954#940' '#956#949' '#964#951#957' '#949#966#945#961#956#959#947#942
      Visible = ivAlways
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000120B0000120B00000000000000000000C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4916964846C77D5E0E3C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4D6B99BA16346816C77D4E2E5C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4B4AFB7A799A1B9A192E7BB90AE7C645F3C4DBCC0C8D0DC
        DFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4B2A39E9672669D7664AA8067C6
        9F80EEC69FD0A98784564765363A8C7882C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        B7A398B49073CDAD8FE3BE9BECC6A0F5D3ACFFDDB5F4CDA7E3BD99C59F7E8857
        4775525AC8D0D4C8D0D4C8D0D4BAA99EC2A48CE6CBAEFEE1C2FBDAB9C07A51C5
        6E3BC87341D69469FEDAB8F7D3B1DFBD9A9D6E58745158C8D0D4C6BDB7C4A892
        EDD2BCFFE7CCFFE6CBFFECD0D8B3948E24009A390EF1CFAFFFE6C6FFE2C0FBD8
        B8E0C1A18859498B7681C7B0A5E5CBB6FFEAD4FFEAD4FFE8D1FFEFD7DEBDA28C
        2600A4471DFCDFC3FFE6CAFFE3C6FFE4C8F9D9BDCDAE9372474FD4C3B7F5E1D2
        FFEFDBFFECD9FFEBD6FFF3DFDDBFA78D2700A3471FFBDFC6FFE9D1FFE6CDFFE6
        CCFFE5CCE8CCB1936F6AD7CCC0F6E8DBFFF2E4FFEFDFFFEFDDFFFAEBE3C8B490
        2600A4471EFBE2CCFFEDD9FFE9D1FFE9D2FFEAD3ECD6C0937273DCD1C7F7EEE3
        FFF6EBFFF3E7FFF6ECF1E4D6A76646821900903C19FAE6D5FFF2E0FFEDD9FFEC
        D8FFF2DEEFD8C5A28384DCD2CBF4EDE8FFFCF7FFF6EFFFF8F3F3E5D9CDB2A2CE
        B7ACCCB5A6FDEFE3FFF3E6FFF0E1FFF3E6FFF9EBE5C9B9B7A8A8D9D9D9EDE6E2
        FCFAF9FFFEFAFFFBF5FFFEFAF6ECE4E3AE8EF9D5BDFFF8F1FFF6EBFFF7EFFFFD
        FAFAEEE1D1B9AFC8D0D4C8D0D4E4E0DEF3ECEAFDFDFDFFFFFDFFFFFFBB9B8D79
        0D009F4C24F8F2ECFFFEFCFFFFFFFAF4F1E4D5CAC6C5C5C8D0D4C8D0D4C8D0D4
        E0E0E0F3F0F1FCF9FCFEFEFFE9E2DE966F67BEA69BFEFFFFFEFBFBFCF4F1E3DB
        D7C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4D6D9DCEDEAEBF6F3F2F9F9FAFE
        FFFFFCFFFFFAF4F2F9F1F0DEDEDFC8D0D4C8D0D4C8D0D4C8D0D4}
      OnClick = About1Click
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = #917#953#963#945#947#969#947#942' / '#917#958#945#947#969#947#942'    '
      Category = 0
      Visible = ivAlways
      Glyph.Data = {
        76060000424D7606000000000000360000002800000014000000140000000100
        2000000000004006000000000000000000000000000000000000000000020000
        000400000004000000060000000300000004563E0CA66F510FC2020100140000
        00030000000C0000000B0000000A0000000A0000000800000007000000060000
        0005000000030000000000000000000000000000000000000000000000004A36
        0A92A67917FFA47817FF664A0EB3010100090000000000000001000000010000
        0001000000000000000000000000000000000000000000000000000000000000
        00000000000000000000523C0C80A57917FFA07516FF9F7516FFA57917FF684D
        0FA2010100020000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000047340A6AA77B17FFA97D
        17FFAA7D17FFAA7D17FFAA7C17FFA87C17FF61470D8E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00003A2B0657B38616FBB98A19FFBB8B1AFFBB8C1AFFBB8C1AFFBB8B1AFFBA8A
        19FFB78917FF503C097800000000000000000000000000000000000000000000
        000000000000000000000000000042310B60C68E2BFBCC942CFFC59123FFBF8F
        19FFC08F1BFFC08F1BFFBF8F1AFFC29020FFCC922AFFD09129FF593B0B740000
        0000000000000000000000000000000000000000000000000000000000002F22
        0644402E0B5A37280C4C9A711ACEC6951CFFC5931DFFC5931DFFC5941BFFA579
        1ED53424104233261045271C0736000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000876412B5CC99
        1FFFCA971EFFCA971EFFCB981EFF986C13B80D1307201C632BA9246028A52666
        2BB028662BB1296429AD0711071E000000000000000000000000000000000000
        000000000000000000008C6914B8D19D21FFCF9B20FFCF9B20FFCF9C21FF9B6B
        13B414210C3934933FFF3C933DFF3C933DFF3C933DFF3C933DFF0F240F3F0000
        00000000000000000000000000000000000000000000000000008F6A16B8D7A0
        24FFD49E23FFD49E23FFD59F24FF9E6D15B415210C3934943FFF3C943DFF3C94
        3DFF3C943DFF3C943DFF0F250F3F000000000000000000000000000000000000
        00000000000000000000926C17B8DBA227FFD8A026FFD8A026FFD8A127FFA170
        17B416220D39369943FF3E9941FF3E9941FF3E9941FF3E9941FF0F26103F0000
        0000000000000000000000000000000000000000000000000000966F1BB9E3A7
        2EFFDFA52CFFDEA52DFFE1A72DFFA1711BB815250E3D389F46FF3F9E45FF3F9E
        45FF3F9E45FF3F9E44FF1027113F000000000000000000000000000000000000
        00000000000000000000654A1381BB8730DFB27E2BD4B87925CEB07424C66B45
        107A1626113C3FAD5FFF46AD5EFF46AD5EFF46AD5EFF45AA5BFF0F25103A0000
        0000000000000000000000000000000000000000000000000000010100020907
        010C05040107161506243F3D1664202E114A275830814CB36CFF4DB26AFF4DB2
        6AFF4DB26BFF4CB169FF23572B82183E1C5E1533194C00000000000000000000
        00000000000000000000000000000000000000000000031108185DAF86DB50BD
        82FF4EB570FF54B775FF53B774FF53B774FF53B774FF53B875FF4EB46FFF61BF
        85FF57906EB30000000000000000000000000000000000000000000000000000
        000000000000000000000A170C2168AF82DC62BE86FF5ABB7BFF5CBC7EFF5CBC
        7EFF5DBC7EFF58BA79FF6AC38FFF558F68B40A160C1F00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000C22
        0E3274BF93EB69C38DFF64C186FF66C288FF63C084FF71C796FF5A9B6FC4040B
        050F000000000000000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000122F15437FCA9FF572C996FF6CC6
        8EFF79CC9DFF68AF82D80613071C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000183C1E5286D1A9FA84D1A7FF72BB8FE30A1D0C2A000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000214B2C6374BB
        98DE0E2813380000000000000000000000000000000000000000}
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSeparator2'
        end
        item
          Visible = True
          ItemName = 'importerMI'
        end
        item
          Visible = True
          ItemName = 'dxBarSeparator3'
        end
        item
          Visible = True
          ItemName = 'expExcelMI'
        end
        item
          Visible = True
          ItemName = 'expCSVMI'
        end
        item
          Visible = True
          ItemName = 'expXMLMI'
        end
        item
          Visible = True
          ItemName = 'expHTMLMI'
        end
        item
          Visible = True
          ItemName = 'expPDFMI'
        end
        item
          Visible = True
          ItemName = 'dxBarSeparator1'
        end
        item
          Visible = True
          ItemName = 'exportOfficialMI'
        end>
    end
    object importerMI: TdxBarButton
      Caption = #917#953#963#945#947#969#947#942' '#945#960#972' '#940#955#955#945' '#960#961#959#947#961#945#956#956#945#964#945' ('#945#961#967#949#943#945' EXCEL 97-2003 '#954#945#953' CSV)'
      Category = 0
      Visible = ivAlways
      Glyph.Data = {
        36090000424D3609000000000000360000002800000018000000180000000100
        2000000000000009000000000000000000000000000000000000000000000000
        00000000000300000019000000320000003B0000004B0000004B0000004B0000
        004B0000003B0000003200000019000000030000000000000000000000000000
        00000000000000000000000000000000000000000000000000000000001F0000
        00580101017F1E1B1A974F4C4CB161605FBA7E736BCB735E50CB694C39CB6340
        2CCB452B1ABA362012B1150C07970100007F000000580000001F000000000000
        0000000000000000000000000000000000000000000000000000241A139B8964
        4ED9DDB39AFDEFD9CCFFFBF4F1FFF9F4F1FFEDD8CBFFD9B297FFC78F6AFFBB7A
        52FFB26D43FFA46038FF9E5C35FF9F5D39FD754C32DB2118129B000000000000
        0000000000000000000000000000000000000000000000000000CB9270FCD39A
        77FFE0B69CFFEFD9CCFFFBF4F1FFF9F4F1FFEDD8CBFFD9B297FFC78F6AFFBB7A
        52FFB26D43FFA46038FF9B5B34FF9C5D37FFA56A45FFB4805DFC000000000000
        0000000000000000000000000000000000000000000000000000D19673FFD39A
        77FFE0B69CFFEFD9CCFFFBF4F1FFF9F4F1FFEDD8CBFFD9B297FFC78F6AFFBB7A
        52FFB26D43FFA46038FF9B5B34FF9C5D37FFA56A45FFB6835EFF000000000000
        0000000000000000000000000000000000000000000000000000D19673FFD39E
        7CFFDCB69BFFE8CFBDFFF1E2D7FFF5EAE3FFF8EFE8FFF6EDE6FFEEDBCDFFE8D0
        C0FFDFC1ABFFC09172FFB3815FFFAD7553FFAA724DFFB6835EFF000000000000
        0000000000000000000000000000000000000000000000000000CC9975FFD5A9
        8BFFE3C3AEFFE7CFBFFFE7D0BFFFDDBFA7FF759964FF809162FFC79474FFC58C
        68FFBF8159FFBB7C56FFB3744FFFAE7049FFB07753FFB6815CFFA56B45FFA56B
        45FFA56B45FFA56B45FFA56B45FFA56B45FFA56B45FF6D4931A5E3C4AFFFEAD5
        C7FFEAD5C7FFE7D0C0FFE3C8B6FF8FA77DFF248C3AFF218E3AFF303B26FF5E45
        33FFA17051FFBC7C54FFB8744AFFB46D42FFB2693DFFB37047FFE8D5C8FFE8D4
        C5FFE7D3C3FFE7D1C0FFE6CFBCFFE6D1C0FFEFE4DBFFAD7450FF0E0D0C0F4A44
        3F51877B7393A6978DB79CA085D3299141FE2C9646FF288A3AFF1D7E35FF3638
        25FF473225FF674732FFA7704DFFB77A53FFB47C58FFB38260FFAF7E5EFFAD7C
        5CFFAB7B5AFFA97A59FFA77757FFA77757FFF9F4EEFFC49673FF000000000000
        000000000000020A040F209346DD34AF5FFF38B966FF36B965FF2C9A47FF1C74
        32FF8EA094FFB8B8B8FF9E775CFFDBDBDBFFEEEEEEFFB88968FFEFEFEFFFEFEF
        EFFFAF805EFFEEEEEEFFEDEDEDFFA97958FFF9F4EFFFC9A07DFF000000000000
        0000000201031C9752BF3BC87BFF45D78AFF45D88BFF45D88BFF46D88AFF32A5
        56FF26763BFFD8E7DDFFD3A482FFF5F5F5FFF5F5F5FFC79675FFF2F2F2FFF1F1
        F1FFBB8A69FFEFEFEFFFEEEEEEFFB0815FFFF9F4EFFFC39673FF000000000000
        000021804E974DD18BFF5CDF9EFF60E3A0FF61E3A2FF64E4A4FF5BE09EFF60E4
        A2FF3BC179FF266A32FFC5A682FFD4A583FFD0A180FFCD9E7CFFC79776FFC494
        73FFC19170FFBD8E6DFFB88968FFB68565FFF9F4F1FFC1926EFF000000000000
        00000C3115871A602DFF6EDBA2FF82EBB8FF84EDB9FF85EDBCFF56D395FF155A
        26FF0C501CFF0C501CFFD3AF8BFFF8F8F8FFF6F6F6FFD4A584FFF4F4F4FFF4F4
        F4FFC89977FFF1F1F1FFEFEFEFFFBC8C6BFFF9F5F1FFC1926EFF000000000000
        000000000000000000006BCF9AFD96F2C5FFA4F2CDFFA5F2CDFF88E2B7FF1A64
        2BFFFEFEFEFFFCFCFCFFEEC09FFFF9F9F9FFF9F9F9FFE3B492FFF6F6F6FFF5F5
        F5FFD7A785FFF4F4F4FFF1F1F1FFC99A79FFFBF5F2FFC39572FF000000000000
        0000000000000000000077CB9DF4A0F4CBFFA6F4CFFFB0F5D4FFB7EDD4FF1D6D
        33FFF9CCAAFFF5C8A6FFF4C5A5FFEFC1A0FFEDBF9CFFEABB9AFFE4B694FFE0B2
        90FFDDAF8EFFDBAB8AFFD4A583FFD1A181FFFBF6F4FFC49572FF000000000000
        0000000000000000000068C895F48AEFBFFF88EFBCFF89EFBDFFC9F2DDFF2376
        3BFFF8FCF9FFFEFEFEFFF8CBA9FFFCFCFCFFFBFBFBFFEFC1A0FFF8F8F8FFF8F8
        F8FFE4B695FFF5F5F5FFF4F4F4FFD8A988FFFBF6F4FFC39470FF000000000000
        000000000000000000004AC282F45CE7A1FF59E6A0FF56E69EFF90E7BDFF1C76
        39FFEEF6F1FFFFFFFFFFFBCDADFFFCFCFCFFFCFCFCFFF5C7A6FFF9F9F9FFF8F8
        F8FFEBBD9BFFF6F6F6FFF5F5F5FFDFB08FFFFBF6F4FFC0916CFF000000000000
        0000000000000000000029B569EF32D881FF31D881FF2FD87EFF58DB99FF1E82
        42FF20A94FFF20A44DFF739F6AFFFBCDADFFFBCDADFFFBCDADFFF9CCABFFF8CB
        A9FFF6C8A7FFF4C7A5FFEFC1A0FFEDBF9EFFFBF6F4FFBB8966FF000000000000
        000000000000000000001A9B4FDC21C466FF22C566FF22C364FF32C977FF1D8A
        46FF1A853EFF248747FF177737FFFFFFFFFFFEFEFEFFFBCDADFFFCFCFCFFFCFC
        FCFFF9CCABFFF9F9F9FFF8F8F8FFF2C4A4FFFBF6F4FFB8845FFF000000000000
        00000000000000000000156A2FB0209C46FF22A047FF229E45FF23A952FF1B95
        4BFF125F2CFF2B7345FF2A8746FFFFFFFFFFFFFFFFFFFBCDADFFFEFEFEFFFEFE
        FEFFFBCDADFFFCFCFCFFFBFBFBFFFBCDADFFFBF8F5FFB37E58FF000000000000
        000000000000000000000A260F4C207E31FF248231FF248131FF248030FF2281
        34FF126631FF1D6C35FF458548FFC08157FFBD7D55FFBB7B51FFB4754CFFB273
        49FFAE7047FFAB6C44FFA6673FFFA2643BFFFBF8F5FFB6815BFF000000000000
        0000000000000000000000000000154A1B97257E2EFF24802FFF248130FF2782
        32FF21883DFF2F8C4BFFAA8B5DFFC4855CFFC18359FFBD8056FFB87A51FFB676
        4EFFB3744BFFAF7249FFAA6C43FFA6683EFFFBF8F5FFBD8B68FF000000000000
        000000000000000000000000000000000000986340EAF9F6F2FFFBF1EBFFF9EF
        EAFFF9EFEAFFF9EFEAFFF8EFE8FFF8EEE8FFF8EEE8FFF8EEE7FFF8EDE6FFF6EB
        E4FFF6EBE3FFF5EAE3FFF5E8E2FFF2E6DDFFF8F1EDFFAC7F5BEF000000000000
        0000000000000000000000000000000000005939258ACFAD95FFB07D5AFFA56A
        44FFA56A44FFA56A44FFA56A44FFA56A44FFA56A44FFA56A44FFA56A44FFA56A
        44FFA56A44FFA56A44FFA56A44FFA56A44FFA56A44FF5939258A}
      OnClick = importerMIClick
    end
    object expExcelMI: TdxBarButton
      Caption = 'EXCEL ('#959#953' '#945#960#959#948#949#943#958#949#953#962' '#972#960#969#962' '#964#953#962' '#941#967#949#964#949' '#966#953#955#964#961#940#961#949#953' '#954#945#953' '#959#956#945#948#959#960#959#953#942#963#949#953')'
      Category = 0
      Hint = 'EXCEL ('#959#953' '#945#960#959#948#949#943#958#949#953#962' '#972#960#969#962' '#964#953#962' '#941#967#949#964#949' '#966#953#955#964#961#940#961#949#953' '#954#945#953' '#959#956#945#948#959#960#959#953#942#963#949#953')'
      Visible = ivAlways
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000120B0000120B00000000000000000000006400006600
        0066000066000066000066000066000066000066000066000066000066000066
        0000660000660000660000660000660000660000660000660000660000660000
        6600006600006600006600006600006600006600006600006600006600006600
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF006600006600006600006600FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF006600006600006600006600006600006600006600006600006600
        FFFFFF006600006600006600006600006600006600006600FFFFFF0066000066
        00006600006600006600006600006600FFFFFF00660000660000660000660000
        6600006600FFFFFF006600006600006600FFFFFF006600006600006600006600
        FFFFFF006600006600006600006600006600FFFFFF0066000066000066000066
        00FFFFFF006600006600006600006600FFFFFFFFFFFF006600006600006600FF
        FFFF006600006600006600006600006600FFFFFF006600006600006600006600
        FFFFFFFFFFFFFFFFFF006600FFFFFF006600006600006600006600FFFFFFFFFF
        FFFFFFFF006600006600006600006600FFFFFFFFFFFF006600FFFFFF00660000
        6600006600006600006600006600FFFFFFFFFFFF006600006600006600006600
        FFFFFF006600FFFFFF0066000066000066000066000066000066000066000066
        00FFFFFF006600006600006600006600FFFFFF00660000660000660000660000
        6600FFFFFF006600006600006600006600FFFFFF006600006600006600006600
        FFFFFF006600006600006600006600FFFFFFFFFFFFFFFFFF0066000066000066
        00FFFFFF006600006600006600006600FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF006600006600006600006600
        0066000066000066000066000066000066000066000066000066000066000066
        0000660000660000660000660000660000660000660000660000660000660000
        6600006600006600006600006600006600006600006600006600}
      OnClick = excelMIClick
    end
    object dxBarSeparator2: TdxBarSeparator
      Caption = #932#924#919#924#913' '#917#921#931#913#915#937#915#919#931' '#916#917#916#927#924#917#925#937#925' (IMPORT)  '#945#960#972' '#940#955#955#949#962' '#949#966#945#961#956#959#947#941#962
      Category = 0
      Hint = #932#924#919#924#913' '#917#921#931#913#915#937#915#919#931' '#916#917#916#927#924#917#925#937#925' (IMPORT)  '#945#960#972' '#940#955#955#949#962' '#949#966#945#961#956#959#947#941#962
      Visible = ivAlways
    end
    object dxBarSeparator3: TdxBarSeparator
      Caption = #932#924#919#924#913' '#917#926#913#915#937#915#919#931' '#916#917#916#927#924#917#925#937#925' (EXPORT) '#963#949
      Category = 0
      Hint = #932#924#919#924#913' '#917#926#913#915#937#915#919#931' '#916#917#916#927#924#917#925#937#925' (EXPORT) '#963#949
      Visible = ivAlways
    end
    object expCSVMI: TdxBarButton
      Caption = 
        'CSV ('#946#945#963#953#954#942' '#960#955#951#961#959#966#959#961#943#945' '#963#949' CSV '#956#949' '#946#940#963#951' '#964#959' '#966#953#955#964#961#940#961#953#963#956#945' '#964#959#965' '#960#943#957#945#954#940' ' +
        #963#945#962')'
      Category = 0
      Hint = 
        'CSV ('#946#945#963#953#954#942' '#960#955#951#961#959#966#959#961#943#945' '#963#949' CSV '#956#949' '#946#940#963#951' '#964#959' '#966#953#955#964#961#940#961#953#963#956#945' '#964#959#965' '#960#943#957#945#954#940' ' +
        #963#945#962')'
      Visible = ivAlways
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000100B0000100B00000000000000000000FFFFFFFFFFFF
        FFFFFFBC9A82BC957BBC957BBD9376BF9172BF8E6EC18C69C28A65C38861C484
        5DC58258C88055C88055FFFFFFFFFFFFFFFFFFBE9D85FCEDE3FCECE1FCEAE0FB
        E9DEFBE8DCFBE7DBFBE6D9FBE5D7FAE4D6FAE3D4FAE2D2C68159FFFFFFFFFFFF
        FFFFFFC0A088FCEEE5FCEDE3D6AE90D6AE90D6AE90D6AE90D6AE90D6AE90D6AE
        90FAE4D6FAE3D4C4845EFFFFFFFFFFFFFFFFFFC2A28CFCEFE6FCEEE5FCEDE3FC
        ECE1FCEAE0FBE9DEFBE8DCFBE7DBFBE6D9FBE5D7FAE4D6C38962FFFFFFFFFFFF
        FFFFFFC4A58FFDF0E8FCEFE6D6AE90D6AE90D6AE90D6AE90D6AE90D6AE90D6AE
        90FBE6D9FBE5D7C18B67FFFFFFFFFFFFFFFFFFC6A892FDF1EAFDF0E8FCEFE6FC
        EEE5FCEDE3FCECE1FCEAE0FBE9DEFBE8DCFBE7DBFBE6D9C08E6CFFFFFFFFFFFF
        FFFFFFC8AB95FDF2EBFDF1EAD6AE90D6AE90D6AE90D6AE90D6AE90D6AE90D6AE
        90FBE8DCFBE7DBBF9072FFFFFFFFFFFFFFFFFFCAAD99FDF3EDFDF2EBFDF1EAFD
        F0E8FCEFE6FCEEE5FCEDE3FCECE1FCEAE0FBE9DEFBE8DCBD9376009A00009A00
        009A00009A00009A00009A00009A00009A00009A00009A00009A00009A00009A
        00FCEAE0FBE9DEBB957A009A00009A00FDF4EFFDF4EF009A00FDF4EFFDF4EF00
        9A00009A00009A00FDF4EF009A00009A00FCECE1FCEAE0BA977E009A00FDF4EF
        009A00009A00009A00009A00009A00FDF4EF009A00FDF4EF009A00FDF4EF009A
        00FCEDE3FCECE1BA977E009A00FDF4EF009A00009A00009A00FDF4EF009A0000
        9A00009A00FDF4EF009A00FDF4EF009A00BA977EBA977EBA977E009A00009A00
        FDF4EFFDF4EF009A00009A00FDF4EFFDF4EF009A00FDF4EF009A00FDF4EF009A
        00FFFFFFFFFFFFBA977E009A00009A00009A00009A00009A00009A00009A0000
        9A00009A00009A00009A00009A00009A00FFFFFFBA977EFFFFFFFFFFFFFFFFFF
        FFFFFFD8C0AFFFFBF9FEFAF7FEF9F5FEF8F4FEF7F2FEF6F0FDF4EFBA977EFFFF
        FFBA977EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDAC3B3D4BBA9D1B6A3CDB19DC9
        AC97C6A791C2A28BBE9D85BA977EBA977EFFFFFFFFFFFFFFFFFF}
      OnClick = csvMIClick
    end
    object expXMLMI: TdxBarButton
      Caption = 'XML ('#959#953' '#945#960#959#948#949#943#958#949#953#962' '#972#960#969#962' '#964#953#962' '#941#967#949#964#949' '#966#953#955#964#961#940#961#949#953' '#954#945#953' '#959#956#945#948#959#960#959#953#942#963#949#953')'
      Category = 0
      Hint = 'XML ('#959#953' '#945#960#959#948#949#943#958#949#953#962' '#972#960#969#962' '#964#953#962' '#941#967#949#964#949' '#966#953#955#964#961#940#961#949#953' '#954#945#953' '#959#956#945#948#959#960#959#953#942#963#949#953')'
      Visible = ivAlways
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFBC9A82BC957BBC957BBD9376BF9172BF8E6EC18C69C28A65C38861C484
        5DC58258C88055C88055FFFFFFFFFFFFFFFFFFBE9D85FCEDE3FCECE1FCEAE0FB
        E9DEFBE8DCFBE7DBFBE6D9FBE5D7FAE4D6FAE3D4FAE2D2C68159FFFFFFFFFFFF
        FFFFFFC0A088FCEEE5FCEDE3D6AE90D6AE90D6AE90D6AE90D6AE90D6AE90D6AE
        90FAE4D6FAE3D4C4845EFFFFFFFFFFFFFFFFFFC2A28CFCEFE6FCEEE5FCEDE3FC
        ECE1FCEAE0FBE9DEFBE8DCFBE7DBFBE6D9FBE5D7FAE4D6C38962FFFFFFFFFFFF
        FFFFFFC4A58FFDF0E8FCEFE6D6AE90D6AE90D6AE90D6AE90D6AE90D6AE90D6AE
        90FBE6D9FBE5D7C18B67FFFFFFFFFFFFFFFFFFC6A892FDF1EAFDF0E8FCEFE6FC
        EEE5FCEDE3FCECE1FCEAE0FBE9DEFBE8DCFBE7DBFBE6D9C08E6CFFFFFFFFFFFF
        FFFFFFC8AB95FDF2EBFDF1EAD6AE90D6AE90D6AE90D6AE90D6AE90D6AE90D6AE
        90FBE8DCFBE7DBBF9072FFFFFFFFFFFFFFFFFFCAAD99FDF3EDFDF2EBFDF1EAFD
        F0E8FCEFE6FCEEE5FCEDE3FCECE1FCEAE0FBE9DEFBE8DCBD93760066FF0066FF
        0066FF0066FF0066FF0066FF0066FF0066FF0066FF0066FF0066FF0066FF0066
        FFFCEAE0FBE9DEBB957A0066FFFFFBF90066FFFFFBF90066FFFFFBF90066FF00
        66FF0066FFFFFBF90066FFFFFBF9FFFBF9FCECE1FCEAE0BA977E0066FF0066FF
        FFFBF90066FF0066FFFFFBF90066FFFFFBF90066FFFFFBF90066FFFFFBF90066
        FFFCEDE3FCECE1BA977E0066FFFFFBF90066FFFFFBF90066FFFFFBF9FFFBF900
        66FFFFFBF9FFFBF90066FFFFFBF90066FFBA977EBA977EBA977E0066FFFFFBF9
        0066FFFFFBF90066FFFFFBF90066FF0066FF0066FFFFFBF90066FFFFFBF90066
        FFFFFBF9FFFBF9BA977E0066FF0066FF0066FF0066FF0066FF0066FF0066FF00
        66FF0066FF0066FF0066FF0066FF0066FFFFFBF9BA977EFFFFFFFFFFFFFFFFFF
        FFFFFFD8C0AFFFFBF9FEFAF7FEF9F5FEF8F4FEF7F2FEF6F0FDF4EFBA977EFFFB
        F9BA977EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDAC3B3D4BBA9D1B6A3CDB19DC9
        AC97C6A791C2A28BBE9D85BA977EBA977EFFFFFFFFFFFFFFFFFF}
      OnClick = xmlMIClick
    end
    object expHTMLMI: TdxBarButton
      Caption = 'HTML ('#959#953' '#945#960#959#948#949#943#958#949#953#962' '#972#960#969#962' '#964#953#962' '#941#967#949#964#949' '#966#953#955#964#961#940#961#949#953' '#954#945#953' '#959#956#945#948#959#960#959#953#942#963#949#953')'
      Category = 0
      Hint = 'HTML ('#959#953' '#945#960#959#948#949#943#958#949#953#962' '#972#960#969#962' '#964#953#962' '#941#967#949#964#949' '#966#953#955#964#961#940#961#949#953' '#954#945#953' '#959#956#945#948#959#960#959#953#942#963#949#953')'
      Visible = ivAlways
      Glyph.Data = {
        36050000424D3605000000000000360400002800000010000000100000000100
        08000000000000010000120B0000120B00000001000000000000D8CCC400D64F
        1A00F4EAE400F9B16300FAC88D00FBF4EC00FDECB800FEFCF800E5D3C800EAB3
        8600968D8900EAE1DC00F1E3DC00FCDAA900FEF8DA00FDEAC200E4DAD400D1B9
        A900F6873100C5AF9C00F0DFD600FDA64F00CFC4BD00D7794400D4936900ADAA
        A800BC9E8400EA9B6B00FEF3C300FCBF7900F1BCA100FFFFFF00202020002121
        2100222222002323230024242400252525002626260027272700282828002929
        29002A2A2A002B2B2B002C2C2C002D2D2D002E2E2E002F2F2F00303030003131
        3100323232003333330034343400353535003636360037373700383838003939
        39003A3A3A003B3B3B003C3C3C003D3D3D003E3E3E003F3F3F00404040004141
        4100424242004343430044444400454545004646460047474700484848004949
        49004A4A4A004B4B4B004C4C4C004D4D4D004E4E4E004F4F4F00505050005151
        5100525252005353530054545400555555005656560057575700585858005959
        59005A5A5A005B5B5B005C5C5C005D5D5D005E5E5E005F5F5F00606060006161
        6100626262006363630064646400656565006666660067676700686868006969
        69006A6A6A006B6B6B006C6C6C006D6D6D006E6E6E006F6F6F00707070007171
        7100727272007373730074747400757575007676760077777700787878007979
        79007A7A7A007B7B7B007C7C7C007D7D7D007E7E7E007F7F7F00808080008181
        8100828282008383830084848400858585008686860087878700888888008989
        89008A8A8A008B8B8B008C8C8C008D8D8D008E8E8E008F8F8F00909090009191
        9100929292009393930094949400959595009696960097979700989898009999
        99009A9A9A009B9B9B009C9C9C009D9D9D009E9E9E009F9F9F00A0A0A000A1A1
        A100A2A2A200A3A3A300A4A4A400A5A5A500A6A6A600A7A7A700A8A8A800A9A9
        A900AAAAAA00ABABAB00ACACAC00ADADAD00AEAEAE00AFAFAF00B0B0B000B1B1
        B100B2B2B200B3B3B300B4B4B400B5B5B500B6B6B600B7B7B700B8B8B800B9B9
        B900BABABA00BBBBBB00BCBCBC00BDBDBD00BEBEBE00BFBFBF00C0C0C000C1C1
        C100C2C2C200C3C3C300C4C4C400C5C5C500C6C6C600C7C7C700C8C8C800C9C9
        C900CACACA00CBCBCB00CCCCCC00CDCDCD00CECECE00CFCFCF00D0D0D000D1D1
        D100D2D2D200D3D3D300D4D4D400D5D5D500D6D6D600D7D7D700D8D8D800D9D9
        D900DADADA00DBDBDB00DCDCDC00DDDDDD00DEDEDE00DFDFDF00E0E0E000E1E1
        E100E2E2E200E3E3E300E4E4E400E5E5E500E6E6E600E7E7E700E8E8E800E9E9
        E900EAEAEA00EBEBEB00ECECEC00EDEDED00EEEEEE00EFEFEF00F0F0F000F1F1
        F100F2F2F200F3F3F300F4F4F400F5F5F500F6F6F600F7F7F700F8F8F800F9F9
        F900FAFAFA00FBFBFB00FCFCFC00FDFDFD00FEFEFE0000000000FEFEFEFE190A
        0A0A0A0A0A0A0A0A0A19FEFEFE19050B0B02050502020202050AFEFEFE0A1717
        1813000202020216020AFE000904091E0D041A1602020C0B020A02180D0E0707
        0E0D031A10020C16020A1301040F0E0D1B09011716020C10020A1B01120D1C1D
        01120101110B0C16020A1B12030F0F1D12120101130B1408140A09031C0F041D
        0315120113100800140A0F1D0E0E060D1D15121700080800080AFE040D05071C
        04031209050C0816080AFEFE0D060E1C0415090205141611110AFEFEFE0D0404
        040D0707020800001110FEFEFE020207070707050C080500FEFEFEFEFE020507
        070707050C0800FEFEFEFEFEFE02100C0B0B0B100816FEFEFEFE}
      OnClick = htmlMIClick
    end
    object exportOfficialMI: TdxBarButton
      Caption = #917#958#945#947#969#947#942' '#947#953#945' '#964#959' '#965#960#959#965#961#947#949#943#959' '#927#953#954#959#957#959#956#953#954#974#957
      Category = 0
      Hint = #917#958#945#947#969#947#942' '#947#953#945' '#964#959' '#965#960#959#965#961#947#949#943#959' '#927#953#954#959#957#959#956#953#954#974#957
      Visible = ivNever
      OnClick = exportOfficialMIClick
    end
    object dxBarSeparator1: TdxBarSeparator
      Caption = #917#926#913#915#937#915#919' - '#917#960#943#963#951#956#945' '#963#964#959#953#967#949#943#945
      Category = 0
      Hint = #917#926#913#915#937#915#919' - '#917#960#943#963#951#956#945' '#963#964#959#953#967#949#943#945
      Visible = ivNever
    end
    object dxBarButton1: TdxBarButton
      Caption = 'Office2010Black'
      Category = 0
      Hint = 'Office2010Black'
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object dxBarButton2: TdxBarButton
      Caption = 'Office2010Blue'
      Category = 0
      Hint = 'Office2010Blue'
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object dxBarButton3: TdxBarButton
      Caption = 'Office2010Silver'
      Category = 0
      Hint = 'Office2010Silver'
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object btnChanges: TdxBarButton
      Caption = #913#955#955#945#947#941#962' '#945#957#940' '#941#954#948#959#963#951
      Category = 0
      Hint = #913#955#955#945#947#941#962' '#945#957#940' '#941#954#948#959#963#951
      Visible = ivAlways
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000000000000000
        000000000000000000000000000400000015000000230000001F000000130000
        000A000000030000000100000001000000000000000000000000000000000000
        00000000000000000000000000130000004B000000740000006D000000540000
        003C000000280000001A0000000A000000010000000000000000000000000000
        000000000000000000043F210F7DD9B6A1FF4F4042DC193460D1101A2BAF0000
        007D0B0D0F610000003200000011000000010000000000000000000000000000
        00000000000000000013934D22EABF9E99FF3C51C8FF79799BFF4B6EACF25E7F
        B0E75F7088BD0D0E0F4400000022000000160000000900000001000000000000
        0000000000033F210E7BAD5F24FFC78C5AFFD3AA8EFFF4EAE4FFA97657F71F10
        07BF0000008D0000006B000000620000004E0000002000000004000000000000
        00000000000F985320E9B66E36FFFAE3B1FFF4D59AFFCC9664FFE8D1BFFFC195
        75FB391E10D4212830E0100F11D1080C118A0000002F00000005000000000000
        00032C19095DB4691AFFE6C399FFFAE3B1FFF9DEA1FFF8D990FFCC9452FFB191
        82FF4B4E5AFF7795B6FF6C86A6FA0D151F9A000000410000000D000000000000
        000F8C560FD2C4874AFFFBE8C0FFFAE3B1FFF7DB9FFFE0BB7EFF9E6D52FF754A
        52FF4D8CCCFF8DB8E3FF7D5438EA0B0602AD0000006C00000020000000032F1E
        045DBB7418FFF1DABAFFFBE8C0FFF4DAABFFD9B484FF8E756EFF70555DFF9452
        1BFFAD853AFFD3A331FFD5A150FFB28429F10000007C000000260000000F9969
        0BD2D4A572FFFBEDCFFFF5E0B9FFD6B28CFF8F5040FF8A4725FF89A3BBFFCBA7
        6DFFEEC24DFFF4C73BFFEDBB22FFD2972AFF000000520000001534240458CD8D
        20FFF7EAD3FFF9EACDFFDBBC9CFF94573EFF96531AFFB26E0DFFCA8C39FFEDC3
        66FFF5CC55FFF4C73BFFE4AD1FFF4B3307960000001E00000004A67906C9E0BD
        91FFFCF3DFFFDCC1A7FF7C716FFFA4610FFFBF7C1CFFD39C5EFFF0CA7BFFF5CF
        6CFFF5CC55FFE5B12DFF4C3406940000001E0000000400000000CE9331FFFEF9
        EEFFEAD9C6FFB07A42FF7F8783FFBA9B6DFFDDB184FFF6D68FFFF6D47EFFF5CF
        6CFFE6B53CFF4D35058E0000001900000004000000000000000050300973BA93
        5DE2AD8753FFC07D1EFFD19961FFDFCBB6FFF9E0A6FFF8D990FFF6D47EFFD19F
        32F8271B02690000001400000002000000000000000000000000000000000000
        00007B8D9EF0CBA383F2DBAD6EFFF7DDA7FFF9DEA1FFF8D990FFD2A239F6281C
        0366000000140000000200000000000000000000000000000000000000000000
        0000171B1F21202020240D0901155A3D0674C69C43E2C79C40E4281C034B0000
        000F000000020000000000000000000000000000000000000000}
      OnClick = btnChangesClick
    end
    object dxBarSubItem3: TdxBarSubItem
      Caption = 'Online Video Tutorials'
      Category = 0
      Visible = ivAlways
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000120B0000120B00000000000000000000FFFFFF030303
        0303030303030303030203030203030202030303030303030303030203030303
        03020303030302030303FFFFFF0606060D0D0D0D0D0D0D0E0D0D0D0D0D0D0D0D
        0D0D0E0D0D0D0D0D0E0D0E0D0D0D0D0D0D0E0D0D0E0E0E060606FFFFFF0B0C0C
        1616161616161616161616161616161616161616161616161616161616161616
        161616161717170C0B0CFFFFFF1211121C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C
        1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1D1D1D121211FFFFFF171717
        2424242424242424242424242424242424242424242424242424242424242424
        24242424252525171717FFFFFF1D1D1D2C2C2C2C2C2C2C2C2C2C2C2C2C2C2C2C
        2C2C2C2C2C2C2C2C2C2C2C2C2C2C2C2C2C2C2C2C2D2D2D1D1D1CFFFFFF212122
        3333333434343535353535353434343333333434343535353535353434343434
        34343434373737212121FFFFFF25252525252525252525252525252525252525
        2525252525252525252525252525252525252525252525252525484848313131
        AEAEAE8383831212120808084D4D4DA5A5A55C5C5C0909090E0E0E6060609898
        98404040090909272727292929424242858585FFFFFFADADAD212121282828B0
        B0B0F1F1F17A7A7A191919393939BBBBBBD0D0D05151512020203636363F3F3F
        0F0F0F696969BEBEBE8A8A8A5E5E5E5D5D5DC0C0C0CFCFCF6868685454546464
        64BFBFBFADADAD6A6A6A444444585858151515707070BFBFBFB4B4B4B9B9B9DF
        DFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF393939535353
        AFAFAFFFFFFFA5A5A52A2A2A0E0E0E3F3F3FA8A8A8ADADAD8787879E9E9EC2C2
        C2E0E0E0EEEEEEFAFAFABDBDBDABABABB5B5B59393933B3B3B2222226B6B6BE2
        E2E2DFDFDF6464641E1E1E262626848484CDCDCD9A9A9A707070FFFFFFFFFFFF
        FFFFFFFCFCFCEDEDEDD6D6D6C2C2C2B6B6B68888885A5A5A4D4D4D9D9D9DEBEB
        EBA7A7A73C3C3C505050FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFF6F6F6E5E5E5D8D8D8BFBFBF9999996E6E6E9F9F9F}
      ItemLinks = <
        item
          Visible = True
          ItemName = 'video1MI'
        end
        item
          Visible = True
          ItemName = 'video2MI'
        end
        item
          Visible = True
          ItemName = 'video3MI'
        end
        item
          Visible = True
          ItemName = 'video4MI'
        end>
    end
    object video1MI: TdxBarButton
      Caption = #915#949#957#953#954#972' Tutorial '#963#967#949#964#953#954#940' '#956#949' '#964#959' InvoiceX'
      Category = 0
      Hint = #915#949#957#953#954#972' Tutorial '#963#967#949#964#953#954#940' '#956#949' '#964#959' InvoiceX'
      Visible = ivAlways
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF
        0000000000000000000000000000000000000000000000000000000000000000
        00000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF
        000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000
        00000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF
        000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000
        00000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF00000000000000000000
        0000000000000000000000000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF
        000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000
        00000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF
        000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000
        00000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF
        0000000000000000000000000000000000000000000000000000000000000000
        00000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      OnClick = video1MIClick
    end
    object video2MI: TdxBarButton
      Caption = #928#974#962' '#957#945' '#954#940#957#949#964#949' IMPORT '#948#949#948#959#956#941#957#945' '#945#960#972' '#940#955#955#949#962' '#949#966#945#961#956#959#947#941#962
      Category = 0
      Hint = #928#974#962' '#957#945' '#954#940#957#949#964#949' IMPORT '#948#949#948#959#956#941#957#945' '#945#960#972' '#940#955#955#949#962' '#949#966#945#961#956#959#947#941#962
      Visible = ivAlways
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF
        0000000000000000000000000000000000000000000000000000000000000000
        00000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF
        000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000
        00000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF
        000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000
        00000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF00000000000000000000
        0000000000000000000000000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF
        000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000
        00000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF
        000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000
        00000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF
        0000000000000000000000000000000000000000000000000000000000000000
        00000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      OnClick = video2MIClick
    end
    object video3MI: TdxBarButton
      Caption = #917#960#945#957#945#966#959#961#940' Grid '#963#964#951#957' '#945#961#967#953#954#942' '#964#959#965' '#956#959#961#966#942
      Category = 0
      Hint = #917#960#945#957#945#966#959#961#940' Grid '#963#964#951#957' '#945#961#967#953#954#942' '#964#959#965' '#956#959#961#966#942
      Visible = ivAlways
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF
        0000000000000000000000000000000000000000000000000000000000000000
        00000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF
        000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000
        00000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF
        000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000
        00000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF00000000000000000000
        0000000000000000000000000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF
        000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000
        00000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF
        000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000
        00000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF
        0000000000000000000000000000000000000000000000000000000000000000
        00000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      OnClick = video3MIClick
    end
    object video4MI: TdxBarButton
      Caption = #924#945#950#953#954#941#962' '#917#961#947#945#963#943#949#962
      Category = 0
      Hint = #924#945#950#953#954#941#962' '#917#961#947#945#963#943#949#962
      Visible = ivAlways
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF
        0000000000000000000000000000000000000000000000000000000000000000
        00000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF
        000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000
        00000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF
        000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000
        00000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF00000000000000000000
        0000000000000000000000000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF
        000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000
        00000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF
        000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000
        00000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF
        0000000000000000000000000000000000000000000000000000000000000000
        00000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      OnClick = video4MIClick
    end
    object btnDonate: TdxBarButton
      Caption = #922#940#957#964#949' '#948#969#961#949#940' '#956#941#963#969' PayPal'
      Category = 0
      Hint = #922#940#957#964#949' '#948#969#961#949#940' '#956#941#963#969' PayPal'
      Visible = ivAlways
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000202E0000202E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFF
        FFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF0000FF0000FF0000FFFFFFFF0000FF0000FF0000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFF
        FFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFF
        FFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FF0000FF0000FFFF
        FFFF0000FF0000FF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF0000FF0000FF0000FFFFFFFF0000FF0000FF0000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FF0000FF0000FFFF
        FFFF0000FF0000FF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF0000FF0000FF0000FFFFFFFF0000FF0000FF0000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFF
        FFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      OnClick = btnDonateClick
    end
    object expPDFMI: TdxBarButton
      Caption = 'PDF ('#959#953' '#945#960#959#948#949#943#958#949#953#962' '#972#960#969#962' '#964#953#962' '#941#967#949#964#949' '#966#953#955#964#961#940#961#949#953' '#954#945#953' '#959#956#945#948#959#960#959#953#942#963#949#953')'
      Category = 0
      Hint = 'PDF ('#959#953' '#945#960#959#948#949#943#958#949#953#962' '#972#960#969#962' '#964#953#962' '#941#967#949#964#949' '#966#953#955#964#961#940#961#949#953' '#954#945#953' '#959#956#945#948#959#960#959#953#942#963#949#953')'
      Visible = ivAlways
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000120B0000120B00000000000000000000FFF9FFFCF1FF
        FFFAFF1F132F0A002F14043410140F0C1403100A15120818170F0F1E19042623
        00211800160000321421FFF8FFFFFBFFFCF7FF0000128574B4D0C2FCE4E8F3D7
        E1DBD3D4E2E3E3F1E0E3E1D0D4C1CFD5B2F3F2D8F5E6EA150114F2F1F5FDFFFF
        FAFFFF0000118B7ECA766ABC9AA1C8E2F2FFD2E2F3B3C5D0CBE1DCE0F7E9DCEE
        E1CFDCDAE0E2FA00001EFEFFFAFBFFF6FAFFF400030CC8BFFF7A71C8757CB5B7
        C7ECC5D9F8B9D1E7C9E3EFA9C3CFB9CEE4C3D1F5A5ACDD0C1148FFFFF1FFFFEB
        FFFFE2000900D6D0FFBAB1FF8688C959629B8997CC8C9DD091A5D5A5B3ED9AA2
        EF767ACD4B4FA000004EFFF8EEFFFEE9FFFFE0030C00D2CCEFE2DAFFC3C2FA7C
        7EB89FA4E39FA7EA555E9E5F65AC4A499F7272C6A8ACED030A41FFF5FFFFF0F7
        FFFFE9090500E2D7EBDFD5F9D2CCEF9B97C1D1C9FF9991D77570ADCFC9FFDFD9
        FFD0D0F4DBE5E5000700FFEFFFFEECF9FFFFF5070400E5DFE4D4D0DCE7E7F3D5
        D1EE5F4F8F8C7BC4DACCFFDACFFDD4CBECDBD6DFE3EAC91F2C00FFFAFCFFFFFC
        FFFFF7000500CFD7CDDCE4DDD0DBD3DCDFEE6C5C9CB29EEBE5D5FFCDBFE3E7DA
        F0F0E6E6E8E5C6151800F4FBFFF4FBFFEDF2FF000017D9E3FBC2CEE0CFDDE9CD
        D2F1675AA8B3A0F5E0D5FFE2DCF5D7CEDBD6CECEF1EAD62C290A000030000037
        00005100005600004A00004000003700003B00005900004000001CDAE2EFCFD1
        DBF3F3F9DBD6D708040300003C32399A2320A52A22B51D19A82D2EAE242A951F
        248B29269A2C2B8B000B31BECEDBE0E4FFD5D4F6D1CBEE1A133600002B353F8C
        2A2A9C211AA0312AB32E2BB0282B9E3638A84241B522248200001FCAD9F90000
        2E00003800003705004700002000002400003C00004200005600004900004300
        004000003F000039000011C0D0E70E0A51392D87483B8B080046FFFDFFFBF7FF
        E9E7FB0D0827D6D3FAD7D6FFDCDFFFCDD3FECAD2F7CEDDEDDDF7E0B4C8BC1413
        3F40397200001CEFF2FFFFFFF6FFFFF7FFFFF906030000000600000A00000F00
        000F000008000F030010000013000E0E2C010026FAFEFFFAFFF7}
      OnClick = pdfMIClick
    end
    object N3: TdxBarButton
      Caption = #904#958#959#948#959#962
      Category = 1
      Visible = ivAlways
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        200000000000000400000000000000000000000000000000000001011F2C0203
        5E840203608704066287070A6687090D69870B106C870E136F870F1571870F15
        72871016728710167287101672871016728710166F8306082428010287C20609
        B8FF0C11BFFF1218C7FF1720CEFF1D27D6FF222EDCFF2533E1FF2634E2FF2330
        DEFF1F2AD8FF1923D1FF141BCAFF0E14C2FF080CBBFF101698BD00008DCE0506
        B5FF0A0EBDFF0F15C4FF141CCAFF1B24D1FF5158DCFF9397DEFFA6A8E0FF5960
        DCFF1C25D3FF161ECDFF1118C6FF0C11BFFF0709B8FF0D129BC900018ECE0203
        B2FF070AB9FF0C10BFFF2429C8FFDBDBEFFFFFFFFFFFEEEEF6FFE5E5F0FFFFFF
        FFFFDEDFF0FF2B30C8FF0E13C1FF090CBBFF0405B4FF0B0F99C901028FCE0000
        AFFF0305B4FF1215BBFFCBCCE8FFEFEFF6FF5255B9FF141ABCFF141BC1FF5D60
        BDFFF3F3F8FFCDCEE7FF1216BDFF0507B6FF0001B0FF0A0E98C9020391CE0000
        AFFF0000AFFF9696D3FFF7F7FBFF4041B4FF0B10BEFF0C11C0FF0D11C0FF0C10
        BFFF4A4CB9FFFAFAFCFF9798D3FF0001B0FF0000AFFF090D97C9030591CE0000
        AFFF0000AFFFE3E3F0FFB2B2D8FF0204B9FF0305BEFF0305C1FF0305C1FF0405
        BDFF0305B8FFABABD6FFEDEDF5FF0303AFFF0000AFFF090C95C9040693CE0000
        B7FF0000C6FFF0F0F6FF8A8ACEFF0000D6FF0000D8FF1919DAFF1212DAFF0000
        D7FF0000D6FF8B8BD3FFFFFFFFFF0606BEFF0000B4FF070A94C90304AAE00000
        D1FF0000D2FFCECEE6FFD3D3EAFF0909DAFF0202DDFFE6E6F3FFCBCBE9FF0000
        DCFF0000DAFFAEAEDFFFE8E8F2FF0000D0FF0000D0FF0406A3D80203B9EB0000
        D2FF0000D6FF6464CCFFFFFFFFFF9999DCFF0808E2FFFFFFFFFFE8E8F2FF0000
        E1FF6767DDFFFEFEFEFF7979C9FF0000D5FF0000D2FF0203B3E60304B9EB0000
        D4FF0000D9FF0101DAFF9E9ED4FFFFFFFFFF2121E1FFFFFFFFFFE8E8F2FF2D2D
        E1FFFFFFFFFFADADD7FF0707D4FF0000D8FF0000D3FF0203B3E60304BAEB0000
        D7FF0000DCFF0000E1FF0202E0FF3C3CC6FF0808E7FFFFFFFFFFE8E8F2FF1919
        DDFF6A6AC5FF0303DBFF0000DFFF0000DBFF0000D6FF0102B3E60305BCEB0000
        D9FF0000DEFF0000E3FF0000E8FF0000EDFF0505F0FFFCFCFDFFE2E2EFFF0000
        EFFF0000ECFF0000E7FF0000E2FF0000DDFF0000D8FF0101B2E60406BDEB0000
        DAFF0000E0FF0000E5FF0000EAFF0000F0FF0000F4FF7272CEFF5D5DCAFF0000
        F3FF0000EEFF0000E9FF0000E4FF0000DEFF0000D9FF0001B2E60507B6E30000
        DBFF0000E1FF0000E6FF0000ECFF0000F1FF0000F7FF0000FCFF0000FBFF0000
        F5FF0000F0FF0000EAFF0000E5FF0000DFFF0000DAFF0000A8DD080B4452090D
        98B80A0D9DBC080C9FBC070B9FBC070AA1BD0608A1BD0507A2BD0407A1BD0405
        9EBD03049CBE020399BE010297BE010194BE00018CB90000364D}
      OnClick = N3Click
    end
    object Skin1: TdxBarButton
      Caption = #935#969#961#943#962' Skin'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        76060000424D7606000000000000360000002800000014000000140000000100
        2000000000004006000000000000000000000000000000000000000000000000
        00000000000000000000000000000202020335353554696969A8808080CCA0A0
        A0FFA0A0A0FF808080CC6B6B6BAB353535540202020300000000000000000000
        0000000000000000000000000000000000000000000000000000181818277E7E
        7EC9A0A0A0FFA0A0A0FFA1A1A1FFA0A0A0FFA0A0A0FFA0A0A0FFA0A0A0FFA0A0
        A0FF7F7F7FC91A1A1A2A00000000000000000000000000000000000000000000
        0000020202034D4D4D7B9C9C9CF9A0A0A0FFA0A0A0FFA0A0A0FFA0A0A0FFA0A0
        A0FFA0A0A0FFA0A0A0FFA0A0A0FFA0A0A0FFA0A0A0FF9C9C9CF94D4D4D7B0202
        0203000000000000000000000000000000005959598DA1A1A1FFA0A0A0FFA1A1
        A1FFA0A0A0FFA1A1A1FFA0A0A0FFA0A0A0FFA0A0A0FFA0A0A0FFA1A1A1FFA1A1
        A1FFA0A0A0FFA1A1A1FFA0A0A0FF5858588D0000000000000000000000003737
        3757A0A0A0FFA0A0A0FFA1A1A1FFC0C0C0FFACACACFFA0A0A0FFA0A0A0FFA0A0
        A0FFA1A1A1FFA0A0A0FFA0A0A0FFA9A9A9FFC2C2C2FFA2A2A2FFA0A0A0FFA0A0
        A0FF373737570000000004040406898989DBA0A0A0FFA0A0A0FFB7B7B7FFD2D2
        D2FFD0D0D0FFACACACFFA1A1A1FFA0A0A0FFA0A0A0FFA0A0A0FFA9A9A9FFCECE
        CEFFD2D2D2FFBEBEBEFFA0A0A0FFA1A1A1FF8B8B8BDE040404063939395AA0A0
        A0FFA0A0A0FFA0A0A0FFA1A1A1FFC2C2C2FFD2D2D2FFD0D0D0FFACACACFFA1A1
        A1FFA0A0A0FFA9A9A9FFCECECEFFD2D2D2FFC7C7C7FFA3A3A3FFA0A0A0FFA0A0
        A0FFA0A0A0FF3838385A686868A5A1A1A1FFA0A0A0FFA0A0A0FFA1A1A1FFA1A1
        A1FFC2C2C2FFD2D2D2FFD0D0D0FFACACACFFAAAAAAFFCECECEFFD2D2D2FFC6C6
        C6FFA3A3A3FFA1A1A1FFA1A1A1FFA0A0A0FFA0A0A0FF686868A5888888D8A0A0
        A0FFA0A0A0FFA0A0A0FFA1A1A1FFA0A0A0FFA1A1A1FFC2C2C2FFD2D2D2FFD0D0
        D0FFCFCFCFFFD2D2D2FFC6C6C6FFA2A2A2FFA1A1A1FFA1A1A1FFA0A0A0FFA0A0
        A0FFA1A1A1FF888888D8979797F0A1A1A1FFA0A0A0FFA0A0A0FFA1A1A1FFA0A0
        A0FFA0A0A0FFA1A1A1FFC5C5C5FFD2D2D2FFD2D2D2FFC9C9C9FFA2A2A2FFA0A0
        A0FFA0A0A0FFA0A0A0FFA0A0A0FFA0A0A0FFA0A0A0FF989898F3979797F0A1A1
        A1FFA1A1A1FFA0A0A0FFA0A0A0FFA0A0A0FFA0A0A0FFA9A9A9FFCDCDCDFFD2D2
        D2FFD2D2D2FFD0D0D0FFACACACFFA0A0A0FFA1A1A1FFA0A0A0FFA1A1A1FFA0A0
        A0FFA1A1A1FF9C9C9CF9888888D8A0A0A0FFA0A0A0FFA0A0A0FFA0A0A0FFA0A0
        A0FFA9A9A9FFCECECEFFD2D2D2FFC6C6C6FFC2C2C2FFD2D2D2FFD0D0D0FFADAD
        ADFFA0A0A0FFA1A1A1FFA0A0A0FFA0A0A0FFA0A0A0FF8B8B8BDE686868A5A0A0
        A0FFA0A0A0FFA0A0A0FFA1A1A1FFA9A9A9FFCDCDCDFFD2D2D2FFC6C6C6FFA2A2
        A2FFA1A1A1FFC2C2C2FFD2D2D2FFD0D0D0FFACACACFFA0A0A0FFA0A0A0FFA0A0
        A0FFA0A0A0FF686868A53838385AA0A0A0FFA0A0A0FFA0A0A0FFA9A9A9FFCECE
        CEFFD2D2D2FFC6C6C6FFA2A2A2FFA0A0A0FFA1A1A1FFA1A1A1FFC2C2C2FFD2D2
        D2FFD0D0D0FFADADADFFA0A0A0FFA0A0A0FFA1A1A1FF3838385A040404068F8F
        8FE4A0A0A0FFA1A1A1FFB0B0B0FFD1D1D1FFC6C6C6FFA2A2A2FFA0A0A0FFA0A0
        A0FFA0A0A0FFA0A0A0FFA1A1A1FFC2C2C2FFD2D2D2FFB7B7B7FFA0A0A0FFA1A1
        A1FF898989DB040404060000000037373757A1A1A1FFA0A0A0FFA0A0A0FFAFAF
        AFFFA2A2A2FFA0A0A0FFA0A0A0FFA0A0A0FFA0A0A0FFA0A0A0FFA0A0A0FFA1A1
        A1FFAFAFAFFFA0A0A0FFA0A0A0FFA0A0A0FF3737375700000000000000000000
        00005757578AA0A0A0FFA0A0A0FFA0A0A0FFA0A0A0FFA0A0A0FFA0A0A0FFA0A0
        A0FFA0A0A0FFA0A0A0FFA1A1A1FFA1A1A1FFA0A0A0FFA1A1A1FFA0A0A0FF5757
        578A0000000000000000000000000000000000000000484848729B9B9BF6A0A0
        A0FFA1A1A1FFA0A0A0FFA0A0A0FFA0A0A0FFA0A0A0FFA0A0A0FFA1A1A1FFA0A0
        A0FFA1A1A1FF9A9A9AF648484872000000000000000000000000000000000000
        00000000000000000000181818277F7F7FC9A0A0A0FFA0A0A0FFA1A1A1FFA0A0
        A0FFA1A1A1FFA0A0A0FFA0A0A0FFA0A0A0FF7F7F7FC918181827000000000000
        0000000000000000000000000000000000000000000000000000000000000202
        020333333351686868A5808080CC9E9E9EFC9E9E9EFC808080CC686868A53333
        3351020202030000000000000000000000000000000000000000}
      OnClick = Skin1Click
    end
    object Blue1: TdxBarButton
      Caption = 'Blue'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object Caramel1: TdxBarButton
      Caption = 'Caramel'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object Coffee1: TdxBarButton
      Caption = 'Coffee'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object Darkroom1: TdxBarButton
      Caption = 'Darkroom'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object DarkSide1: TdxBarButton
      Caption = 'DarkSide'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object Foggy1: TdxBarButton
      Caption = 'Foggy'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object GlassOceans1: TdxBarButton
      Caption = 'GlassOceans'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object iMaginary1: TdxBarButton
      Caption = 'iMaginary'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object Lilian1: TdxBarButton
      Caption = 'Lilian'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object LiquidSky1: TdxBarButton
      Caption = 'LiquidSky'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object LondonLiquidSky1: TdxBarButton
      Caption = 'LondonLiquidSky'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object McSkin1: TdxBarButton
      Caption = 'McSkin'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object MoneyTwins1: TdxBarButton
      Caption = 'MoneyTwins'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object Office2007Black1: TdxBarButton
      Caption = 'Office2007Black'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object Office2007Blue1: TdxBarButton
      Caption = 'Office2007Blue'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object Office2007Green1: TdxBarButton
      Caption = 'Office2007Green'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object Office2007Pink1: TdxBarButton
      Caption = 'Office2007Pink'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object Office2007Silver1: TdxBarButton
      Caption = 'Office2007Silver'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object Pumpkin1: TdxBarButton
      Caption = 'Pumpkin'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object Seven1: TdxBarButton
      Caption = 'Seven'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object Sharp1: TdxBarButton
      Caption = 'Sharp'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object Silver1: TdxBarButton
      Caption = 'Silver'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object Springtime1: TdxBarButton
      Caption = 'Springtime'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object Stardust1: TdxBarButton
      Caption = 'Stardust'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object Summer20081: TdxBarButton
      Caption = 'Summer2008'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object Valentine1: TdxBarButton
      Caption = 'Valentine'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object Xmas2008Blue1: TdxBarButton
      Caption = 'Xmas2008Blue'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object Black1: TdxBarButton
      Caption = 'Black'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object BluePrint1: TdxBarButton
      Caption = 'BluePrint'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object DevExpressDarkStyle1: TdxBarButton
      Caption = 'DevExpressDarkStyle'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object DevExpressStyle1: TdxBarButton
      Caption = 'DevExpressStyle'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object HighContrast1: TdxBarButton
      Caption = 'HighContrast'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object SevenClassic1: TdxBarButton
      Caption = 'SevenClassic'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object SharpPlus1: TdxBarButton
      Caption = 'SharpPlus'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object TheAsphaltWorld1: TdxBarButton
      Caption = 'TheAsphaltWorld'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object UserSkin1: TdxBarButton
      Caption = 'UserSkin'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object VS20101: TdxBarButton
      Caption = 'VS2010'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object WhitePrint1: TdxBarButton
      Caption = 'WhitePrint'
      Category = 2
      Visible = ivAlways
      Glyph.Data = {
        D6080000424DD608000000000000360000002800000018000000170000000100
        200000000000A008000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000432E2066B48866D8CCA887DAB79B
        82C8765F4D9E14110F2600000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B09080F6D462BA3CD8450FFF0BC8EFFFFEFD8FFFFFC
        EEFFFFE3C5FFB68866DC0D0C0B17000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000734D34A9B56A36FFD89460FFDBAB81FFD7BA9FFFECDC
        CDFFFDEDDEFFE2A576FF684A378C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000563E2F74905426FF9A7541FFA16E45FF956B51FFA98874FF9F75
        57FFCC9D77FFD89765FFB87142F8231F1D350000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001E1A1724874B29EDA95F2FFFAB7142FF79553AFF71503CFF715B4EFF6A4B
        3AFF895635FFC07F4FFFAE6330FF593D2EB61A2D386130525C89193C4C69070B
        0F1B000000000000000000000000000000000000000000000000000000000000
        000068514494884319FFB26432FFCE8958FFD5B79DFFC2A994FFA78C78FF7C60
        4FFF71492FFFB06F42FF9C5D36FF734D38FF4C99BAFF81F6FFFF66E6FFFF3AA6
        D9EF1A3345670000000000000000000000000000000000000000000000003833
        303F7D4426FCA3633BFFBC713FFFD5915FFFA77F61FF8D7561FF988573FFCEB5
        9DFFC69771FFA96D44FF8E5F42FF7F4D2FFF5B7580FF8ADFF6FF6BE3FCFF48CE
        FFFF25A4F5FF274F6F8D0000000000000000000000000000000000000000674D
        40A9925834FFD8A27BFFD5986CFFDB9D6DFFE9C5A5FFE8CCB3FFB7967CFFAC84
        64FFD39463FF99694CFF86583BFF78472CFF284156FF498DBBFF4EA4D2FF3FB6
        EEFF33B0FCFF208CDEFF283E53700000000000000000000000000A08080C7346
        2EE2B37145FFD18C5BFFD79565FFE1A474FFF9E5D0FFFFFFFDFFFEE4C6FFEEB9
        8CFFC08D69FF977C6FFF8E664FFF6B3D28FF223949FF5B83A0FF3B729EFF125E
        9AFF2C9DE8FF2796E6FF1963A5E00B0E121700000000000000000F0B091B7743
        27F2B16939FFB76D3DFFC17947FFE0A473FFFCF1E6FFFFF9F2FFF6D0A8FFE5A8
        77FFAE8871FFA08A7EFFAC8A73FF714530FF142130FF235782FF3177A7FF3684
        B4FF329FE3FF2998E7FF1874C0FF25405B7D0000000000000000231D1A2EA665
        3DFC7C4320FF723D1DFF743B1BFFC08B63FFFFF6E8FFFFEED8FFEDBD92FFC983
        51FF976E58FF94725DFFA77A5BFF784B33FF2D4758FF55A7C5FF63B9CCFF5ABC
        D9FF3BB7FDFF2797E7FF1A78C5FF185DA0F424282B2F000000006F5E5685E7A3
        74FFB37C58FF814B28FF8E5632FFCE9870FFFFE9CBFFFADBB9FFCE956BFF9655
        2EFF69483BFF735344FF8F5F41FF6D442FFF2E4D5EFF5FA9C0FF74CBDCFF6AC8
        E4FF3FB8FBFF3EABEFFF5CB5E4FF1D6BADFF3D56719C000000005235279AC388
        5FFFFFCFAAFFD58F66FFB96D42FFEABD96FFFFE9C7FFEAC19DFFB9754CFF804D
        2CFF472F26FF5A3E33FF805239FF4F372EFF3A6D89FF89E2FEFF99F6FEFF6BDE
        FCFF3CB9FAFF58C1F6FF6BCAF3FF4FA4D5FF194D86E00406080B5D3A27AD904F
        27FFBF7849FFC68053FFD99E72FFFAD5B0FFFFE5C2FFE9BE99FFC58159FFDDA5
        81FF997E72FFA78171FFBD7B57FF43393BFF2778ACFF6DDBFFFFAFF8FBFF7AE3
        FAFF39BAFCFF40B1FAFF45B0F3FF49ADEAFF1B5EA1FD05101C317D553DD99651
        26FFBE7340FFD99562FFF0BE91FFFDDAB5FFFFDEBAFFF5C99FFFDB9E71FFDCA5
        81FFC0A9A0FFC19682FF975B3CFF283346FF1A78C9FF58CDFFFF9EF4FCFF88E9
        FBFF37B5FAFF2A91E0FF1C78C4FF1871BFFF0F5FA5FF314355777F5A44C4A35A
        2CFFC77D4AFFE0A16FFFF3C59AFFFEDBB7FFFDD9B3FFF4C59AFFE9AC7AFFD18F
        5FFF9D725AFF80533DFF593423FF193A5AFF1068BEFF3DA8E7FF7FEEFDFF73E6
        FDFF46BAEFFF196FB5FF004DA3FF0E5BA3FF35AAD9FF284C6B940C0B0B0C4934
        2860A16741CEE6AA7AFFF9CEA4FFFFDDB7FFFDD5ACFFF4C293FFEAAE7CFFDE9D
        6BFF9E6C4EFF6B3D25FF4C3026FF063D71FF0A5CAAFF2886C8FF5ECFECFF67E5
        FEFF3AB1E7FF1D87CAFF3BA8DCFF81DEF4FF4BACDBFF1A4069A8000000000000
        0000070604092B1F1633654F3E6FB09278BCD9B18DE3ECB789FDE8AA79FFD58E
        5AFF8F522EFF69351AFF3C3636FF186CAAFF328FD7FF1C76C9FF57C4E5FF6CE9
        FFFF46C6FAFF2693D1FF45AED1FF55B3D3FF1862A1FF29507DCA000000000000
        000000000000000000000000000000000000070604093A2E2641634A3B6D5842
        35705445419A477280FF45A0BCFF5CB6E8FF4B91D1FF3084C7FF6ADCF3FF70EB
        FDFF53D4FDFF3CB7F8FF2D99DCFF237FC0FF1961A2FF1B4779CF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001A1D1F22297DB8F53397C6FF3D95C4FF42A0D1FF62D3F6FF75EDFCFF73ED
        FAFF5BDBFAFF44C1F9FF39AAE7FF2E8FCCFF1A65A6FF10427ADC000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000255B8EC23892CEFF5BC8EBFF6EE5FBFF6EE4FCFF76EEFBFF78F2
        FCFF66E5FCFF50CFFDFF3BB3EDFF298FCEFF256499DE3B455164000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000405C7691227BC2FF49AEE2FF6EE5F9FF78F3FCFF78F5FDFF76F5
        FCFF64E3F8FF45B4E0EF4484A8BD3D4F5B6907090B0E00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000028323C443679B1E63380B7E24A9FC7DF56ACC5D35394A5B5456D
        7A891C333E49040A0F1300000000000000000000000000000000}
      OnClick = Black1Click
    end
    object Backup1: TdxBarButton
      Caption = 'Backup ('#913#960#959#952#942#954#949#965#963#951' '#957#941#959#965' '#945#957#964#953#947#961#940#966#959#965')'
      Category = 3
      Visible = ivAlways
      Glyph.Data = {
        F6020000424DF60200000000000036000000280000000E000000100000000100
        180000000000C0020000C40E0000C40E00000000000000000000C8D0D4000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000084840084840000000000000000000000000000000000
        00C6C6C6C6C6C600000000848400000000000000000084840084840000000000
        00000000000000000000000000C6C6C6C6C6C600000000848400000000000000
        00008484008484000000000000000000000000000000000000C6C6C6C6C6C600
        0000008484000000000000000000848400848400000000000000000000000000
        0000000000000000000000000000008484000000000000000000848400848400
        8484008484008484008484008484008484008484008484008484008484000000
        0000000000008484008484000000000000000000000000000000000000000000
        0000000084840084840000000000000000008484000000C6C6C6C6C6C6C6C6C6
        C6C6C6C6C6C6C6C6C6C6C6C6C6C6C60000000084840000000000000000008484
        000000C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C60000000084
        840000000000000000008484000000C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6
        C6C6C6C6C6C6C60000000084840000000000000000008484000000C6C6C6C6C6
        C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C600000000848400000000000000
        00008484000000C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C600
        00000000000000000000000000008484000000C6C6C6C6C6C6C6C6C6C6C6C6C6
        C6C6C6C6C6C6C6C6C6C6C6000000C6C6C6000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D40000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D40000}
      OnClick = btnBackupClick
    end
    object Restore1: TdxBarButton
      Caption = 'Restore ('#917#960#945#957#940#954#964#951#963#951' '#945#960#959#952#951#954#949#965#956#941#957#959#965' '#945#957#964#953#947#961#940#966#959#965')'
      Category = 3
      Visible = ivAlways
      Glyph.Data = {
        A6020000424DA6020000000000003600000028000000100000000D0000000100
        18000000000070020000C40E0000C40E00000000000000000000C8D0D4000000
        000000000000000000000000000000000000000000000000000000000000C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D400000000000000848400848400848400848400
        8484008484008484008484008484000000C8D0D4C8D0D4C8D0D4C8D0D4000000
        00FFFF0000000084840084840084840084840084840084840084840084840084
        84000000C8D0D4C8D0D4C8D0D4000000FFFFFF00FFFF00000000848400848400
        8484008484008484008484008484008484008484000000C8D0D4C8D0D4000000
        00FFFFFFFFFF00FFFF0000000084840084840084840084840084840084840084
        84008484008484000000C8D0D4000000FFFFFF00FFFFFFFFFF00FFFF00000000
        0000000000000000000000000000000000000000000000000000C8D0D4000000
        00FFFFFFFFFF00FFFFFFFFFF00FFFFFFFFFF00FFFFFFFFFF00FFFF000000C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4000000FFFFFF00FFFFFFFFFF00FFFFFFFFFF00
        FFFFFFFFFF00FFFFFFFFFF000000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4000000
        00FFFFFFFFFF00FFFF000000000000000000000000000000000000000000C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4000000000000000000C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4000000000000000000C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4000000000000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4000000C8D0D4C8D0D4C8D0D4000000C8D0D4000000C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D40000000000000000
        00C8D0D4C8D0D4C8D0D4}
      OnClick = btnRestoreClick
    end
    object N6: TdxBarButton
      Caption = #902#948#949#953#945#963#956#945' '#946#940#963#951#962' '#948#949#948#959#956#941#957#969#957' ('#924#951#948#949#957#953#963#956#972#962' '#972#955#969#957' '#964#969#957' '#948#949#948#959#956#941#957#969#957')'
      Category = 3
      Visible = ivAlways
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C30E0000C30E00000000000000000000DCDCDCDCDCDC
        DCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDC
        DCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDCDCDCDCDCDCDCDCDCDCDCDCDCDCDC
        DCDCDC333333333333333333333333333333333333333333333333333333FFFF
        FFDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDC333333FFFFFFCCCCCCCCCCCC99
        9999999999999999666666333333FFFFFFDCDCDCDCDCDCDCDCDCDCDCDCDCDCDC
        DCDCDC333333FFFFFF999999CCCCCC666666999999333333666666333333FFFF
        FFDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDC333333FFFFFF999999CCCCCC66
        6666999999333333666666333333FFFFFFDCDCDCDCDCDCDCDCDCDCDCDCDCDCDC
        DCDCDC333333FFFFFF999999CCCCCC666666999999333333666666333333FFFF
        FFDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDC333333FFFFFF999999CCCCCC66
        6666999999333333666666333333FFFFFFDCDCDCDCDCDCDCDCDCDCDCDCDCDCDC
        DCDCDC333333FFFFFF999999CCCCCC666666999999333333666666333333FFFF
        FFDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDC333333FFFFFF999999CCCCCC66
        6666999999333333666666333333FFFFFFDCDCDCDCDCDCDCDCDCDCDCDCDCDCDC
        DCDCDC333333FFFFFFCCCCCCCCCCCC999999999999999999666666333333FFFF
        FFFFFFFFDCDCDCDCDCDCDCDCDCDCDCDC33333333333333333333333333333333
        3333333333333333333333333333333333FFFFFFDCDCDCDCDCDCDCDCDCDCDCDC
        333333CCCCCCCCCCCCCCCCCCCCCCCC9999999999999999999999996666663333
        33FFFFFFDCDCDCDCDCDCDCDCDCDCDCDC33333333333333333333333333333333
        3333333333333333333333333333333333DCDCDCDCDCDCDCDCDCDCDCDCDCDCDC
        DCDCDCDCDCDCDCDCDCDCDCDC333333333333333333DCDCDCDCDCDCDCDCDCDCDC
        DCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDC
        DCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDC}
      OnClick = btnClearDBClick
    end
    object N2: TdxBarSubItem
      Caption = #913#961#967#949#943#959'     '
      Category = 4
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'N3'
        end>
    end
    object N8: TdxBarSubItem
      Caption = #920#941#956#945#964#945' (Skins)     '
      Category = 4
      Visible = ivAlways
      Glyph.Data = {
        26060000424D2606000000000000360000002800000014000000130000000100
        200000000000F005000000000000000000000000000000000000000000000000
        000000000000000000000B0705105E3F2B87C79D7ADECEB499D59A816CB72C24
        1F4B000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000018131021815233B8D991
        5AFFF4CAA1FFFCF6E7FFFFFBE9FFD3A37DEF201A163200000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000706050971482BB0A36B36FFBE8355FFBA9073FFC1A28BFFE0C2AAFFE3A7
        78FF8C5E41B30202020300000000000000000000000000000000000000000000
        00000000000000000000000000000000000055403473995325FFA16D3CFF7C55
        38FF765645FF735747FF895939FFC3804FFFAB6435F4322F2F7115272E491630
        3B5304080A120000000000000000000000000000000000000000000000001D1A
        18227B4E33D39F501FFFCA8453FFC2A389FFAB937FFF8B7362FF714E37FFA869
        3DFF9B572EFF5F5754F962BFD5F46DE5F6F63896BFDA152D3D57000000000000
        0000000000000000000001010101625146898D4F2CFFBE794AFFD4905EFFB38D
        6DFF99806CFFAE9883FFCDA17CFFAB7148FF8C5737FF6C4C3BFF65A1BAFF74E3
        FFFF4BD3FFFF28A2EEFA1F3E58720000000000000000000000001713111D7A4D
        34DFBF8358FFDFA376FFDEA071FFF5D8BDFFEEDAC8FFC7A282FFCC9265FF9F74
        59FF87553AFF55392DFF356283FF4F92BFFF2E91CBFF2BA6F4FF237FC8EF1C28
        34460000000000000000201814358A4F2CFFC57A47FFCE8451FFE2A574FFFCF1
        E7FFFFFAF1FFFDCD9EFFC89572FF9F877BFF9D755EFF4D3025FF21435EFF376E
        9CFF226AA1FF2E9BE3FF1B89DBFF23517BAA0506070800000000312620489655
        2EFF814520FF7E421EFFBF8860FFFFF7EBFFFDE7CDFFE3A471FFAA7757FF987A
        67FFA77A5BFF593C2FFF2C6280FF53A8C6FF55B2D2FF38AFF5FF218DDDFF0B60
        ADFF36424C5B000000007762558FDA996EFF986543FF814825FFC59069FFFFEC
        CFFFF1CAA5FFB16F43FF6D422DFF6D4E40FF8B593AFF543E35FF448097FF77D0
        E1FF6CCEE8FF3EB7FAFF4CB4EFFF388EC6FF385F85C0070A0C115A3927A7CF96
        6DFFE9AC84FFC1754AFFE7B892FFFFE7C5FFDCA882FFA86740FF654636FF6549
        3EFF875439FF3D3E43FF59ADD0FFA2FDFFFF75E3FCFF3EBBFBFF5DC6FBFF62C3
        F0FF276CA9FA0B131D2F6A422BC6995327FFC47A49FFDEA375FFFBD6B0FFFFE2
        BEFFE1AE85FFDB9A71FFC4A594FFBD9685FFA56747FF25405FFF3AACE9FF9EF7
        FFFF8CEAFAFF38B6FCFF35A1EEFF379CE3FF1B6BB2FF14283F68835539DBA75B
        2BFFD38B57FFEFBB8DFFFEDBB6FFFEDBB4FFF2BF92FFDF9E6EFFB98E75FF9D71
        5DFF633C29FF114174FF2693E5FF7BE7FEFF83EEFEFF3EB1EDFF1368B7FF004B
        A0FF187CBCFF3459759B2E292634724A319BCC8A5BF3F4C599FFFFDDB7FFFDD5
        ACFFF2BD8DFFE8A671FFB07753FF6F3E25FF3F2C28FF024382FF1570BFFF53C0
        E6FF6AE7FEFF38ACE2FF1A7FC2FF51B0D9FF5ABFE4FF204E77AF000000000201
        01021C151022513D2F5A987E68A2C39D7DCFDDAA7FEEDA9B6BF49E613BF76739
        20FE384852FF2680C2FF247ACCFF43A8D9FF6CE9FEFF41BFF5FF2D9BD4FF53B9
        D9FF2E7FB4FF214B7BCB00000000000000000000000000000000000000000303
        02042E241E333F2E24494239356344758DED41A3C7FF51A2D9FF3D8CCCFF5ECC
        ECFF73EEFDFF53D4FDFF39B2F3FF298FD1FF1B69AAFF0F427BDC000000000000
        000000000000000000000000000000000000000000000000000000000000306D
        9DC03394CAFF4DB2D8FF61D2F3FF75EDFEFF77F0FBFF5FE0FCFF46C4FAFF2D9F
        DEFF186EB1FF325477B500000000000000000000000000000000000000000000
        000000000000000000000000000034526F8B2D87CAFF5ECFF2FF78F3FFFF78F4
        FDFF77F7FDFF63E4FBFF4ABDEDFA4A86AAC12A3E4F650F121419000000000000
        0000000000000000000000000000000000000000000000000000000000002631
        3B443275ADE03B8BBDE055AFCBDA59A7B7C44D808C9B2241505E0A19232B070A
        0C0E0101010100000000}
      ItemLinks = <
        item
          Visible = True
          ItemName = 'Skin1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'Black1'
        end
        item
          Visible = True
          ItemName = 'Blue1'
        end
        item
          Visible = True
          ItemName = 'BluePrint1'
        end
        item
          Visible = True
          ItemName = 'Caramel1'
        end
        item
          Visible = True
          ItemName = 'Coffee1'
        end
        item
          Visible = True
          ItemName = 'Darkroom1'
        end
        item
          Visible = True
          ItemName = 'DarkSide1'
        end
        item
          Visible = True
          ItemName = 'DevExpressDarkStyle1'
        end
        item
          Visible = True
          ItemName = 'DevExpressStyle1'
        end
        item
          Visible = True
          ItemName = 'Foggy1'
        end
        item
          Visible = True
          ItemName = 'GlassOceans1'
        end
        item
          Visible = True
          ItemName = 'HighContrast1'
        end
        item
          Visible = True
          ItemName = 'iMaginary1'
        end
        item
          Visible = True
          ItemName = 'Lilian1'
        end
        item
          Visible = True
          ItemName = 'LiquidSky1'
        end
        item
          Visible = True
          ItemName = 'LondonLiquidSky1'
        end
        item
          Visible = True
          ItemName = 'McSkin1'
        end
        item
          Visible = True
          ItemName = 'MoneyTwins1'
        end
        item
          Visible = True
          ItemName = 'Office2007Black1'
        end
        item
          Visible = True
          ItemName = 'Office2007Blue1'
        end
        item
          Visible = True
          ItemName = 'Office2007Green1'
        end
        item
          Visible = True
          ItemName = 'Office2007Pink1'
        end
        item
          Visible = True
          ItemName = 'Office2007Silver1'
        end
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end
        item
          Visible = True
          ItemName = 'Pumpkin1'
        end
        item
          Visible = True
          ItemName = 'Seven1'
        end
        item
          Visible = True
          ItemName = 'SevenClassic1'
        end
        item
          Visible = True
          ItemName = 'Sharp1'
        end
        item
          Visible = True
          ItemName = 'SharpPlus1'
        end
        item
          Visible = True
          ItemName = 'Silver1'
        end
        item
          Visible = True
          ItemName = 'Springtime1'
        end
        item
          Visible = True
          ItemName = 'Stardust1'
        end
        item
          Visible = True
          ItemName = 'Summer20081'
        end
        item
          Visible = True
          ItemName = 'TheAsphaltWorld1'
        end
        item
          Visible = True
          ItemName = 'UserSkin1'
        end
        item
          Visible = True
          ItemName = 'Valentine1'
        end
        item
          Visible = True
          ItemName = 'VS20101'
        end
        item
          Visible = True
          ItemName = 'WhitePrint1'
        end
        item
          Visible = True
          ItemName = 'Xmas2008Blue1'
        end>
    end
    object N4: TdxBarSubItem
      Caption = #913#957#964#943#947#961#945#966#945' '#945#963#966#945#955#949#943#945#962'     '
      Category = 4
      Visible = ivAlways
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000004800FF004200FF004400FF004900FF0000000000000000000000000000
        00004C4136FF4C4136FF4C4136FF4C4136FF4C4136FFB5B1ACFFFFFFFFFFFFFF
        FFFF005D00FF00FFFFFF1DBA2FFF005D00FF000000000000000000000000786E
        66FFF9F6F3FFFDFCFBFFFCFCFBFFF3F0ECFFD6CBC0FFFFFFFFFF004800FF0040
        00FF005D00FF3ED550FF1BB532FF005D00FF004400FF004900FF7E746BFFF9F6
        F3FFF9F6F3FFFDFCFBFFFCFCFBFFF3F0ECFFD6CBC0FFFFFFFFFF005D00FF00FF
        FFFF11A103FF0EA20EFF0EA618FF0FAA1BFF11AE17FF005D00FF7E746BFFF9F6
        F3FFFFFFFFFFFEFEFDFFFEFDFCFFFAF7F5FFEFE8E2FFFFFFFFFF005D00FF00FF
        FFFF00FFFFFF00FFFFFF1BAF28FF23BA27FF00FFFFFF005D00FF847A70FFF9F6
        F3FF9D9084FF9D9084FF9D9084FF9D9084FF9D9084FFFFFFFFFF005D00FF005D
        00FF005D00FF00FFFFFF1EAD1CFF005D00FF005D00FF005D00FF877C73FF9D90
        84FFF9F6F3FFFBFAF9FFF7F5F3FFEBE5E0FFD6CBC0FFE6E0DAFFFFFFFFFFFFFF
        FFFF005D00FF00FFFFFF00FFFFFF005D00FF00000000000000008D8278FFF9F6
        F3FFF9F6F3FFFBFAF9FFF7F5F3FFEBE5E0FFD6CBC0FFC2B4A6FFCCBFB3FFFFFF
        FFFF005D00FF005D00FF005D00FF005D00FF00000000000000008D8278FFF9F6
        F3FFFFFFFFFFFEFDFCFFFCFAF8FFF7F3F0FFEFE8E2FFD0C6BCFFD0C6BCFFEFEB
        E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000093887DFFF9F6
        F3FF9D9084FF9D9084FF9D9084FF9D9084FF9D9084FF9D9084FF9D9084FF9D90
        84FF9D9084FF9D9084FFD2C9BFFF93887DFF0000000000000000968A80FF9D90
        84FFF9F6F3FFFAF8F6FFF0ECE8FFE3DBD3FFD6CBC0FFC2B4A6FFCCBFB3FFE6DE
        D6FFECE5DFFFDFD7CFFF9D9084FF968A80FF00000000000000009C9085FFF9F6
        F3FFF9F6F3FFFAF8F6FFF0ECE8FFE3DBD3FFD6CBC0FFC2B4A6FFCCBFB3FFE6DE
        D6FFECE5DFFFDFD7CFFFD2C9BFFF9C9085FF00000000000000009C9085FFF9F6
        F3FFFFFFFFFFFDFCFBFFF9F6F3FFF4EFEAFFEFE8E2FFD0C6BCFFD0C6BCFFD7CE
        C5FFDFD7CFFFE7E0D8FFD2C9BFFF9C9085FF00000000000000009C9085FFF9F6
        F3FF9D9084FF9D9084FF9D9084FF9D9084FF9D9084FF9D9084FF9D9084FF9D90
        84FF9D9084FF9D9084FFD2C9BFFF9C9085FF0000000000000000000000009D90
        84FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF9D9084FF000000000000000000000000000000000000
        00009D9084FF9D9084FF9D9084FF9D9084FF9D9084FF9D9084FF9D9084FF9D90
        84FF9D9084FF9D9084FF00000000000000000000000000000000}
      ItemLinks = <
        item
          Visible = True
          ItemName = 'Backup1'
        end
        item
          Visible = True
          ItemName = 'Restore1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'N6'
        end>
    end
    object N7: TdxBarButton
      Caption = #914#959#942#952#949#953#945
      Category = 4
      Visible = ivAlways
    end
  end
  object qrCorrect: TMyQuery
    Connection = EmbeddedConnection
    Left = 994
    Top = 400
  end
  object qrAnual: TMyQuery
    Connection = EmbeddedConnection
    SQL.Strings = (
      
        'select u.idUsr, usr.fullname, count(*) qty, sum(amount) total, c' +
        'ast(year(invDate) as unsigned) setYear, usr.anualGoal'
      'from usrinv u, usr, expenseType'
      'where u.idUsr=usr.idUsr'
      
        'and u.idexpensetype=expensetype.idexpensetype and expensetype.in' +
        'cluded is true'
      'group by u.idusr, year(invDate)'
      'order by year(invDate) desc, usr.fullname')
    OnCalcFields = qrAnualCalcFields
    Left = 433
    Top = 248
    object qrAnualidUsr: TLargeintField
      FieldName = 'idUsr'
      Origin = 'u.idUsr'
    end
    object qrAnualfullname: TWideStringField
      FieldName = 'fullname'
      Origin = 'usr.fullname'
      Size = 45
    end
    object qrAnualqty: TLargeintField
      FieldName = 'qty'
      Origin = 'qty'
    end
    object qrAnualtotal: TFloatField
      FieldName = 'total'
      Origin = 'total'
    end
    object qrAnuala: TFloatField
      FieldKind = fkCalculated
      FieldName = 'a'
      Calculated = True
    end
    object qrAnualb: TFloatField
      FieldKind = fkCalculated
      FieldName = 'b'
      Calculated = True
    end
    object qrAnualdesired: TFloatField
      FieldKind = fkCalculated
      FieldName = 'desired'
      Calculated = True
    end
    object qrAnualfinal: TFloatField
      FieldKind = fkCalculated
      FieldName = 'final'
      Calculated = True
    end
    object qrAnualanualGoal: TFloatField
      FieldName = 'anualGoal'
      Origin = 'usr.anualgoal'
    end
    object qrAnualsetYear: TLargeintField
      FieldName = 'setYear'
      Origin = 'setYear'
    end
  end
  object dsAnual: TMyDataSource
    DataSet = qrAnual
    Left = 481
    Top = 248
  end
  object qr2: TMyQuery
    Connection = EmbeddedConnection
    Left = 1114
    Top = 400
  end
  object zip: TZipForge
    ExtractCorruptedFiles = False
    CompressionLevel = clMax
    CompressionMode = 9
    CurrentVersion = '6.00 '
    SpanningMode = smNone
    SpanningOptions.AdvancedNaming = False
    SpanningOptions.VolumeSize = vsAutoDetect
    Options.FlushBuffers = True
    Options.OEMFileNames = True
    InMemory = False
    Zip64Mode = zmAuto
    UnicodeFilenames = True
    EncryptionMethod = caPkzipClassic
    Left = 708
    Top = 562
  end
  object ilExport: TImageList
    Left = 546
    Top = 169
    Bitmap = {
      494C010105000A00A80010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000002000000001002000000000000020
      00000000000000000000000000000000000000000000FCF1FF00FFFAFF001F13
      2F000A002F001404340010140F000C140300100A150012081800170F0F001E19
      0400262300002118000016000000321421000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFF8FF00FFFBFF00FCF7FF000000
      12008574B400D0C2FC00E4E8F300D7E1DB00D3D4E200E3E3F100E0E3E100D0D4
      C100CFD5B200F3F2D800F5E6EA00150114000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F2F1F500FDFFFF00FAFFFF000000
      11008B7ECA00766ABC009AA1C800E2F2FF00D2E2F300B3C5D000CBE1DC00E0F7
      E900DCEEE100CFDCDA00E0E2FA0000001E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FEFFFA00FBFFF600FAFFF4000003
      0C00C8BFFF007A71C800757CB500B7C7EC00C5D9F800B9D1E700C9E3EF00A9C3
      CF00B9CEE400C3D1F500A5ACDD000C1148000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFF100FFFFEB00FFFFE2000009
      0000D6D0FF00BAB1FF008688C90059629B008997CC008C9DD00091A5D500A5B3
      ED009AA2EF00767ACD004B4FA00000004E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFF8EE00FFFEE900FFFFE000030C
      0000D2CCEF00E2DAFF00C3C2FA007C7EB8009FA4E3009FA7EA00555E9E005F65
      AC004A499F007272C600A8ACED00030A41000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFF5FF00FFF0F700FFFFE9000905
      0000E2D7EB00DFD5F900D2CCEF009B97C100D1C9FF009991D7007570AD00CFC9
      FF00DFD9FF00D0D0F400DBE5E500000700000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFEFFF00FEECF900FFFFF5000704
      0000E5DFE400D4D0DC00E7E7F300D5D1EE005F4F8F008C7BC400DACCFF00DACF
      FD00D4CBEC00DBD6DF00E3EAC9001F2C00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFAFC00FFFFFC00FFFFF7000005
      0000CFD7CD00DCE4DD00D0DBD300DCDFEE006C5C9C00B29EEB00E5D5FF00CDBF
      E300E7DAF000F0E6E600E8E5C600151800000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F4FBFF00F4FBFF00EDF2FF000000
      1700D9E3FB00C2CEE000CFDDE900CDD2F100675AA800B3A0F500E0D5FF00E2DC
      F500D7CEDB00D6CECE00F1EAD6002C290A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000300000003700000051000000
      560000004A00000040000000370000003B00000059000000400000001C00DAE2
      EF00CFD1DB00F3F3F900DBD6D700080403000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000003C0032399A002320A5002A22
      B5001D19A8002D2EAE00242A95001F248B0029269A002C2B8B00000B3100BECE
      DB00E0E4FF00D5D4F600D1CBEE001A1336000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000002B00353F8C002A2A9C00211A
      A000312AB3002E2BB000282B9E003638A8004241B5002224820000001F00CAD9
      F90000002E000000380000003700050047000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000020000000240000003C000000
      42000000560000004900000043000000400000003F000000390000001100C0D0
      E7000E0A5100392D8700483B8B00080046000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFDFF00FBF7FF00E9E7FB000D08
      2700D6D3FA00D7D6FF00DCDFFF00CDD3FE00CAD2F700CEDDED00DDF7E000B4C8
      BC0014133F004039720000001C00EFF2FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFF600FFFFF700FFFFF9000603
      00000000060000000A0000000F0000000F0000000800000F0300001000000013
      00000E0E2C0001002600FAFEFF00FAFFF7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000660000006600000066
      0000006600000066000000660000006600000066000000660000006600000066
      000000660000006600000066000000660000000000000000000000000000BC9A
      8200BC957B00BC957B00BD937600BF917200BF8E6E00C18C6900C28A6500C388
      6100C4845D00C5825800C8805500C8805500000000000000000000000000BC9A
      8200BC957B00BC957B00BD937600BF917200BF8E6E00C18C6900C28A6500C388
      6100C4845D00C5825800C8805500C88055000000000000000000000000000000
      0000ADAAA800968D8900968D8900968D8900968D8900968D8900968D8900968D
      8900968D8900968D8900968D8900ADAAA8000066000000660000006600000066
      0000006600000066000000660000006600000066000000660000006600000066
      000000660000006600000066000000660000000000000000000000000000BE9D
      8500FCEDE300FCECE100FCEAE000FBE9DE00FBE8DC00FBE7DB00FBE6D900FBE5
      D700FAE4D600FAE3D400FAE2D200C6815900000000000000000000000000BE9D
      8500FCEDE300FCECE100FCEAE000FBE9DE00FBE8DC00FBE7DB00FBE6D900FBE5
      D700FAE4D600FAE3D400FAE2D200C6815900000000000000000000000000ADAA
      A800FBF4EC00EAE1DC00EAE1DC00F4EAE400FBF4EC00FBF4EC00F4EAE400F4EA
      E400F4EAE400F4EAE400FBF4EC00968D89000066000000660000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000066000000660000000000000000000000000000C0A0
      8800FCEEE500FCEDE300D6AE9000D6AE9000D6AE9000D6AE9000D6AE9000D6AE
      9000D6AE9000FAE4D600FAE3D400C4845E00000000000000000000000000C0A0
      8800FCEEE500FCEDE300D6AE9000D6AE9000D6AE9000D6AE9000D6AE9000D6AE
      9000D6AE9000FAE4D600FAE3D400C4845E00000000000000000000000000968D
      8900D7794400D7794400D4936900C5AF9C00D8CCC400F4EAE400F4EAE400F4EA
      E400F4EAE400CFC4BD00F4EAE400968D89000066000000660000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000660000006600000066
      000000660000006600000066000000660000000000000000000000000000C2A2
      8C00FCEFE600FCEEE500FCEDE300FCECE100FCEAE000FBE9DE00FBE8DC00FBE7
      DB00FBE6D900FBE5D700FAE4D600C3896200000000000000000000000000C2A2
      8C00FCEFE600FCEEE500FCEDE300FCECE100FCEAE000FBE9DE00FBE8DC00FBE7
      DB00FBE6D900FBE5D700FAE4D600C389620000000000D8CCC400EAB38600FAC8
      8D00EAB38600F1BCA100FCDAA900FAC88D00BC9E8400CFC4BD00F4EAE400F4EA
      E400F1E3DC00EAE1DC00F4EAE400968D89000066000000660000FFFFFF000066
      0000006600000066000000660000006600000066000000660000FFFFFF000066
      000000660000006600000066000000660000000000000000000000000000C4A5
      8F00FDF0E800FCEFE600D6AE9000D6AE9000D6AE9000D6AE9000D6AE9000D6AE
      9000D6AE9000FBE6D900FBE5D700C18B6700000000000000000000000000C4A5
      8F00FDF0E800FCEFE600D6AE9000D6AE9000D6AE9000D6AE9000D6AE9000D6AE
      9000D6AE9000FBE6D900FBE5D700C18B6700F4EAE400D4936900FCDAA900FEF8
      DA00FEFCF800FEFCF800FEF8DA00FCDAA900F9B16300BC9E8400E4DAD400F4EA
      E400F1E3DC00CFC4BD00F4EAE400968D89000066000000660000FFFFFF000066
      00000066000000660000006600000066000000660000FFFFFF00006600000066
      000000660000FFFFFF000066000000660000000000000000000000000000C6A8
      9200FDF1EA00FDF0E800FCEFE600FCEEE500FCEDE300FCECE100FCEAE000FBE9
      DE00FBE8DC00FBE7DB00FBE6D900C08E6C00000000000000000000000000C6A8
      9200FDF1EA00FDF0E800FCEFE600FCEEE500FCEDE300FCECE100FCEAE000FBE9
      DE00FBE8DC00FBE7DB00FBE6D900C08E6C00C5AF9C00D64F1A00FAC88D00FDEA
      C200FEF8DA00FCDAA900EA9B6B00EAB38600D64F1A00D7794400CFC4BD00F4EA
      E400F1E3DC00E4DAD400F4EAE400968D89000066000000660000FFFFFF000066
      000000660000006600000066000000660000FFFFFF0000660000006600000066
      000000660000FFFFFF000066000000660000000000000000000000000000C8AB
      9500FDF2EB00FDF1EA00D6AE9000D6AE9000D6AE9000D6AE9000D6AE9000D6AE
      9000D6AE9000FBE8DC00FBE7DB00BF907200000000000000000000000000C8AB
      9500FDF2EB00FDF1EA00D6AE9000D6AE9000D6AE9000D6AE9000D6AE9000D6AE
      9000D6AE9000FBE8DC00FBE7DB00BF907200EA9B6B00D64F1A00F6873100FCDA
      A900FEF3C300FCBF7900D64F1A00F6873100D64F1A00D64F1A00D1B9A900EAE1
      DC00F1E3DC00CFC4BD00F4EAE400968D89000066000000660000FFFFFF00FFFF
      FF00006600000066000000660000FFFFFF000066000000660000006600000066
      000000660000FFFFFF000066000000660000000000000000000000000000CAAD
      9900FDF3ED00FDF2EB00FDF1EA00FDF0E800FCEFE600FCEEE500FCEDE300FCEC
      E100FCEAE000FBE9DE00FBE8DC00BD937600000000000000000000000000CAAD
      9900FDF3ED00FDF2EB00FDF1EA00FDF0E800FCEFE600FCEEE500FCEDE300FCEC
      E100FCEAE000FBE9DE00FBE8DC00BD937600EA9B6B00F6873100F9B16300FDEA
      C200FDEAC200FCBF7900F6873100F6873100D64F1A00D64F1A00C5AF9C00EAE1
      DC00F0DFD600E5D3C800F0DFD600968D89000066000000660000FFFFFF00FFFF
      FF00FFFFFF0000660000FFFFFF0000660000006600000066000000660000FFFF
      FF00FFFFFF00FFFFFF000066000000660000009A0000009A0000009A0000009A
      0000009A0000009A0000009A0000009A0000009A0000009A0000009A0000009A
      0000009A0000FCEAE000FBE9DE00BB957A000066FF000066FF000066FF000066
      FF000066FF000066FF000066FF000066FF000066FF000066FF000066FF000066
      FF000066FF00FCEAE000FBE9DE00BB957A00EAB38600F9B16300FEF3C300FDEA
      C200FAC88D00FCBF7900F9B16300FDA64F00F6873100D64F1A00C5AF9C00E4DA
      D400E5D3C800D8CCC400F0DFD600968D89000066000000660000FFFFFF00FFFF
      FF0000660000FFFFFF0000660000006600000066000000660000006600000066
      0000FFFFFF00FFFFFF000066000000660000009A0000009A0000FDF4EF00FDF4
      EF00009A0000FDF4EF00FDF4EF00009A0000009A0000009A0000FDF4EF00009A
      0000009A0000FCECE100FCEAE000BA977E000066FF00FFFBF9000066FF00FFFB
      F9000066FF00FFFBF9000066FF000066FF000066FF00FFFBF9000066FF00FFFB
      F900FFFBF900FCECE100FCEAE000BA977E00FDEAC200FCBF7900FEF8DA00FEF8
      DA00FDECB800FCDAA900FCBF7900FDA64F00F6873100D7794400D8CCC400E5D3
      C800E5D3C800D8CCC400E5D3C800968D89000066000000660000FFFFFF000066
      0000FFFFFF000066000000660000006600000066000000660000006600000066
      000000660000FFFFFF000066000000660000009A0000FDF4EF00009A0000009A
      0000009A0000009A0000009A0000FDF4EF00009A0000FDF4EF00009A0000FDF4
      EF00009A0000FCEDE300FCECE100BA977E000066FF000066FF00FFFBF9000066
      FF000066FF00FFFBF9000066FF00FFFBF9000066FF00FFFBF9000066FF00FFFB
      F9000066FF00FCEDE300FCECE100BA977E0000000000FAC88D00FCDAA900FBF4
      EC00FEFCF800FEF3C300FAC88D00F9B16300F6873100EAB38600FBF4EC00F1E3
      DC00E5D3C800CFC4BD00E5D3C800968D89000066000000660000FFFFFF000066
      000000660000006600000066000000660000FFFFFF0000660000006600000066
      000000660000FFFFFF000066000000660000009A0000FDF4EF00009A0000009A
      0000009A0000FDF4EF00009A0000009A0000009A0000FDF4EF00009A0000FDF4
      EF00009A0000BA977E00BA977E00BA977E000066FF00FFFBF9000066FF00FFFB
      F9000066FF00FFFBF900FFFBF9000066FF00FFFBF900FFFBF9000066FF00FFFB
      F9000066FF00BA977E00BA977E00BA977E000000000000000000FCDAA900FDEC
      B800FEF8DA00FEF3C300FAC88D00FDA64F00EAB38600F4EAE400FBF4EC00F0DF
      D600CFC4BD00D1B9A900D1B9A900968D89000066000000660000FFFFFF000066
      0000006600000066000000660000FFFFFF00FFFFFF00FFFFFF00006600000066
      000000660000FFFFFF000066000000660000009A0000009A0000FDF4EF00FDF4
      EF00009A0000009A0000FDF4EF00FDF4EF00009A0000FDF4EF00009A0000FDF4
      EF00009A00000000000000000000BA977E000066FF00FFFBF9000066FF00FFFB
      F9000066FF00FFFBF9000066FF000066FF000066FF00FFFBF9000066FF00FFFB
      F9000066FF00FFFBF900FFFBF900BA977E00000000000000000000000000FCDA
      A900FAC88D00FAC88D00FAC88D00FCDAA900FEFCF800FEFCF800F4EAE400E5D3
      C800D8CCC400D8CCC400D1B9A900E4DAD4000066000000660000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000066000000660000009A0000009A0000009A0000009A
      0000009A0000009A0000009A0000009A0000009A0000009A0000009A0000009A
      0000009A000000000000BA977E00000000000066FF000066FF000066FF000066
      FF000066FF000066FF000066FF000066FF000066FF000066FF000066FF000066
      FF000066FF00FFFBF900BA977E0000000000000000000000000000000000F4EA
      E400F4EAE400FEFCF800FEFCF800FEFCF800FEFCF800FBF4EC00F1E3DC00E5D3
      C800FBF4EC00D8CCC40000000000000000000066000000660000006600000066
      0000006600000066000000660000006600000066000000660000006600000066
      000000660000006600000066000000660000000000000000000000000000D8C0
      AF00FFFBF900FEFAF700FEF9F500FEF8F400FEF7F200FEF6F000FDF4EF00BA97
      7E0000000000BA977E000000000000000000000000000000000000000000D8C0
      AF00FFFBF900FEFAF700FEF9F500FEF8F400FEF7F200FEF6F000FDF4EF00BA97
      7E00FFFBF900BA977E000000000000000000000000000000000000000000F4EA
      E400FBF4EC00FEFCF800FEFCF800FEFCF800FEFCF800FBF4EC00F1E3DC00E5D3
      C800D8CCC4000000000000000000000000000066000000660000006600000066
      0000006600000066000000660000006600000066000000660000006600000066
      000000660000006600000066000000660000000000000000000000000000DAC3
      B300D4BBA900D1B6A300CDB19D00C9AC9700C6A79100C2A28B00BE9D8500BA97
      7E00BA977E00000000000000000000000000000000000000000000000000DAC3
      B300D4BBA900D1B6A300CDB19D00C9AC9700C6A79100C2A28B00BE9D8500BA97
      7E00BA977E00000000000000000000000000000000000000000000000000F4EA
      E400E4DAD400F1E3DC00EAE1DC00EAE1DC00EAE1DC00E4DAD400E5D3C800CFC4
      BD0000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000200000000100010000000000000100000000000000000000
      000000000000000000000000FFFFFF0080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000008000E000E000F0000000E000E000E000
      0000E000E000E0000000E000E00080000000E000E00000000000E000E0000000
      0000E000E00000000000E000E000000000000000000000000000000000000000
      0000000000008000000000000000C000000000060000E000000000050001E003
      0000E00BE003E0070000E007E007E00F00000000000000000000000000000000
      000000000000}
  end
  object cxHintStyleController1: TcxHintStyleController
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpAuto
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    Left = 26
    Top = 473
  end
  object popupCMP: TPopupMenu
    OnPopup = popupCMPPopup
    Left = 1106
    Top = 514
    object miApplyExpenseType: TMenuItem
      Caption = 
        #917#966#945#961#956#959#947#942' '#964#973#960#959#965' '#949#958#972#948#969#957' '#963#949' '#972#955#949#962' '#964#953#962' '#945#960#959#948#949#943#958#949#953#962' '#960#959#965' '#941#967#959#965#957' '#942#948#951' '#949#954#948#959#952 +
        #949#943
      OnClick = miApplyExpenseTypeClick
    end
  end
  object tblExpenseMasterType: TMyTable
    TableName = 'expensemastertype'
    Connection = EmbeddedConnection
    BeforeEdit = tblExpenseMasterTypeBeforeEdit
    AfterPost = tblExpenseMasterTypeAfterPost
    BeforeDelete = tblExpenseMasterTypeBeforeDelete
    OnDeleteError = tblExpenseMasterTypeDeleteError
    OnPostError = tblExpenseMasterTypePostError
    Left = 1080
    Top = 101
  end
  object dsExpenseMasterType: TMyDataSource
    DataSet = tblExpenseMasterType
    Left = 1112
    Top = 101
  end
  object tblExpenseDetailType: TMyTable
    TableName = 'expensetype'
    MasterFields = 'idexpenseMasterType'
    DetailFields = 'idExpenseMasterType'
    MasterSource = dsExpenseMasterType
    Connection = EmbeddedConnection
    AfterInsert = tblExpenseDetailTypeAfterInsert
    BeforeEdit = tblExpenseDetailTypeBeforeEdit
    AfterPost = tblExpenseMasterTypeAfterPost
    BeforeDelete = tblExpenseDetailTypeBeforeDelete
    OnDeleteError = tblExpenseDetailTypeDeleteError
    OnPostError = tblExpenseDetailTypePostError
    Left = 1104
    Top = 157
  end
  object dsExpenseDetailType: TMyDataSource
    DataSet = tblExpenseDetailType
    Left = 1136
    Top = 157
  end
  object cxLocalizer1: TcxLocalizer
    FileName = '.\Common\localization_greek.res'
    StorageType = lstResource
    Left = 1136
    Top = 31
  end
end
