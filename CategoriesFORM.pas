unit CategoriesFORM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPC3Painter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxContainer, cxLabel, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, DBAccess, MyAccess, MemDS, cxNavigator,
  cxDBNavigator;

type
  TCategoriesFRM = class(TForm)
    tvExpenseMasterType: TcxGridDBTableView;
    lvlExpenseMasterType: TcxGridLevel;
    gridExpenseMasterType: TcxGrid;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    dsExpenseMasterType: TMyDataSource;
    tblExpenseMasterType: TMyTable;
    MyConnection1: TMyConnection;
    tvExpenseMasterTypeidexpenseMasterType: TcxGridDBColumn;
    tvExpenseMasterTypedescr: TcxGridDBColumn;
    cxDBNavigator1: TcxDBNavigator;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    gridExpenseType: TcxGrid;
    tvExpenseType: TcxGridDBTableView;
    lvlExpenseType: TcxGridLevel;
    dsExpenseDetailType: TMyDataSource;
    tblExpenseDetailType: TMyTable;
    cxDBNavigator2: TcxDBNavigator;
    tvExpenseTypeidexpenseType: TcxGridDBColumn;
    tvExpenseTypedescr: TcxGridDBColumn;
    tvExpenseTypeidExpenseMasterType: TcxGridDBColumn;
    tvExpenseTypeincluded: TcxGridDBColumn;
    cxLabel7: TcxLabel;
    cxLabel8: TcxLabel;
    cxLabel9: TcxLabel;
    cxLabel10: TcxLabel;
    cxStyleRepository1: TcxStyleRepository;
    styleBlack: TcxStyle;
    procedure tvExpenseMasterTypedescrCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CategoriesFRM: TCategoriesFRM;

implementation

uses Main;

{$R *.dfm}

procedure TCategoriesFRM.FormCreate(Sender: TObject);
begin
    tblExpenseMasterType.Open;
    tblExpenseDetailType.Open;
end;

procedure TCategoriesFRM.tvExpenseMasterTypedescrCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  AColumn: TcxCustomGridTableItem;
begin
    AColumn := (Sender as TcxGridDBTableView).GetColumnByFieldName('idexpenseMasterType');
    if AViewInfo.RecordViewInfo.GridRecord.Values[AColumn.Index]<=18 then
        ACanvas.Font.Color := clGrayText
    else
        ACanvas.Font.Color := clBlack;
end;

end.
