object CategoriesFRM: TCategoriesFRM
  Left = 596
  Top = 108
  Caption = #922#945#964#951#947#959#961#943#949#962' / '#933#960#959#954#945#964#951#947#959#961#943#949#962' '#949#958#972#948#969#957
  ClientHeight = 846
  ClientWidth = 939
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object gridExpenseMasterType: TcxGrid
    Left = 24
    Top = 244
    Width = 321
    Height = 517
    TabOrder = 0
    object tvExpenseMasterType: TcxGridDBTableView
      DataController.DataSource = dsExpenseMasterType
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsBehavior.IncSearch = True
      OptionsView.GroupByBox = False
      object tvExpenseMasterTypeidexpenseMasterType: TcxGridDBColumn
        DataBinding.FieldName = 'idexpenseMasterType'
        Visible = False
      end
      object tvExpenseMasterTypedescr: TcxGridDBColumn
        Caption = #914#945#963#953#954#942' '#954#945#964#951#947#959#961#943#945' '#949#958#972#948#969#957
        DataBinding.FieldName = 'descr'
        OnCustomDrawCell = tvExpenseMasterTypedescrCustomDrawCell
        HeaderAlignmentHorz = taCenter
        Styles.Content = styleBlack
      end
    end
    object lvlExpenseMasterType: TcxGridLevel
      GridView = tvExpenseMasterType
    end
  end
  object cxLabel1: TcxLabel
    Left = 24
    Top = 17
    Caption = 
      #917#948#974' '#956#960#959#961#949#943#964#949' '#957#945' '#960#961#959#963#952#941#963#949#964#949'/'#956#949#964#945#946#940#955#949#964#949'/'#945#966#945#953#961#941#963#949#964#949' '#964#953#962' '#948#953#954#941#962' '#963#945#962' '#954 +
      #945#964#951#947#959#961#943#949#962' '#954#945#953' '#965#960#959#954#945#964#951#947#959#961#943#949#962' '#949#958#972#948#969#957'.'
    Transparent = True
  end
  object cxLabel2: TcxLabel
    Left = 24
    Top = 33
    Caption = 
      #916#949#957' '#941#967#949#964#949' '#964#951' '#948#965#957#945#964#972#964#951#964#945' '#956#949#964#945#946#959#955#942#962' '#942' '#945#966#945#943#961#949#963#951#962' '#942#948#951' '#965#960#940#961#967#959#965#963#945#962' '#954#945#964 +
      #951#947#959#961#943#945#962'/'#965#960#959#954#945#964#951#947#959#961#943#945#962' '#949#958#972#948#969#957'.'
    Enabled = False
    Transparent = True
  end
  object cxDBNavigator1: TcxDBNavigator
    Left = 24
    Top = 219
    Width = 320
    Height = 25
    Buttons.Images = MainFRM.ImageList1
    Buttons.First.Visible = False
    Buttons.PriorPage.Visible = False
    Buttons.Prior.Visible = False
    Buttons.Next.Visible = False
    Buttons.NextPage.Visible = False
    Buttons.Last.Visible = False
    Buttons.Insert.Hint = #917#953#963#945#947#969#947#942' '#957#941#959#965' '#967#949#953#961#953#963#964#942
    Buttons.Insert.ImageIndex = 50
    Buttons.Append.Visible = False
    Buttons.Delete.Hint = #916#953#945#947#961#945#966#942' '#967#949#953#961#953#963#964#942
    Buttons.Delete.ImageIndex = 49
    Buttons.Edit.Hint = #924#949#964#945#946#959#955#942' '#967#949#953#961#953#963#964#942
    Buttons.Edit.ImageIndex = 64
    Buttons.Edit.Visible = True
    Buttons.Post.Hint = #913#960#959#952#942#954#949#965#963#951' '#960#961#959#963#952#942#954#951#962'/'#956#949#964#945#946#959#955#974#957
    Buttons.Post.ImageIndex = 0
    Buttons.Cancel.Hint = #913#954#973#961#969#963#951' '#960#961#959#963#952#942#954#951#962'/'#956#949#964#945#946#959#955#942#962
    Buttons.Cancel.ImageIndex = 39
    Buttons.Refresh.Visible = False
    Buttons.SaveBookmark.Visible = False
    Buttons.GotoBookmark.Visible = False
    Buttons.Filter.Visible = False
    DataSource = dsExpenseMasterType
    InfoPanel.Font.Charset = DEFAULT_CHARSET
    InfoPanel.Font.Color = clDefault
    InfoPanel.Font.Height = -11
    InfoPanel.Font.Name = 'Tahoma'
    InfoPanel.Font.Style = []
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
  end
  object cxLabel3: TcxLabel
    Left = 24
    Top = 56
    Caption = 
      #932#959' '#963#973#963#964#951#956#945' '#967#961#951#963#953#956#959#960#959#953#949#943' '#964#959' '#963#965#957#948#965#945#963#956#972' 3 '#967#961#969#956#940#964#969#957' '#947#953#945' '#957#945' '#963#945#962' '#946#959#951#952#942 +
      #963#949#953' '#963#964#953#962' '#949#957#941#961#947#949#953#941#962' '#963#945#962'.'
    Transparent = True
  end
  object cxLabel4: TcxLabel
    Left = 61
    Top = 72
    Caption = 
      '1) '#915#954#961#953' '#967#961#974#956#945' '#941#967#959#965#957' '#959#953' '#949#947#947#961#945#966#941#962' '#960#959#965' '#945#957#942#954#959#965#957' '#963#964#959' '#963#973#963#964#951#956#945' '#954#945#953' '#948#949' '#948 +
      #941#967#959#957#964#945#953' '#945#955#955#945#947#941#962' ('#945#957' '#949#960#953#967#949#953#961#942#963#949#964#949' '#945#955#955#945#947#942' '#948#949' '#952#945' '#947#943#957#949#953' '#964#943#960#959#964#945').'
    Transparent = True
  end
  object cxLabel5: TcxLabel
    Left = 61
    Top = 85
    Caption = '2) '#924#945#973#961#959' '#967#961#974#956#945' '#941#967#959#965#957' '#959#953' '#949#947#947#961#945#966#941#962' '#960#959#965' '#960#961#959#963#964#941#952#951#954#945#957' '#945#960#972' '#949#963#940#962'.'
    Transparent = True
  end
  object cxLabel6: TcxLabel
    Left = 61
    Top = 98
    Caption = 
      '3) '#922#972#954#954#953#957#959' '#967#961#974#956#945' '#941#967#959#965#957' '#959#953' '#949#947#947#961#945#966#941#962' '#960#959#965' '#960#961#959#963#964#941#952#951#954#945#957' '#945#960#972' '#949#963#940#962' '#954#945#953' ' +
      #941#967#959#965#957' '#941#963#964#974' '#954#945#953' '#956#953#945' '#945#960#972#948#949#953#958#951' '#960#959#965' '#948#941#953#967#949#957#949#953' '#963#949' '#945#965#964#972' '#964#959#957' '#964#973#960#972' '#949#958#972#948#959#965 +
      '.  '#932#945' '#941#958#959#948#945' '#945#965#964#940' '#948#949' '#948#941#967#959#957#964#945#953' '#948#953#945#947#961#945#966#942
    Transparent = True
  end
  object gridExpenseType: TcxGrid
    Left = 440
    Top = 244
    Width = 393
    Height = 517
    TabOrder = 8
    object tvExpenseType: TcxGridDBTableView
      DataController.DataSource = dsExpenseDetailType
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsBehavior.IncSearch = True
      OptionsView.GroupByBox = False
      object tvExpenseTypeidexpenseType: TcxGridDBColumn
        DataBinding.FieldName = 'idexpenseType'
        Visible = False
      end
      object tvExpenseTypedescr: TcxGridDBColumn
        Caption = #933#960#959#954#945#964#951#947#959#961#943#945' '#949#958#972#948#969#957
        DataBinding.FieldName = 'descr'
        HeaderAlignmentHorz = taCenter
        Styles.Content = styleBlack
        Width = 274
      end
      object tvExpenseTypeidExpenseMasterType: TcxGridDBColumn
        DataBinding.FieldName = 'idExpenseMasterType'
        Visible = False
      end
      object tvExpenseTypeincluded: TcxGridDBColumn
        Caption = #915#953#945' '#913#966#959#961#959#955#972#947#951#964#959
        DataBinding.FieldName = 'included'
        HeaderAlignmentHorz = taCenter
        Styles.Content = styleBlack
        Width = 94
      end
    end
    object lvlExpenseType: TcxGridLevel
      GridView = tvExpenseType
    end
  end
  object cxDBNavigator2: TcxDBNavigator
    Left = 440
    Top = 219
    Width = 390
    Height = 25
    Buttons.Images = MainFRM.ImageList1
    Buttons.First.Visible = False
    Buttons.PriorPage.Visible = False
    Buttons.Prior.Visible = False
    Buttons.Next.Visible = False
    Buttons.NextPage.Visible = False
    Buttons.Last.Visible = False
    Buttons.Insert.Hint = #917#953#963#945#947#969#947#942' '#957#941#959#965' '#967#949#953#961#953#963#964#942
    Buttons.Insert.ImageIndex = 50
    Buttons.Append.Visible = False
    Buttons.Delete.Hint = #916#953#945#947#961#945#966#942' '#967#949#953#961#953#963#964#942
    Buttons.Delete.ImageIndex = 49
    Buttons.Edit.Hint = #924#949#964#945#946#959#955#942' '#967#949#953#961#953#963#964#942
    Buttons.Edit.ImageIndex = 64
    Buttons.Edit.Visible = True
    Buttons.Post.Hint = #913#960#959#952#942#954#949#965#963#951' '#960#961#959#963#952#942#954#951#962'/'#956#949#964#945#946#959#955#974#957
    Buttons.Post.ImageIndex = 0
    Buttons.Cancel.Hint = #913#954#973#961#969#963#951' '#960#961#959#963#952#942#954#951#962'/'#956#949#964#945#946#959#955#942#962
    Buttons.Cancel.ImageIndex = 39
    Buttons.Refresh.Visible = False
    Buttons.SaveBookmark.Visible = False
    Buttons.GotoBookmark.Visible = False
    Buttons.Filter.Visible = False
    DataSource = dsExpenseDetailType
    InfoPanel.Font.Charset = DEFAULT_CHARSET
    InfoPanel.Font.Color = clDefault
    InfoPanel.Font.Height = -11
    InfoPanel.Font.Name = 'Tahoma'
    InfoPanel.Font.Style = []
    ParentShowHint = False
    ShowHint = True
    TabOrder = 9
  end
  object cxLabel7: TcxLabel
    Left = 24
    Top = 196
    Caption = #914#913#931#921#922#917#931' '#922#913#932#919#915#927#929#921#917#931' '#917#926#927#916#937#925
    ParentFont = False
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -13
    Style.Font.Name = 'Tahoma'
    Style.Font.Style = [fsBold]
    Style.IsFontAssigned = True
    Transparent = True
  end
  object cxLabel8: TcxLabel
    Left = 440
    Top = 196
    Caption = #933#928#927#922#913#932#919#915#927#929#921#917#931' '#917#926#927#916#937#925' ('#946#945#963#953#954#942#962' '#954#945#964#951#947#959#961#943#945#962')'
    ParentFont = False
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -13
    Style.Font.Name = 'Tahoma'
    Style.Font.Style = [fsBold]
    Style.IsFontAssigned = True
    Transparent = True
  end
  object cxLabel9: TcxLabel
    Left = 24
    Top = 132
    Caption = 
      '"'#915#953#945' '#945#966#959#961#959#955#972#947#951#964#959'" '#948#951#955#974#957#949#953' '#972#964#953' '#959' '#964#973#960#959#962' '#949#958#972#948#959#965' '#960#941#961#957#949#953' '#956#941#961#959#962' '#963#964#945' '#941#958 +
      #959#948#945' '#960#959#965' '#948#953#954#945#953#959#955#959#947#959#973#957#964#945#953' '#947#953#945' '#964#959' '#945#966#959#961#959#955#972#947#951#964#972' '#956#945#962
    Style.TextStyle = [fsBold]
    Transparent = True
  end
  object cxLabel10: TcxLabel
    Left = 24
    Top = 148
    Caption = 
      #913#957' '#948#949#957' '#949#943#957#945#953' '#964#963#949#954#945#961#953#963#956#941#957#959' '#964#972#964#949' '#952#949#969#961#949#943#964#945#953' '#969#962' '#964#973#960#959#962' '#949#958#972#948#959#965' '#960#959#965' '#948#949' ' +
      #963#967#949#964#943#950#949#964#945#953' '#956#949' '#964#959' '#945#966#959#961#959#955#972#947#951#964#959' ('#960'.'#967'. '#916#917#922#927').'
    Style.TextStyle = [fsBold]
    Transparent = True
  end
  object dsExpenseMasterType: TMyDataSource
    DataSet = tblExpenseMasterType
    Left = 168
    Top = 357
  end
  object tblExpenseMasterType: TMyTable
    TableName = 'expensemastertype'
    Connection = MainFRM.EmbeddedConnection
    Left = 136
    Top = 357
  end
  object MyConnection1: TMyConnection
    Database = 'invoices'
    Options.UseUnicode = True
    Options.Charset = 'utf8'
    Username = 'root'
    Password = '1'
    Server = 'localhost'
    LoginPrompt = False
    Left = 568
    Top = 8
  end
  object dsExpenseDetailType: TMyDataSource
    DataSet = tblExpenseDetailType
    Left = 576
    Top = 349
  end
  object tblExpenseDetailType: TMyTable
    TableName = 'expensetype'
    MasterFields = 'idexpenseMasterType'
    DetailFields = 'idExpenseMasterType'
    MasterSource = dsExpenseMasterType
    Connection = MainFRM.EmbeddedConnection
    Left = 544
    Top = 349
  end
  object cxStyleRepository1: TcxStyleRepository
    PixelsPerInch = 96
    object styleBlack: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = clWhite
      TextColor = clBlack
    end
  end
end
