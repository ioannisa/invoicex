unit VideoWindowFORM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, OleCtrls, SHDocVw, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxLabel,
  FlashPlayerControl, cxImage, ShellAPI;

type
  TVideoWindowFRM = class(TForm)
    Panel1: TPanel;
    cxLabel1: TcxLabel;
    lblGetFlash: TcxLabel;
    lblReload: TcxLabel;
    lblTitle: TcxLabel;
    flash: TFlashPlayerControl;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    lblError1: TcxLabel;
    lblError2: TcxLabel;
    lblError3: TcxLabel;
    procedure FormActivate(Sender: TObject);
    procedure lblGetFlashClick(Sender: TObject);
    procedure lblReloadClick(Sender: TObject);
  private
    first: boolean;
    selectedURL: String;
    videoW, videoH: integer;
    procedure execute(what: String);
    procedure showConnectionProblem(showProblem: boolean);
  public
    Constructor Create(AOwner: TComponent; url, title: String; browserWidth, browserHeight: integer); overload;
  end;

var
  VideoWindowFRM: TVideoWindowFRM;

implementation

uses Main, wininet;

{$R *.dfm}

{ TVideoWindowFRM }



Function CheckUrl(url:string):boolean;
var
hSession, hfile, hRequest: hInternet;
dwindex,dwcodelen :dword;
dwcode:array[1..20] of char;
res : pchar;
begin
  if pos('http://',lowercase(url))=0 then
     url := 'http://'+url;
  Result := false;
  hSession := InternetOpen('InetURL:/1.0',
       INTERNET_OPEN_TYPE_PRECONFIG,nil, nil, 0);
  if assigned(hsession) then
    begin
      hfile := InternetOpenUrl(
           hsession,
           pchar(url),
           nil,
           0,
           INTERNET_FLAG_RELOAD,
           0);
      dwIndex  := 0;
      dwCodeLen := 10;
      HttpQueryInfo(hfile, HTTP_QUERY_STATUS_CODE,
              @dwcode, dwcodeLen, dwIndex);
      res := pchar(@dwcode);
      result:= (res ='200') or (res ='302');
      if assigned(hfile) then
        InternetCloseHandle(hfile);
      InternetCloseHandle(hsession);
    end;

end;

{procedure TPreviewForm.FormResize(Sender: TObject);
var
  wPage, hPage, wClient, hClient: integer;
begin
  // get the printer dimensions
  wPage := GetDeviceCaps(Printer.Handle, PHYSICALWIDTH);
  hPage := GetDeviceCaps(Printer.Handle, PHYSICALHEIGHT);
  // get the client window dimensions.
  hClient := Panel2.ClientHeight;
  // initially adjust width to match height
  wClient := MulDiv(Panel2.ClientHeight, wPage, hPage);
  // if that doesn't fit, then do it the other way
  if wClient > Panel2.ClientWidth then
  begin
    wCLient := Panel2.ClientWidth;
    hClient := MulDiv(Panel2.ClientWidth, hPage, wPage);
    // center the page in the window
    PreviewPanel.Top := ((Panel2.ClientHeight - hClient) div 2) - Panel1.Height;
  end
  else
  begin
    // center the page in the window
    PreviewPanel.Left := (Panel2.ClientWidth - wClient) div 2;
    PreviewPanel.Top  := Panel1.Height;
  end;
  // now set size of panel
  PreviewPanel.Width  := wClient;
  PreviewPanel.Height := hClient
end;}

constructor TVideoWindowFRM.Create(AOwner: TComponent; url, title: String;
  browserWidth, browserHeight: integer);
begin
    inherited Create(AOwner);

    videoW:=browserWidth;
    videoH:=browserHeight;
    selectedURL:=url;
    lblTitle.Caption:=title;
    first:=true;
end;

procedure TVideoWindowFRM.FormActivate(Sender: TObject);
var diffW, diffH: integer;
begin
    if not first then exit;
    first:=false;

    diffW:=Self.Width-ClientWidth;
    diffH:=Self.Height-ClientHeight;

    Width:=videoW+diffW;
    Height:=videoH+Panel1.Height+diffH;

    showConnectionProblem(not CheckUrl(selectedURL));
    flash.movie:=selectedURL;
end;

procedure TVideoWindowFRM.execute(what: String);
begin
    ShellExecute(Application.Handle,
             PChar('open'),
             PChar(what),
             PChar(0),
             nil,
             SW_NORMAL);

end;

procedure TVideoWindowFRM.lblGetFlashClick(Sender: TObject);
begin
    execute('http://get.adobe.com/flashplayer/');
end;

procedure TVideoWindowFRM.lblReloadClick(Sender: TObject);
begin
    showConnectionProblem(not CheckUrl(selectedURL));
    flash.movie:=selectedURL;
    flash.Refresh;
end;

procedure TVideoWindowFRM.showConnectionProblem(showProblem: boolean);
begin
    lblError1.Visible:=showProblem;
    lblError2.Visible:=showProblem;
    lblError3.Visible:=showProblem;
end;

end.
