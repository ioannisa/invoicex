-- MyDAC version: 5.90.0.58
-- MySQL server version: 5.1.41-community-debug-embedded-log
-- MySQL client version: 5.1.41
-- Script date 01/07/2010 8:02:14 ��
-- ---------------------------------------------------------------------- 
-- Server: 
-- Database: invoices

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
-- 
-- Database invoices structure
-- 

DROP DATABASE IF EXISTS invoices;
CREATE DATABASE invoices;
USE invoices;

-- 
-- Table structure for table  expensemastertype
-- 

DROP TABLE IF EXISTS expensemastertype;
CREATE TABLE `expensemastertype` (
  `idexpenseMasterType` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descr` varchar(45) NOT NULL,
  PRIMARY KEY (`idexpenseMasterType`),
  UNIQUE KEY `Index_2` (`descr`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table expensemastertype
-- 

LOCK TABLES expensemastertype WRITE;
/*!40000 ALTER TABLE expensemastertype DISABLE KEYS */;
INSERT INTO expensemastertype(idexpenseMasterType, descr) VALUES
  (1, '�������'),
  (2, '����������'),
  (3, '����������'),
  (4, '������'),
  (5, '���������'),
  (6, '�������'),
  (7, '����������'),
  (8, '�����������'),
  (9, '����� �����'),
  (10, '������������'),
  (11, '�������� ����������'),
  (12, '������'),
  (13, '���������� �������'),
  (15, '��������'),
  (14, '��������� �����'),
  (16, '�������'),
  (17, '�������'),
  (18, '������');
/*!40000 ALTER TABLE expensemastertype ENABLE KEYS */;
UNLOCK TABLES;

-- 
-- Table structure for table  expensetype
-- 

DROP TABLE IF EXISTS expensetype;
CREATE TABLE `expensetype` (
  `idexpenseType` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descr` varchar(85) NOT NULL,
  `idExpenseMasterType` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idexpenseType`),
  UNIQUE KEY `Index_2` (`idExpenseMasterType`,`descr`) USING BTREE,
  CONSTRAINT `FK_expensetype_1` FOREIGN KEY (`idExpenseMasterType`) REFERENCES `expensemastertype` (`idexpenseMasterType`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table expensetype
-- 

LOCK TABLES expensetype WRITE;
/*!40000 ALTER TABLE expensetype DISABLE KEYS */;
INSERT INTO expensetype(idexpenseType, descr, idExpenseMasterType) VALUES
  (3, '', 1),
  (4, 'CD, DVD, Video Club', 2),
  (5, 'Internet (Online ���������, ���������)', 2),
  (6, '��������� ������ (�����, ����������, �����������)', 2),
  (7, '����������, ��������, ������������', 2),
  (8, '���������, ����, �����, Club', 2),
  (9, '��������������, ������, ������, ���������', 2),
  (10, '������ ���������, ������', 2),
  (11, '�����, �������, ������� ���', 3),
  (12, '�����������, ��������� ������', 3),
  (13, '��������� ��������', 3),
  (14, '����� �������', 3),
  (15, '����������, ��������', 3),
  (16, '������� ������������', 3),
  (17, '�����, �����, ������������', 3),
  (18, '�������� (�������, �����, ������)', 4),
  (19, '����������', 4),
  (20, '���������', 4),
  (21, '����� (�����, ��������, ������������)', 4),
  (22, '����� (���������, ���������, �������)', 5),
  (23, '���������', 6),
  (24, '����������, ���������, ������', 6),
  (25, '���������� ������', 6),
  (26, '��������� (Spa, �����, ��������������)', 6),
  (27, '����������, ���������, ���������', 6),
  (28, '�������', 6),
  (29, '��������', 7),
  (30, '������� �����', 7),
  (31, '�������, ���������� ��������', 7),
  (32, '��������� ����, �����������, �������', 8),
  (33, '���� (�����, ���, ���)', 8),
  (34, '����������� �������� (Forthnet, Tellas, On �.�.�.)', 8),
  (35, '�������', 8),
  (36, '������ ���������', 8),
  (37, '�����������, ���������, ������ �����', 8),
  (38, '', 9),
  (39, '����� �����������, �������, Service', 10),
  (40, '�������, ���������', 10),
  (41, '������', 10),
  (42, '���������, ������', 10),
  (43, '��������� (����������, ������, ������)', 10),
  (44, '��������', 10),
  (45, '����, ����������� �����', 10),
  (46, '���� ��������', 11),
  (47, '��������, �����', 11),
  (48, '��������, ���� �����, ����, ���������', 11),
  (49, '����������� (�/�, TV, Stereo, DVD, ���������)', 11),
  (50, '���������� ����� (�������������, �������)', 11),
  (51, '����� ���� (��������, ��������, �����, ���������)', 11),
  (52, '������, ����� ������, ���������', 12),
  (53, '���������� ���������� (���������, �������������)', 13),
  (54, '����������', 13),
  (55, '���������, ����������, ����������', 13),
  (56, '����������, ���������, ���� ������', 14),
  (57, '�������, ����������, ���������', 15),
  (58, '��������� (����������, ������, ������)', 16),
  (59, '����������', 16),
  (60, '����������, ����� ��� ���������', 16),
  (61, 'Fast Food, ��������, ���������', 17),
  (62, 'Super Market', 17),
  (63, '����������, ��������������', 17),
  (64, '����, ����, ����� ������, �����', 17),
  (65, '����������, �������, �����������, �����, ���', 17),
  (66, '����������, ���������', 18),
  (67, '�����', 18),
  (68, '�������, �����, ���������', 18);
/*!40000 ALTER TABLE expensetype ENABLE KEYS */;
UNLOCK TABLES;

-- 
-- Table structure for table  store
-- 

DROP TABLE IF EXISTS store;
CREATE TABLE `store` (
  `idstore` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descr` varchar(45) DEFAULT NULL,
  `afm` varchar(9) DEFAULT NULL,
  `idExpenseType` int(10) unsigned zerofill DEFAULT NULL,
  PRIMARY KEY (`idstore`),
  UNIQUE KEY `Index_2` (`afm`),
  KEY `FK_store_1` (`idExpenseType`),
  CONSTRAINT `FK_store_1` FOREIGN KEY (`idExpenseType`) REFERENCES `expensetype` (`idexpenseType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table store
-- 


-- 
-- Table structure for table  usr
-- 

DROP TABLE IF EXISTS usr;
CREATE TABLE `usr` (
  `idusr` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fullname` varchar(45) NOT NULL,
  `anualgoal` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`idusr`),
  UNIQUE KEY `Index_2` (`fullname`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table usr
-- 


-- 
-- Table structure for table  usrinv
-- 

DROP TABLE IF EXISTS usrinv;
CREATE TABLE `usrinv` (
  `idusrinv` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idusr` int(10) unsigned NOT NULL,
  `invnum` varchar(25) NOT NULL,
  `invDate` date NOT NULL,
  `idstore` int(10) unsigned NOT NULL,
  `amount` double NOT NULL,
  `reason` longtext,
  `idExpenseType` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idusrinv`),
  KEY `FK_usrinv_1` (`idusr`),
  KEY `FK_usrinv_2` (`idstore`),
  KEY `FK_usrinv_3` (`idExpenseType`),
  CONSTRAINT `FK_usrinv_1` FOREIGN KEY (`idusr`) REFERENCES `usr` (`idusr`),
  CONSTRAINT `FK_usrinv_2` FOREIGN KEY (`idstore`) REFERENCES `store` (`idstore`),
  CONSTRAINT `FK_usrinv_3` FOREIGN KEY (`idExpenseType`) REFERENCES `expensetype` (`idexpenseType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table usrinv
-- 


-- 
-- Table structure for table  v_expensetype
-- 

DROP VIEW IF EXISTS v_expensetype;
CREATE ALGORITHM=UNDEFINED DEFINER=`ODBC`@`localhost` SQL SECURITY DEFINER VIEW `v_expensetype` AS select `et`.`idexpenseType` AS `idDetail`,`et`.`descr` AS `descrDetail`,`emt`.`idexpenseMasterType` AS `idMaster`,`emt`.`descr` AS `descrMaster`,(case trim(`et`.`descr`) when '' then `emt`.`descr` else concat(`emt`.`descr`,' - ',`et`.`descr`) end) AS `fullDescr` from (`expensetype` `et` join `expensemastertype` `emt`) where (`et`.`idExpenseMasterType` = `emt`.`idexpenseMasterType`);








/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
