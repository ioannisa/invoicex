object InvoiceFRM: TInvoiceFRM
  Left = 502
  Top = 262
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #922#945#964#945#967#974#961#953#963#951' / '#924#949#964#945#946#959#955#942' '#945#960#972#948#949#953#958#951#962
  ClientHeight = 365
  ClientWidth = 402
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnCloseQuery = FormCloseQuery
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object cxGroupBox1: TcxGroupBox
    Left = 8
    Top = 8
    Caption = #931#964#959#953#967#949#943#945' '#917#960#953#967#949#943#961#953#963#951#962
    TabOrder = 0
    Height = 137
    Width = 387
    object cbboxStoreAfm: TcxLookupComboBox
      Left = 80
      Top = 72
      Properties.DropDownAutoSize = True
      Properties.DropDownListStyle = lsEditList
      Properties.ImmediatePost = True
      Properties.KeyFieldNames = 'idstore'
      Properties.ListColumns = <
        item
          Caption = #913'.'#934'.'#924'.'
          SortOrder = soAscending
          Width = 75
          FieldName = 'afm'
        end
        item
          Caption = #917#960#969#957#965#956#943#945
          FieldName = 'descr'
        end>
      Properties.ListSource = dsStore
      Properties.MaxLength = 9
      Properties.PostPopupValueOnTab = True
      Properties.OnChange = cbboxStoreAfmPropertiesChange
      Properties.OnCloseUp = cbboxStoreAfmPropertiesCloseUp
      Properties.OnEditValueChanged = cbboxStoreAfmPropertiesCloseUp
      Properties.OnNewLookupDisplayText = cbboxStoreAfmPropertiesNewLookupDisplayText
      EditValue = 0
      Style.Color = clWindow
      TabOrder = 0
      OnKeyDown = cbboxStoreAfmKeyDown
      Width = 265
    end
    object cbboxStoreName: TcxLookupComboBox
      Left = 80
      Top = 99
      Properties.DropDownAutoSize = True
      Properties.DropDownListStyle = lsEditList
      Properties.ImmediatePost = True
      Properties.KeyFieldNames = 'idstore'
      Properties.ListColumns = <
        item
          Caption = #913'.'#934'.'#924'.'
          Width = 75
          FieldName = 'afm'
        end
        item
          Caption = #917#960#969#957#965#956#943#945
          SortOrder = soAscending
          FieldName = 'descr'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dsStore
      Properties.MaxLength = 45
      Properties.PostPopupValueOnTab = True
      Properties.OnChange = cbboxStoreAfmPropertiesChange
      Properties.OnCloseUp = cbboxStoreNamePropertiesCloseUp
      Properties.OnEditValueChanged = cbboxStoreNamePropertiesCloseUp
      Properties.OnNewLookupDisplayText = cbboxStoreAfmPropertiesNewLookupDisplayText
      EditValue = 0
      TabOrder = 1
      OnKeyDown = cbboxStoreNameKeyDown
      Width = 265
    end
    object lblAfm: TcxLabel
      Left = 35
      Top = 73
      Caption = #913'.'#934'.'#924'.'
      Style.TextColor = clRed
      Transparent = True
    end
    object lblBrand: TcxLabel
      Left = 24
      Top = 100
      Caption = #917#960#969#957#965#956#943#945
      Style.TextColor = clRed
      Transparent = True
    end
    object cxLabel2: TcxLabel
      Left = 57
      Top = 13
      Caption = #925#941#949#962' '#949#960#953#967#949#953#961#942#963#949#953#962' '#954#945#964#945#967#969#961#959#973#957#964#945#953' '#949#948#974' '#945#965#964#959#956#940#964#969#962'.'
      Enabled = False
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsItalic]
      Style.TextColor = clWindowText
      Style.IsFontAssigned = True
      Transparent = True
    end
    object cxLabel3: TcxLabel
      Left = 57
      Top = 40
      Caption = #915#953#945' '#956#949#964#945#946#959#955#942' '#942#948#951' '#954#945#964#945#967#969#961#953#956#941#957#951#962' '#949#960#953#967#949#943#961#951#963#951#962
      Enabled = False
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsItalic]
      Style.TextColor = clWindowText
      Style.IsFontAssigned = True
      Transparent = True
    end
    object cxLabel4: TcxLabel
      Left = 57
      Top = 53
      Caption = #960#961#941#960#949#953' '#957#945' '#960#940#964#949' '#963#964#959' tab "'#917#960#953#967#949#953#961#942#963#949#953#962'" '#963#964#951#957' '#945#961#967#953#954#951' '#959#952#972#957#951
      Enabled = False
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsItalic]
      Style.TextColor = clWindowText
      Style.IsFontAssigned = True
      Transparent = True
    end
    object lblWrong: TcxLabel
      Left = 346
      Top = 69
      Caption = #923#940#952#959#962' '#913#934#924
      Style.TextColor = clRed
      Properties.Alignment.Horz = taCenter
      Properties.WordWrap = True
      Transparent = True
      Width = 37
      AnchorX = 365
    end
  end
  object cxGroupBox2: TcxGroupBox
    Left = 8
    Top = 151
    Caption = #931#964#959#953#967#949#943#945' '#913#960#972#948#949#953#958#951#962
    TabOrder = 1
    Height = 162
    Width = 387
    object cbboxReason: TcxLookupComboBox
      Left = 80
      Top = 78
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'idDetail'
      Properties.ListColumns = <
        item
          Caption = #916#953#945#952#941#963#953#956#959#953' '#964#973#960#959#953' '#949#958#972#948#969#957
          FieldName = 'fullDescr'
        end>
      Properties.ListOptions.ShowHeader = False
      Properties.ListSource = dsExpense
      Properties.OnChange = edtDatePropertiesChange
      EditValue = 0
      TabOrder = 3
      OnKeyDown = cbboxReasonKeyDown
      Width = 265
    end
    object edtAmount: TcxCurrencyEdit
      Left = 240
      Top = 51
      Properties.DisplayFormat = ',0.00 '#8364';-,0.00 '#8364
      Properties.OnChange = edtDatePropertiesChange
      TabOrder = 2
      OnKeyDown = edtAmountKeyDown
      Width = 105
    end
    object edtDate: TcxDateEdit
      Left = 80
      Top = 24
      Properties.DateOnError = deNull
      Properties.SaveTime = False
      Properties.ShowTime = False
      Properties.OnChange = edtDatePropertiesChange
      TabOrder = 0
      OnKeyDown = edtDateKeyDown
      Width = 105
    end
    object edtInvNum: TcxTextEdit
      Left = 80
      Top = 51
      Properties.CharCase = ecUpperCase
      Properties.MaxLength = 25
      Properties.OnChange = edtDatePropertiesChange
      TabOrder = 1
      OnKeyDown = edtInvNumKeyDown
      Width = 105
    end
    object lblDate: TcxLabel
      Left = 39
      Top = 25
      Caption = #919#956'/'#957#943#945
      Style.TextColor = clRed
      Transparent = True
    end
    object lblInvNum: TcxLabel
      Left = 21
      Top = 52
      Caption = #913#961'. '#913#960#959#948'.'
      Style.TextColor = clPurple
      Transparent = True
    end
    object lblAmount: TcxLabel
      Left = 205
      Top = 52
      Caption = #928#959#963#972
      Style.TextColor = clRed
      Transparent = True
    end
    object lblReason: TcxLabel
      Left = 3
      Top = 80
      Caption = #932#973#960#959#962' '#917#958#972#948#959#965
      Style.TextColor = clRed
      Transparent = True
    end
    object edtComment: TcxTextEdit
      Left = 80
      Top = 115
      TabOrder = 4
      OnKeyDown = edtCommentKeyDown
      Width = 265
    end
    object cxLabel7: TcxLabel
      Left = 38
      Top = 116
      Caption = #931#967#972#955#953#959
      Style.TextColor = clWindowText
      Transparent = True
    end
    object ckboxFixedDate: TcxCheckBox
      Left = 205
      Top = 24
      Hint = 
        #931#964#945#952#949#961#942' '#919#956'/'#957#943#945' '#963#949' '#960#949#961#943#960#964#969#963#951' '#960#959#965' '#941#967#959#965#956#949' '#960#959#955#955#941#962' '#945#960#959#948#949#943#958#949#953#962' '#947#953#945' '#964#951#957 +
        ' '#943#948#953#945' '#951#956#941#961#945
      TabStop = False
      Caption = #931#964#945#952#949#961#942' '#919#956'/'#957#943#945
      TabOrder = 10
      Transparent = True
      Width = 113
    end
  end
  object btnOk: TcxButton
    Left = 266
    Top = 329
    Width = 60
    Height = 25
    Caption = #927#922
    TabOrder = 2
    OnClick = btnOkClick
  end
  object btnCancel: TcxButton
    Left = 332
    Top = 329
    Width = 59
    Height = 25
    Caption = #913#954#973#961#969#963#951
    TabOrder = 3
    OnClick = btnCancelClick
  end
  object cxLabel1: TcxLabel
    Left = 8
    Top = 321
    Caption = #924#949' [TAB] '#942' [ENTER] '#956#949#964#945#966#941#961#949#963#964#949' '#963#964#959' '#949#960#972#956#949#957#959' '#960#949#948#943#959
    Enabled = False
    ParentFont = False
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Tahoma'
    Style.Font.Style = [fsItalic]
    Style.TextColor = clWindowText
    Style.IsFontAssigned = True
    Transparent = True
  end
  object cxLabel5: TcxLabel
    Left = 8
    Top = 338
    Caption = #924#949' [CTRL]+[ENTER] '#960#953#941#950#949#964#949' '#949#957#945#955#955#945#954#964#953#954#940' '#964#959' ['#927#922'] '
    Enabled = False
    ParentFont = False
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Tahoma'
    Style.Font.Style = [fsItalic]
    Style.TextColor = clWindowText
    Style.IsFontAssigned = True
    Transparent = True
  end
  object tblStore: TMyTable
    TableName = 'store'
    Connection = MainFRM.EmbeddedConnection
    Filtered = True
    Left = 208
    Top = 96
  end
  object dsStore: TMyDataSource
    DataSet = tblStore
    Left = 248
    Top = 96
  end
  object qr: TMyQuery
    Connection = MainFRM.EmbeddedConnection
    Left = 136
    Top = 96
  end
  object qrExpense: TMyQuery
    Connection = MainFRM.EmbeddedConnection
    SQL.Strings = (
      
        'SELECT idDetail, fullDescr FROM v_expensetype order by fullDescr' +
        ';')
    Left = 208
    Top = 247
  end
  object dsExpense: TMyDataSource
    DataSet = qrExpense
    Left = 248
    Top = 247
  end
  object timerFocus: TTimer
    Enabled = False
    Interval = 1
    OnTimer = timerFocusTimer
    Left = 320
    Top = 96
  end
  object timerAFM: TTimer
    Enabled = False
    Interval = 1
    OnTimer = timerAFMTimer
    Left = 112
    Top = 23
  end
end
