object VideoWindowFRM: TVideoWindowFRM
  Left = 286
  Top = 114
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = 
    #928#961#959#946#959#955#942' VIDEO TUTORIAL - '#928#961#959#963#959#967#942' '#945#960#945#953#964#949#943' '#963#973#957#948#949#963#951' '#963#964#959' Internet '#954#945 +
    #953' '#949#947#954#945#964#940#963#964#945#963#951' Flash Player'
  ClientHeight = 632
  ClientWidth = 1134
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1134
    Height = 33
    Align = alTop
    TabOrder = 0
    object cxLabel1: TcxLabel
      Left = 24
      Top = 17
      Caption = #913#957' '#948#949#957' '#941#967#949#964#949' '#949#947#954#945#964#945#963#964#942#963#949#953' '#964#959' Adobe Flash '#960#953#941#963#964#949
      Properties.Alignment.Horz = taRightJustify
      Transparent = True
      AnchorX = 265
    end
    object lblGetFlash: TcxLabel
      Left = 264
      Top = 17
      Cursor = crHandPoint
      Caption = #949#948#974
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsUnderline]
      Style.TextColor = clBlue
      Style.IsFontAssigned = True
      Transparent = True
      OnClick = lblGetFlashClick
    end
    object lblReload: TcxLabel
      Left = 307
      Top = 17
      Cursor = crHandPoint
      Caption = #917#960#945#957#945#966#959#961#964#974#963#964#949' '#964#959' video'
      Style.TextColor = clBlue
      Style.TextStyle = [fsUnderline]
      Properties.Alignment.Horz = taLeftJustify
      Transparent = True
      OnClick = lblReloadClick
    end
    object lblTitle: TcxLabel
      Left = 24
      Top = 0
      Caption = 'TITLE'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      Properties.Alignment.Horz = taLeftJustify
      Transparent = True
    end
    object cxLabel3: TcxLabel
      Left = 293
      Top = 17
      Caption = '-'
      Properties.Alignment.Horz = taLeftJustify
      Transparent = True
    end
  end
  object flash: TFlashPlayerControl
    Left = 0
    Top = 33
    Width = 1134
    Height = 599
    Align = alClient
    TabOrder = 1
    Movie = ' '
    FrameNum = -1
    Playing = False
    Quality = 1
    ScaleMode = 0
    AlignMode = 0
    BackgroundColor = 0
    Loop = True
    WMode = 'Window'
    Menu = True
    Scale = 'ShowAll'
    DeviceFont = False
    EmbedMovie = True
    BGColor = '000000'
    Quality2 = 'High'
    AllowFullscreen = True
    ExplicitTop = 25
  end
  object cxLabel2: TcxLabel
    Left = -2
    Top = 31
    AutoSize = False
    Caption = 'CYBERNATE SOFTWARE'
    ParentColor = False
    Style.Color = clBlack
    Style.TextColor = clWhite
    Height = 20
    Width = 129
  end
  object lblError1: TcxLabel
    Left = 36
    Top = 103
    Caption = #913#916#933#925#913#924#921#913' '#931#933#925#916#917#931#919#931':'
    ParentColor = False
    ParentFont = False
    Style.Color = clBlack
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -21
    Style.Font.Name = 'Tahoma'
    Style.Font.Style = [fsBold]
    Style.TextColor = clWhite
    Style.IsFontAssigned = True
    Visible = False
  end
  object lblError2: TcxLabel
    Left = 36
    Top = 154
    Caption = 
      #916#949#957' '#949#943#957#945#953' '#948#965#957#945#964#942' '#951' '#963#973#957#948#949#963#951' '#956#949' '#964#959' Cybernate Server '#947#953#945' '#964#951' '#955#942#968#951' '#964#959 +
      #965' VIDEO '#945#961#967#949#943#959#965'.'
    ParentColor = False
    ParentFont = False
    Style.Color = clBlack
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -19
    Style.Font.Name = 'Tahoma'
    Style.Font.Style = []
    Style.TextColor = clWhite
    Style.IsFontAssigned = True
    Visible = False
  end
  object lblError3: TcxLabel
    Left = 36
    Top = 187
    Caption = 
      #904#955#949#947#958#964#949' '#964#951' '#963#973#957#948#949#963#942' '#963#945#962' '#963#964#959' Internet '#942' '#964#959' '#949#957#948#949#967#972#956#949#957#959' '#957#945' '#941#967#949#953' '#960#941#963#949 +
      #953' '#959' cybernate server.'
    ParentColor = False
    ParentFont = False
    Style.Color = clBlack
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -19
    Style.Font.Name = 'Tahoma'
    Style.Font.Style = []
    Style.TextColor = clWhite
    Style.IsFontAssigned = True
    Visible = False
  end
end
