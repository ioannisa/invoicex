object PreviewCSVFrm: TPreviewCSVFrm
  Left = 571
  Top = 157
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = #917#960#953#955#959#947#942' '#963#969#963#964#959#973' '#948#953#945#967#969#961#953#963#964#953#954#959#973' (Delimiter) '#964#959#965' CSV '#945#961#967#949#943#959#965' '#963#945#962
  ClientHeight = 289
  ClientWidth = 613
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object cxLabel11: TcxLabel
    Left = 8
    Top = 167
    Caption = #916#949#943#947#956#945' '#951' 1'#951' '#963#949#953#961#940' '#945#960#972' '#964#959' CSV '#945#961#967#949#943#959' '#963#945#962' ('#922#972#954#954#953#957#959' '#967#961#974#956#945')'
    Style.TextStyle = [fsBold]
    Transparent = True
  end
  object cxLabel12: TcxLabel
    Left = 8
    Top = 232
    Caption = #928#959#953#972' '#948#953#945#967#969#961#953#963#964#953#954#972' '#964#945#953#961#953#940#950#949#953' '#963#964#959' '#945#961#967#949#943#959' '#963#945#962';'
    Style.TextStyle = [fsBold]
    Transparent = True
  end
  object cxGroupBox1: TcxGroupBox
    Left = 8
    Top = 8
    Caption = #916#953#945#952#941#963#953#956#945' '#948#953#945#967#969#961#953#963#964#953#954#940' (Delmiters)'
    TabOrder = 2
    Height = 153
    Width = 597
    object cxLabel4: TcxLabel
      Left = 29
      Top = 24
      Caption = 'Koma'
      Style.TextStyle = [fsBold]
      Transparent = True
    end
    object cxLabel6: TcxLabel
      Left = 29
      Top = 104
      Caption = 'Tab'
      Style.TextStyle = [fsBold]
      Transparent = True
    end
    object cxLabel5: TcxLabel
      Left = 29
      Top = 65
      Caption = 'Semicolon'
      Style.TextStyle = [fsBold]
      Transparent = True
    end
    object cxLabel3: TcxLabel
      Left = 108
      Top = 104
      Caption = '[ '#9' ]'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Courier New'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      Transparent = True
    end
    object cxLabel2: TcxLabel
      Left = 108
      Top = 64
      Caption = '[ ; ]'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Courier New'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      Transparent = True
    end
    object cxLabel1: TcxLabel
      Left = 108
      Top = 24
      Caption = '[ , ]'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Courier New'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      Transparent = True
    end
    object cxLabel7: TcxLabel
      Left = 269
      Top = 24
      Caption = '[aaaaa , bbbbb , ccc , dd , e]'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Courier New'
      Style.Font.Style = []
      Style.TextColor = clBlue
      Style.IsFontAssigned = True
      Transparent = True
    end
    object cxLabel8: TcxLabel
      Left = 269
      Top = 64
      Caption = '[fffff ; ggggg ; hhh ; ii ; j]'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Courier New'
      Style.Font.Style = []
      Style.TextColor = clBlue
      Style.IsFontAssigned = True
      Transparent = True
    end
    object cxLabel10: TcxLabel
      Left = 269
      Top = 104
      Caption = '[kkkkk '#9' llll '#9' mmmmmm '#9' nnnn]'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Courier New'
      Style.Font.Style = []
      Style.TextColor = clBlue
      Style.IsFontAssigned = True
      Transparent = True
    end
    object cxLabel9: TcxLabel
      Left = 173
      Top = 25
      Caption = #916#949#943#947#956#945
      Enabled = False
      Style.TextStyle = [fsBold]
      Transparent = True
    end
    object cxLabel13: TcxLabel
      Left = 173
      Top = 65
      Caption = #916#949#943#947#956#945
      Enabled = False
      Style.TextStyle = [fsBold]
      Transparent = True
    end
    object cxLabel14: TcxLabel
      Left = 173
      Top = 105
      Caption = #916#949#943#947#956#945
      Enabled = False
      Style.TextStyle = [fsBold]
      Transparent = True
    end
  end
  object lblLine: TcxLabel
    Left = 5
    Top = 182
    Caption = '[aaaaa , bbbbb , ccc , dd , e]'
    ParentFont = False
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Courier New'
    Style.Font.Style = []
    Style.TextColor = clRed
    Style.IsFontAssigned = True
    Transparent = True
  end
  object btnOK: TcxButton
    Left = 530
    Top = 229
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 4
    OnClick = btnOKClick
  end
  object cbboxDelim: TcxComboBox
    Left = 261
    Top = 231
    Properties.DropDownListStyle = lsFixedList
    Properties.Items.Strings = (
      'Koma [,]'
      'Semicolon [;]'
      'Tab ['#9']')
    Properties.OnChange = cbboxDelimPropertiesChange
    TabOrder = 5
    Width = 121
  end
  object cxLabel15: TcxLabel
    Left = 8
    Top = 255
    Caption = #932#945' '#960#949#948#943#945' '#963#945#962' '#960#949#961#953#955#945#956#946#940#957#959#957#964#945#953' '#963#949' "quotes";'
    Style.TextStyle = [fsBold]
    Transparent = True
  end
  object cbboxQuote: TcxComboBox
    Left = 261
    Top = 253
    Properties.DropDownListStyle = lsFixedList
    Properties.Items.Strings = (
      #956#959#957#972'  '#39'quote'#39
      #948#953#960#955#972' "quote"'
      #967#969#961#943#962'  quote')
    Properties.OnChange = cbboxQuotePropertiesChange
    TabOrder = 7
    Width = 121
  end
end
