unit PreviewCSVFORM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, cxLabel, cxGroupBox, Menus, StdCtrls, cxButtons,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxMemo, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver;

type
  TPreviewCSVFrm = class(TForm)
    cxLabel11: TcxLabel;
    cxLabel12: TcxLabel;
    cxGroupBox1: TcxGroupBox;
    cxLabel4: TcxLabel;
    cxLabel6: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel1: TcxLabel;
    cxLabel7: TcxLabel;
    cxLabel8: TcxLabel;
    cxLabel10: TcxLabel;
    cxLabel9: TcxLabel;
    cxLabel13: TcxLabel;
    cxLabel14: TcxLabel;
    lblLine: TcxLabel;
    btnOK: TcxButton;
    cbboxDelim: TcxComboBox;
    cxLabel15: TcxLabel;
    cbboxQuote: TcxComboBox;
    procedure btnOKClick(Sender: TObject);
    procedure cbboxDelimPropertiesChange(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure cbboxQuotePropertiesChange(Sender: TObject);
  private
    allowClose: boolean;
  public
    Constructor Create(AOwner: TComponent; FileName: TFileName); overload;
  end;

var
  PreviewCSVFrm: TPreviewCSVFrm;
  CSVDelim: Char;
  CSVQuote: Char;

implementation

uses Main;

{$R *.dfm}

{ TPreviewCSVFrm }

procedure TPreviewCSVFrm.btnOKClick(Sender: TObject);
var errMsg: String;
begin
    errMsg:='';

    if cbboxDelim.ItemIndex=-1 then
        errMsg:='������ ������������ ������'
    else if cbboxQuote.ItemIndex=-1 then
        errMsg:='������ quote ��� �� ����� ���';

    allowClose:=(errMsg='');

    if allowClose then
        Close
    else
        MessageDlg(errMsg, mtError, [mbOK], 0);
end;

procedure TPreviewCSVFrm.cbboxDelimPropertiesChange(Sender: TObject);
begin
    case cbboxDelim.ItemIndex of
        0: CSVDelim:=',';
        1: CSVDelim:=';';
        2: CSVDelim:=#9;
    end;
end;

procedure TPreviewCSVFrm.cbboxQuotePropertiesChange(Sender: TObject);
begin
    case cbboxQuote.ItemIndex of
        0: CSVQuote:='''';
        1: CSVQuote:='"';
        2: CSVQuote:=#1;
    end;
end;

constructor TPreviewCSVFrm.Create(AOwner: TComponent; FileName: TFileName);
var F: TextFile;
    sl: TStringList;
begin
    inherited Create(AOwner);

    allowClose:=false;

    sl:=TStringList.Create;
    sl.LoadFromFile(FileName);
    lblLine.Caption:=sl[0];
    sl.Free;
end;

procedure TPreviewCSVFrm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose:=allowClose;
end;

end.
