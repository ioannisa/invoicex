unit ImporterFORM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, cxSSheet, ExtCtrls, cxContainer, cxEdit, cxLabel, Menus,
  StdCtrls, cxButtons, cxTextEdit, cxMaskEdit, cxSpinEdit, cxDBEdit,
  cxDropDownEdit, DB, DBClient, cxPC, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxCurrencyEdit, MemDS, DBAccess, MyAccess, wait,
  PreviewCSVFORM, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxGDIPlusClasses, cxGroupBox, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint;

type
  TImporterFRM = class(TForm)
    panelUp: TPanel;
    panelLeft: TPanel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    dlgRestore: TOpenDialog;
    cldsImport: TClientDataSet;
    dsImport: TDataSource;
    cxLabel9: TcxLabel;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    ss: TcxSpreadSheetBook;
    Panel4: TPanel;
    tvImp: TcxGridDBTableView;
    lvlImp: TcxGridLevel;
    gridImp: TcxGrid;
    tvImpColumn1: TcxGridDBColumn;
    tvImpColumn2: TcxGridDBColumn;
    tvImpColumn3: TcxGridDBColumn;
    tvImpColumn4: TcxGridDBColumn;
    tvImpColumn5: TcxGridDBColumn;
    btnInsert: TcxButton;
    qr: TMyQuery;
    btnCancel: TcxButton;
    qr2: TMyQuery;
    cxGroupBox1: TcxGroupBox;
    cxLabel3: TcxLabel;
    edtStartLine: TcxSpinEdit;
    cxLabel10: TcxLabel;
    edtEndLine: TcxSpinEdit;
    cxGroupBox2: TcxGroupBox;
    cxLabel4: TcxLabel;
    cbboxAfm: TcxComboBox;
    cxLabel5: TcxLabel;
    cbboxBrand: TcxComboBox;
    cxLabel6: TcxLabel;
    cbboxDate: TcxComboBox;
    cxLabel7: TcxLabel;
    cbboxInvNum: TcxComboBox;
    cxLabel8: TcxLabel;
    cbboxAmount: TcxComboBox;
    btnOpenFile: TcxButton;
    btnCheck: TcxButton;
    cxLabel11: TcxLabel;
    Image1: TImage;
    cxLabel12: TcxLabel;
    Image2: TImage;
    cxLabel13: TcxLabel;
    procedure btnOpenFileClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnCheckClick(Sender: TObject);
    procedure btnInsertClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure edtStartLinePropertiesEditValueChanged(Sender: TObject);
  private
    procedure parseCSV(filename: TFileName);
  public
    { Public declarations }
  end;

var
  ImporterFRM: TImporterFRM;
  importedNewCmp: Boolean;

implementation

uses Main;

{$R *.dfm}

procedure TImporterFRM.btnCancelClick(Sender: TObject);
begin
    if MessageDlg('������� ���������;', mtConfirmation, [mbYes, mbNo], 0)=mrNo then exit;

    cxTabSheet1.Visible:=true;
    cxPageControl1.ActivePageIndex:=0;
    cxTabSheet2.Visible:=false;

    panelUp.Visible:=true;
    panelLeft.Visible:=true;

    importedNewCmp:=false;
end;

procedure TImporterFRM.btnCheckClick(Sender: TObject);
var i: integer;
    errMsg: String;
    tmp1: String;

    AFM, AFM2: String;

    locator, j: integer;
    errorField, errorFieldName: String;
begin
    if cbboxAfm.ItemIndex=-1 then
        errMsg:='��� ��������� ������ ��� �� ���'
    else if cbboxBrand.ItemIndex=-1 then
         errMsg:='��� ��������� ������ ��� ��� ��������'
    else if cbboxDate.ItemIndex=-1 then
         errMsg:='��� ��������� ������ ��� ��� ����������'
    else if cbboxInvNum.ItemIndex=-1 then
         errMsg:='��� ��������� ������ ��� ��� ������ ���������'
    else if cbboxAmount.ItemIndex=-1 then
         errMsg:='��� ��������� ������ ��� �� ����'
    else begin
        tmp1:='�� ������� �� ������� ��� ����� ������� ��� ����������� ����� - ';
        if cbboxAfm.ItemIndex=cbboxBrand.ItemIndex then
            errMsg:=tmp1+'[���, ��������]'
        else if cbboxAfm.ItemIndex=cbboxDate.ItemIndex then
            errMsg:=tmp1+'[���, ��/���]'
        else if cbboxAfm.ItemIndex=cbboxInvNum.ItemIndex then
            errMsg:=tmp1+'[���, ��.����.]'
        else if cbboxAfm.ItemIndex=cbboxAmount.ItemIndex then
            errMsg:=tmp1+'[���, ����]'

        else if cbboxBrand.ItemIndex=cbboxDate.ItemIndex then
            errMsg:=tmp1+'[��������, ��/���]'
        else if cbboxBrand.ItemIndex=cbboxInvNum.ItemIndex then
            errMsg:=tmp1+'[��������, ��.����.]'
        else if cbboxBrand.ItemIndex=cbboxAmount.ItemIndex then
            errMsg:=tmp1+'[��������, ����]'

        else if cbboxDate.ItemIndex=cbboxInvNum.ItemIndex then
            errMsg:=tmp1+'[��/���, ��.����.]'
        else if cbboxDate.ItemIndex=cbboxAmount.ItemIndex then
            errMsg:=tmp1+'[��/���, ����]'

        else if cbboxInvNum.ItemIndex=cbboxAmount.ItemIndex then
            errMsg:=tmp1+'[��.����., ����]'
    end;


    if errMsg<>'' then begin
        MessageDlg(errMsg, mtError, [mbOk], 0);
        exit;
    end;

    waitForm.setMessage('�������� ���������');
    waitForm.Show;
    Application.ProcessMessages;

    with cldsImport do begin
        DisableControls;
        EmptyDataSet;

        for i := edtStartLine.EditValue-1 to edtEndLine.EditValue-1 do begin
            try
                AFM:=copy(Trim(VarToStr(ss.pages[ss.ActivePage].GetCellObject(cbboxAfm.ItemIndex, i).CellValue)),1,9);
                AFM2:=StringOfChar('0', 9-length(AFM))+AFM;

                Insert;
                locator:=1;
                FieldByName('afm').AsString:=AFM2; //copy(Trim(VarToStr(ss.pages[ss.ActivePage].GetCellObject(cbboxAfm.ItemIndex, i).CellValue)),1,9);
                locator:=2;
                FieldByName('brand').AsString:=copy(Trim(VarToStr(ss.pages[ss.ActivePage].GetCellObject(cbboxBrand.ItemIndex, i).CellValue)),1,45);
                locator:=3;
                FieldByName('date').AsDateTime:=ss.pages[ss.ActivePage].GetCellObject(cbboxDate.ItemIndex, i).CellValue;
                locator:=4;
                FieldByName('amount').AsFloat:=ss.pages[ss.ActivePage].GetCellObject(cbboxAmount.ItemIndex, i).CellValue;
                locator:=5;
                FieldByName('invNum').AsString:=copy(Trim(VarToStr(ss.pages[ss.ActivePage].GetCellObject(cbboxInvNum.ItemIndex, i).CellValue)),1,25);
                Post;
            except
                on e:Exception do begin
                    case locator of
                        1: begin
                            j:=cbboxAfm.ItemIndex;
                            errorField:=AFM2;
                            errorFieldName:='���';
                           end;
                        2: begin
                            j:=cbboxBrand.ItemIndex;
                            errorField:=VarToStr(ss.pages[ss.ActivePage].GetCellObject(j, i).CellValue);
                            errorFieldName:='��������';
                           end;
                        3: begin
                            j:=cbboxDate.ItemIndex;
                            errorField:=VarToStr(ss.pages[ss.ActivePage].GetCellObject(j, i).CellValue);
                            errorFieldName:='����������';
                           end;
                        4: begin
                            j:=cbboxAmount.ItemIndex;
                            errorField:=VarToStr(ss.pages[ss.ActivePage].GetCellObject(j, i).CellValue);
                            errorFieldName:='����';
                           end;
                        5: begin
                            j:=cbboxInvNum.ItemIndex;
                            errorField:=VarToStr(ss.pages[ss.ActivePage].GetCellObject(j, i).CellValue);
                            errorFieldName:='������� ���������';
                           end;
                    end;

                    ss.pages[ss.ActivePage].SelectCell(j, i);
                    if not PtInRect(ss.pages[ss.ActivePage].Corners, ss.pages[ss.ActivePage].ActiveCell) then
                        ss.pages[ss.ActivePage].Corners := Rect(j, i, j, i);

                    Cancel;

                    if MessageDlg(
                        '� ������� �������� �� ����� '+IntToStr(i+1)+' ��� ����������� �� ��������.'#13#10#13#10+

                        '������ ��� �����: ['+errorFieldName+']'#13#10+
                        '�� ����: ['+errorField+']'#13#10#13#10+

                        '������ ���������: '+e.Message+#13+#10+''+#13+#10+
                        '���������� �������� ��������� ��� ��� ����� ��������� ������ ��� ������� ���;', mtError, [mbYes, mbNo], 0)=mrNo
                    then
                        break
                    else continue;
                end;
            end;
        end;
        EnableControls;
    end;

    waitForm.Hide;

    cxTabSheet2.TabVisible:=true;
    cxPageControl1.ActivePageIndex:=1;
    cxTabSheet1.TabVisible:=false;
    panelUp.Visible:=false;
    panelLeft.Visible:=false;

    MessageDlg('������� ��� �������� ����� ��� ������� ���.'+#13+#10+'��� �� ����������� �������� �����, �� ������� ����������, �� ���� ���.', mtWarning, [mbOK], 0);
end;

procedure TImporterFRM.btnInsertClick(Sender: TObject);
var ds: Char;
    cmpID, idExpenseType: String;
    usrid: String;

    countCMP: integer;
    countINV: integer;
begin
    ds:=DecimalSeparator;
    DecimalSeparator:=#46;
    try
        qr.Connection.StartTransaction;

        usrID:=VarToStr(TMainFRM(Owner).cbboxUsr.EditValue);

        countCMP:=0;
        countINV:=0;

        waitForm.setMessage('�������� ���������...');
        waitForm.Show;
        Application.ProcessMessages;

        with cldsImport do begin
            First;
            while not Eof do begin
                qr.SQL.Text:=
                    'SELECT idusrinv FROM usrinv u, store s '#13#10+
                    ' where u.idStore=s.idStore '#13#10+
                    ' and s.afm='+QuotedStr(FieldByName('afm').AsString)+
                    ' and invdate='+QuotedStr(FormatDateTime('yyyy-mm-dd', FieldByName('date').AsDateTime))+
                    ' and invNum='+QuotedStr(FieldByName('invNum').AsString)+
                    ' and amount='+FloatToStrF(FieldByName('amount').AsFloat, ffFixed, 15, 2);
                qr.Active:=true;

                if qr.RecordCount=0 then begin
                    qr.SQL.Text:=
                        'insert into store(afm,descr) values ('+
                        QuotedStr(FieldByName('afm').AsString)+', '+
                        QuotedStr(FieldByName('brand').AsString)+')';
                    try
                        qr.Execute;

                        inc(countCMP);

                        qr.SQL.Text:='select @@identity as id';
                        qr.Active:=true;

                        idExpenseType:='3'; // unknown type
                        cmpID:=qr.FieldByName('id').AsString;
                    except
                        qr2.SQL.Text:='select idStore, idExpenseType from store where afm='+QuotedStr(FieldByName('afm').AsString);
                        qr2.Active:=true;

                        cmpID:=qr2.FieldByName('idStore').AsString;

                        if VarIsNull(qr2.FieldByName('idExpenseType').AsVariant) then
                            idExpenseType:='3'
                        else
                            idExpenseType:=qr2.FieldByName('idExpenseType').AsString;
                    end;

                    qr.SQL.Text:=
                        'insert into usrinv (idUsr, invNum, invDate, idStore, amount, idExpenseType) values('#13#10+
                        usrid+', '+
                        QuotedStr(FieldByName('invNum').AsString)+', '+
                        QuotedStr(FormatDateTime('yyyy-mm-dd', FieldByName('date').AsDateTime))+', '+
                        cmpID+', '+
                        FloatToStrF(FieldByName('amount').AsFloat, ffFixed, 15, 2)+', '+
                        idExpenseType+')';

                    try
                        qr.Execute;

                        inc(countINV);
                    except
                        {on e:Exception do begin
                            showmessage(qr.SQL.Text+#13#10+e.Message);
                        end;}
                    end;
                end;
                Next;
            end;
        end;

        qr.Connection.Commit;
    finally
        DecimalSeparator:=ds;
    end;

    waitForm.Hide;

    MessageDlg('�������������'+#13+#10+''+#13+#10+IntToStr(countCMP)+' ������������'+#13+#10+IntToStr(countINV)+' ����������', mtInformation, [mbOK], 0);

    cxTabSheet1.Visible:=true;
    cxPageControl1.ActivePageIndex:=0;
    cxTabSheet2.Visible:=false;

    panelUp.Visible:=true;
    panelLeft.Visible:=true;

    if countINV>0 then begin
        importedNewCmp:=true;
        Close;
    end;
end;

procedure TImporterFRM.btnOpenFileClick(Sender: TObject);
var fileExt: String;
begin
    if dlgRestore.Execute then begin
        try
            ss.Pages[ss.ActivePage].ClearAll;

            fileExt:=LowerCase(ExtractFileExt(dlgRestore.FileName));

            if fileExt='.csv'  then begin
                try
                    parseCSV(dlgRestore.FileName)
                except
                    on e:Exception do begin
                        MessageDlg('������ ���� ��� �������� ��� CSV ������� �� ������ ���������:'+#13+#10+''+#13+#10+e.Message, mtError, [mbYes, mbNo], 0);
                    end
                end;
            end else if fileExt='.xls' then
                ss.LoadFromFile(dlgRestore.FileName)
            else
                MessageDlg('��������� ������� �� CSV ��� XLS ������', mtError, [mbOK], 0);
        except
            MessageDlg('�� ������� ������.'+#13+#10+'������� �� ������ ��� ��� EXCEL ��� ���������� �� �� ����� 2003'#13#10+'�� ������ ���� �� ���� �������� xls', mtError, [mbOK], 0);
        end;
        edtStartLine.EditValue:=1;
    end;
end;

procedure TImporterFRM.edtStartLinePropertiesEditValueChanged(Sender: TObject);
begin
    if edtEndLine.EditValue<edtStartLine.EditValue then
        edtEndLine.EditValue:=edtStartLine.EditValue;
end;

procedure TImporterFRM.FormCreate(Sender: TObject);
begin
    cxTabSheet2.TabVisible:=false;

    importedNewCmp:=false;

    MessageDlg('���� ������ XLS (Office 97-2003) ��� CSV ������ ���������� ��� ��� ���.  ���� ������ ���� XLSX ������ �� ����������� �� office 2003 XLS ��� Excel'#13#10#13#10+
               '�� �������� ���������� ��� import �� ����� ��� �������� ---> ['+TMainFRM(Owner).cbboxUsr.Text+']'#13#10#13#10+
               '����� BACKUP �� �������� ��� ���� ��� �������� ���� ��������� ��� �� EXCEL', mtInformation, [mbOK], 0);

    with cldsImport do begin
        CreateDataSet;
        Open;
    end;
end;

procedure TImporterFRM.parseCSV(filename: TFileName);
Var ts, fileContents: TStrings;
    line,token : WideString;

    i,j, row: integer;
    screen: TPreviewCSVFrm;
begin
    screen:=TPreviewCSVFrm.Create(Self, filename);
    try
        screen.ShowModal;
    finally
        screen.Free;
    end;

    Ts := TStringList.Create;
    fileContents := TStringList.Create;

    fileContents.LoadFromFile(filename);

    ts.StrictDelimiter:=true;
    ts.Delimiter:=CSVDelim;
    if CSVDelim=',' then
        ts.QuoteChar:=CSVQuote
    else
        ts.QuoteChar:=#1;

    row:=0;

    waitForm.setMessage('��������� �������');
    waitForm.Show;
    Application.ProcessMessages;

    for i := 0 to fileContents.Count - 1 do begin
        line:=fileContents.Strings[i];
        ts.DelimitedText := line;

        for j := 0 to ts.Count - 1 do begin
            token:=ts[j];

            if not (CSVQuote=#1) then begin
                if ((Copy(token, 1, 1)=CSVQuote) and (copy(token, length(token), 1)=CSVQuote)) then begin
                    token:=copy(token, 2, length(token)-2);
                    StringReplace(token, CSVQuote+CSVQuote, CSVQuote, [rfReplaceAll]);
                end;
            end;

            ss.pages[ss.ActivePage].GetCellObject(j, Row).text := token;
        end;
        inc(row);
    end;
    waitForm.Hide;

    ts.free;
    fileContents.Free;
end;

end.
